<?php

// use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('register', 'Auth\RegisterController@register')->name("register");
Route::post('login',    'Auth\LoginController@login')->name("login");
Route::post('logout',   'Auth\LoginController@logout')->name("logout");
Route::post('ping',     'Auth\LoginController@ping')->name("ping");
Route::post('hello',    'Auth\LoginController@hello')->name("hello");

//mot de passe oublié
Route::post('password/forgot', 'Api\Auth\ForgotPasswordController')->name('password.forgot');
Route::post('password/reset',  'Api\Auth\ForgotPasswordController');

//TODO Proteger
Route::get('siret/search')->uses('SiretController@search');

// a mon avis elle n'est pas utilisée
// Route::middleware('auth:api')
//     ->get('/user', function (Request $request) {
//         return $request->user();
//     });

//Route::group(['middleware' => ['auth:api', 'auth:token']], function () {
Route::group(['middleware' => ['auth:token,api']], function () {
    Route::group(['middleware' => ['active-token']], function () { // On met à jour la date de dernière connexion si c'est une connexion via Token Passport
        Route::get('user',                          'Auth\LoginController@index'); // Route qui affiche les infos du client (juste pour tester les différents tokens)
        Route::put('user',                          'Auth\LoginController@update');
        Route::post('user',                         'Auth\LoginController@store');

        Route::get('LdeFrais',                      'LdeFraisController@index');
        Route::get('LdeFrais/{id}',                 'LdeFraisController@show');
        Route::post('LdeFrais',                     'LdeFraisController@store');
        Route::put('LdeFrais/{id}',                 'LdeFraisController@update');
        Route::delete('LdeFrais/{id}',              'LdeFraisController@delete');

        Route::get('NdeFrais',                      'NdeFraisController@index');
        Route::get('NdeFrais/{id}',                 'NdeFraisController@show');
        Route::get('NdeFraisPDF/{id}',              'NdeFraisController@webBuildPDF');

        Route::get('NdeFraisDetails/{id}',          'NdeFraisController@details');
        Route::get('NdeFraisDetailsPDF/{id}',       'NdeFraisController@webBuildPDFJustificatifs');

        Route::post('upload',                       'DependencyUploadController@upload');
        Route::get('upload',                        'DependencyUploadController@checkChunk');

        Route::get('ldfImages/{lenom}/{image}',     'LdeFraisImagesController')->where(['file_name' => '*.jpeg']);

        Route::get('config/typeFraisPro',           'ConfigurationController@typeFraisPro');
        Route::get('config/typeFraisPerso',         'ConfigurationController@typeFraisPerso');
        Route::get('config/moyenPaiementPro',       'ConfigurationController@moyenPaiementPro');

        Route::get('Entreprise',                    'EntrepriseController@index');
        Route::put('Entreprise',                    'EntrepriseController@update'); //dans le cas où une personne a les droits ad hoc sur une seule entreprise ...

        //Un utilisateur qui a déjà fait alès - albi et a déjà indiqué le kilométrage dans son historique ...
        Route::post('geoDist',                       'LdeFraisController@geoDist');

    });
});

Auth::guard('api')->user();  // instance of the logged user
Auth::guard('api')->check(); // if a user is authenticated
Auth::guard('api')->id();    // the id of the authenticated user
