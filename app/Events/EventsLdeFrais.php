<?php
/*
 * EventsLdeFrais.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use App\NdeFrais;
use App\LdeFrais;

class EventsLdeFrais
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $ldf;
    public $contexte;
    // public $ndf;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    //        event(new LdeFraisPDFAvailable($this));
    //subEvent = PDFAvailable or SignedPDFAvailable
    public function __construct(LdeFrais $ldf, $contexte)
    {
        //
        Log::debug("EventsLdeFrais :: construct pour $contexte");
        $this->ldf  = $ldf;
        $this->contexte = $contexte;
        // $this->ndf  = $ldf->getNdeFrais();

        // Log::debug(json_encode($this));
        Log::debug("EventsLdeFrais :: construct end");
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        Log::debug("EventsLdeFrais :: broadcastOn");
        return new PrivateChannel('EventsLdeFrais.' . $this->contexte);
    }
}
