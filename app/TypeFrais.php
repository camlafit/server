<?php
/*
 * TypeFrais.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Log;
use DB;

class TypeFrais extends Model
{
  protected $guarded = ['id','label','slug','account','pro','perso'];

  //
  public function ldeFrais()
  {
    return $this->hasMany('App\LdeFrais');
  }

  public function getIDfromSlug($slug)
  {
    //   DB::enableQueryLog();
    // Log::debug('===================');
    // Log::debug("Résultat du calcul pour $cv : $km = $total");
    // Log::debug('===================');
    $res = $this->where('slug', $slug)->first();
    //   Log::debug("On cherche slug (getIDfromSlug TypeFrais.php): $slug ...");
    //   Log::debug($res);
    //   Log::debug(DB::getQueryLog());
    return $res->id;
  }

  public function getIDfromLabel($label)
  {
    //   DB::enableQueryLog();
    // Log::debug('===================');
    // Log::debug("Résultat du calcul pour $cv : $km = $total");
    // Log::debug('===================');
    $res = $this->where('label', $label)->first();
    //   Log::debug("On cherche label: $label ...");
    //   Log::debug($res);
    //   Log::debug(DB::getQueryLog());
    return $res->id;
  }

  public function getLabelFromID($id)
  {
    //   DB::enableQueryLog();
    // Log::debug('===================');
    $res = $this->where('id', $id)->first();
    //   Log::debug("On cherche id: $id ...");
    //   Log::debug($res);
    //   Log::debug(DB::getQueryLog());
    return $res->label;
  }

  public function getSlugFromID($id)
  {
    //   DB::enableQueryLog();
    // Log::debug('===================');
    $res = $this->where('id', $id)->first();
    //   Log::debug("On cherche id: $id ...");
    //   Log::debug($res);
    //   Log::debug(DB::getQueryLog());
    return $res->slug;
  }
}
