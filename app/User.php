<?php
/*
 * User.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

use DB;
use Schema;
use Mail;
use Swift_Encoding;
use App\LdeFrais;
use App\NdeFrais;
use DatabaseSeeder;
use App\Jobs\ProcessCreateAccount;
use App\Jobs\ProcessSendEmail;
use Spatie\Permission\Models\Role;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Spatie\Permission\Traits\HasRoles;
use App\Mail\UserMailInvitation;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Notifications\ResetPassword;
use Lab404\Impersonate\Models\Impersonate;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;
use QrCode;

class User extends Authenticatable
{
    use LogsActivity, Impersonate;
    protected static $logName = 'User';
    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = ['password', 'api_token', 'updated_at', 'remember_token'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    use Notifiable;
    use HasRoles;
    use SoftDeletes;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // public $nomComplet = "Gaston Lagafe";

    protected $fillable = [
        'firstname', 'name', 'email', 'password', 'adresse', 'cp', 'ville', 'pays', 'send_copy_to',
        'compta_email', 'compta_ik', 'compta_peage', 'compta_hotel', 'compta_train',
        'compta_carburant0recup', 'compta_carburant60recup', 'compta_carburant80recup', 'compta_carburant100recup',
        'compta_taxi', 'compta_restauration', 'compta_divers',
        'compta_compteperso', 'compta_compteprocb', 'compta_compteproesp',
        'compta_tvadeductible', 'main_role', 'mode_simple', 'corrector_id', 'creator_id',
    ];
    protected $dates = ['created_at', 'deleted_at'];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
        'password',
    ];

    protected $guarded = ['id'];

    public function __construct(array $attributes = [])
    {
        // Log::debug("======================== __construct__ User ===================");
        // Log::debug("Base de données : " . env('APP_NAME'));

        //On passe au parent
        parent::__construct($attributes);

        //Pour les valeurs par defaut on pioche directement dans la base de donnees pour eviter de la duplication et source d'erreurs
        if (env('DB_CONNECTION') === 'sqlite') {
            $t = DB::select('PRAGMA table_info([users])');
            // Log::debug($t);
            foreach ($t as $key => $value) {
                $k = $value->name;
                if (is_string($value->dflt_value) && strpos($value->dflt_value, "'") !== false) {
                    $v = str_replace("'", "", $value->dflt_value);
                } else {
                    $v = $value->dflt_value;
                }
                if ($this->$k == "") {
                    $this->$k = $v;
                }
            }
        } else {
            $t = DB::select('describe users');
            foreach ($t as $key => $value) {
                $k = $value->Field;
                if (is_string($value->Default) && strpos($value->Default, "'") !== false) {
                    $v = str_replace("'", "", $value->Default);
                } else {
                    $v = $value->Default;
                }
                if ($this->$k == "") {
                    $this->$k = $v;
                }
            }
        }

        //Mot de passe par defaut car dans la gestion des utilisateurs on ne demande pas de mot de passe quand on cree un compte
        if ($this->password == "") {
            $this->password = "xxxxxx";
        }
        if ($this->api_token == "") {
            $this->api_token = $this->generateToken();
        }
    }

    public function delete()
    {
        // Le softDeletes laisse ce compte dans la base de données, on suffixe son mail pour eviter le refus de re-créer un compte plus tard
        // avec cette même adresse mail
        $this->email .= "-deleted-" . time();
        $this->save();
        parent::delete();
    }


    /**
     * setMainRole : affecte le role principal de l'utilisateur
     *
     * @param  mixed $roleName
     * @return void
     */
    public function setMainRole($roleName)
    {
        $role = Role::findByName($roleName, 'web');
        $this->main_role = $role->id;
        $this->assignRole($role);
        $this->save();
        return $role->id;
    }


    // public function getAuthIdentifierName()
    // {
    //     //On passe au parent
    //     parent::getAuthIdentifierName();

    //     $this->nomComplet = $this->firstname . " " . $this->name;
    //     return $this->nomComplet;
    // }

    public function entreprises()
    {
        return $this->belongsToMany(Entreprise::class)->withPivot('role_id'); //, 'entreprise_user', 'user_id', 'role_id');
    }

    public function NdeFrais()
    {
        return $this->hasMany('App\NdeFrais');
    }

    public function LdeFrais()
    {
        return $this->hasMany('App\LdeFrais');
    }

    public function CalculDistanceAnnuelle($year)
    {
        // TODO SELECT SUM(distance) FROM `lde_frais` WHERE user_id=2 AND YEAR(ladate)='2018'
        $t = DB::table('lde_frais')
            ->select(DB::raw('SUM(distance) as total'))
            ->whereYear('ladate', '=', $year)
            ->value('total');
        // Log::debug('======== getTotalKMfor ===========');
        // Log::debug($t);
        return $t;
    }

    public function generateToken()
    {
        $this->api_token = Str::random(60);
        if ($this->id) {
            $this->save();
        }
        return $this->api_token;
    }

    //Retourne les initiales du compte eventuellement limitee aux $size premieres lettres
    //pour eviter des intiiales du genre JTCCD
    public function initials($size = 0)
    {
        $ret = '';
        // foreach (explode(' ', preg_replace("/[^a-zA-Z ]+/", "", ($this->name))) as $word)
        //     $ret .= strtoupper($word[0]);
        $ret .= strtoupper(Str::slug($this->firstname)[0]) . strtoupper(Str::slug($this->name)[0]);

        if ($size > 0) {
            return substr($ret, 0, $size);
        }
        return $ret;
    }

    //retourne les codes comptables de l'utilisateur
    /*
        'compta_email', 'compta_ik', 'compta_peage', 'compta_hotel', 'compta_train',
        'compta_carburant0recup', 'compta_carburant60recup', 'compta_carburant80recup', 'compta_carburant100recup',
        'compta_taxi', 'compta_restauration', 'compta_divers',
        'compta_compteperso', 'compta_compteprocb', 'compta_compteproesp',
        'compta_tvadeductible',
        */
    public function getComptaCode($slug)
    {
        //Si slug commence par compta_ on ne touche pas sinon on prefixe de compta
        if (Str::startsWith($slug, 'compta_')) {
            $slug_complet = $slug;
        } else {
            $slug_complet = "compta_$slug";
        }
        $val = $this->$slug_complet;
        // Log::debug("getComptaCode pour $slug : $slug_complet soit $val");
        return $val;
    }

    //Retourne un tableau avec toutes les valeurs des comptes comptables (pratique pour créer un nouveau compte)
    public function getComptaConfig()
    {
        $r = array();
        foreach ($this->fillable as $compte) {
            if (Str::startsWith($compte, "compta_")) {
                // Log::debug("On cherche pour $compte");
                $r[$compte] = $this->$compte;
            }
        }
        return $r;
    }

    public function ajouteDataDemo()
    {
        ProcessCreateAccount::dispatch($this);
    }

    public function envoyerMailInvitation()
    {
        //Allez il faut envoyer un mail de bienvenue ...
        //Si on a affaire à un revendeur

        //Si on a affaire à un utilisateur
        // if ($this->hasRole("utilisateur") || $this->hasRole("adminRevendeur") || $this->hasRole("adminEntreprise") || $this->hasRole("responsableEntreprise") ) {

        $details = array(
            'to' => $this->email,
            'bcc' => env('MAIL_NOTIFICATIONS', 'suivi@doliscan.fr'),
            'subject' => "doliscan: Nouvelle entrée",
            'objectMail' => new UserMailInvitation($this),
        );
        ProcessSendEmail::dispatch($details);

        activity()
            ->by($this)
            ->causedBy(\sharp_user())
            ->performedOn($this)
            ->withProperty('email', $this->email)
            ->log('envoyerMailInvitation to ' . $this->email);
        return "Mail envoyé";
        // } else {
        // return "Aucun mail type n'est encore prêt pour ce type de profil ...";
        // }
    }

    //Recupere la listes des utilisateurs auxquels j'ai accès
    static function getMyUsers($orderBy = 'name', $orderDir = 'asc', $as_get = true)
    {
        Log::debug("getMyUsers : ");
        $users = null;
        $u = User::findOrFail(sharp_user()->id);
        if (sharp_user()->hasRole('superAdmin')) {
            $users = User::orderBy($orderBy, $orderDir);
        }

        if (sharp_user()->hasRole('adminRevendeur')) {
            //+ tous les utilisateurs de "mes" entreprises
            //1 les entreprises
            $entreprises = $u->getEntreprises()->pluck('id');
            // Log::debug("retour des entreprises : " . json_encode($entreprises));

            //On recupere tous les utilisateurs créés par cet utilisateur
            //+ myself mais dont le profil est < à moi (pour pas avoir d'escalation de privileges)


            $collection = collect();
            $usersID = $collection
                ->concat(DB::table('users')
                    ->whereRaw(DB::raw("((creator_id='" . $u->id . "' OR id='" . $u->id . "') AND main_role <= '" . $u->main_role . "')"))
                    ->pluck('id'))
                ->concat(DB::table('entreprise_user')
                    ->select('user_id AS id')
                    ->where('role_id', '<=', $u->main_role)
                    ->whereIN('entreprise_id', $entreprises)
                    ->pluck('id'))
                ->unique()
                ->values();

            Log::debug("getMyUsers : retour des userid 2 : " . json_encode($usersID->all()));
            $users = User::whereIN('id', $usersID->all())
                ->orderBy($orderBy, $orderDir);
        }
        //Tous les utilisateurs qui sont sous la responsabilité de ce responsable d'entreprise ...
        // autrement dit tous les comptes utilisateurs rattachés à l'entreprise dont il est responsable :)
        else if (sharp_user()->hasRole('responsableEntreprise') || sharp_user()->hasRole('adminEntreprise') || sharp_user()->hasRole('serviceComptabilite')) {
            //+ tous les utilisateurs de "mes" entreprises
            //1 les entreprises
            $entreprises = $u->getEntreprises()->pluck('id');
            // Log::debug("retour des entreprises : " . json_encode($entreprises));

            //On recupere tous les utilisateurs créés par cet utilisateur
            //+ myself
            $collection = collect();
            $usersID = $collection
                ->concat(DB::table('users')
                    ->whereRaw(DB::raw("((creator_id='" . $u->id . "' OR id='" . $u->id . "') AND main_role <= '" . $u->main_role . "')"))
                    ->pluck('id'))
                ->concat(DB::table('entreprise_user')
                    ->select('user_id AS id')
                    ->where('role_id', '<=', $u->main_role)
                    ->whereIN('entreprise_id', $entreprises)
                    ->pluck('id'))
                ->unique()
                ->values();

            Log::debug("getMyUsers : retour des userid 2bis : " . json_encode($usersID->all()));
            $users = User::whereIN('id', $usersID->all())
                ->orderBy($orderBy, $orderDir);
        }
        //Moi ...
        else if (sharp_user()->hasRole('utilisateur')) {
            $users = $u;
        }

        // if ($users == null) {
        //     $users = new user();
        // }
        if ($as_get) {
            $r = $users->get();
        } else {
            $r = $users;
        }
        // Log::debug("  getMyUsers : " . json_encode($r));
        return $r;
    }

    //Tellement genial, tellement simple ... pour avoir le nom complet directement on utilisera
    //$user->full_name ... la magie de Laravel, cf
    //https://medium.com/@petehouston/laravel-fact-make-computed-attributes-for-eloquent-models-fc78fe5f1aa4
    public function getFullNameAttribute()
    {
        return $this->firstname . ' ' . $this->name;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        activity()
            ->by($this)
            ->causedBy(\sharp_user())
            ->performedOn($this)
            ->withProperty('email', $this->email)
            ->log('sendPasswordResetNotification to ' . $this->email);

        $this->notify(new ResetPassword($token));
    }

    //L'adresse mail de l'utilisateur
    public function getMail()
    {
        return $this->email;
    }

    //L'adresse mail du comptable à qui il faut envoyer les NDF
    public function getMailCompta()
    {
        return $this->compta_email;
    }

    //L'adresse mail de la personne à mettre en copie
    public function getMailCopyTo()
    {
        return $this->send_copy_to;
    }

    //L'entreprise a laquelle cet utilisateur est liée ... posera probablement un soucis pour les personnes
    //qui ont plusieurs rôles sur plusieurs entreprises (ex. revendeurs)
    //TODO: interdire aux comptes revendeurs et + (relecteurs ?) de faire des notes de frais avec leur compte rvd
    public function getEntreprise()
    {
        $uidtmp = $this->id;
        $societe = Entreprise::whereHas('users', function ($q) use ($uidtmp) {
            $q->where('user_id', $uidtmp);
        })->first();
        return $societe;
    }

    /**
     * Add a user's role on a Entreprise object
     *
     * @param Role|string|int $role
     * @param Entreprise|string|int $entreprise
     *
     * @return true on success
     */
    public function addRoleOnEntreprise($role, $entreprise)
    {
        $rid = 0;
        $eid = 0;
        if (is_int($role)) {
            $rid = $role;
        } elseif (is_string($role)) {
            $r = Role::findByName($role, 'web');
            $rid = $r->id;
        } elseif (is_object($role)) {
            $rid = $role->id;
        }

        if (is_int($entreprise)) {
            $eid = $entreprise;
        } elseif (is_string($entreprise)) {
            $e = Entreprise::where('name', $entreprise)->first();
            $eid = $e->id;
        } elseif (is_object($entreprise)) {
            $eid = $entreprise->id;
        }

        //On evite les doublons (pourquoi n'y a t il pas d'index ?)
        //-> pour eviter les messages d'erreurs de type "can't insert"
        //de l'époque où on ne savait pas que updateOrInsert existait !
        if ($rid > 0 && $eid > 0) {
            DB::table("entreprise_user")->updateOrInsert([
                'entreprise_id' => $eid,
                'role_id' => $rid,
                'user_id' => $this->id,
            ]);
            return true;
        }
        return false;
    }


    //La liste des entreprises auxquelles j'ai accès
    public function getEntreprises($orderBy = 'name', $orderDir = 'asc', $as_get = true)
    {
        Log::debug("User::getEntreprises");
        $entreprises = null;

        //Note: On ne liste que les utilisateurs pour lesquels on a des droits ...
        //Si on est superAdmin -> tout
        if (sharp_user()->hasRole('superAdmin')) {
            Log::debug("  getEntreprises : superAdmin");
            $entreprises = Entreprise::with('users')->where('id', '>', 0);
        }
        //Toutes les entreprises pour lesquelles cet utilisateur a les droits adminRevendeur
        if (sharp_user()->hasRole('adminRevendeur')) {
            Log::debug("  getEntreprises : adminRevendeur");
            $roleId = Role::findByName('adminRevendeur', 'web')->id;
            $entreprises = User::find(sharp_user()->id)->entreprises(); //->where('role_id', $roleId);
        }

        //Un DSI ... sa boite
        if (sharp_user()->hasRole('adminEntreprise')) {
            Log::debug("  getEntreprises : adminEntreprise");
            $roleId = Role::findByName('adminEntreprise', 'web')->id;
            $entreprises = User::find(sharp_user()->id)->entreprises(); //->where('role_id', $roleId);
        }

        //Le service comptabilité ... son entreprise
        if (sharp_user()->hasRole('serviceComptabilite')) {
            Log::debug("  getEntreprises : serviceComptabilite");
            $roleId = Role::findByName('serviceComptabilite', 'web')->id;
            $entreprises = User::find(sharp_user()->id)->entreprises(); //->where('role_id', $roleId);
        }

        //Un chef d'entreprise ... sa boite :)
        if (sharp_user()->hasRole('responsableEntreprise')) {
            Log::debug("  getEntreprises : responsableEntreprise");
            $roleId = Role::findByName('responsableEntreprise', 'web')->id;
            $entreprises = User::find(sharp_user()->id)->entreprises(); //->where('role_id', $roleId);
        }

        //Un utilisateur lambda ... sa boite s'il en a une !
        if (sharp_user()->hasRole('utilisateur')) {
            Log::debug("  getEntreprises : utilisateur");
            $roleId = Role::findByName('utilisateur', 'web')->id;
            $entreprises = User::find(sharp_user()->id)->entreprises(); //->where('role_id', $roleId);
            Log::debug(json_encode($entreprises->get()));
        }

        if ($entreprises) {
            $entreprises->orderBy($orderBy, $orderDir);
        }

        if ($as_get) {
            return $entreprises->get();
        } else {
            return $entreprises;
        }
    }

    public function canImpersonate()
    {
        //Qui a le droit de changer d'utilisateur -> superadmin
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        } else {
            return false;
        }
        // return $this->is_admin == 1;
    }

    public function canBeImpersonated()
    {
        if (sharp_user()->hasRole('superAdmin')) {
            return false;
        } else {
            return true;
        }
        // return $this->can_be_impersonated == 1;
    }

    /**
     * seedDemoData : injecte des données de démonstration pour ce compte utilisateur (utile pour les serveurs de démo)
     *
     * @return void
     */
    public function seedDemoData()
    {
        Log::debug("User::seedDemoData");
        //Quand on est en mode déploiement initial on peut être dans la situation où on injecte un jeu de données de base...
        if (App::isDownForMaintenance()) {
            Log::debug(" mode maintenance on ne fait rien");
            return;
        }
        $faker = \Faker\Factory::create('fr_FR');
        $faker->seed(142);
        $faker->addProvider(new \App\Faker\DoliSCAN($faker));
        $faker->addProvider(new \Faker\Provider\fr_FR\Person($faker));

        $s = new DatabaseSeeder();
        $s->seedDataForUser($this->email, $faker, [], 3);
    }

    /**
     * canHaveNDF : retourne true si cet utilisateur peut avoir des notes de frais, pour résumer, les comptes suivants ne peuvent
     *              pas avoir de notes de frais: superAdmin, adminRevendeur, adminEntreprise, inactif
     *
     * @return void
     */
    public function canHaveNDF()
    {
        $excludeArray = ['superAdmin', 'adminRevendeur', 'adminEntreprise', 'inactif'];
        $role = Role::findById($this->main_role, 'web');
        if (\in_array($role->name, $excludeArray)) {
            return false;
        }
        return true;
    }

    /**
     * qrCodeForApp : retourne un qrCode pour autoconfiguration de l'application
     *                contenu du qrCode: serverURI;login;
     *                l'utilisateur n'aura plus qu'a entrer son mot de passe
     *
     * @return void
     */
    public function qrCodeForApp($format = 'png', $size = 140)
    {
        $userInfo = env('APP_DOMAIN', "doliscan.fr") . ";" . $this->email;
        return QrCode::format($format)->size($size)->generate($userInfo);
    }

    public function qrCodeImageURLForApp()
    {
        return "/user/qrCodeForApp/" . $this->email;
    }
}
