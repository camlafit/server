<?php
/*
 * LdeFraisSharpList.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use App\User;
use App\NdeFrais;
use App\LdeFrais;
use Spatie\Permission\Models\Role;
use Code16\Sharp\EntityList\SharpEntityList;
use Code16\Sharp\EntityList\EntityListQueryParams;
use Code16\Sharp\EntityList\Containers\EntityListDataContainer;
use Illuminate\Support\Facades\Log;
use Code16\Sharp\Utils\LinkToEntity;
use App\Sharp\Commands\NdeFraisExportPDFCommand;

class LdeFraisSharpList extends SharpEntityList
{
    /**
     * Build list containers using ->addDataContainer()
     *
     * @return void
     */
    public function buildListDataContainers()
    {
        $this->addDataContainer(
            EntityListDataContainer::make('ladate')
                ->setLabel('La date')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('label')
                ->setLabel('Intitulé')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('ttc')
                ->setLabel('TTC')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('type_frais_id')
                ->setLabel('Type de frais')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('moyen_paiement_id')
                ->setLabel('Moyen de paiement')
                ->setSortable()

        );
    }

    /**
     * Build list layout using ->addColumn()
     *
     * @return void
     */

    public function buildListLayout()
    {
        $this->addColumn('ladate', 2)
            ->addColumn('label', 4)
            ->addColumn('ttc', 1)
            ->addColumn('type_frais_id', 2)
            ->addColumn('moyen_paiement_id', 2);
    }

    /**
     * Build list config
     *
     * @return void
     */
    public function buildListConfig()
    {
        $this->setPaginated()
            ->setSearchable()
            ->setInstanceIdAttribute('id')
            ->setDefaultSort('ladate');
        // ->addInstanceCommand("imprimer_ndf", NdeFraisExportPDFCommand::class);
    }

    /**
     * Retrieve all rows data as array.
     *
     * @param EntityListQueryParams $params
     * @return array
     */
    public function getListData(EntityListQueryParams $params)
    {
        // Log::debug("========================");
        // Log::debug($params->filterFor('ldefrais'));
        // Log::debug("========================");
        $userid = \sharp_user()->id;
        if (sharp_user()->hasPermissionTo('show others NdeFrais')) {
            $userid = session('usertoget');
        }
        $ldfs = LdeFrais::where('user_id', $userid);
        if ($params->filterFor('nde_frais_id')) {
            $ldfs->where('nde_frais_id', $params->filterFor('nde_frais_id'));
        }
        $ldfs->orderBy($params->sortedBy(), $params->sortedDir());

        collect($params->searchWords())
            ->each(function ($word) use ($ldfs) {
                $ldfs->where(function ($query) use ($word) {
                    $query->orWhere('label', 'like', $word)
                        ->orWhere('ttc', 'like', $word);
                });
            });


        // Log::debug($ndfs->toSql());
        return $this->setCustomTransformer(
            "ttc",
            function ($ttc, $ldeFrais) {
                if ($ttc == 0)
                    return "-";
                else
                    return nbFR($ttc) . "&nbsp;€";
            }
        )->setCustomTransformer(
            "label",
            function ($label, $ldeFrais) {
                return $ldeFrais->getResume();
            }
        )->setCustomTransformer(
            "type_frais_id",
            function ($label, $ldeFrais) {
                return $ldeFrais->getTypeFrais();
            }
        )->setCustomTransformer(
            "moyen_paiement_id",
            function ($label, $ldeFrais) {
                return $ldeFrais->getMoyenPaiement();
            }
        )->transform($ldfs->paginate(50));
        // ->setCustomTransformer("ndeFrais", function($ndfs, $ldfs) {
        //     return $ldfs->NdeFrais->map(function($edit) {
        //         return (new LinkToEntity($ldfs->name, "LdeFrais"))
        //             ->setTooltip("Détails de la note de frais")
        //             ->setSearch($ldfs->name)
        //             ->render();
        //     })->implode("<br>");
        // })
    }
}
