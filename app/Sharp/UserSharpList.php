<?php
/*
 * UserSharpList.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use App\User;
use App\Entreprise;
use Spatie\Permission\Models\Role;
use Code16\Sharp\EntityList\SharpEntityList;
use Code16\Sharp\EntityList\EntityListQueryParams;
use Code16\Sharp\EntityList\Containers\EntityListDataContainer;
use App\Sharp\Commands\UserEnvoyerMailInvitationCommand;
use App\Sharp\Commands\UserEnvoyerMailCurrentNdfCommand;
use App\Sharp\Commands\UserIncarnerCommand;
use App\Sharp\Commands\UserShowNdfCommand;
use Illuminate\Support\Facades\Log;

class UserSharpList extends SharpEntityList
{
    /**
     * Build list containers using ->addDataContainer()
     *
     * @return void
     */
    public function buildListDataContainers()
    {
        $this->addDataContainer(
            EntityListDataContainer::make('firstname')
                ->setLabel('Prénom')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('name')
                ->setLabel('Nom')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('email')
                ->setLabel('Mail')
                ->setSortable()
        );
        if (sharp_user()->hasRole('superAdmin') || sharp_user()->hasRole('adminRevendeur')) {
            $this->addDataContainer(
                EntityListDataContainer::make('entreprises')
                    ->setLabel('Entreprises')
                    ->setSortable()
            );
        }
        $this->addDataContainer(
            EntityListDataContainer::make('main_role')
                ->setLabel('Profil')
                ->setSortable()
        );
    }

    /**
     * Build list layout using ->addColumn()
     *
     * @return void
     */

    public function buildListLayout()
    {
        $this->addColumn('firstname', 2)
            ->addColumn('name', 2)
            ->addColumn('email', 3)
            ->addColumn('entreprises', 2)
            ->addColumn('main_role', 3);
    }

    /**
     * Build list config
     *
     * @return void
     */
    public function buildListConfig()
    {
        $this->setPaginated()
            ->setSearchable()
            ->setInstanceIdAttribute('id')
            ->setDefaultSort('firstname', 'asc')
            ->addInstanceCommand("envoyer_mail_invitation", UserEnvoyerMailInvitationCommand::class)
            ->addInstanceCommand("envoyer_mail_ndf_du_mois", UserEnvoyerMailCurrentNdfCommand::class)
            ->addInstanceCommand("incarner", UserIncarnerCommand::class)
            ->addInstanceCommand("show_ndf", UserShowNdfCommand::class);
    }

    /**
     * Retrieve all rows data as array.
     *
     * @param EntityListQueryParams $params
     * @return array
     */
    public function getListData(EntityListQueryParams $params)
    {
        Log::debug("******************* sort by " . $params->sortedDir());
        $users = sharp_user()->getMyUsers($params->sortedBy(), $params->sortedDir(), false);

        collect($params->searchWords())
            ->each(function ($word) use ($users) {
                $users->where(function ($query) use ($word) {
                    $query->orWhere('name', 'like', $word)
                        ->orWhere('firstname', 'like', $word)
                        ->orWhere('email', 'like', $word);
                });
            });

        // Log::debug(" =================================== ");
        // Log::debug($users->toSql());
        // Log::debug(" =================================== ");

        return $this
            ->setCustomTransformer("entreprises", function ($entreprises, $user) {
                // Log::debug(" =================================== ");
                // Log::debug(" ici ");
                // Log::debug($users->toSql());
                // Log::debug(" =================================== ");
                return $user->entreprises()->count();
            })
            ->setCustomTransformer("main_role", function ($role, $user) {
                // Log::debug(" =================================== ");
                // Log::debug(" ici ");
                // Log::debug($user->toSql());
                // Log::debug(" =================================== ");
                return Role::where("id", $role)->get()->pluck('name')->first();
            })
            ->transform(
                $users->paginate(30)
            );
    }
}
