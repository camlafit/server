<?php
/*
 * LdeFraisVoituresSharpFormatter.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp\Formatters;

use Code16\Sharp\Form\Fields\Formatters\SharpFieldFormatter;
use Code16\Sharp\Form\Fields\SharpFormField;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;

class LdeFraisVoituresSharpFormatter extends SharpFieldFormatter
{

    /**
     * @param SharpFormField $field
     * @param $value
     * @return mixed
     */
    function toFront(SharpFormField $field, $value)
    {
        Log::debug("LdeFraisVoituresSharpFormatter::toFront FIELD: " . serialize($field) . " || VALUE: " . serialize($value));
        //On ajoute du texte
        $txt = "";
        if ($value != "") {
            $tab = explode(';', $value);
            $txt = $tab[0] . " " . $tab[1] . " (" . $tab[2] . ")";
        }
        Log::debug("  -> return : $txt");
        return $txt;
    }

    /**
     * @param SharpFormField $field
     * @param string $attribute
     * @param $value
     * @return mixed
     */
    function fromFront(SharpFormField $field, string $attribute, $value)
    {

    }
}
