<?php
/*
 * ActivitySharpList.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use App\User;
use App\Entreprise;
use Spatie\Permission\Models\Role;
use Code16\Sharp\EntityList\SharpEntityList;
use Code16\Sharp\EntityList\EntityListQueryParams;
use Code16\Sharp\EntityList\Containers\EntityListDataContainer;
use App\Sharp\Commands\UserEnvoyerMailInvitationCommand;
use Illuminate\Support\Facades\Log;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Support\Str;

class ActivitySharpList extends SharpEntityList
{
    /**
     * Build list containers using ->addDataContainer()
     *
     * @return void
     */
    public function buildListDataContainers()
    {
        $this->addDataContainer(
            EntityListDataContainer::make('id')
                ->setLabel('ID')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('created_at')
                ->setLabel('Date')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('log_name')
                ->setLabel('Log')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('description')
                ->setLabel('Description')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('extrait')
                ->setLabel('Extrait')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('causer_id')
                ->setLabel('uid')
                ->setSortable()
        );
    }

    /**
     * Build list layout using ->addColumn()
     *
     * @return void
     */

    public function buildListLayout()
    {
        $this->addColumn('created_at', 2)
            ->addColumn('causer_id', 1)
            ->addColumn('log_name', 1)
            ->addColumn('description', 4)
            ->addColumn('extrait', 4);
    }

    /**
     * Build list config
     *
     * @return void
     */
    public function buildListConfig()
    {
        $this->setPaginated()
            ->setSearchable()
            ->setInstanceIdAttribute('id')
            ->setDefaultSort('created_at','desc');
        //            ->addInstanceCommand("envoyer_mail_invitation", UserEnvoyerMailInvitationCommand::class);
    }

    /**
     * Retrieve all rows data as array.
     *
     * @param EntityListQueryParams $params
     * @return array
     */
    public function getListData(EntityListQueryParams $params)
    {
        // Log::debug("*******************");

        //Tous les logs
        if (sharp_user()->hasRole('superAdmin')) {
            //$logs = Activity::where('log_name' , 'default');
            $logs = Activity::orderBy($params->sortedBy(), $params->sortedDir());
        } else {
            $logs = Activity::where('causer_id', sharp_user()->id)->orderBy($params->sortedBy(), $params->sortedDir());
        }

        collect($params->searchWords())
            ->each(function ($word) use ($logs) {
                $logs->where(function ($query) use ($word) {
                    $query->orWhere('description', 'like', $word)
                        ->orWhere('properties', 'like', $word);
                });
            });

        return $this->setCustomTransformer("extrait", function ($extrait, $logs) {
            return Str::limit($logs->properties->flatten()->implode(','), 50);
        })->transform(
            $logs->paginate(30)
        );
    }
}
