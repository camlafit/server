<?php
/*
 * DefaultAccountsSettingsSharpForm.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use App\User;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\SharpSingleForm;

class DefaultAccountsSettingsSharpForm extends SharpSingleForm
{
    use WithSharpFormEloquentUpdater;

    /**
     * Build form fields using ->addField()
     *
     * @return void
     */
    function buildFormFields()
    {
        $this->addField(
            SharpFormTextField::make('compta_ik')
                ->setLabel('Indemnités kilométriques')
                ->setPlaceholder("625100")
        )->addField(
            SharpFormTextField::make('compta_peage')
                ->setLabel('Péages & Parking')
        )->addField(
            SharpFormTextField::make('compta_train')
                ->setLabel('Transports (Train/Avion...)')

        )->addField(
            SharpFormTextField::make('compta_hotel')
                ->setLabel('Hébergement (hôtel)')
        )->addField(
            SharpFormTextField::make('compta_taxi')
                ->setLabel('Taxi')
        )->addField(
            SharpFormTextField::make('compta_restauration')
                ->setLabel('Restauration')
        )->addField(
            SharpFormTextField::make('compta_divers')
                ->setLabel('Frais divers')

        )->addField(
            SharpFormTextField::make('compta_compteperso')
                ->setLabel('Compte personnel')
        )->addField(
            SharpFormTextField::make('compta_compteprocb')
                ->setLabel('Compte pro CB')
        )->addField(
            SharpFormTextField::make('compta_compteproesp')
                ->setLabel('Compte pro Especes')

        )->addField(
            SharpFormTextField::make('compta_carburant0recup')
                ->setLabel('Carburant zéro récup. tva')
        )->addField(
            SharpFormTextField::make('compta_carburant60recup')
                ->setLabel('Carburant 60% récup. tva')
        )->addField(
            SharpFormTextField::make('compta_carburant80recup')
                ->setLabel('Carburant 80% récup. tva')
        )->addField(
            SharpFormTextField::make('compta_carburant100recup')
                ->setLabel('Carburant 100% récup. tva')

        )->addField(
            SharpFormTextField::make('compta_tvadeductible')
                ->setLabel('Compte TVA déductible')
        )->addField(
            SharpFormTextField::make('send_copy_to')
                ->setLabel('Envoyer les mails en copie à')
        )->addField(
            SharpFormTextField::make('compta_email')
                ->setLabel('Adresse mail de votre service comptable')

        );
    }

    /**
     * Build form layout using ->addTab() or ->addColumn()
     *
     * @return void
     */
    function buildFormLayout()
    {
        $this->addColumn(12, function (FormLayoutColumn $column) {
            $column->withFields('send_copy_to|6', 'compta_email|6')
                ->withFields('compta_ik|3', 'compta_peage|3', 'compta_train|3', 'compta_hotel|3')
                ->withFields('compta_taxi|3', 'compta_restauration|3', 'compta_divers|3', 'compta_tvadeductible|3')
                ->withFields('compta_carburant0recup|3', 'compta_carburant60recup|3', 'compta_carburant80recup|3', 'compta_carburant100recup|3')
                ->withFields('compta_compteperso|3', 'compta_compteprocb|3', 'compta_compteproesp|3');
        });
    }

    /**
     * @return array
     */
    protected function findSingle()
    {
        return $this->transform(User::findOrFail(auth()->id()));
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function updateSingle(array $data)
    {
        $this->save(User::findOrFail(auth()->id()), $data);
    }
}
