<?php
/*
 * PluginSharpForm.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use App\User;
use App\Plugin;
use App\TypeFrais;
use Spatie\Permission\Models\Role;
use Code16\Sharp\Form\SharpForm;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Layout\FormLayoutFieldset;
use Code16\Sharp\Form\Fields\SharpFormNumberField;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Fields\SharpFormListField;
use Code16\Sharp\Form\Fields\SharpFormHtmlField;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;
use Illuminate\Support\Facades\Log;
use Code16\Sharp\Form\Fields\SharpFormTagsField;
use Code16\Sharp\Form\Fields\SharpFormSelectField;
use Code16\Sharp\Show\Layout\ShowLayoutColumn;
use Illuminate\Http\Request;

class PluginSharpForm extends SharpForm
{
    use WithSharpFormEloquentUpdater;
    private $_pluginPath;

    function __construct()
    {
        Log::debug("PluginSharpForm :: __construct ");
        Log::debug(json_encode(request()->segments()));
        $id = request()->segment(count(request()->segments()));
        $p = Plugin::find($id);
        Log::debug($p->name);

        // //On devrait donc aller chercher le find de
        $classpath = '\\Modules\\' . $p->name . '\\Sharp\\PluginSharpForm';
        Log::debug("on essaye d'utiliser $classpath ...");
        $this->_pluginPath = new $classpath($this);
    }

    function find($id): array
    {
        Log::debug("PluginSharpForm :: find " . $id);
        // $p = Plugin::find($id);
        $p = $this->_pluginPath->find($id);
        // Log::debug($p);

        //On devrait donc aller chercher le find de
        // $classpath = '\\Modules\\' . $p->name . '\\Sharp\\PluginSharpForm';
        // Log::debug("on essaye d'utiliser $classpath ...");
        // $this->_pluginPath = new $classpath();

        return $this->transform(
            $p
        );
    }

    function update($id, array $data)
    {
        // Log::debug("PluginSharpForm :: update " . $id);
        // $instance = $id ? LdeFrais::findOrFail($id) : new LdeFrais;

        // return tap($instance, function ($ldefrais) use ($data) {
        //     //On ignore les champs qui n'existent pas dans la table
        //     $this->ignore(["type_frais_slug", "pictureuri"])
        //         ->save($ldefrais, $data);
        // });
    }

    function delete($id)
    {
        // Log::debug("PluginSharpForm :: delete " . $id);
        // LdeFrais::findOrFail($id)->delete();
    }


    function buildFormFields()
    {
        Log::debug("buildFormFields::");
        $this->_pluginPath->buildFormFields();
    }

    function buildFormLayout()
    {
        Log::debug("buildFormLayout::");
        $this->_pluginPath->buildFormLayout();
    }

}
