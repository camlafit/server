<?php
/*
 * dashRevendeur.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use DB;
use Code16\Sharp\Dashboard\SharpDashboard;
use Code16\Sharp\Dashboard\DashboardQueryParams;
use Code16\Sharp\Dashboard\Widgets\SharpLineGraphWidget;
use Code16\Sharp\Dashboard\Widgets\SharpBarGraphWidget;
use Code16\Sharp\Dashboard\Widgets\SharpPanelWidget;
use Code16\Sharp\Dashboard\Widgets\SharpOrderedListWidget;
use Code16\Sharp\Dashboard\Widgets\SharpGraphWidgetDataSet;
use Code16\Sharp\Dashboard\Widgets\SharpPieGraphWidget;
use Code16\Sharp\Dashboard\Layout\DashboardLayoutRow;
use Illuminate\Support\Facades\Log;

use App\User;
use App\Entreprise;

class dashRevendeur extends SharpDashboard
{
    /**
     * Build dashboard's widget using ->addWidget.
     */
    protected function buildWidgets()
    {
        Log::debug("************buildWidgets*******");
        $this->addWidget(
            SharpPanelWidget::make("welcome")
                ->setInlineTemplate("<h1>Bienvenue !</h1> <div style='text-align: left'><p>Votre profil est 'revendeur' ... et vous pouvez utiliser cette interface pour gérer vos clients & utilisateurs.</p><p>Commencez par créer une entreprise, puis un utilisateur et pensez enfin à associer l'utilisateur à l'entreprise en lui affectant un rôle...</p><p>N'oubliez pas que toute la documentation est accessible depuis le menu Aide</p></div>")
        )->addWidget(
            SharpOrderedListWidget::make("statistiquesChiffres")
                ->setTitle("Vos clients")
        )->addWidget(
            SharpPanelWidget::make("facturation")
                ->setInlineTemplate("<h2>Facturation</h2><br /><p style=\"text-align: left\">Votre facturation est trimestrielle et fonction du nombre de comptes utilisateurs actifs à la date de la facturation.<br /><a href=\"" . route('webLinkToInvoices') . "\">Vos factures</a></p>")
        );
    }

    /**
     * Build dashboard's widgets layout.
     */
    protected function buildWidgetsLayout()
    {
        Log::debug("************buildWidgetsLayout*******");
        $this
            ->addRow(function (DashboardLayoutRow $row) {
                $row->addWidget(12, "welcome");
            })
            ->addRow(function (DashboardLayoutRow $row) {
                $row->addWidget(3, "statistiquesChiffres");
                $row->addWidget(6, "facturation");
            });
    }

    /**
     * Build dashboard's widgets data, using ->addGraphDataSet and ->setPanelData
     *
     * @param DashboardQueryParams $params
     */
    protected function buildWidgetsData(DashboardQueryParams $params)
    {
        Log::debug("**************buildWidgetsData*****");
        $this->setPanelData(
            "welcome",
            ["count" => 10]
        );

        $this->setPanelData(
            "facturation",
            ["count" => 10]
        );

        $u = new User();
        $nbUsers = count($u->getMyUsers());
        $e = new Entreprise();
        $nbEntrep = count($e->getMyEntreprises());
        $this->setOrderedListData(
            "statistiquesChiffres",
            [
                [
                    "label" => "Nombre d'entreprises",
                    "count" => $nbEntrep,
                ],
                [
                    "label" => "Nombre d'utilisateurs",
                    "count" => $nbUsers,
                ],
            ]
        );
    }
}
