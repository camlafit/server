<?php
/*
 * UserEnvoyerMailCurrentNdfCommand.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp\Commands;

use App\User;
use Code16\Sharp\EntityList\Commands\InstanceCommand;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Fields\SharpFormTextareaField;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Illuminate\Support\Facades\Log;
use App\NdeFrais;

class UserEnvoyerMailCurrentNdfCommand extends InstanceCommand
{

    /**
     * @return string
     */
    public function label(): string
    {
        return "Envoyer la note de frais par mail.";
    }

    public function description(): string
    {
        return "Envoie la dernière note de frais cloturée par mail.";
    }


    /**
     * @param string $instanceId
     * @param array $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function execute($instanceId, array $data = []): array
    {
        //TODO
        // $message = User::findOrFail($instanceId)->envoyerMailCurrentNDF();
        // return $this->info($message);
        $u = User::findOrFail($instanceId);
        Log::debug('======= UserEnvoyerMailCurrentNdfCommand / execute pour $instanceId ============');
        // echo $user;
        $ndf = $u->NdeFrais->sortBy('fin')->where('status', NdeFrais::STATUS_CLOSED)->last();
        if ($ndf) {
            $data['raison'] = "closed";
            $n = new NdeFraisSendMail();
            Log::debug("Envoi de la Note de Frais #" . $ndf->id . " de " . $u->email);
            return $n->execute($ndf->id,$data);
        }
        return $this->info("Note de frais introuvable");
    }

    /**
     * @param $instanceId
     * @return bool
     */
    public function authorizeFor($instanceId): bool
    {
        return true;
    }

    public function buildFormFields()
    {
        $this->addField(
            SharpFormTextField::make("email")
                ->setLabel("Adresse mail")
                ->setInputTypeText()
        );
    }

    protected function initialData($instanceId): array
    {
        Log::debug("initialData : ");

        return [
            "email" => \sharp_user()->email,
        ];
    }

    public function buildFormLayout(FormLayoutColumn &$column)
    {
    }
}
