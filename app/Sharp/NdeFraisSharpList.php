<?php
/*
 * NdeFraisSharpList.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use App\User;
use App\NdeFrais;
use App\LdeFrais;
use Spatie\Permission\Models\Role;
use Code16\Sharp\EntityList\SharpEntityList;
use Code16\Sharp\EntityList\EntityListQueryParams;
use Code16\Sharp\EntityList\Containers\EntityListDataContainer;
use Illuminate\Support\Facades\Log;
use Code16\Sharp\Utils\LinkToEntity;
use App\Sharp\Commands\NdeFraisExportPDFCommand;
use App\Sharp\Commands\NdeFraisExportJustificatifsPDFCommand;
use App\Sharp\Commands\NdeFraisAPSignPDFCommand;
use App\Sharp\Commands\NdeFraisSendMail;
use Code16\Sharp\Exceptions\Form\SharpApplicativeException;
use Illuminate\Support\Facades\Request;
use App\Plugin;

class NdeFraisSharpList extends SharpEntityList
{
    /**
     * Build list containers using ->addDataContainer()
     *
     * @return void
     */
    public function buildListDataContainers()
    {
        $this->addDataContainer(
            EntityListDataContainer::make('user')
                ->setLabel('Utilisateur')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('debut')
                ->setLabel('Période')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('label')
                ->setLabel('Intitulé')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('status')
                ->setLabel('État')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('montant')
                ->setLabel('montant')
                ->setSortable()
        );
    }

    /**
     * Build list layout using ->addColumn()
     *
     * @return void
     */

    public function buildListLayout()
    {
        $this->addColumn('debut', 2)
            ->addColumn('user', 2)
            ->addColumn('label', 2)
            ->addColumn('status', 2)
            ->addColumn('montant', 3);
    }

    /**
     * Build list config
     *
     * @return void
     */
    public function buildListConfig()
    {
        $this->setPaginated()
            ->setSearchable()
            ->setInstanceIdAttribute('id')
            ->setDefaultSort('debut', 'desc')
            ->addInstanceCommand("imprimer_ndf", NdeFraisExportPDFCommand::class)
            ->addInstanceCommand("signer_ndf", NdeFraisAPSignPDFCommand::class)
            ->addInstanceCommand("imprimer_justifs_ndf", NdeFraisExportJustificatifsPDFCommand::class)
            ->addInstanceCommand("send_mail", NdeFraisSendMail::class);

        //On imagine ajouter des commandes issues des plugins ...
        $p = new Plugin();
        $toAdd = $p->getCommandsNDFSharpList();
        foreach ($toAdd as $t) {
            $this->addInstanceCommand($t['command'], $t['class']);
        }
    }

    /**
     * Retrieve all rows data as array.
     *
     * @param EntityListQueryParams $params
     * @return array
     */
    public function getListData(EntityListQueryParams $params)
    {
        $userid = sharp_user()->id;
        //C'est ici qu'on pourrait avoir le comptable de la société qui aurait le droit de consulter les notes de frais en cours des salariés ...
        if (sharp_user()->hasPermissionTo('show others NdeFrais')) {
            Log::debug("NdeFraisSharpList::getListData on a la perm 'show others NdeFrais' et on liste des NDF de user : " . session('usertoget'));
            //On vérifie si cet utilisateur est "possible"
            $users_possibles = User::getMyUsers()->pluck('id')->toArray();

            if (\in_array(session('usertoget'), $users_possibles)) {
                $userid = session('usertoget');
            } else {
                throw new SharpApplicativeException("Vous n'avez pas le droit d'accéder à cette ressource !");
            }
        }
        $ndfs = NdeFrais::where('user_id', $userid)->orderBy($params->sortedBy(), $params->sortedDir());

        collect($params->searchWords())
            ->each(function ($word) use ($ndfs) {
                $ndfs->where(function ($query) use ($word) {
                    $query->orWhere('label', 'like', $word)
                        ->orWhere('montant', 'like', $word);
                });
            });

        // Log::debug($ndfs->toSql());

        return $this->setCustomTransformer(
            "status",
            function ($status) {
                if ($status == NdeFrais::STATUS_OPEN)
                    return "en cours";
                else if ($status == NdeFrais::STATUS_FREEZED)
                    return "gelée";
                else if ($status == NdeFrais::STATUS_CLOSED)
                    return "clôturée"; // . nbFR($montant) . "&nbsp;€";
            }
        )->setCustomTransformer(
            "montant",
            function ($montant) {
                if ($montant == 0)
                    return "Total non disponible";
                return nbFR($montant) . "&nbsp;€";
            }
        )->setCustomTransformer(
            "user",
            function ($user, $ndf) {
                // Log::debug("getListData:on cherche pour " . $ndf->user_id);
                $u = User::where('id', $ndf->user_id)->first();
                // Log::debug($u);
                return $u->firstname . " " . $u->name;
            }
        )
            ->transform($ndfs->paginate(30));
        // ->setCustomTransformer("ndeFrais", function($ndfs, $ldfs) {
        //     return $ldfs->NdeFrais->map(function($edit) {
        //         return (new LinkToEntity($ldfs->name, "LdeFrais"))
        //             ->setTooltip("Détails de la note de frais")
        //             ->setSearch($ldfs->name)
        //             ->render();
        //     })->implode("<br>");
        // })
    }
}
