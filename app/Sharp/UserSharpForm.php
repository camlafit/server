<?php
/*
 * UserSharpForm.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use App\User;
use App\Entreprise;
use App\NdeFrais;
use Spatie\Permission\Models\Role;
use Code16\Sharp\Form\SharpForm;
use Code16\Sharp\Http\WithSharpContext;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Layout\FormLayoutFieldset;
use Code16\Sharp\Form\Layout\FormLayoutTab;
use Code16\Sharp\Form\Fields\SharpFormSelectField;
use Code16\Sharp\Form\Fields\SharpFormCheckField;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Fields\SharpFormListField;
use Code16\Sharp\Form\Fields\SharpFormAutocompleteField;
use Code16\Sharp\Form\Fields\SharpFormHtmlField;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Code16\Sharp\Exceptions\Form\SharpApplicativeException;

class UserSharpForm extends SharpForm
{
    use WithSharpFormEloquentUpdater, WithSharpContext;
    /**
     * Retrieve a Model for the form and pack all its data as JSON.
     *
     * @param $id
     * @return array
     */
    public function find($id): array
    {
        // Log::debug("*******************");
        $user = User::with('entreprises', 'roles')->findOrFail($id);

        //On vire son mot de passe pour laisser le champ vide
        $user->password = "";

        //Il faut supprimer les relations auxquelles on a pas accès, par exemple si un revendeur modifie la fiche d'un utilisateur
        //qui est membres d'entreprises hors de son périmètre ...
        $entreprises_possibles = Entreprise::getMyEntreprises()->pluck('id')->toArray();
        // Log::debug("  liste de mes entreprises possibles... ");
        // Log::debug($user);

        $e = collect();
        foreach ($user->entreprises as $entreprise) {
            $eid = $entreprise->pivot['entreprise_id'];
            if (in_array($eid, $entreprises_possibles)) {
                $entreprise['entreprise_id'] = $eid;
                $entreprise['role_id'] = $entreprise->pivot['role_id'];
                $e[] = $entreprise;
            } else {
                Log::debug("il faudrait virer " . $eid);
                unset($entreprise);
            }
        }
        unset($user->entreprises);
        $user->entreprises = $e->values();
        // Log::debug("  apres: ");
        // Log::debug($user);

        // Log::debug(" +++++++++++++++++++++ ");
        // Log::debug($this->transform($user));
        // Log::debug(" ===================== ");

        // Log::debug("*******************");
        //cf https://sharp.code16.fr/docs/guide/how-to-transform-data.html#custom-transformers
        return $this->transform(
            $user
        );
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed the instance id
     */
    public function update($id, array $data)
    {
        if ($this->context()->isCreation()) {
            $dup = User::where("email", $data['email'])->first();
            if ($dup) {
                throw new SharpApplicativeException("Erreur, un compte avec cette adresse mail (" . $data['email'] . ") existe déjà !");
            }
        }
        $instance = $id ? User::findOrFail($id) : new User;
        Log::debug("******* UserSharpForm update ************");
        // Log::debug($data);

        //Il manque le user_id pour la serie entreprises ... je l'ajoute ici meme si je suis certain qu'il y a une meilleure façon de faire !
        // foreach ($data['entreprises'] as $key => $value) {
        //     $data['entreprises'][$key]['user_id'] = $id;
        // }
        // Log::debug($data['entreprises']);

        return tap($instance, function ($user) use ($data) {

            //Par defaut on ne sauvegarde pas ces donnees
            $ignore = ["entreprises", "roles", "aide_lien", "aide_lien_roles", "aide_lien_compta"];
            if ($data['password'] != "") {
                Log::debug("Affectation d'un mot de passe ...");
                $password = $data['password'];
                $data['password'] = Hash::make($password);
            } else {
                //Et si le pass est vide idem on ne le sauvegarde pas
                $ignore[] = 'password';
            }

            //pour l'instant on refuse de mettre à jour le mail (donc si on est en mode "update" on ignore le mail)
            if (!$this->context()->isCreation()) {
                $ignore[] = 'email';
            }

            //Lors de la creation du compte on affecte le user courrant comme creator de ce compte
            //ça permet de le "voir" ensuite dans la liste de "mes utilisateurs"
            //Pourquoi on change le createur de cet utilisateur ?
            //20200529 je prefere laisser l'id du vrai createur ... a voir les conséquences
            if ($this->context()->isCreation()) {
                $user->creator_id = sharp_user()->id;
            }

            //On ignore les champs qui n'existent pas dans la table users
            $this->ignore($ignore)->save($user, $data);

            //relations, c'est super complexe ... car l'utilisateur courant n'a peut-être pas accès à toutes les relations
            //de cette personne (exemple un revendeur qui ne peut pas voir les relations qu'un autre administrateur aurait sur ce compte)
            //il faut donc actualiser les relations accessibles qui ont été modifiées ...
            $relations = $this->cleanup($data['entreprises']);

            //Liste des entreprises auxquelles j'ai accès, cet utilisateur a peut-être d'autres relations que je n'ai pas le droit de voir
            $entreprises_possibles = Entreprise::getMyEntreprises()->pluck('id')->toArray();
            $anciennes_relations = array();
            $u = User::with('entreprises', 'roles')->findOrFail($user->id);
            $i = 0;
            foreach ($u->entreprises as $entreprise) {
                if (in_array($entreprise->pivot['entreprise_id'], $entreprises_possibles)) {
                    $anciennes_relations[$i]['entreprise_id'] = $entreprise->pivot['entreprise_id'];
                    $anciennes_relations[$i]['role_id']       = $entreprise->pivot['role_id'];
                    $anciennes_relations[$i]['user_id']       = $entreprise->pivot['user_id'];
                    $i++;
                }
            }

            $nouvelles_relations = array();
            $i = 0;
            foreach ($data['entreprises'] as $entreprise => $value) {
                if (in_array($value['entreprise_id'], $entreprises_possibles)) {
                    $nouvelles_relations[$i]['entreprise_id'] = $value['entreprise_id'];
                    $nouvelles_relations[$i]['role_id']       = $value['role_id'];
                    $nouvelles_relations[$i]['user_id']       = $user->id;
                    $i++;
                }
            }

            $diffA = $this->relationsDiff($anciennes_relations, $nouvelles_relations);
            $diffB = $this->relationsDiff($nouvelles_relations, $anciennes_relations);
            Log::debug("  UserSharpForm On souhaite avoir les nouvelles relations : " . json_encode($nouvelles_relations));
            Log::debug("  UserSharpForm On avait comme anciennes relations : " . json_encode($anciennes_relations));
            Log::debug("  UserSharpForm DIFF 1 : " . json_encode($diffA));
            Log::debug("  UserSharpForm DIFF 2 : " . json_encode($diffB));

            //Il faut ajouter les relations diffB
            foreach ($diffB as $k => $v) {
                Log::debug("  UserSharpForm ajout de relation sur entreprise id " . $v['entreprise_id'] . " role id " . $v['role_id'] . " pour user id " . $v['user_id']);
                $u = User::findOrFail($v['user_id']);
                $u->addRoleOnEntreprise($v['role_id'], $v['entreprise_id']);
            }
            //et supprimer  les relations diffA
            foreach ($diffA as $k => $v) {
                Log::debug("  UserSharpForm supprime la relation sur entreprise id " . $v['entreprise_id'] . " role id " . $v['role_id'] . " pour user id " . $v['user_id']);
                DB::table('entreprise_user')->where('entreprise_id', $v['entreprise_id'])
                    ->where('role_id', $v['role_id'])
                    ->where('user_id', $v['user_id'])
                    ->delete();
            }

            //Et ensuite on fait les relations ?
            //$user->entreprises()->sync([1 => ['entreprise_id' => $data['entreprise_id'], 'user_id' => $id, 'role_id' => $data['role_id']]]);
            //$user->entreprises()->detach();
            // $user->entreprises()->sync($relations);

            //Le rôle global de cet utilisateur
            $user->syncRoles($data['main_role']);

            //Si c'est une création de compte on lui créé sa 1ere note de frais ?
            if ($this->context()->isCreation()) {
                Log::debug("  UserSharpForm en mode création...");
                $ndf = new NdeFrais();
                $ndf->makeNew($user->id);
            }
        });
    }

    function create(): array
    {
        // Log::debug("******* CREATE ************");

        //On recupere les infos du compte actuel pour les appliquer comme config par défaut aux nouveaux comptes
        $config = sharp_user()->getComptaConfig();
        $u = new User($config);
        Log::debug($this->transform($u));
        return $this->transform($u);
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        User::findOrFail($id)->delete();
    }

    /**
     * Build form fields using ->addField()
     *
     * @return void
     */
    public function buildFormFields()
    {
        $this->addField(
            SharpFormTextField::make('firstname')
                ->setLabel('Prénom')
        )->addField(
            SharpFormTextField::make('name')
                ->setLabel('Nom')
        )->addField(
            SharpFormTextField::make('email')
                ->setLabel('Mail')
            // ->setHelpMessage("Si vous devez changer l'adresse mail d'un utilisateur veuillez prendre contact avec votre administrateur système ...")
        )->addField(
            SharpFormTextField::make('adresse')
                ->setLabel('Adresse')
        )->addField(
            SharpFormTextField::make('cp')
                ->setLabel('Code Postal')
        )->addField(
            SharpFormTextField::make('ville')
                ->setLabel('Ville')
        )->addField(
            SharpFormTextField::make('pays')
                ->setLabel('Pays')
        )->addField(
            SharpFormHtmlField::make('aide_lien_compta')
                ->setInlineTemplate(
                    "<a href='#' onClick=\"HelpWindow=window.open('/docs/1.0/g-configuration-compta','HelpWindow','width=800,height=400'); return false;\">Aide (Configuration de la compta)</a>"
                )
        )->addField(
            SharpFormTextField::make('send_copy_to')
                ->setLabel('Envoyer les mails en copie à')
        )->addField(
            SharpFormTextField::make('compta_email')
                ->setLabel('Adresse mail de votre service comptable')
        )->addField(
            SharpFormTextField::make('compta_ik')
                ->setLabel('Indemnités kilométriques')
                ->setPlaceholder("625100")
        )->addField(
            SharpFormTextField::make('compta_peage')
                ->setLabel('Péages & Parking')
        )->addField(
            SharpFormTextField::make('compta_train')
                ->setLabel('Transports (Train/Avion...)')

        )->addField(
            SharpFormTextField::make('compta_hotel')
                ->setLabel('Hébergement (hôtel)')
        )->addField(
            SharpFormTextField::make('compta_taxi')
                ->setLabel('Taxi')
        )->addField(
            SharpFormTextField::make('compta_restauration')
                ->setLabel('Restauration')
        )->addField(
            SharpFormTextField::make('compta_divers')
                ->setLabel('Frais divers')

        )->addField(
            SharpFormTextField::make('compta_compteperso')
                ->setLabel('Compte personnel')
        )->addField(
            SharpFormTextField::make('compta_compteprocb')
                ->setLabel('Compte pro CB')
        )->addField(
            SharpFormTextField::make('compta_compteproesp')
                ->setLabel('Compte pro Especes')

        )->addField(
            SharpFormTextField::make('compta_carburant0recup')
                ->setLabel('Carburant zéro récup. tva')
        )->addField(
            SharpFormTextField::make('compta_carburant60recup')
                ->setLabel('Carburant 60% récup. tva')
        )->addField(
            SharpFormTextField::make('compta_carburant80recup')
                ->setLabel('Carburant 80% récup. tva')
        )->addField(
            SharpFormTextField::make('compta_carburant100recup')
                ->setLabel('Carburant 100% récup. tva')

        )->addField(
            SharpFormTextField::make('compta_tvadeductible')
                ->setLabel('Compte TVA déductible')
        )->addField(
            SharpFormSelectField::make(
                "main_role",
                Role::where('id', '<=', sharp_user()->main_role)->orderBy("id")->get()->pluck('name', 'id')->all()
            )
                ->setDisplayAsDropdown()
                ->setLabel('Rôle principal')
        )->addField(
            SharpFormHtmlField::make('aide_lien_roles')
                ->setInlineTemplate(
                    "<a href='#' onClick=\"HelpWindow=window.open('/docs/1.0/g-roles','HelpWindow','width=800,height=400'); return false;\">Aide (rôles des utilisateurs)</a>"
                )
        )->addField(
            SharpFormTextField::make('password')
                ->setLabel('Mot de passe')
                ->setInputTypePassword()
        )->addField(
            SharpFormHtmlField::make('aide_lien')
                ->setInlineTemplate(
                    "<a href='#' onClick=\"HelpWindow=window.open('/docs/1.0/g-modeSimple','HelpWindow','width=800,height=400'); return false;\">Aide (à savoir sur le mode simple)</a>"
                )
        )->addField(
            SharpFormCheckField::make('mode_simple', "Actif")
                ->setLabel('Mode Simple')
                ->setHelpMessage("Accepte les scan de facturettes sans détails")
        )->addField(
            SharpFormAutocompleteField::make('corrector_id', "local")
                ->setLabel("Personne à notifier")
                ->setLocalSearchKeys(["name", "firstname"])
                ->setLocalValues(User::getMyUsers('name', 'asc', true))
                ->setListItemInlineTemplate("{{firstname}} {{name}} <{{email}}>")
                ->setResultItemInlineTemplate("{{firstname}} {{name}} <{{email}}>")
                ->addConditionalDisplay('mode_simple', true)
                ->setHelpMessage("Sera en charge de vérifier les facturettes envoyées par l'utilisateur")
        )->addField(
            SharpFormListField::make("entreprises")
                ->setLabel("Associer cet utilisateur à une entreprise")
                ->setAddable()->setAddText("Associer à une entreprise existante")
                ->setRemovable()
                ->addItemField(
                    SharpFormAutocompleteField::make("entreprise_id", "local")
                        ->setLabel("Entreprise")
                        ->setLocalSearchKeys(["name", "email"])
                        ->setLocalValues(Entreprise::getMyEntreprises('name'))
                        ->setListItemInlineTemplate("{{name}}")
                        ->setResultItemInlineTemplate("{{name}}")
                )->addItemField(
                    SharpFormAutocompleteField::make("role_id", "local")
                        ->setLabel("Role")
                        ->setLocalSearchKeys(["name"])
                        ->setLocalValues(Role::where('id', '<=', sharp_user()->main_role)->orderBy("id")->get()->all())
                        ->setListItemInlineTemplate("{{name}}")
                        ->setResultItemInlineTemplate("{{name}}")
                )
        );
    }

    /**
     * Build form layout using ->addTab() or ->addColumn()
     *
     * @return void
     */
    public function buildFormLayout()
    {
        $this->addTab("Fiche", function (FormLayoutTab $tab) {
            $tab->addColumn(12, function (FormLayoutColumn $column) {
                $column->withFields('firstname|4', 'name|4', 'email|4');
                $column->withFields('adresse|12');
                $column->withFields('cp|4', 'ville|4', 'pays|4');
                $column->withFieldset("Configuration avancée", function (FormLayoutFieldset $fieldset) {
                    //Gestion du role
                    if (sharp_user()->hasPermissionTo('assign MainRole to User')) {
                        $fieldset->withFields('main_role|4', 'password|4', 'aide_lien_roles|3');
                    }
                    $fieldset->withFields('mode_simple|3', 'corrector_id|6', 'aide_lien|3');
                    return $fieldset;
                });
            });
        })->addTab("Comptabilité", function (FormLayoutTab $tab) {
            $tab->addColumn(12, function (FormLayoutColumn $column) {
                $column->withFieldset("Configuration de la comptabilité", function (FormLayoutFieldset $fieldset) {
                    return $fieldset->withFields('send_copy_to|4', 'compta_email|4', 'aide_lien_compta|3')
                        ->withFields('compta_ik|3', 'compta_peage|3', 'compta_train|3', 'compta_hotel|3')
                        ->withFields('compta_taxi|3', 'compta_restauration|3', 'compta_divers|3', 'compta_tvadeductible|3')
                        ->withFields('compta_carburant0recup|3', 'compta_carburant60recup|3', 'compta_carburant80recup|3', 'compta_carburant100recup|3')
                        ->withFields('compta_compteperso|3', 'compta_compteprocb|3', 'compta_compteproesp|3');
                });
            });
        })->addTab("Entreprises", function (FormLayoutTab $tab) {
            $tab->addColumn(12, function (FormLayoutColumn $column) {
                $column->withSingleField('entreprises', function (FormLayoutColumn $fieldset) {
                    return $fieldset->withFields('entreprise_id|6', 'role_id|6');
                });
            });
        });
    }

    private function cleanup($tab)
    {
        $t = array();
        foreach ($tab as $key => $val) {
            if ($val['entreprise_id'] == null || $val['role_id'] == null) {
                // Log::debug("cleanup : on a un champ null ...");
            } else {
                $t[] = $val;
            }
        }
        // Log::debug("cleanup : on retourne " . json_encode($t));
        return $t;
    }

    //cherche dans tab2 si on a role/entreprise/userid de tab1
    private function relationsDiff($tab1, $tab2)
    {
        $aReturn = array();

        //Pour se simplifier la vie on passe par un tableau de texte du genre 2;3;4
        $t2 = array();
        for ($i = 0; $i < count($tab2); $i++) {
            $t2[$i] = implode(';', $tab2[$i]);
        }

        for ($i = 0; $i < count($tab1); $i++) {
            $str = implode(';', $tab1[$i]);
            if (in_array($str, $t2)) {
            } else {
                $aReturn[] = $tab1[$i];
            }
        }

        return $aReturn;
    }
}
