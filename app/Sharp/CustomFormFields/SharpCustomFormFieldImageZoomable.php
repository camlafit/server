<?php
/*
 * SharpCustomFormFieldImageZoomable.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp\CustomFormFields;

use Code16\Sharp\Form\Fields\Formatters\TextFormatter;
use Code16\Sharp\Form\Fields\SharpFormField;
use Illuminate\Support\Facades\Log;

class SharpCustomFormFieldImageZoomable extends SharpFormField
{
    const FIELD_TYPE = "custom-imageZoomable";

    /**
     * @param string $key
     * @return static
     */
    public static function make(string $key)
    {
        Log::debug("SharpCustomFormFieldImageZoomable :: make " . serialize($key));
        return new static($key, static::FIELD_TYPE, new TextFormatter);
    }

    /**
     * @return array
     * @throws \Code16\Sharp\Exceptions\Form\SharpFormFieldValidationException
     */
    public function toArray(): array
    {
        // Log::debug("SharpCustomFormFieldImageZoomable::toArray " . serialize(parent::buildArray([])));
        return parent::buildArray([]);
    }
}
