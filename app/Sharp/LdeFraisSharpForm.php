<?php
/*
 * LdeFraisSharpForm.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use App\User;
use App\LdeFrais;
use App\TypeFrais;
use Spatie\Permission\Models\Role;
use Code16\Sharp\Form\SharpForm;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Layout\FormLayoutFieldset;
use Code16\Sharp\Form\Fields\SharpFormNumberField;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Fields\SharpFormListField;
use Code16\Sharp\Form\Fields\SharpFormHtmlField;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;
use Illuminate\Support\Facades\Log;
use App\Sharp\Formatters\LdeFraisSharpFormatter;
use App\Sharp\Formatters\LdeFraisVoituresSharpFormatter;
use Code16\Sharp\Form\Fields\SharpFormTagsField;
use Code16\Sharp\Form\Fields\SharpFormSelectField;
use Code16\Sharp\Show\Layout\ShowLayoutColumn;
use App\Sharp\CustomFormFields\SharpCustomFormFieldImageZoomable;

class LdeFraisSharpForm extends SharpForm
{
    use WithSharpFormEloquentUpdater;

    function find($id): array
    {
        $userid = \sharp_user()->id;
        if (sharp_user()->hasPermissionTo('show others NdeFrais')) {
            $userid = session('usertoget');
        }
        $ldfs = LdeFrais::where('user_id', $userid);

        // Log::debug("LdeFraisSharpForm :: find " . $id);
        $lde = LdeFrais::where('user_id', $userid)->findOrFail($id);

        // Log::debug("LdeFraisSharpForm :: image " . $lde->getImageURI());
        $lde->type_frais_slug = $lde->getTypeFraisSlug();

        //debug eric
        // $pdf = "/home/erics/dev/openNoteDeFrais/serveur.adminpanel/storage/LdeFrais/es@alpha.devtemp.fr/202006/20200615-restaurati...-resto-rdv-client-X3ZkYzqXo8.pdf";
        // $lde->makePDF("/home/erics/dev/openNoteDeFrais/serveur.adminpanel/storage/LdeFrais/es@alpha.devtemp.fr/202006/20200615-restaurati...-resto-rdv-client-X3ZkYzqXo8.jpeg",$pdf);
        // if (file_exists($pdf) && is_file($pdf)) {
        //     $lde->signAndArchivePDF($pdf);
        // }
        //end debug eric

        //Le modele LdeFrais a un fileName "incomplet" donc on passe par un transformer
        //pour ajouter la propriete pictureuri qui fournit l'URI complète d'accès à l'image
        return $this->setCustomTransformer("pictureuri", function ($picture, LdeFrais $l) {
            $r = $l->getImageURI();
            Log::debug("Création de pictureuri : $r");
            return $r;
        })->transform(
            $lde
        );
    }

    function update($id, array $data)
    {
        Log::debug("LdeFraisSharpForm :: update " . $id);
        $instance = $id ? LdeFrais::findOrFail($id) : new LdeFrais;

        return tap($instance, function ($ldefrais) use ($data) {
            //On ignore les champs qui n'existent pas dans la table
            $this->ignore(["type_frais_slug", "pictureuri"])
                ->save($ldefrais, $data);
        });
    }

    function delete($id)
    {
        Log::debug("LdeFraisSharpForm :: delete " . $id);
        LdeFrais::findOrFail($id)->delete();
    }


    function buildFormFields()
    {
        $formatter        = new LdeFraisSharpFormatter;
        $formatterVoiture = new LdeFraisVoituresSharpFormatter;
        $style = "background: transparent";
        $this->addField(
            SharpFormTextField::make("label")
                ->setLabel("Objet")
        )->addField(
            SharpFormTextField::make("ladate")
                ->setLabel("Date")
        )->addField(
            SharpFormTextField::make("ht")
                ->setLabel("HT")
        )->addField(
            SharpFormTextField::make("ttc")
                ->setLabel("TTC")
        )->addField(
            SharpFormSelectField::make(
                "type_frais_slug",
                TypeFrais::orderBy("id")->get()->pluck('label', 'slug')->all()
            )
                ->setDisplayAsDropdown()
                ->setMultiple(false)
                ->setLabel('Type de frais')
                ->setReadOnly(true)
        )->addField(
            SharpFormTextField::make("tvaTx1")
                ->setReadOnly(true)
                ->setFormatter($formatter)
                ->setExtraStyle($style)
                ->addConditionalDisplay("type_frais_slug", "!ik")
        )->addField(
            SharpFormTextField::make("tvaTx2")
                ->setReadOnly(true)
                ->setFormatter($formatter)
                ->addConditionalDisplay("type_frais_slug", "!ik")
        )->addField(
            SharpFormTextField::make("tvaTx3")
                ->setReadOnly(true)
                ->setFormatter($formatter)
                ->addConditionalDisplay("type_frais_slug", "!ik")
        )->addField(
            SharpFormTextField::make("tvaTx4")
                ->setReadOnly(true)
                ->setFormatter($formatter)
                ->addConditionalDisplay("type_frais_slug", "!ik")
        )->addField(
            SharpFormTextField::make("tvaVal1")
                ->addConditionalDisplay("type_frais_slug", "!ik")
        )->addField(
            SharpFormTextField::make("tvaVal2")
                ->addConditionalDisplay("type_frais_slug", "!ik")
        )->addField(
            SharpFormTextField::make("tvaVal3")
                ->addConditionalDisplay("type_frais_slug", "!ik")
        )->addField(
            SharpFormTextField::make("tvaVal4")
                ->addConditionalDisplay("type_frais_slug", "!ik")
        )->addField(
            SharpFormTextField::make("invites")
                ->setLabel("Invité ou détails des frais engagés")
        )->addField(
            SharpFormTextField::make("depart")
                ->setLabel("Départ")
                ->addConditionalDisplay("type_frais_slug", "ik")
        )->addField(
            SharpFormTextField::make("arrivee")
                ->setLabel("Arrivée")
                ->addConditionalDisplay("type_frais_slug", "ik")
        )->addField(
            SharpFormTextField::make("distance")
                ->setLabel("Distance")
                ->addConditionalDisplay("type_frais_slug", "ik")
        )->addField(
            SharpFormTextField::make("vehicule")
                ->setLabel("Véhicule")
                ->addConditionalDisplay("type_frais_slug", ["ik", "carburant"])
                ->setReadOnly(true)
                ->setFormatter($formatterVoiture)
        )->addField(
            SharpCustomFormFieldImageZoomable::make('pictureuri')
        );
    }

    function buildFormLayout()
    {
        $this->addColumn(4, function (FormLayoutColumn $column) {
            $column->withSingleField("pictureuri");
        });

        $this->addColumn(8, function (FormLayoutColumn $column) {
            $column
                ->withFieldset("Général", function (FormLayoutFieldset $fieldset) {
                    return $fieldset->withFields('label|8', 'ladate|4')
                        ->withFields('invites|12')
                        ->withFields('ht|3', 'ttc|3', 'type_frais_slug|6');
                })
                ->withFieldset("TVA", function (FormLayoutFieldset $fieldset) {
                    return $fieldset->withFields('tvaTx1|4', 'tvaVal1|2', 'tvaTx2|4', 'tvaVal2|2')
                        ->withFields('tvaTx3|4', 'tvaVal3|2', 'tvaTx4|4', 'tvaVal4|2');
                })
                ->withFieldset("Indemnités kilométriques ou Frais de Carburant :", function (FormLayoutFieldset $fieldset) {
                    return $fieldset->withFields('depart|3', 'arrivee|3', 'distance|3', 'vehicule|3');
                });
        });
    }
}
