<?php
/*
 * EntrepriseSharpForm.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use App\User;
use App\Entreprise;
use Spatie\Permission\Models\Role;
use Code16\Sharp\Form\SharpForm;
use Code16\Sharp\Http\WithSharpContext;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Layout\FormLayoutFieldset;
use Code16\Sharp\Form\Layout\FormLayoutTab;
use Code16\Sharp\Form\Fields\SharpFormNumberField;
use Code16\Sharp\Form\Fields\SharpFormHtmlField;
use Code16\Sharp\Form\Fields\SharpFormCheckField;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Fields\SharpFormListField;
use Code16\Sharp\Form\Fields\SharpFormAutocompleteField;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class EntrepriseSharpForm extends SharpForm
{
    use WithSharpFormEloquentUpdater, WithSharpContext;

    function find($id): array
    {
        $entreprise = Entreprise::with('users')->findOrFail($id);
        Log::debug("EntrepriseSharpForm : find " . $id);

        //Pour les relations je ne sais pas encore comment faire automatique, il faut aller chercher le contenu du pivot...
        foreach ($entreprise->users as $user) {
            $user['user_id'] = $user->pivot['user_id'];
            $user['role_id'] = $user->pivot['role_id'];
        }

        // unset($entreprise->eprefs->id);
        // Log::debug($entreprise);

        return $this->transform(
            $entreprise
        );
    }

    function update($id, array $data)
    {
        Log::debug("EntrepriseSharpForm : update " . $id);
        $instance = $id ? Entreprise::findOrFail($id) : new Entreprise;

        $instance['creator_id'] = sharp_user()->id;

        //Il manque le entreprise_id pour la serie users ... je l'ajoute ici meme si je suis certain qu'il y a une meilleure façon de faire !
        // $e_r = $data['users'];
        // foreach ($e_r as $key => $value) {
        //     $value['entreprise_id'] = $this->_entrepriseId;
        // }

        return tap($instance, function ($entreprise) use ($data) {
            // Log::debug(" =============== TAP =================");
            // Log::debug(serialize($id));
            // Log::debug(" =============== END TAP =================");

            //On ignore les champs qui n'existent pas dans la table entreprise
            $this->ignore(["users", "aide_compta_global_ndf"])->save($entreprise, $data);

            //On ajoute l'utilisateur en cours comme createur si on est en mode creation de la fiche
            if ($this->context()->isCreation()) {
                $t = array(
                    'id' => NULL,
                    'user_id' => sharp_user()->id,
                    'role_id' => sharp_user()->main_role,
                );
                $data['users'][] = $t;

                // Log::debug(" =============== creation =================");
                // Log::debug(" =============== creation =================");
            }

            //relations, c'est super complexe ... car l'utilisateur courant n'a peut-être pas accès à toutes les relations
            //de cette entreprise il faut donc actualiser les relations accessibles qui ont été modifiées ...

            //Liste des entreprises auxquelles j'ai accès, cet utilisateur a peut-être d'autres relations que je n'ai pas le droit de voir
            $users_possibles = User::getMyUsers()->pluck('id')->toArray();

            //Anciennes relations
            $anciennes_relations = array();
            $e = Entreprise::with('users', 'roles')->findOrFail($entreprise->id);
            $i = 0;
            foreach ($e->users as $user) {
                if (in_array($user->pivot['user_id'], $users_possibles)) {
                    $anciennes_relations[$i]['entreprise_id'] = $entreprise->id;
                    $anciennes_relations[$i]['user_id']       = $user->pivot['user_id'];
                    $anciennes_relations[$i]['role_id']       = $user->pivot['role_id'];
                    $i++;
                }
            }

            $nouvelles_relations = array();
            $i = 0;
            foreach ($data['users'] as $user => $value) {
                if (in_array($value['user_id'], $users_possibles)) {
                    $nouvelles_relations[$i]['entreprise_id'] = $entreprise->id;
                    $nouvelles_relations[$i]['user_id']       = $value['user_id'];
                    $nouvelles_relations[$i]['role_id']       = $value['role_id'];
                    $i++;
                }
            }

            $diffA = $this->relationsDiff($anciennes_relations, $nouvelles_relations);
            $diffB = $this->relationsDiff($nouvelles_relations, $anciennes_relations);
            Log::debug("On souhaite avoir les nouvelles relations : " . json_encode($nouvelles_relations));
            Log::debug("On avait comme anciennes relations : " . json_encode($anciennes_relations));
            Log::debug("DIFF 1 : " . json_encode($diffA));
            Log::debug("DIFF 2 : " . json_encode($diffB));

            //Il faut ajouter les relations diffB
            foreach ($diffB as $k => $v) {
                $u = User::findOrFail($v['user_id']);
                $u->addRoleOnEntreprise($v['role_id'], $v['entreprise_id']);
            }
            //et supprimer  les relations diffA
            foreach ($diffA as $k => $v) {
                DB::table('entreprise_user')->where('entreprise_id', $v['entreprise_id'])
                    ->where('role_id', $v['role_id'])
                    ->where('user_id', $v['user_id'])
                    ->delete();
            }

            // $relations = $this->cleanup($data['users']);
            // //Et ensuite on fait les relations ?
            // $entreprise->users()->detach();
            // $entreprise->users()->sync($relations);
        });
    }

    function delete($id)
    {
        Log::debug("EntrepriseSharpForm : delete " . $id);
        Entreprise::findOrFail($id)->delete();
    }

    function create(): array
    {
        Log::debug("EntrepriseSharpForm : create ");
        $e = new Entreprise();
        Log::debug($this->transform($e));
        return $this->transform($e);
    }


    function buildFormFields()
    {

        //Seuls les admins & revendeurs peuvent activer l'option ... ou le client
        //final mais via un formulaire "web propre" à faire
        $_archivage_ro = true;
        if (sharp_user()->hasRole('superAdmin') || sharp_user()->hasRole('adminRevendeur')) {
            $_archivage_ro = false;
        }

        $this->addField(
            SharpFormAutocompleteField::make("siret", "remote")
                ->setLabel("Num. SIREN/SIRET")
                ->setPlaceholder("Saisissez le numéro et patientez")
                ->setRemoteMethodGET()
                ->setSearchMinChars(9)
                ->setItemIdAttribute('siret')
                ->setRemoteSearchAttribute('siret')
                ->setListItemTemplatePath("sharp/templates/siret_delegate_result.vue")
                ->setResultItemInlineTemplate("{{siret}}")
                ->setRemoteEndpoint("/api/siret/search/")
        )->addField(
            SharpFormTextField::make("name")
                ->setLabel("Entreprise")
        )->addField(
            SharpFormTextField::make("adresse")
                ->setLabel("Adresse")
        )->addField(
            SharpFormTextField::make("cp")
                ->setLabel("Code postal")
        )->addField(
            SharpFormTextField::make("ville")
                ->setLabel("Ville")
        )->addField(
            SharpFormTextField::make("pays")
                ->setLabel("Pays")
        )->addField(
            SharpFormTextField::make("email")
                ->setLabel("Mail")
        )->addField(
            SharpFormTextField::make("web")
                ->setLabel("Site web")
        )->addField(
            SharpFormTextField::make("tel")
                ->setLabel("Téléphone")
        )->addField(
            SharpFormCheckField::make('eprefs:compta_global_ndf_enable', "Actif")
                ->setLabel("Export global des notes")
                ->setHelpMessage("Regrouper les notes de frais de la société")
        )->addField(
            SharpFormTextField::make("eprefs:compta_global_ndf_target")
                ->setLabel("Adresse mail de destination")
                ->addConditionalDisplay('eprefs:compta_global_ndf_enable', true)
                ->setHelpMessage("Cette adresse recevra le fichier global d'import de toutes les notes de frais (service comptabilité)")
        )->addField(
            SharpFormHtmlField::make('aide_compta_global_ndf')
                ->setInlineTemplate(
                    "<a href='#' onClick=\"HelpWindow=window.open('/docs/1.0/g-exportGlobal','HelpWindow','width=800,height=400'); return false;\">Aide (export global)</a>"
                )
        )->addField(
            SharpFormCheckField::make('eprefs:archivage', "Actif")
                ->setLabel("Archivage")
                ->setReadOnly($_archivage_ro)
                ->setHelpMessage("Archiver les documents (notes et justificatifs) : 5€/mois/utilisateur")
        )->addField(
            SharpFormCheckField::make('eprefs:archive_probante', "Actif")
                ->setLabel("Archivage à valeur probante")
                ->setReadOnly($_archivage_ro)
                ->addConditionalDisplay('eprefs:archivage', true)
                ->setHelpMessage("Conserve les documents selon la durée légale en vigueur (10 ans) : 5€/mois/utilisateur de plus")
        )->addField(
            SharpFormListField::make("users")
                ->setLabel("Utilisateurs")
                ->setAddable()->setAddText("Associer à un utilisateur existant")
                // ->setFormatter($formatter)
                ->setRemovable()
                ->addItemField(
                    SharpFormAutocompleteField::make("user_id", "local")
                        ->setLabel("Utilisateur")
                        ->setLocalSearchKeys(["name", "firstname", "email"])
                        ->setLocalValues(User::getMyUsers('name', 'asc', true))
                        ->setListItemInlineTemplate("{{firstname}} {{name}} <{{email}}>")
                        ->setResultItemInlineTemplate("{{firstname}} {{name}} <{{email}}>")
                )->addItemField(
                    SharpFormAutocompleteField::make("role_id", "local")
                        ->setLabel("Role")
                        ->setLocalSearchKeys(["name"])
                        ->setLocalValues(Role::where('id', '<=', sharp_user()->main_role)->orderBy("id")->get()->all())
                        ->setListItemInlineTemplate("{{name}}")
                        ->setResultItemInlineTemplate("{{name}}")
                )
        );
    }

    function buildFormLayout()
    {
        $this->addTab("Fiche", function (FormLayoutTab $tab) {
            $tab->addColumn(12, function (FormLayoutColumn $column) {
                $column->withFields('siret|4', 'name|8');
                $column->withFields('adresse|8', 'cp|4');
                $column->withFields('ville|4', 'pays|4');
                $column->withFields('tel|4', 'email|4', 'web|4');
            });
        })->addTab("Options", function (FormLayoutTab $tab) {
            $tab->addColumn(12, function (FormLayoutColumn $column) {
                $column->withFieldset("Options générales", function (FormLayoutFieldset $fieldset) {
                    return $fieldset->withFields('eprefs:compta_global_ndf_enable|3', 'eprefs:compta_global_ndf_target|6', 'aide_compta_global_ndf|3')
                        ->withFields('eprefs:archivage|6', 'eprefs:archive_probante|6');
                });
            });
        })->addTab("Utilisateurs", function (FormLayoutTab $tab) {
            $tab->addColumn(12, function (FormLayoutColumn $column) {
                $column->withSingleField('users', function (FormLayoutColumn $fieldset) {
                    return $fieldset->withFields('user_id|6', 'role_id|6');
                });
            });
        });
    }

    private function cleanup($tab)
    {
        $t = array();
        foreach ($tab as $key => $val) {
            if ($val['user_id'] == null || $val['role_id'] == null) {
                // Log::debug("cleanup : on a un champ null ...");
            } else {
                $t[] = $val;
            }
        }
        // Log::debug("cleanup : on retourne " . serialize($t));
        return $t;
    }

    //cherche dans tab2 si on a role/entreprise/userid de tab1
    private function relationsDiff($tab1, $tab2)
    {
        $aReturn = array();

        //Pour se simplifier la vie on passe par un tableau de texte du genre 2;3;4
        $t2 = array();
        for ($i = 0; $i < count($tab2); $i++) {
            $t2[$i] = implode(';', $tab2[$i]);
        }

        for ($i = 0; $i < count($tab1); $i++) {
            $str = implode(';', $tab1[$i]);
            if (in_array($str, $t2)) {
            } else {
                $aReturn[] = $tab1[$i];
            }
        }

        return $aReturn;
    }
}
