<?php
/*
 * dashUtilisateur.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use DB;
use Code16\Sharp\Dashboard\SharpDashboard;
use Code16\Sharp\Dashboard\DashboardQueryParams;
use Code16\Sharp\Dashboard\Widgets\SharpLineGraphWidget;
use Code16\Sharp\Dashboard\Widgets\SharpBarGraphWidget;
use Code16\Sharp\Dashboard\Widgets\SharpPanelWidget;

use Code16\Sharp\Dashboard\Widgets\SharpGraphWidgetDataSet;
use Code16\Sharp\Dashboard\Widgets\SharpPieGraphWidget;
use Code16\Sharp\Dashboard\Layout\DashboardLayoutRow;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class dashUtilisateur extends SharpDashboard
{
    /**
     * Build dashboard's widget using ->addWidget.
     */
    protected function buildWidgets()
    {
        Log::debug("************buildWidgets*******");
        $this->addWidget(
            SharpPanelWidget::make("welcome")
                ->setInlineTemplate("<h1>Bienvenue !</h1>
                <div style='text-align: left'>
                    <p>&nbsp;</p>
                    <p>Votre profil est 'utilisateur', vous pourrez voir l'évolution de vos notes de frais sur ce tableau de bord.</p>
                </div>")

        )->addWidget(
            SharpBarGraphWidget::make("notesDeFrais")
                ->setTitle("Montant des remboursements")
        )->addWidget(
            SharpLineGraphWidget::make("facturettes")
                ->setTitle("Nombre de justificatifs (facturettes)")
        );
    }

    /**
     * Build dashboard's widgets layout.
     */
    protected function buildWidgetsLayout()
    {
        Log::debug("************buildWidgetsLayout*******");
        $this
            ->addRow(function (DashboardLayoutRow $row) {
                $row->addWidget(12, "welcome");
            })->addRow(function (DashboardLayoutRow $row) {
                $row->addWidget(6, "notesDeFrais")
                    ->addWidget(6, "facturettes");
            });
    }

    /**
     * Build dashboard's widgets data, using ->addGraphDataSet and ->setPanelData
     *
     * @param DashboardQueryParams $params
     */
    protected function buildWidgetsData(DashboardQueryParams $params)
    {
        Log::debug("**************buildWidgetsData*****");
        $this->setPanelData(
            "welcome",
            ["count" => 10]
        );

        // Log::debug("**************buildWidgetsData*****");
        //SQLite ne sait pas faire des group by comme on le souhaite (ou je ne sais pas le faire)
        //Alors j'ai bricolé un truc qui génère des données (mais pas les bonnes) pour pouvoir avancer sachant que la
        //prod est sur un MariaDB :)
        if (env('DB_CONNECTION') === 'sqlite') {
            $queryNDF = Auth::user()->NdeFrais()->select(DB::raw("strftime('%Y-%m',created_at) as label, montant as value"));
            $queryLDF = Auth::user()->LdeFrais()->select(DB::raw("strftime('%Y-%m',created_at) as label, count(*) as value"));
            $queryNDF->groupBy(DB::raw('label'));
            $queryLDF->groupBy(DB::raw('label'));
        } else {
            $queryNDF = Auth::user()->NdeFrais()->select(DB::raw("DATE_FORMAT(created_at,'%Y-%m') as label, montant as value"))->groupBy('montant');
            $queryLDF = Auth::user()->LdeFrais()->select(DB::raw("DATE_FORMAT(created_at,'%Y-%m') as label, count(*) as value"));

            $queryNDF->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m')"));
            $queryLDF->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m')"));
        }

        $dataNDF = $queryNDF
            ->orderBy("debut")
            ->get()
            ->pluck("value", "label");

        $dataLDF = $queryLDF
            ->orderBy("ladate")
            ->get()
            ->pluck("value", "label");

        $this->addGraphDataSet(
            "notesDeFrais",
            SharpGraphWidgetDataSet::make($dataNDF)
                ->setLabel("")
                ->setColor("green")
        );

        $this->addGraphDataSet(
            "facturettes",
            SharpGraphWidgetDataSet::make($dataLDF)
                ->setLabel("")
                ->setColor("grey")
        );
    }
}
