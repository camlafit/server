<?php
/*
 * NdeFraisPolicy.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp\Policies;

use App\User;
use App\NdeFrais;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Permission;

class NdeFraisPolicy
{

    public function entity()
    {
        if (sharp_user()->hasPermissionTo('show others NdeFrais')) {
        // if (sharp_user()->hasRole('serviceComptabilite')) {
            Log::debug("NdeFraisPolicy::entity perms 'show others NdeFrais' : true");
            return true;
        }
        return sharp_user()->hasPermissionTo('show NdeFrais');
        // return sharp_user()->hasGroup('admin');
    }

    public function view(User $u, $ndf)
    {
        Log::debug("NdeFraisPolicy::view (sharp)...");
        if (sharp_user()->hasPermissionTo('show others NdeFrais')) {
            Log::debug("NdeFraisPolicy::view perm 'show others NdeFrais'");
            $users_possibles = User::getMyUsers()->pluck('id')->toArray();
            Log::debug("  NdeFraisPolicy::view On regarde donc si la note de frais ref $ndf appartient a un des utilisateurs suivants " . \json_encode($users_possibles));

            $ndfu = NdeFrais::findOrFail($ndf);
            if ($ndfu) {
                if (in_array($ndfu->user_id, $users_possibles)) {
                    Log::debug("  NdeFraisPolicy::view OK");
                    return true;
                }
            }
            Log::debug("  NdeFraisPolicy::view PAS OK");
            return false;
        }


        // return sharp_user()->owner_id == $user->id;
        return sharp_user()->hasPermissionTo('show NdeFrais');
    }

    public function update()
    {
        //si on autorise l'update on aurait un bouton "Modifier" dans SharpAdmin ...
        return false;
        // return sharp_user()->hasPermissionTo('edit NdeFrais');
    }

    public function delete()
    {
        return sharp_user()->hasPermissionTo('delete NdeFrais');
    }

    public function create()
    {
        //si on autorise l'update on aurait un bouton "Nouveau" dans SharpAdmin ...
        return false;
        //return sharp_user()->hasPermissionTo('create NdeFrais');
    }
}
