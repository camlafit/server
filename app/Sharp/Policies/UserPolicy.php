<?php
/*
 * UserPolicy.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp\Policies;

use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Entreprise;
use Illuminate\Support\Facades\Log;

class UserPolicy
{

    public function entity()
    {
        return sharp_user()->hasPermissionTo('show User');
        // return sharp_user()->hasGroup('admin');
    }

    public function view(User $u, $umodif)
    {
        //Super Admin peut tout faire :)
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        }
        // return sharp_user()->owner_id == $user->id;
        // return sharp_user()->hasPermissionTo('show User');

        Log::debug("L'utilisateur " . $u->id . " veut pouvoir regarder l'utilisateur " . $umodif);
        if (sharp_user()->hasPermissionTo('show User')) {
            //On cherche pour voir si un lien de "subordination" entre $u et $umodif existe
            $users_possibles = User::getMyUsers()->pluck('id')->toArray();
            Log::debug("On regarde donc si $umodif est dans " . \json_encode($users_possibles));

            if (in_array($umodif, $users_possibles)) {
                return true;
            }
        }
        return false;
    }

    public function update(User $u, $umodif)
    {
        //Super Admin peut tout faire :)
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        }
        //Qui a le droit de modifier un utilisateur ? surtout pas un utilisateur lambda
        if (sharp_user()->hasRole('utilisateur')) {
            return false;
        }

        Log::debug("L'utilisateur " . $u->id . " veut modifier l'utilisateur " . $umodif);
        if (sharp_user()->hasPermissionTo('edit User')) {
            Log::debug("  il a la permission 'edit User' ...");

            //On cherche pour voir si un lien de "subordination" entre $u et $umodif existe
            $users_possibles = User::getMyUsers()->pluck('id')->toArray();
            Log::debug("On regarde donc si $umodif est dans " . \json_encode($users_possibles));

            if (in_array($umodif, $users_possibles)) {
                Log::debug("  autorisé");
                return true;
            }
        }
        Log::debug("  refusé");
        return false;
        // //ensuite il faut dire quels utilisateurs on a le droit de modifier ...
        // return sharp_user()->hasPermissionTo('edit User');
    }

    public function delete()
    {
        //Super Admin peut tout faire :)
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        }
        //TODO GRAVE
        //Attention pour les responsables entreprise on ne peut supprimer que SES utilisateurs
        if (sharp_user()->hasRole('responsableEntreprise')) {
            return sharp_user()->hasPermissionTo('delete User');
        }
        return false;
    }

    public function create()
    {
        return sharp_user()->hasPermissionTo('create User');
    }
}
