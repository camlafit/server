<?php
/*
 * MailSendNDF.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\NdeFrais;
use App\Entreprise;
use App\Http\Controllers\NdeFraisController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class MailSendNDF extends Mailable
{
    use Queueable, SerializesModels;

    private $_ndfC;

    /**
     * Create a new message instance.
     * $raison = send, closed, freezed
     * @return void
     */
    public function __construct(NdeFraisController $ndfC, $raison = "send")
    {
        Log::debug("=============== MailSendNDF::construct : " . $raison);
        $this->_ndfC = $ndfC;
        $this->_raison = $raison;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        Log::debug("=============== MailSendNDF::build");
        $u = $this->_ndfC->getUser();
        $userName = $u->firstname . " " . $u->name;
        $societe = $u->getEntreprise();
        if ($societe) {
            $socname = $societe->name;
        }
        else {
            $socname = "Société inconnue";
        }
        $mois  = $this->_ndfC->getMonth();
        $annee = $this->_ndfC->getYear();
        $userEmail = $u->email;

        $fics = $this->_ndfC->getExportedFilesNames();
        Log::debug("  Liste des fichiers à joindre au mail : " . implode(',',$fics));

        $attachments = array();

        //Quand on freeze une ndf on n'envoie que le PDF
        $filter = "";
        if ($this->_raison == "freezed") {
            $filter = ".pdf";
        }

        foreach ($fics as $fic) {
            if (file_exists($fic) && is_file($fic)) {
                if ($filter != "") {
                    if (Str::endsWith(Str::lower($fic), $filter)) {
                        Log::debug("=============== MailSendNDF::attach 1 : " . $fic);
                        $f['uri']   = $this->_ndfC->getDownloadURI($fic);
                        $f['label'] = basename($fic);
                        $attachments[] = $f;
                    }
                } else {
                    Log::debug("=============== MailSendNDF::attach 2 : " . $fic);
                    $f['uri']   = $this->_ndfC->getDownloadURI($fic);
                    $f['label'] = basename($fic);
                    $attachments[] = $f;
                }
            }
        }


        $mail = $this->subject("[" . config('app.name') . "] Note de Frais - $socname - $userName - $mois/$annee")
            ->view('emails.ndf.' . $this->_raison, [
                'currentURI' => config('app.url'),
                'userName'   => $userName,
                'userEmail'  => $userEmail,
                'attachFiles' => $attachments
            ])
            ->text('emails.ndf.' . $this->_raison . "_text", [
                'currentURI' => config('app.url'),
                'userName'   => $userName,
                'userEmail'  => $userEmail,
                'attachFiles' => $attachments
            ]);

        Log::debug("=============== MailSendNDF::filtre : " . $filter);

        //Ajout des pièces jointes
        // modification mai 2020: les pièces jointes sont trop grosses, on donne un deeplink de téléchargement
        /*
        foreach ($fics as $fic) {
            if (file_exists($fic) && is_file($fic)) {
                if ($filter != "") {
                    if (Str::endsWith(Str::lower($fic), $filter)) {
                        Log::debug("=============== MailSendNDF::attach 1 : " . $fic);
                        $mail->attach($fic, [
                            'as' => $this->makeNiceName($fic),
                        ]);
                    }
                } else {
                    Log::debug("=============== MailSendNDF::attach 2 : " . $fic);
                    $mail->attach($fic, [
                        'as' => $this->makeNiceName($fic),
                    ]);
                }
            }
        }
        */
        return $mail;
    }

    private function makeNiceName($f)
    {
        $fic = basename($f);
        return $fic;
    }
}
