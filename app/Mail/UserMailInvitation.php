<?php
/*
 * UserMailInvitation.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Mail;

use Illuminate\Support\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class UserMailInvitation extends Mailable
{
    use Queueable, SerializesModels;

    private $_user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->_user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $datedujour = Carbon::today()->setTimezone('Europe/Paris')->isoFormat('ll');

        $token = app('auth.password.broker')->createToken($this->_user);

        DB::table(config('auth.passwords.users.table'))->insert([
            'email' => $this->_user->email,
            'token' => $token
        ]);

        $resetUrl = url(config('app.url') . route('password.reset', $token, false));

        return $this->subject("[" . config('app.name') . "] Invitation : votre assistant note de frais ...")
            ->view('emails.account.new', [
                'currentURI' => url()->current(),
                'name' => $this->_user->firstname . " " . $this->_user->name,
                'email' => $this->_user->email,
                'role' => Role::where("id", $this->_user->main_role)->get()->pluck('name')->first(),
                'passwd' => $resetUrl,
            ])
            ->text('emails.account.new_text', [
                'currentURI' => url()->current(),
                'name' => $this->_user->firstname . " " . $this->_user->name,
                'email' => $this->_user->email,
                'role' => Role::where("id", $this->_user->main_role)->get()->pluck('name')->first(),
                'passwd' => $resetUrl,
            ]);

        // ->attach(
        //     "storage/NdeFrais/demo@cap-rel.fr/20190831-doliscan-note_de_frais-export.pdf",
        //     array(
        //         'as' => '20190831-doliscan-note_de_frais-export.pdf',
        //         'mime' => 'application/pdf'
        //     )
        // );
    }
}
