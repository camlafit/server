<?php
/*
 * MailCloseNDF.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use App\NdeFrais;
use Illuminate\Support\Str;


class MailCloseNDF extends Mailable
{
    use Queueable, SerializesModels;

    private $_ndfC;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(NdeFrais $ndfC)
    {
        //
        Log::debug("=============== MailCloseNDF::construct");
        $this->_ndfC = $ndfC;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        Log::debug("=============== MailCloseNDF::build");
        $fics = $this->_ndfC->getExportedFilesNames();

        $attachments = array();

        //Quand on freeze une ndf on n'envoie que le PDF
        foreach ($fics as $fic) {
            if (file_exists($fic) && is_file($fic)) {
                Log::debug("=============== MailCloseNDF::attach : " . $fic);
                $f['uri']   = $this->_ndfC->getDownloadURI($fic);
                $f['label'] = basename($fic);
                $attachments[] = $f;
            }
        }
        return $this->subject("[" . config('app.name') . "] Fermeture de la note de frais du mois ...")
            ->view('emails.ndf.closed', [
                'currentURI' => config('app.url'),
                'attachFiles' => $attachments
            ])
            ->text('emails.ndf.closed_text', [
                'currentURI' => config('app.url'),
                'attachFiles' => $attachments
            ]);
    }
}
