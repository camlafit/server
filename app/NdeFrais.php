<?php
/*
 * NdeFrais.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Sharp\Commands\NdeFraisSendMail;
use Illuminate\Support\Carbon;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class NdeFrais extends Model
{
    use SoftDeletes;

    use LogsActivity;
    protected static $logName = 'NdeFrais';
    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = ['updated_at'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    protected $fillable = ['label', 'debut', 'fin', 'montant', 'status', 'user_id'];
    protected $dates = ['created_at', 'deleted_at'];
    // protected $hidden = ['id'];
    // protected $guarded = ['id'];
    protected $appends = ['sid'];

    //Les différents etats possibles des notes de frais (liste à améliorer en fonction des situations à gérer)
    const STATUS_CLOSED = 0;
    const STATUS_OPEN = 1;
    const STATUS_FREEZED = 2;

    private $_reference = "";

    //Public pour simplifier les accesseurs
    public $_filePDFFullPath = "";
    //Nom du fichier de synthèse
    public $_filePDFName = "";
    //Nom du fichier qui concatène toutes les PJ justificatifs perso
    public $_filePDFFullPathJustificatifs = "";
    public $_filePDFNameJustificatifs = "";


    public function getSIdAttribute()
    {
        return $this->getRouteKey();
    }

    //
    public function user()
    {
        return $this->hasOne(User::class);
    }

    //
    public function ldeFrais()
    {
        return $this->hasMany(LdeFrais::class)->orderBy('ladate');
    }

    public function makeNew($userid)
    {
        Log::debug('makeNew ...');
        $u = User::findOrFail($userid);
        if ($u->canHaveNDF()) {
            //Evite d'en créer une nouvelle si on a une note en cours
            $checkDup = $this->where('status', NdeFrais::STATUS_OPEN)->where('user_id', $userid)->first();
            if ($checkDup) {
                echo "- previous ndf is not closed !\n";
                Log::debug('Duplicate ...');
                return $checkDup->id;
            } else {
                Log::debug("Création d'une NDF...");
                //La date en francais svp
                setlocale(LC_ALL, "fr_FR.UTF-8");
                setlocale(LC_TIME, "fr_FR.UTF-8");
                $m = date('n');
                $this->create([
                    'label'   => ucfirst(strftime("%B")) . " " . date('Y'),
                    //first & last day of current month
                    'debut'   => date('Y-m-d', mktime(1, 1, 1, $m, 1, date('Y'))),
                    'fin'     => date('Y-m-d', mktime(1, 1, 1, $m + 1, 0, date('Y'))),
                    'status' => NdeFrais::STATUS_OPEN,
                    'montant' => 0,
                    'user_id' => $userid
                ]);
                Log::debug("Création NDF ok...");
            }
        } else {
            Log::debug("Création refusée, ce compte utilisateur n'a pas le droit d'avoir des NDF...");
        }
    }

    public function close()
    {
        $u = User::findOrFail($this->user_id);
        if ($u->canHaveNDF()) {
            //Ferme la note de frais en cours, affecte les totaux et permet ensuite la generation du PDF
            //et son export
            //Calcul du grand total de la note de frais
            $n = new NdeFraisSendMail();
            $data['raison'] = "closed";
            $n->execute($this->id, $data);
            $this->update([
                'status' => NdeFrais::STATUS_CLOSED,
                'montant' => $this->ldeFrais()->sum('ttc')
            ]);
        } else {
            Log::debug("Close impossible, ce compte utilisateur n'a pas le droit d'avoir des NDF...");
        }
    }

    public function freeze()
    {
        $u = User::findOrFail($this->user_id);
        if ($u->canHaveNDF()) {
            //Freeze la note de frais en cours: permet au responsable de modifier mais toute nouvelle saisie
            //sera faite sur la note de frais suivante
            $n = new NdeFraisSendMail();
            $data['raison'] = "freezed";
            $n->execute($this->id, $data);
            $this->update([
                'status' => NdeFrais::STATUS_FREEZED,
                'montant' => $this->ldeFrais()->sum('ttc')
            ]);
        } else {
            Log::debug("Close impossible, ce compte utilisateur n'a pas le droit d'avoir des NDF...");
        }
    }

    public function send()
    {
        $u = User::findOrFail($this->user_id);
        if ($u->canHaveNDF()) {
            $n = new NdeFraisSendMail();
            $data['raison'] = "send";
            $n->execute($this->id, $data);
        } else {
            Log::debug("Close impossible, ce compte utilisateur n'a pas le droit d'avoir des NDF...");
        }
    }

    //Référence de la note de frais
    public function getReference()
    {
        if ($this->_reference == "") {
            $u = User::where('id', $this->user_id)->first();
            $debutDuMois = Carbon::parse($this->fin)->startOfMonth();
            $this->_reference = "NDF-" . $u->initials(2) . $debutDuMois->format("Ymd");
        }
        return $this->_reference;
    }

    //Pour ameliorer le champ "description" des logs
    public function getDescriptionForEvent(string $eventName): string
    {
        $m = "";
        if (null !== Auth::user()) {
            $m = "by " . Auth::user()->email;
        }
        return "{$eventName} $m";
    }
}
