<?php
/*
 * PassClient.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Passport;

use Laravel\Passport\Token;
use Laravel\Passport\Client as Client;
use Illuminate\Support\Facades\Log;

class PassClient extends Client
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'oauth_clients';

    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The guarded attributes on the model.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'grant_types' => 'array',
        'personal_access_client' => 'bool',
        'password_client' => 'bool',
        'revoked' => 'bool',
    ];

    /**
     * The temporary plain-text client secret.
     *
     * @var string|null
     */
    protected $plainSecret;

    /**
     * Determine if the client should skip the authorization prompt.
     *
     * @return bool
     */
    public function skipsAuthorization()
    {
        return config('passport.skips_authorization');
    }

    static function getMyClients($orderBy = 'name')
    {
        //On recupere la liste des "Clients" (Applications) de cet utilisateur qui n'ont pas déjà de token associé
        $c = PassClient::where('user_id', \sharp_user()->id)
            ->whereNotIn('id', function ($query) {
                $query->select('client_id')
                    ->from('oauth_access_tokens')
                    ->where('user_id', \sharp_user()->id);
            })
            ->orderBy($orderBy);
        $r = $c->get();
        Log::debug("  getMyClients return : " . json_encode($r));
        return $r;
    }
}
