<?php
/*
 * LdeFrais.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

use App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use DB;
use App\TypeFrais;
use App\MoyenPaiement;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Events\EventsLdeFrais;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class LdeFrais extends Model
{
    use SoftDeletes;

    use LogsActivity;
    protected static $logName = 'LdeFrais';
    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = ['updated_at'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    // liste des champs modifiables automatiquement
    protected $fillable = [
        'label', 'fileName', 'fileCheck', 'ladate', 'ht', 'ttc', 'tvaTx1', 'tvaTx2', 'tvaTx3', 'tvaTx4', 'tvaVal1',
        'tvaVal2', 'tvaVal3', 'tvaVal4', 'depart', 'arrivee', 'distance', 'invites', 'vehicule', 'vehiculecv',
        'user_id', 'type_frais_id', 'moyen_paiement_id', 'nde_frais_id'
    ];
    //    protected $hidden = ['id'];
    // protected $guarded = ['id'];
    protected $appends = ['sid'];
    protected $dates = ['created_at', 'deleted_at'];

    public function delete()
    {
        //On fait un delete industriel ...mais il faut aussi supprimer le fichier image et ça laravel ne le fait pas tout seul
        // Log::debug("LdeFrais delete, supprime le fichier " . $this->getFullFileName());
        if ($this->getFullFileName()) {
            unlink($this->getFullFileName());
        }

        // Log::debug("LdeFrais delete on passe au parent");
        parent::delete();
    }

    public function getSIdAttribute()
    {
        return $this->getRouteKey();
    }

    //Retourne une petite icone de type fonte awesome en fonction du type de frais
    public function getAwesomeIconeForCategorie($id)
    {
        // Log::debug("getAwesomeIconeForCategorie: $id");
        $r = "";
        switch ($id) {
            case 1:
                $r = "fas fa-utensils fa-cutlery";
                break;
            case 2:
                $r = "fas fa-parking fa-tachometer";
                break;
            case 3:
                $r = "fas fa-bed";
                break;
            case 4:
                $r = "fas fa-plane";
                break;
            case 5:
                $r = "fas fa-taxi";
                break;
            case 6:
                $r = "fas fa-gas-pump";
                break;
            case 7:
                $r = "fas fa-gift";
                break;
            case 8:
                $r = "fas fa-car-side fa-car";
                break;
        }
        // Log::debug("getAwesomeIconeForCategorie return: $r");
        return $r;
        //return "<i class=\"fas $r\"></i> &nbsp; ";
    }

    //Retourne une petite icone de type fonte awesome en fonction du texte
    public function getAwesomeIconeFor($string)
    {
        $s = strtolower(Str::ascii($string));
        // Log::debug("getAwesomeIconeFor: $s");
        $r = "";

        //Idée d'amélioration
        //Algo pour trouver la meilleure icone : on commence par les noms les plus longs puis on termine par les plus courts
        //Comme ça "ter" ne risque pas de s'appliquer pour "sandwich intermarché" par exemple

        // ================= Restauration =================
        if (Str::contains($s, ['bistrot', 'restaurant', 'resto', 'brasserie'])) {
            $r = "fas fa-cutlery fa-utensils";
        } else if (Str::contains($s, ['sandwich', 'buger', 'mcdonald', 'mc donald', 'macdo'])) {
            $r = "fas fa-cutlery fa-hamburger";
        } else if (Str::contains($s, ['café', 'cafe', 'collation'])) {
            $r = "fas fa-coffee";
        } else if (Str::contains($s, ['cocktail'])) {
            $r = "fas fa-cocktail";
        }

        // ================= Transport =================
        else if (Str::contains($s, ['tgv', 'train', 'ter'])) {
            $r = "fas fa-train";
        } else if (Str::contains($s, ['metro', 'rer', 'métro'])) {
            $r = "fas fa-subway";
        } else if (Str::contains($s, ['bus', 'autobus'])) {
            $r = "fas fa-bus";
        } else if (Str::contains($s, ['blablacar', 'covoit'])) {
            $r = "fas fa-shuttle-van";
        } else if (Str::contains($s, ['vtc', 'taxi'])) {
            $r = "fas fa-taxi";
        } else if (Str::contains($s, ['parking'])) {
            $r = "fas fa-parking fa-tachometer";
        } else if (Str::contains($s, ['peage', 'péage'])) {
            $r = "fas fa-road";
        } else if (Str::contains($s, ['essence', 'carburant'])) {
            $r = "fas fa-gas-pump";
        } else if (Str::contains($s, ['voiture', ''])) {
            $r = "fas fa-car-side";
        }
        //ajouter les autres noms locaux ...
        else if (Str::contains($s, ['velo', 'vélo', 'v3', 'biclou', 'velib'])) {
            $r = "fas fa-biking";
        }
        // ================= Livraison / poste etc. =================
        else if (Str::contains($s, ['dhl'])) {
            $r = "fab fa-dhl";
        } else if (Str::contains($s, ['UPS'])) {
            $r = "fab fa-ups";
        } else if (Str::contains($s, ['courrier', 'timbre', 'la poste', 'affranchissement'])) {
            $r = "fas fa-envelope";
        }
        // ================= Cadeaux / Divers =================
        else if (Str::contains($s, ['fleur'])) {
            $r = "fas fa-leaf";
        }
        // ================= Hébergement =================
        else if (Str::contains($s, ['airbnb', 'bnb'])) {
            $r = "fab fa-airbnb fa-bed";
        }
        // ================= A Trier =================
        else if (Str::contains($s, ['amazon'])) {
            $r = "fab fa-amazon";
        }
        // ================= Informatique =================
        else if (Str::contains($s, ['android'])) {
            $r = "fab fa-android";
        } else if (Str::contains($s, ['apple', 'iphone', 'ipad'])) {
            $r = "fab fa-apple";
        } else if (Str::contains($s, ['dropbox'])) {
            $r = "fab fa-dropbox";
        } else if (Str::contains($s, ['casque', 'écouteurs'])) {
            $r = "fas fa-headphones";
        } else if (Str::contains($s, ['mobile', 'téléphone', 'telephone'])) {
            $r = "fas fa-mobile";
        }

        // ================= Journaux / Livres =================
        else if (Str::contains($s, ['livre', 'librairie'])) {
            $r = "fas fa-book";
        } else if (Str::contains($s, ['journal', 'revue'])) {
            $r = "fas fa-book-open";
        } else if (Str::contains($s, ['ovh', 'online', 'scaleway'])) {
            $r = "fas fa-server";
        } else if (Str::contains($s, ['google'])) {
            $r = "fas fa-google";
        } else if (Str::contains($s, ['photo'])) {
            $r = "fas fa-camera";
        } else if (Str::contains($s, ['bricolage'])) {
            $r = "fas fa-hammer";
        } else if (Str::contains($s, ['clé', 'clef'])) {
            $r = "fas fa-key";
        } else if (Str::contains($s, ['the', 'thé', 'tisane'])) {
            $r = "fas fa-mug-hot";
        } else if (Str::contains($s, ['journal', 'magazine', 'revue', 'quotidien'])) {
            $r = "fas fa-newspaper";
        } else if (Str::contains($s, ['fournitures', 'administratives'])) {
            $r = "fas fa-paperclip";
        }
        //TODO matériel médical pour infirmière par exemple
        else if (Str::contains($s, ['stethoscope', ''])) {
            $r = "fas fa-stethoscope";
        } else if (Str::contains($s, ['hydroalcool', 'pharma'])) {
            $r = "fas fa-plus-square";
        } else if (Str::contains($s, ['bricolage'])) {
            $r = "fas fa-wrench";
        }

        //Régule d'ik
        else if (Str::contains($s, ['régule', 'regule'])) {
            $r = "fas fa-eur";
        }
        // Log::debug("getAwesomeIconeFor return: $r");
        return $r;
        //return "<i class=\"fas $r \"></i> &nbsp; ";
    }


    // Retourne un résumé de la ligne de note de frais pour un affichage simple
    public function getResume()
    {
        // Log::debug("LdeFrais getResume: $this");

        //Ajout juin 2019 : on tente l'ajout d'une petite icone en fonction de la dépense
        $icone = $this->getAwesomeIconeFor($this->label);
        //Si on a pas d'icone specifique alors on fallback sur l'icone de la catégorie
        if ($icone == "") {
            $icone = $this->getAwesomeIconeForCategorie($this->type_frais_id);
            // Log::debug("getAwesomeIconeForCategorie: $icone");
        }

        //On ajoute toujours le titre de la dépense
        $retour = "<i class=\"$icone\"></i> ";

        //Dans le cas des IK on trafique un peu le titre
        if (!is_null($this->depart)) {
            $retour .= " $this->depart -> $this->arrivee ($this->distance km) ";
        }

        $retour .=  $this->label;

        //Dans le cas du carburant on trafique un peu le titre aussi -> plus besoin ça se fait dans la partie "compta"
        // if ($this->getTypeFraisSlug() == "carburant") {
        //     $retour .= " <i>(";
        //     if ($this->extractVehiculeIsUtilitaire()) {
        //         $retour .= "VU, ";
        //     } else {
        //         $retour .= "VP, ";
        //     }
        //     $retour .= $this->extractVehiculeNom() . ", " . $this->extractVehiculeCarburant() . ")</i>";
        //     // $retour .= substr($this->vehicule, 0, strpos($this->vehicule, ";"));
        // }

        if (!is_null($this->invites)) {
            $retour .= " - " . $this->invites;
        }
        return $retour;
    }

    public function getTypeFrais()
    {
        $r = TypeFrais::findOrFail($this->type_frais_id);
        return $r->label;
    }

    public function getTypeFraisSlug()
    {
        $r = TypeFrais::findOrFail($this->type_frais_id);
        return $r->slug;
    }

    public function getLabelSlug()
    {
        return $this->label()->slug;
    }


    public function getMoyenPaiement()
    {
        $r = MoyenPaiement::findOrFail($this->moyen_paiement_id);
        return $r->label;
    }


    // Retourne la liste des vehicules utilisés par userid sur la période (année)
    public function getVehiculesForUser($userid, $year)
    {
        return $this->where('user_id', $userid)->whereYear('ladate', $year)->whereNotNull('vehicule')->distinct()->pluck('vehicule');
    }

    // Retourne le nombre de KM effecturé cette année jusqu'à dateFin sur ce véhicule, cette ndf incluse
    public function getKMforVehiculeThisYear($userid, $vehicule, $year, $dateFin, $ndfid)
    {
        //Attention, si la date la plus ancienne n'est pas janvier on ajoute alors le KM fait "cette année avant l'utilisation de DS"
        // $res = $this->where('user_id', $userid)->whereYear('ladate', $year)->where('vehicule',$vehicule)->get(['ladate','distance']);
        // Log::debug('=================== getKMforVehiculeThisYear');
        // DB::enableQueryLog();
        //where('nde_frais_id', '<=', $ndfid)->
        $res = $this->where('user_id', $userid)->where('ladate', '<=', $dateFin)->whereYear('ladate', $year)->where('vehicule', $vehicule)->sum('distance');
        //Si l'utilisateur a créé son compte dans le courant de l'année on a peut-être des KM "initiaux" à ajouter au total
        $dateCrea = User::where('id', $userid)->whereDate('created_at', '>', $year . '-01-01')->get();
        if (!$dateCrea->isEmpty()) {
            $km = $this->extractVehiculeKM($vehicule);
            // Log::debug("Attention, compte utilisateur créé après le 1er janvier de l'année en cours ... on ajoute les KM initiaux : " . $km);
            $res += $km;
        }

        // Log::debug("YEAR: Total distance pour $vehicule sur la période $year : $res");
        // Log::debug(DB::getQueryLog());
        // Log::debug('=================== end getKMforVehiculeThisYear');
        return $res;
    }

    // Retourne le nombre de KM effecturé ce mois-ci (cette ndf) sur ce véhicuke
    public function getKMforVehiculeThisMonth($userid, $vehicule, $ndfid)
    {
        // Log::debug('=================== getKMforVehiculeThisMonth');
        $res = $this->where('user_id', $userid)->where('nde_frais_id', $ndfid)->where('vehicule', $vehicule)->sum('distance');
        // Log::debug("MONTH: Total distance pour $vehicule sur la note de frais ref $ndfid : $res");
        return $res;
        // Log::debug('=================== end getKMforVehiculeThisMonth');
    }


    //Extrait la partie "nom" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450"
    public function extractVehiculeNom($v = "")
    {
        if ($v == "") {
            $v = $this->vehicule;
        }
        $tab = explode(';', $v);
        //Au pire si pour une raison inconnue on a pas l'info
        $res = "inconnu";
        if (isset($tab[0]) && $tab[0] != "") {
            $res = $tab[0];
        }
        return $res;
    }

    //Extrait la partie "km fait avant utilisation de DS" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450"
    public function extractVehiculeKM($v = "")
    {
        if ($v == "") {
            $v = $this->vehicule;
        }
        $tab = explode(';', $v);
        $res = 0;
        if (isset($tab[4]) && is_int($tab[4])) {
            $res = $tab[4] + 0;
        }
        return $res;
    }

    //Extrait la partie "cv" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450"
    public function extractVehiculeCV($v = "")
    {
        if ($v == "") {
            $v = $this->vehicule;
        }
        $tab = explode(';', $v);
        //Au pire si on a pas l'info on configure à 4cv
        $res = 4;
        if (isset($tab[2]) && $tab[2] != "") {
            $res = Str::replaceFirst('cv', '', $tab[2]);
        }
        return $res;
    }

    //Extrait la partie "vu" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450"
    public function extractVehiculeIsUtilitaire($v = "")
    {
        if ($v == "") {
            $v = $this->vehicule;
        }
        $tab = explode(';', $v);
        //Au pire on dit que c'est un VP si on a pas l'info
        $res = false;
        if (isset($tab[3]) && $tab[3] == "vu") {
            $res = true;
        }
        //Dans tous les autres cas on est en véhicule de tourisme (moto, cyclo etc.)
        return $res;
    }

    //Extrait la partie "vu" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450"
    //valeurs possibles: vu, vp, n1 (pickup ...), moto, cyclo
    public function extractVehiculeType($v = "")
    {
        if ($v == "") {
            $v = $this->vehicule;
        }
        //Au pire si pour une raison inconnue on a pas l'info
        $res = "vp";
        $tab = explode(';', $v);
        if (isset($tab[3]) && $tab[3] != "") {
            $res = $tab[3];
        }
        return $res;
    }

    //A partir du type retourne auto/moto/cyclo
    public function extractVehiculeAMC($v = "")
    {
        $type = $this->extractVehiculeType($v);
        if ($type == "moto") {
            return "moto";
        } else if ($type == "cyclo") {
            return "cyclo";
        }
        return "auto";
    }


    //Extrait la partie "diesel" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450"
    public function extractVehiculeCarburant($v = "")
    {
        if ($v == "") {
            $v = $this->vehicule;
        }
        $tab = explode(';', $v);
        //Au pire on dit que c'est de l'essence
        $res = "essence";
        if (isset($tab[1]) && $tab[1] != "") {
            $res = $tab[1];
        }
        return $res;
    }

    // Retourne vrai si ce regroupement a de la TVA
    public function hasTVA($noteid, $typefraisid, $ispro)
    {
        $t = $this->where('nde_frais_id', $noteid)->where('type_frais_id', $typefraisid)->get();
        // Log::debug($t);

        return $t;
    }

    //Génère un nom de fichier à partir des données de base
    public function generateFileName($force = false)
    {
        // Log::debug("generateFileName");

        if ($this->fileName != "" && !$force) {
            return $this->fileName;
        }
        $ladate          = \DateTime::createFromFormat('Y-m-d', $this->ladate);
        // var destFilename =  "_"
        // + today.getHours().toString() + today.getMinutes().toString() + "-" + subject + "-" + basename(fileName);

        $fic = $ladate->format('Ymd') . '-' . Str::limit(Str::slug($this->getTypeFrais()), 10) . "-" . Str::limit(Str::slug($this->label), 20) . '-' . Str::random(10) . '.jpeg';
        $this->fileName = $fic;
        return $fic;
    }

    //Retourne le nom complet du répertoire dans lequel est stocké le fichier
    public function getFullDirName()
    {
        // Log::debug("=============== getFullDirName");
        $user            = $this->user()->get('email')->first();
        $ladate          = \DateTime::createFromFormat('Y-m-d', $this->ladate);
        $ladateSub       = $ladate->format('Ym');
        $directory       = storage_path() . "/LdeFrais/" . $user->email . "/" . $ladateSub . "/";

        if (file_exists($directory) && is_dir($directory)) {
            return $directory;
        } else {
            return "";
        }
    }

    //Retourne le nom complet du fichier
    public function getFullFileName()
    {
        // Log::debug("=============== getFullFileName");

        $fullFileName    = $this->getFullDirName() . $this->fileName;
        if (file_exists($fullFileName) && is_file($fullFileName)) {
            // echo " [x] $fullFileName\n";
            return $fullFileName;
        } else {
            // echo " [ ] $fullFileName\n";
            return "";
        }
    }

    //Retourne l'URI publique de l'image pour eviter les pb
    ///ldfImages/demo@cap-rel.fr/20191009-restauration-louis-2K4hoz5pdy.jpeg
    public function getImageURI($format = "")
    {
        Log::debug("LdeFrais::getImageURI  format: $format ...");

        $user = $this->user()->get('email')->first();
        $uri = "/ldfImages/" . $user->email . "/" . $this->fileName;

        if ($this->getFullFileName() == "") {
            Log::debug("  Le fichier " . \storage_path() . $uri . " n'existe pas on bascule sur le doc par defaut...");
            $uri = "/images/document_indisponible.jpeg";
        }

        if ($format == "html") {
            $html = "<img src=\"$uri\" style=\"width: 100%;\">";
            Log::debug("  -> return html $html");
            return $html;
        }
        Log::debug("  -> return uri $uri");
        return $uri;
    }

    /**
     * nbf: formate le nombre et le retourne comme une chaine
     *
     * @param  mixed $nb
     * @return string
     */
    public function nbf($nb)
    {
        return (string) \number_format($nb, 2, ".", "");
    }



    /**
     * makePDF: Création d'un PDF à partir de l'image correspondant à la ligne de frais en cours
     *          Idée: on ajoute sur le PDF toutes les infos intéressantes pour faciliter l'OCR
     *          ou le transfert de données à venir via d'autres outils
     * @param  mixed $image
     * @param  mixed $destination
     * @return int : -1 si erreur, 1 si la generation est ok et 2 si le fichier existe déjà
     */
    public function makePDF($image, $destination, $forceUpdate = 0)
    {
        Log::debug("LdeFrais::makePDF pour $image vers $destination");

        //Si le PDF existe déjà on ne le refait pas, sauf si $forceUpdate = 1
        if (\file_exists($destination) && $forceUpdate == 0) {
            return 2;
        }

        $label = ($this->label) ? $this->label : "";
        $label .= ($this->invites) ? ": " . $this->invites : "";
        $label .= ($this->vehicule) ? ", véhicule : " . $this->vehicule : "";

        //TODO ? Gestion des cas particuliers ? exemple du carburant où on ne demande que le TTC
        $montant = "<table>";
        if ($this->getTypeFraisSlug() == 'carburant') {
            if ($this->ttc > 0) {
                $total = $this->ttc;
            } else {
                $total = $this->ht;
            }
            $montant .= "<tr><td>Total</td><td align=\"right\">" . $this->nbf($total) . "€</td></tr>";
        } else {
            //domPDF est un peu limité donc on passe par un bon vieux tableau ...
            if ($this->ht != $this->ttc) {
                $montant .= ($this->ht) ? "<tr><td>Total HT</td><td align=\"right\">" . $this->nbf($this->ht) . "€</td></tr>" : "";
                $montant .= ($this->tvaVal1) ? "<tr><td>Montant TVA " . $this->nbf($this->tvaTx1) . "%</td><td align=\"right\">" . $this->nbf($this->tvaVal1) . "€</td></tr>" : "";
                $montant .= ($this->tvaVal2) ? "<tr><td>Montant TVA " . $this->nbf($this->tvaTx2) . "%</td><td align=\"right\">" . $this->nbf($this->tvaVal2) . "€</td></tr>" : "";
                $montant .= ($this->tvaVal3) ? "<tr><td>Montant TVA " . $this->nbf($this->tvaTx3) . "%</td><td align=\"right\">" . $this->nbf($this->tvaVal3) . "€</td></tr>" : "";
                $montant .= ($this->tvaVal4) ? "<tr><td>Montant TVA " . $this->nbf($this->tvaTx4) . "%</td><td align=\"right\">" . $this->nbf($this->tvaVal4) . "€</td></tr>" : "";
            }
            $montant .= ($this->ttc) ? "<tr><td>Total TTC</td><td align=\"right\">" . $this->nbf($this->ttc) . "€</td></tr>" : "";
        }
        $montant .= "</table>";

        $u = User::findOrFail($this->user_id);

        $pdf = App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $pdf->loadView('ldefraistopdf', [
            'ldf'           => $this,
            'image'         => $image,
            'ladate'        => $this->ladate,
            'label'         => $label,
            'montant'       => $montant,
            'username'      => $u->firstname . " " . $u->name,
        ]);

        if ($label != "") {
            $pdf->getDomPdf()->add_info('Subject', $label);
            $pdf->getDomPdf()->add_info('Producer', config('app.name') . " " . config('app.version'));
            $pdf->getDomPdf()->add_info('Creator', config('app.name') . " " . config('app.version'));
        }

        $pdf->save($destination);

        $r = 0;
        if (\file_exists($destination)) {
            Log::debug("LdeFrais::makePDF fichier $destination OK, on lance l'event");
            event(new EventsLdeFrais($this, "PDFAvailable"));
            $r = 1;
        } else {
            Log::debug("LdeFrais::makePDF ERREUR: le fichier $destination n'existe pas");
            $r = -1;
        }
        return $r;
    }

    public function signAndArchivePDF($fileName)
    {
        // Log::debug("=============== signAndArchivePDF");
        $user = $this->user()->get('email')->first();

        //Le hic c'est que Sharp est propre et conforme au plan de stockage Laravel ... il faut donc transformer le filename
        //en un blabla acceptable ...
        $p = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
        $reste = str_replace($p, "", $fileName);
        //On devrait donc avoir
        $test = Storage::disk('local')->exists($reste);
        // Log::debug("On passe comme lien de download : " . $reste);
        // Log::debug("_________________________________________________________________________________________");

        //On a le PDF il faut maintenant faire une demande auprès d'archive-probante de signature ...
        $client = new \GuzzleHttp\Client();

        $res = $client->request('POST', 'https://sign.archive-probante.fr/api/upload/', [
            'multipart' => [
                [
                    'name'     => 'user',
                    'contents' => $user->email,
                ],
                [
                    'name'     => 'pass',
                    'contents' => 'xxxxxx',
                ],
                [
                    'name'     => 'label',
                    'contents' => $this->label,
                ],
                [
                    'name'     => 'ladate',
                    'contents' => $this->ladate,
                ],
                [
                    'name'     => 'filename',
                    'contents' => \basename($fileName),
                ],
                [
                    'name'     => 'document',
                    'contents' => fopen($fileName, 'r')
                ]
            ]
        ]);

        // Log::debug($res->getStatusCode());
        // Log::debug($res->getBody());
        $name = "";
        if ($res->getStatusCode() == 200) {
            $url = $res->getBody();
            $contents = file_get_contents($url);
            $name = substr($url, strrpos($url, '/') + 1);
            echo "on télécharge $name ...\n";
            \File::put($fileName, $contents);


            if (\file_exists($fileName)) {
                Log::debug("LdeFrais::signAndArchivePDF fichier $fileName OK, on lance l'event");
                event(new EventsLdeFrais($this, "SignedPDFAvailable"));
            } else {
                Log::debug("LdeFrais::signAndArchivePDF ERREUR: le fichier $fileName n'existe pas");
            }
        }
        return;
    }
    //
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    //
    public function ndeFrais()
    {
        return $this->belongsTo('App\NdeFrais');
    }

    public function typeFrais()
    {
        return $this->belongsTo('App\TypeFrais');
    }

    public function moyenPaiement()
    {
        return $this->belongsTo('App\MoyenPaiement');
    }

    //Exporte la majeure partie de l'objet en texte lisible ... pratique pour envoyer une notification
    public function toText()
    {
        $txt = "";
        foreach ($this->fillable as $f) {
            $txt .= " - $f : " . $this->$f . "\n";
        }
        return $txt;
    }

    //Pour ameliorer le champ "description" des logs
    public function getDescriptionForEvent(string $eventName): string
    {
        $m = "";
        if (null !== Auth::user()) {
            $m = "by " . Auth::user()->email;
        }
        return "{$eventName} $m";
    }
}
