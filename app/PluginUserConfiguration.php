<?php
/*
 * PluginUserConfiguration.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Propaganistas\LaravelFakeId\RoutesWithFakeIds;
use Spatie\Activitylog\Traits\LogsActivity;
use App\User;
use App\Plugin;
use Illuminate\Support\Facades\Log;

class PluginUserConfiguration extends Model
{
    use RoutesWithFakeIds;

    use LogsActivity;
    protected static $logName = 'PluginUserConfiguration';
    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = ['updated_at'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    protected $fillable = ['user_id', 'plugin_id', 'config_value'];
    protected $dates = ['created_at', 'deleted_at'];
    // protected $hidden = ['id'];
    // protected $guarded = ['id'];
    public $confJson; //Tout le contenu de la configuration est laissé au plugin, nous on a uniquement un json

    public function users()
    {
        return $this->belongsTo('App\User');
    }

    public function plugins()
    {
        return $this->belongsTo('App\Plugin');
    }

    // public function save(array $options = array())
    // {
    //     Log::debug("PluginUserConfiguration: On sauvegarde...");
    //     Log::debug($this->confJson);
    //     Log::debug("PluginUserConfiguration: On sauvegarde...end");
    //     //$this->saveOrFail();
    // }
}
