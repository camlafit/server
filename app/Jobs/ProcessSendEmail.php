<?php
/*
 * ProcessSendEmail.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;
use Swift_Encoding;

class ProcessSendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $details;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        //
        $this->details = $details;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //TODO
        $retour = 0;

        if (isset($this->details['objectMail'])) {
            $content = $this->details['objectMail'];
            Mail::to($this->details['to'])
                ->send($content, function ($message) {
                    $message->getSwiftMessage()->setEncoder(Swift_Encoding::get8BitEncoding());
                });
        } else {
            //Attention si c'est une chaine (string) il faut en faire un Illuminate\Contracts\Mail\Mailable
            Mail::send([], [], function ($message) {
                $message->to($this->details['to'])
                    ->subject($this->details['subject'])
                    ->setBody($this->details['message']);
                if (isset($this->details['cc'])) {
                    $message->cc($this->details['cc']);
                }
                if (isset($this->details['bcc'])) {
                    $message->bcc($this->details['bcc']);
                }
            });
        }
        return $retour;
    }
}
