<?php
/*
 * AuthServiceProvider.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Providers;

// use App\NdeFrais;
// use App\Policies\NdeFraisPolicy;
use App\User;
use App\Passport\PassToken;
use App\Passport\PassClient;
use App\Passport\AuthCode;
use Illuminate\Support\Str;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use App\Passport\PersonalAccessClient;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Lcobucci\JWT\Parser;
use Illuminate\Support\Facades\Auth;

class AuthServiceProvider extends ServiceProvider
{

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //Pas necessaire car on respecte le plan de nommage
        // NdeFrais::class => NdeFraisPolicy::class,
        // 'App\NdeFrais' => 'App\Policies\NdeFraisPolicy',
        // 'App\Model' => 'App\Policies\ModelPolicy',
        'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        Log::debug("=================== AuthServiceProvider::boot");
        Log::debug(\json_encode(request()->input()));

        if (request()->input('api_token')) {
            Log::debug("  AuthServiceProvider::api_token");

            $u = User::where('api_token', request()->input('api_token'))->first();
            Log::debug('   user: ' . \json_encode($u));
            // return $u;
            Auth::guard('api')->setUser($u);
            return;
        } else {
            //Durant la transition passport
            if (isset($_SERVER['HTTP_AUTHORIZATION'])) {
                $headerRAW = $_SERVER['HTTP_AUTHORIZATION'];
                Log::debug("  AuthServiceProvider::header_raw : $headerRAW");
                if (Str::startsWith($headerRAW, 'Bearer ')) {
                    $tokenID = Str::substr($headerRAW, 7);
                    $u = User::where('api_token', $tokenID)->first();
                    if ($u) {
                        Auth::guard('api')->setUser($u);
                        return;
                    }
                }
            }
        }
        // $this->app['auth']->viaRequest('api', function ($request) {
        //     if ($request->input('api_token')) {
        //         Log::debug("  AuthServiceProvider::api_token");
        //         return User::where('api_token', $request->input('api_token'))->first();
        //     } else {
        //         //Durant la transition passport
        //         if (isset($_SERVER['HTTP_AUTHORIZATION'])) {
        //             $headerRAW = $_SERVER['HTTP_AUTHORIZATION'];
        //             Log::debug("  AuthServiceProvider::header_raw");
        //             if (Str::startsWith($headerRAW, 'Bearer ')) {
        //                 $tokenID = Str::substr($headerRAW, 7);
        //                 return User::where('api_token', $tokenID)->first();
        //             }
        //         }
        //     }
        // });
        // Auth::viaRequest('api', function ($request) {
        Log::debug("=================== AuthServiceProvider::API on bascule sur passport");

        if (isset($_SERVER['HTTP_AUTHORIZATION'])) {
            $headerRAW = $_SERVER['HTTP_AUTHORIZATION'];
            Log::debug("  AuthServiceProvider::header_raw");
            if (Str::startsWith($headerRAW, 'Bearer ')) {
                $cle = Str::substr($headerRAW, 7);
                $tokenID = (new Parser())->parse($cle)->getClaim('jti');
                Log::debug('   AuthServiceProvider::tokenID: ' . $tokenID);
                //On essaye de voir dans passport
                $token = PassToken::findOrFail($tokenID);
                Log::debug('   AuthServiceProvider::token: ' . \json_encode($token));
                $u = User::findOrFail($token->user_id);
                Log::debug('   AuthServiceProvider::user: ' . \json_encode($u));
                // return $u;
                Auth::guard('api')->setUser($u);
                return;
            }
        }
        // });


        Log::debug("=================== AuthServiceProvider::général on bascule sur passport");
        $this->registerPolicies();

        Passport::routes();
        Passport::useTokenModel(PassToken::class);
        Passport::useClientModel(PassClient::class);
        Passport::useAuthCodeModel(AuthCode::class);
        Passport::usePersonalAccessClientModel(PersonalAccessClient::class);

        Passport::tokensExpireIn(now()->addDays(config('passport.token_access_lifetime'))); // Temps d'expiration des Token
        Passport::refreshTokensExpireIn(now()->addDays(config('passport.token_refresh_lifetime'))); // Temps d'expiration des tokens refresh
        Passport::personalAccessTokensExpireIn(now()->addDays(config('passport.token_personal_access_lifetime'))); // Temps d'expiration des token d'accès personnel


        /* ---- DEFINITION DES SCOPES (PORTEES, PERMISSIONS) APPLICABLES AUX TOKENS  ---- */
        /*$scopes = [ // Liste des différentes portées (scopes) de l'application
            'show-ldefrais' => 'show LdeFrais',
            'show-user' => 'show User',
            'edit-user' => 'edit User',
            ...
        ];
        $scopesDefaut = [  // Définition des portées par défaut applicable à un token si aucune portée n'est spécifiée
            'show-user' => 'show User',
            ...
        ];

        $scopes = [];
        $scopesDefaut = [];
        Passport::tokensCan($scopes);
        Passport::setDefaultScope($scopesDefaut);*/

        /* ---- FIN SCOPES ---- */

        /* ---- CLIENT D'ACCES PERSONNEL ---- */
        /*Passport::personalAccessClientId(
            config('passport.personal_access_client.id')
        );
        Passport::personalAccessClientSecret(
            config('passport.personal_access_client.secret')
        );*/
        /* ---- FIN ACCES PERSONNEL ---- */
    }
}
