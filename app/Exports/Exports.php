<?php
/*
 * Exports.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Exports;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use App\User;


class Exports
{
    protected $_user;
    protected $_initials;
    protected $_filename;
    protected $_filenameNDF; //Le fichier PDF de note de frais
    protected $_directory;
    protected $_endOfMonth;
    protected $_content;
    protected $_nbLines; //Nombre de lignes d'écritures comptables
    protected $_lastEcritNum; //Dernier numéro d'écriture utilisé
    protected $_totalDebit;
    protected $_totalCredit;

    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct($user, $filename, $directory, $endOfMonth, $filenameNDF)
    {
        setlocale(LC_ALL, "fr_FR.UTF-8");

        // Log::debug("Création d'un Export " . $filename . " et " . $endOfMonth);
        $this->_user         = $user;
        $this->_initials     = $user->initials(2);
        $this->_filename     = $filename;
        $this->_directory    = $directory;
        $this->_endOfMonth   = $endOfMonth;
        $this->_content      = "";
        $this->_filenameNDF  = $filenameNDF;
        $this->_nbLines      = 0;
        $this->_totalDebit   = 0;
        $this->_totalCredir  = 0;
        $this->_lastEcritNum = 0;
    }

    public function export($filename = "", $forceUpdate = false)
    {
        //TODO repasser en commentaire ... dev time
        $forceUpdate = true;

        //Si on passe un nom de fichier a l'appel de la fonction il est prioritaire
        if ($filename == "") {
            $filename = "$this->_directory/$this->_filename";
        }

        if (file_exists($filename) && !$forceUpdate) {
            // Log::debug("Le fichier FEC existe déjà on le passe tel-quel");
            return true;
        } else {
            // Log::debug("Ecriture du fichier...");
            $file = fopen($filename, 'w');
            fwrite($file, $this->_content);
            fclose($file);

            //TODO: chercher comment on pourrait faire
            //return $this->checkBalanced();
            return true;
        }
        return false;
    }

    public function str_sub_pad($str, $length)
    {
        //On evite les accents dans les fichiers comptables
        //attention le setlocale doit etre actif (voir __construct)
        $str = iconv("UTF-8", "ASCII//TRANSLIT", $str);
        return substr(str_pad($str, $length), 0, $length);
    }

    public function str_sub_pad_left($str, $length)
    {
        //On evite les accents dans les fichiers comptables
        //attention le setlocale doit etre actif (voir __construct)
        $str = iconv("UTF-8", "ASCII//TRANSLIT", $str);
        return substr(str_pad($str, $length, " ", STR_PAD_LEFT), 0, $length);
    }

    public function getFullFileName()
    {
        return $this->_directory . "/" . $this->_filename;
    }

    /**
     * checkBalanced : vérifie si le fichier est équilibré
     *
     * @return true/false
     */
    public function checkBalanced()
    {
        Log::debug("Exports::checkBalanced");
    }
}
