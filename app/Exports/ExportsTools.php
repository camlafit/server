<?php
/*
 * ExportsTools.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Exports;

use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Support\Str;
use App\User;

class ExportsTools
{
    // protected $_user;
    // protected $_initials;
    // protected $_filename;
    // protected $_filenameNDF; //Le fichier PDF de note de frais
    // protected $_directory;
    // protected $_endOfMonth;
    // protected $_content;

    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct()
    {
        setlocale(LC_ALL, "fr_FR.UTF-8");

        // Log::debug("Création d'un Export " . $filename . " et " . $endOfMonth);

        // $this->_user        = $user;
        // $this->_initials    = $user->initials(2);
        // $this->_filename    = $filename;
        // $this->_directory   = $directory;
        // $this->_endOfMonth  = $endOfMonth;
        // $this->_content     = "";
        // $this->_filenameNDF = $filenameNDF;
    }

    public function export($filename = "", $forceUpdate = false)
    {
        // //TODO repasser en commentaire ... dev time
        // $forceUpdate = true;

        // //Si on passe un nom de fichier a l'appel de la fonction il est prioritaire
        // if ($filename == "") {
        //     $filename = "$this->_directory/$this->_filename";
        // }

        // if (file_exists($filename) && !$forceUpdate) {
        //     // Log::debug("Le fichier FEC existe déjà on le passe tel-quel");
        //     return true;
        // } else {
        //     // Log::debug("Ecriture du fichier...");
        //     $file = fopen($filename, 'w');
        //     fwrite($file, $this->_content);
        //     fclose($file);
        //     return true;
        // }
        // return false;
    }

    public function str_sub_pad($str, $length)
    {
        //On evite les accents dans les fichiers comptables
        //attention le setlocale doit etre actif (voir __construct)
        $str = iconv("UTF-8", "ASCII//TRANSLIT", $str);
        return substr(str_pad($str, $length), 0, $length);
    }

    public function getFullFileName()
    {
        return $this->_directory . "/" . $this->_filename;
    }

    //Nettoyer le repertoire ... j'aime pas ça
    public function cleanupPath($rep)
    {
        if (is_dir($rep) && is_file("$rep/quadratus.TXT")) {
            unlink("$rep/quadratus.TXT");
        }
    }

    /**
     * copyFilesAndRenamePrefix
     *
     * @param  mixed $srcDir
     * @param  mixed $dstDir
     * @param  mixed $srcTxt
     * @param  mixed $dstTxt
     * @return void
     */
    public function copyFilesAndRenamePrefix($srcDir, $dstDir, $srcTxt, $dstTxt)
    {
        Log::debug("ExportsTools::copyFilesAndRenamePrefix $srcDir -> $dstDir | $srcTxt -> $dstTxt");
        $success = 0;
        if (!is_dir($dstDir)) {
            mkdir($dstDir, 0770, true);
        }

        // Ouvre un dossier bien connu, et liste tous les fichiers
        if (is_dir($srcDir)) {
            if ($dh = opendir($srcDir)) {
                while (($file = readdir($dh)) !== false) {
                    if (is_dir($file)) {
                    } else {
                        $ext = substr($file, strlen($file) - 3, 3);
                        $dstFile = $dstDir . "/" . Str::replaceFirst($srcTxt, $dstTxt, $file);;
                        $srcFile = $srcDir . "/" . $file;
                        //fichier PDF on copie en changeant de nom
                        if ($ext == "PDF") {
                            Log::debug("  ExportsTools::copyFilesAndRenamePrefix copie de $srcFile vers $dstFile");
                            $success = copy($srcFile, $dstFile);
                        }
                        //fichier TXT on copie en changeant de nom + modification du contenu
                        //on remplace "                       D00" par "                       D01" etc.
                        if ($ext == "TXT") {
                            $success = $this->copyFileAndSed($srcFile, $dstFile, "                       " . $srcTxt, "                       " . $dstTxt);
                        }
                        // copy($file, $dstfile);
                    }
                }
                closedir($dh);
            }
        }
        if ($success) {
            $cmd = "/usr/bin/zip quadratus.zip *.TXT *.PDF";
            Log::debug("  ExportsTools::copyFilesAndRenamePrefix on compresse : " . \json_encode($cmd) . " avec cwd " . $dstDir);
            // cf https://github.com/symfony/symfony/issues/36801
            $process = Process::fromShellCommandline($cmd, $dstDir);
            // $process = new Process($cmd, $dstDir);
            $process->setWorkingDirectory($dstDir);
            $process->run();
            if (!$process->isSuccessful()) {
                Log::debug("  ExportsTools::copyFilesAndRenamePrefix erreur : " . $process->getWorkingDirectory());
                Log::debug("  ExportsTools::copyFilesAndRenamePrefix erreur : " . $process->getErrorOutput());
                Log::debug("  ExportsTools::copyFilesAndRenamePrefix message : " . $process->getOutput());
            }
            if (\file_exists($dstDir."/quadratus.zip")) {
                return true;
            } else {
                Log::debug("  ExportsTools::copyFilesAndRenamePrefix erreur de creation du zip $dstDir/quadratus.zip");
                return false;
            }
        }
    }

    /**
     * copyFileAndSed : pas vraiment une copie, on APPEND au fichier de destination le contenu du fichier sourcce
     *
     * @param  mixed $src : fichier source
     * @param  mixed $dst : fichier de destination dans lequel on ajoute des lignes
     * @param  mixed $srcTxt : masque du texte à remplacer
     * @param  mixed $dstTxt : par quoi on remplace le masque
     * @return void
     */
    public function copyFileAndSed($src, $dst, $srcTxt, $dstTxt)
    {
        Log::debug("ExportsTools::copyFileAndSed '$src' -> '$dst' | '$srcTxt' -> '$dstTxt'");
        $success = 0;
        $lines = file($src);
        if ($fp = fopen($dst, "a")) {
            foreach ($lines as $line) {
                fwrite($fp, preg_replace("/$srcTxt/", $dstTxt, $line));
            }
            fclose($fp);
            $success = 1;
        }

        return $success;
    }
}
