<?php
/*
 * ExportFEC.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Exports;

use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Support\Str;

class ExportFEC extends Exports
{
    /* exporte le fichier FEC */
    public function export($filename = "", $forceUpdate = false)
    {
        Log::debug("ExportFEC : export $filename / $forceUpdate | Nb lines : " . $this->_nbLines);
        $this->exportDocumentation($filename);
        // Log::debug("exportFEC ...");

        // Il faut ajouter une ligne d'entête ... donc code a faire AVANT l'appel au code général du parent
        $f = "JournalCode	JournalLib	EcritureNum	EcritureDate	CompteNum	CompteLib	CompAuxNum	CompAuxLib	PieceRef	PieceDate	EcritureLib	Debit	Credit	EcritureLet	DateLet	ValidDate	Montantdevise	Idevise\n";
        $f = $this->_content;
        $this->_content = $f;

        $verif = ($this->_totalCredit - $this->_totalDebit)/100;
        Log::debug("  ExportFEC::export::verif " . $this->_totalCredit . " - " . $this->_totalDebit . " = $verif");
        if ($verif != 0) {
            Log::debug("  ExportFEC::export on corrige");
            if ($verif > 0) {
                $this->addMiniLine($this->_lastEcritNum + 1, 658000, "ECART ARRONDI", "", "", "ECART D'ARRONDI", abs($verif), 0, "");
            } else {
                $this->addMiniLine($this->_lastEcritNum + 1, 758000, "ECART ARRONDI", "", "", "ECART D'ARRONDI", 0, abs($verif), "");
            }
            $verif2 = $this->_totalCredit - $this->_totalDebit;
            Log::debug("  ExportFEC::export::verif2 $verif2");
        }

        parent::export($filename, $forceUpdate);
    }


    /* Une version minimale de l'ajout d'écritures FEC */
    public function addMiniLine($ecritureNum, $compteNum, $compteLib, $compAuxNum, $compAuxLib, $ecritureLib, $debit, $credit, $ladate)
    {
        $finDuMois = $this->_endOfMonth;
        if ($ladate == "") {
            $ladate = $finDuMois;
        }

        $this->addLine(
            "NDF",
            "NOTE DE FRAIS",
            $ecritureNum,
            $ladate->format("Ymd"),
            $compteNum,
            "NDF-" . $this->_initials . " " . $compteLib,
            $compAuxNum,
            $compAuxLib,
            $this->_initials . substr($finDuMois->format("Y"), -2) . $finDuMois->format("m"),
            $ladate->format("Ymd"),
            $ecritureLib,
            $debit,
            $credit,
            "",
            "",
            "",
            "",
            ""
        );
    }

    /*
    Ajoute une ligne FEC
    Caractère séparateur de zones : TABULATION
    1 : JournalCode : Code journal de l'écriture comptable : Alphanumérique
    2 : JournalLib : Le libellé journal de l’écriture comptable : Alphanumérique
    3 : EcritureNum : Numérotation propre à chaque journal ou numéro sur une séquence continue de l’écriture comptable : Alphanumérique
    4 : EcritureDate : La date de comptabilisation de l’écriture comptable : Date(AAAAMMJJ)
    5 : CompteNum : Le numéro de compte : Alphanumérique
    6 : CompteLib : Le libellé de compte : Alphanumérique
    7 : CompAuxNum : Le numéro de compte auxiliaire (à blanc si non utilisé) :Alphanumérique
    8 : CompAuxLib : Le libellé de compte auxiliaire (à blanc si non utilisé) : Alphanumérique
    9 : PieceRef : La référence de la pièce justificative : Alphanumérique
    10 : PieceDate : La date de la pièce justificative : Date(AAAAMMJJ)
    11 : EcritureLib : Le libellé de l’écriture comptable : Alphanumérique
    12 : Debit : Le montant au débit : Numérique
    13 : Credit : Le montant au crédit : Numérique
    14 : EcritureLet : Le lettrage de l’écriture comptable (à blanc si non utilisé) : Alphanumérique
    15 : DateLet : La date de lettrage (à blanc si non utilisé) : Date (AAAAMMJJ)
    16 : ValidDate : La date de validation de l’écriture comptable : Date (AAAAMMJJ)
    17 : Montantdevise : Le montant en devise (à blanc si non utilisé) : Numérique
    18 : Idevise : L’identifiant de la devise (à blanc si non utilisé) : Alphanumérique
     */
    private function addLine(
        $journalCode,
        $journalLib,
        $ecritureNum,
        $ecritureDate,
        $compteNum,
        $compteLib,
        $compAuxNum,
        $compAuxLib,
        $pieceRef,
        $pieceDate,
        $ecritureLib,
        $debit,
        $credit,
        $ecritureLet,
        $dateLet,
        $validDate,
        $montantdevise,
        $idevise
    ) {

        $deb = str_replace('.', ',', $debit);
        $cred = str_replace('.', ',', $credit);
        $this->_totalDebit  += round($debit*100,2);
        $this->_totalCredit += round($credit*100,2);

        $this->_content .= "$journalCode	$journalLib	$ecritureNum	$ecritureDate	$compteNum	" . strtoupper(Str::ascii($compteLib));
        $this->_content .= "	" . strtoupper(Str::limit(Str::ascii($compAuxNum), 6, '.')) . "	" . strtoupper(Str::ascii($compAuxLib));
        $this->_content .= "	$pieceRef	$pieceDate	" . strtoupper(Str::ascii($ecritureLib));
        $this->_content .= "	$deb	$cred	$ecritureLet	$dateLet	$validDate	$montantdevise	$idevise\n";
        $this->_nbLines++;
        $this->_lastEcritNum = $ecritureNum;
    }

    private function exportDocumentation($filename)
    {
        //Si on passe un nom de fichier a l'appel de la fonction il est prioritaire
        if ($filename == "") {
            $filename = "$this->_directory/$this->_filename";
        }

        $filename = str_replace(".TXT", "-DOCUMENTATION_FEC.TXT", $filename);
        $texte = "Format d'export FEC - DoliSCAN - \n\n";
        $texte .= "Encodage : UTF-8\n\n";
        $texte .= "Caractère séparateur de champ : TABULATION\n\n";
        $texte .= "Liste des champs utilisés dans le fichier:\n\n";
        $texte .= "1 : JournalCode : Code journal de l'écriture comptable : Alphanumérique\n";
        $texte .= "2 : JournalLib : Le libellé journal de l’écriture comptable : Alphanumérique\n";
        $texte .= "3 : EcritureNum : Le numéro sur une séquence continue de l’écriture comptable : Alphanumérique\n";
        $texte .= "4 : EcritureDate : La date de comptabilisation de l’écriture comptable : Date(AAAAMMJJ)\n";
        $texte .= "5 : CompteNum : Le numéro de compte : Alphanumérique\n";
        $texte .= "6 : CompteLib : Le libellé de compte : Alphanumérique\n";
        $texte .= "7 : CompAuxNum : Le numéro de compte auxiliaire (à blanc si non utilisé) :Alphanumérique\n";
        $texte .= "8 : CompAuxLib : Le libellé de compte auxiliaire (à blanc si non utilisé) : Alphanumérique\n";
        $texte .= "9 : PieceRef : La référence de la pièce justificative : Alphanumérique\n";
        $texte .= "10 : PieceDate : La date de la pièce justificative : Date(AAAAMMJJ)\n";
        $texte .= "11 : EcritureLib : Le libellé de l’écriture comptable : Alphanumérique\n";
        $texte .= "12 : Debit : Le montant au débit : Numérique\n";
        $texte .= "13 : Credit : Le montant au crédit : Numérique\n";
        $texte .= "14 : EcritureLet : Le lettrage de l’écriture comptable (à blanc si non utilisé) : Alphanumérique\n";
        $texte .= "15 : DateLet : La date de lettrage (à blanc si non utilisé) : Date (AAAAMMJJ)\n";
        $texte .= "16 : ValidDate : La date de validation de l’écriture comptable : Date (AAAAMMJJ)\n";
        $texte .= "17 : Montantdevise : Le montant en devise (à blanc si non utilisé) : Numérique\n";
        $texte .= "18 : IDevise : L’identifiant de la devise (à blanc si non utilisé) : Alphanumérique\n";
        $texte .= "\n";
        // $texte .= "Raison sociale : CAP-REL SAS\n";
        // $texte .= "Siren : \n";
        // $texte .= "\n";
        // $texte .= "Exercice du 01/01/2013 au 31/12/2013\n";
        $texte .= "\n";
        $texte .= "Export de notes de frais (NDF) visant a être intégré dans le logiciel comptable\n";
        $texte .= "\n";
        // $texte .= "Informations supplémentaires : BICRS - IS\n";
        // $texte .= "\n";
        // $texte .= "Table de correspondance des natures des opérations\n";
        // $texte .= "Code ; Intitulé\n";
        // $texte .= "A ; AVOIR\n";
        // $texte .= "B ; CARTE BANCAIRE\n";
        // $texte .= "C ; CHEQUE\n";
        // $texte .= "D ; EFFETS DOMICILES\n";
        // $texte .= "E ; ESPECE\n";
        // $texte .= "F ; FACTURE\n";
        // $texte .= "L ; LCR\n";
        // $texte .= "P ; PRELEVEMENT\n";
        // $texte .= "R ; REMISE\n";
        // $texte .= "T ; TELEREGLT\n";
        // $texte .= "V ; VIREMENT\n";
        $texte .= "\n";
        $texte .= "Ce fichier est donc ''au format FEC'' mais n'est pas un fichier FEC complet, il vise a être intégré\n";
        $texte .= "dans le logiciel comptable de l'entreprise et le respect du format FEC est uniquement là pour simplifier\n";
        $texte .= "l'interopérabilité générale...\n";
        $texte .= "\n";

        $file = fopen($filename, 'w');
        fwrite($file, $texte);
        fclose($file);
    }

}
