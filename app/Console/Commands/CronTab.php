<?php
/*
 * CronTab.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Console\Commands;

use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Artisan;

class CronTab extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doliscan:crontab';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Internal CronTab, just call this command each day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //TODO
        $retour = 0;

        //Tous les jours
        //1er du mois on génère les nouvelles NDF
        //5 jours avant la fin du mois on cherche toutes les LDF à zéro pour demander à l'OCR manuel de passer pour corriger
        //Dernier jour du mois on ferme les NDF
        //le 5° jours du mois on envoie tout au service RH/Compta : un mail avec les 2 pièces jointes ? ou une PJ et un fichier à télécharger sur archive-probante :-)
        $now = new Carbon();
        $dt  = new Carbon();

        echo "  **********************************************************************\n";
        echo "  *                                                                    *\n";
        echo "  *                             DoliScan                               *\n";
        echo "  *                                                                    *\n";
        echo "  *                                                                    *\n";
        echo "  *                                                                    *\n";
        echo "  *                                                                    *\n";
        echo "  **********************************************************************\n";

        if ($now->isSameDay($dt->startOfMonth())) {
        // if ($now->isSameDay($dt->startOfMonth()->addDay(2))) {
            Log::debug("Crontab: On est le 1er jour du mois");
            echo "On est le 1er jour du mois\n";
            //TODO: ne le lancer qu'une fois ...
            Artisan::call('doliscan:freezendf');
            Artisan::call('doliscan:createnewndf');
        }
        if ($now->isSameDay($dt->startOfMonth()->addDay(5))) {
            Log::debug("Crontab: On est le 6e jour du mois");
            echo "On est le 6° jour du mois\n";
            //On cloture les notes de frais
            Artisan::call('doliscan:closendf');
            //Et il faut envoyer les classeurs de frais si l'option est active
            Artisan::call('doliscan:sendCDF', ['--auto' => true]);
            // Artisan::call('doliscan:searchldftocomplete');
        }
        if ($now->isSameDay($dt->endOfMonth()->subDay(12))) {
            Log::debug("Crontab: On est 15 jours avant la fin du mois");
            echo "On est 15 jours avant la fin du mois\n";
            Artisan::call('doliscan:sendreminder', ['--etape' => '15', '--mail' => true]);
            // Artisan::call('doliscan:searchldftocomplete');
        }
        if ($now->isSameDay($dt->endOfMonth()->subDay(4))) {
            Log::debug("Crontab: On est 5 jours avant la fin du mois");
            echo "On est 5 jours avant la fin du mois\n";
            Artisan::call('doliscan:searchldftocomplete');
            Artisan::call('doliscan:sendreminder', ['--etape' => '25', '--mail' => true]);
        }
        if ($now->isSameDay($dt->endOfMonth())) {
            Log::debug("Crontab: On est le dernier jour du mois");
            echo "On est le dernier jour du mois\n";
            //Nom de code "31" valable y compris si on est le 28 février :-)
            Artisan::call('doliscan:sendreminder', ['--etape' => '31', '--mail' => true]);
        }
        echo "\n";

        return $retour;
    }
}
