<?php
/*
 * SendReminder.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Console\Commands;

use App\Http\Controllers\Controller;
use Illuminate\Console\Command;
use App\User;
use App\NdeFrais;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailReminder;
use Illuminate\Support\Facades\Log;

class SendReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doliscan:sendreminder
                            {--etape= : a quelle etape on se situe}
                            {--mail= : si on envoie un mail ou pas}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a reminder by mail';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //TODO
        $retour = 0;

        //
        $users = User::all();
        foreach ($users as $user) {
            Log::debug("Cron::SendReminder: SendReminder de " . $user->email);
            echo " SendReminder de " . $user->email . ": ";
            $ndf = $user->NdeFrais->sortBy('fin')->where('status', NdeFrais::STATUS_OPEN)->last();
            // Une NDF en cours -> un mail d'information
            if ($ndf) {
                Log::debug("Cron::SendReminder: expédition de " . $ndf->label);
                echo "expédition de " . $ndf->label . "\n";
                Mail::to($user->email)
                    ->bcc(env('MAIL_NOTIFICATIONS', 'suivi@doliscan.fr'))
                    ->send(new MailReminder($this->option('etape')));
                echo "sleep for mail server rate limit\n";
                sleep(env('MAIL_SLEEP_DELAY',30));
            }
        }
        return $retour;
    }
}
