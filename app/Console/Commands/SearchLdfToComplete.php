<?php
/*
 * SearchLdfToComplete.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\NdeFrais;
use App\LdeFrais;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailSearchLdfToComplete;
use Illuminate\Support\Facades\Log;

class SearchLdfToComplete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doliscan:searchldftocomplete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Search all LDF with ttc=0 and ask help from a human to correct it';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //TODO
        $retour = 0;

        //
        $ldeFrais       = new LdeFrais();
        $users = User::all();
        foreach ($users as $user) {
            // echo $user;
            Log::debug("Cron::SearchLdfToComplete: Analyse de la Note de Frais de " . $user->email);
            echo " Analyse de la Note de Frais de " . $user->email . ": ";
            $ndf  = $user->NdeFrais->sortBy('fin')->last();
            if ($ndf) {
                echo "NDF de " . $ndf->label . "\n";
                Log::debug("Cron::SearchLdfToComplete: NDF de " . $ndf->label);
                $ldfs = $ndf->ldeFrais->where('ttc', 0);
                if ($ldfs->count() > 0) {
                    Mail::to(env('MAIL_NOTIFICATIONS', 'suivi@doliscan.fr'))
                        ->send(new MailSearchLdfToComplete($ldfs));
                    echo "sleep for mail server rate limit\n";
                    sleep(env('MAIL_SLEEP_DELAY',30));
                }
                // foreach ($ldfs as $ldf) {
                //     //print_r($ldf);

                // }
            } else {
                echo "Pas de NDF !\n";
            }
        }
        return $retour;
    }
}
