<?php
/*
 * CloseNDF.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Console\Commands;

use App\Http\Controllers\Controller;
use Illuminate\Console\Command;
use App\User;
use App\NdeFrais;
use App\Http\Controllers\NdeFraisController;
use App\LdeFrais;
use Illuminate\Support\Facades\Log;

class CloseNDF extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doliscan:closendf';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Close all Freezed NDF for this month (month ended)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //TODO
        $retour = 0;
        //
        $users = User::all();
        foreach ($users as $user) {
            // echo $user;
            $ndf = $user->NdeFrais->sortBy('fin')->where('status', NdeFrais::STATUS_FREEZED)->last();
            if ($ndf) {
                Log::debug("======================================================================================================");
                Log::debug("=                                                                                                    =");
                Log::debug("=                                                                                                    =");
                Log::debug("=     CloseNDF::handle - Fermeture de la Note de Frais #" . $ndf->id . " de " . $user->email);
                Log::debug("=                                                                                                    =");
                echo "Fermeture de la Note de Frais #" . $ndf->id . " de " . $user->email . "\n";
                $ndf->close();
                sleep(env('MAIL_SLEEP_DELAY',30));

                // $n = new NdeFraisSendMail();
                // $n->execute($ndf->id);
                // $ndfC = new NdeFraisController();
                // $pdf = $ndfC->webBuildPDF($ndf->id, "/tmp/toto.pdf");
                // $pdf = $ndfC->webBuildPDFJustificatifs($ndf->id, "/tmp/toto-justificatifs.pdf");
            }
        }
        return $retour;
    }
}
