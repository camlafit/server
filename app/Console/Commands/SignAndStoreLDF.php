<?php
/*
 * SignAndStoreLDF.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Console\Commands;

use App\Http\Controllers\Controller;
use Illuminate\Console\Command;
use App\User;
use App\LdeFrais;
use Illuminate\Support\Carbon;
use App\Http\Controllers\NdeFraisController;
use Illuminate\Support\Facades\Log;

class SignAndStoreLDF extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doliscan:signandstoreldf';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envoie toute les facturettes en attente de signature vers archive-probante';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //TODO
        $retour = 0;
        //On limite aux 14 derniers jours quand meme ...
        //et fileCheck != car les waitingForUpload ...
        $ldfs = LdeFrais::where('fileName', '!=', '')->where('fileCheck', '!=', '')->whereDate('created_at', '>' , Carbon::now()->subdays(14))->get();
        foreach ($ldfs as $ldf) {
            // echo "On est sur " . $ldf->fileName . "\n";
            if ($img = $ldf->getFullFileName()) {
                $pdf = str_replace(["jpeg","jpg"], "pdf", $img);
                if (!file_exists($pdf)) {
                    echo "Création du PDF pour " . $ldf->fileName . "\n";
                    $ldf->makePDF($img, $pdf);
                    if (file_exists($pdf) && is_file($pdf)) {
                        echo "  signature du PDF\n";
                        $ldf->signAndArchivePDF($pdf);
                    } else {
                        echo "  Erreur de génération du fichier PDF $pdf\n";
                    }
                }
            }
        }
        return $retour;
    }
}
