<?php
/*
 * Entreprise.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

use DB;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Http\Controllers\NdeFraisController;
use App\Exports\ExportsTools;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class Entreprise extends Model
{
    use LogsActivity;
    use HasRoles;
    use SoftDeletes;

    protected static $logName = 'Entreprise';
    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = ['updated_at'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    protected $fillable = ['name', 'adresse', 'cp', 'ville', 'pays', 'email', 'web', 'tel', 'siret'];
    protected $dates = ['created_at', 'deleted_at'];
    protected $guarded = ['id'];

    public function eprefs()
    {
        return $this->hasOne(Epref::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('role_id');
    }

    // protected $hidden = [
    //     'password', 'remember_token',
    // ];

    public function delete()
    {
        // Le softDeletes laisse ce compte dans la base de données, on suffixe son mail pour eviter le refus de re-créer un compte plus tard
        // avec cette même adresse mail
        $suffixe = "-deleted-" . time();
        $this->email .= $suffixe;
        $this->siret .= $suffixe;
        $this->save();
        parent::delete();
    }

    //Recupere la listes des entreprises auxquelles j'ai accès
    static function getMyEntreprises($orderBy = 'name')
    {
        Log::debug(" getMyEntreprises pour " . json_encode(sharp_user()));
        if (sharp_user()->hasRole('superAdmin', 'web')) {
            // Log::debug("  getMyEntreprises : superAdmin");
            $e = Entreprise::orderBy($orderBy);
        } else if (sharp_user()->hasRole('adminRevendeur', 'web')) {
            // Log::debug("  getMyEntreprises : adminRevendeur");
            //On recupere toutes les entreprises crées par cet utilisateur
            //Et les entreprises où je suis adminRevendeur ...
            $e = Entreprise::join('entreprise_user', 'entreprises.id', '=', 'entreprise_user.entreprise_id')
                ->select('entreprise_id AS id', 'entreprises.*', 'entreprise_id', 'role_id', 'user_id')
                ->where('creator_id', '=', sharp_user()->id)
                ->orWhere(
                    function ($query) {
                        $roleid = Role::findByName('adminRevendeur', 'web')->id;
                        $query->where('user_id', sharp_user()->id)
                            ->where('role_id', $roleid);
                    }
                )
                ->groupBy('entreprise_user.entreprise_id');
        } else if (sharp_user()->hasRole('adminEntreprise', 'web')) {
            // Log::debug("  getMyEntreprises : adminEntreprise");
            $e = Entreprise::join('entreprise_user', 'entreprises.id', '=', 'entreprise_user.entreprise_id')
                ->select('entreprise_id AS id', 'entreprises.*', 'entreprise_id', 'role_id', 'user_id')
                ->where('creator_id', '=', sharp_user()->id)
                ->orWhere(
                    function ($query) {
                        $roleid = Role::findByName('adminEntreprise', 'web')->id;
                        $query->where('user_id', sharp_user()->id)
                            ->where('role_id', $roleid);
                    }
                )
                ->groupBy('entreprise_user.entreprise_id');

            // $roleid = Role::findByName('adminEntreprise','web')->id;
            // $e = User::find(sharp_user()->id)->entreprises()->where('role_id', $roleid);
        } else if (sharp_user()->hasRole('responsableEntreprise', 'web')) {
            // Log::debug("  getMyEntreprises : responsableEntreprise");

            $e = Entreprise::join('entreprise_user', 'entreprises.id', '=', 'entreprise_user.entreprise_id')
                ->select('entreprise_id AS id', 'entreprises.*', 'entreprise_id', 'role_id', 'user_id')
                ->where('creator_id', '=', sharp_user()->id)
                ->orWhere(
                    function ($query) {
                        $roleid = Role::findByName('responsableEntreprise', 'web')->id;
                        $query->where('user_id', sharp_user()->id)
                            ->where('role_id', $roleid);
                    }
                )
                ->groupBy('entreprise_user.entreprise_id');
            // $roleid = Role::findByName('responsableEntreprise','web')->id;
            // $e = User::find(sharp_user()->id)->entreprises()->where('role_id', $roleid);
        }
        //simple utilisateur il affiche son entreprise
        else if (sharp_user()->hasRole('utilisateur', 'web')) {
            // Log::debug("  getMyEntreprises : utilisateur");
            $e = Entreprise::join('entreprise_user', 'entreprises.id', '=', 'entreprise_user.entreprise_id')
                ->select('entreprise_id AS id', 'entreprises.*', 'entreprise_id', 'role_id', 'user_id')
                ->where(
                    function ($query) {
                        $roleid = Role::findByName('utilisateur', 'web')->id;
                        $query->where('user_id', sharp_user()->id)
                            ->where('role_id', $roleid);
                    }
                )
                ->groupBy('entreprise_user.entreprise_id');
        }
        // Log::debug("SQL : " . $e->toSql());

        if (!isset($e)) {
            $e = new Entreprise();
        }
        $r = $e->get();
        Log::debug("  getMyEntreprises return : " . json_encode($r));
        return $r;
    }

    //Pour ameliorer le champ "description" des logs
    public function getDescriptionForEvent(string $eventName): string
    {
        $m = "";
        if (null !== Auth::user()) {
            $m = "by " . Auth::user()->email;
        }
        return "{$eventName} $m";
    }
}
