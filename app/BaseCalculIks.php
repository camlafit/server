<?php
/*
 * BaseCalculIks.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

use Log;

class BaseCalculIks extends Model
{
    private $_ladate;
    private $_anneeBareme;

    public function __construct($ladate = null)
    {
        if (is_null($ladate)) {
            $this->_ladate = Carbon::now();
        } else {
            $this->_ladate = $ladate;
        }
    }

    //retourne l'année en cours ... mais si le barème n'est pas encore publiée alors retourne l'année précédente
    //Attention on peut être le 2 février et ne pas encore avoir les baremes de l'année en cours ... ou alors
    //faire un replay du jeu d'essai le 31/10/2019 et être sur la NDF de janvier "époque" ou le bareme de 2019
    //n'était pas encore connu !
    public function anneeBareme()
    {
        // Log::debug('=================== anneeBareme');
        if (!is_null($this->_anneeBareme)) {
            // Log::debug('valeur en cache : ' . $this->_anneeBareme);
            return $this->_anneeBareme;
        }
        // DB::enableQueryLog();
        //a priori bug sur passage mysql ->groupBy('annee')
        $test = $this->select('annee')->where('created_at', '<=', $this->_ladate->format('Y-m-d'))
            ->groupBy('annee')->orderBy('annee', 'desc')->first()->annee;
        // Log::debug('retour de la recherche sql: ' . $test);
        // Log::debug(DB::getQueryLog());
        // DB::disableQueryLog();
        // Log::debug('=================== anneeBareme end');
        $this->_anneeBareme = $test;
        return $this->_anneeBareme;
    }

    //retourne la date a laquelle le nouveau bareme a ete ajouté
    public function dateAnneeBaremeUpdated()
    {
        //l'année de la NDF en cours
        $annee = $this->_ladate->year;
        $testAnnee = $this->where('annee', '=', $annee)->latest()->first();
        // Log::debug($testAnnee);

        if ($testAnnee) {
            $res = $testAnnee->created_at;
        } else {
            $annee -= 1;
            $testAnnee = $this->where('annee', '=', $annee)->latest()->first();
            if ($testAnnee) {
                $res = $testAnnee->created_at;
            }
        }

        // Log::debug("================== : " . $test . "======================");
        return $res;
    }

    public function printBareme()
    {
        return " - Barème année " . $this->anneeBareme() . "";
    }

    //Calcule le montant des IK, on active ou pas l'option "eurobase" si on veut avoir le montant "annualisé" pour
    //faire une régule lors des changements de tranches ...
    public function calcul($cv, $km, $kmtotal, $applyEurBase = false, $anneeBareme = null, $amc = "auto")
    {
        //SELECT * FROM `base_calcul_iks` WHERE `cvmin` <= 3 AND `cvmax` >= 3
        // DB::enableQueryLog();
        //Si on demande le calcul selon un bareme special (par ex. quand on change de bareme en cours d'annee pour voir l'impact)
        if (!is_null($anneeBareme)) {
            //On verifie qu'on a ce bareme dans notre base ?
        } else {
            $anneeBareme = $this->anneeBareme();
        }
        $baseCalcul = $this->where('amc', $amc)->where('annee', $anneeBareme)->where('cvmin', '<=', $cv)->where('cvmax', '>=', $cv)->where('kmmin', '<=', $kmtotal)->where('kmmax', '>=', $kmtotal)->first();
        // Log::debug(" ---------------- Base de calcul : " . $baseCalcul);
        $total = 0;
        if ($baseCalcul)
            $total = $km * $baseCalcul->eurkm;
        if ($applyEurBase) {
            $total += $baseCalcul->eurbase;
        }
        // Log::debug('===================');
        // Log::debug("Résultat du calcul pour $cv : $km = $total");
        // Log::debug('===================');
        return round($total,2);
        // Log::debug(DB::getQueryLog());
        // Log::debug($t);
        // Log::debug('===================');

    }

    //Détail le calcul du montant des IK, on active ou pas l'option "eurobase" si on veut avoir le montant "annualisé" pour
    //faire une régule lors des changements de tranches ...
    public function printCalcul($cv, $km, $kmtotal, $applyEurBase = false, $anneeBareme = null, $amc = "auto")
    {
        if (!is_null($anneeBareme)) {
            //On verifie qu'on a ce bareme dans notre base ?
        } else {
            $anneeBareme = $this->anneeBareme();
        }
        $baseCalcul = $this->where('amc', $amc)->where('annee', '=', $anneeBareme)->where('cvmin', '<=', $cv)->where('cvmax', '>=', $cv)->where('kmmin', '<=', $kmtotal)->where('kmmax', '>=', $kmtotal)->first();
        $operation = "";
        if ($applyEurBase && ($baseCalcul->eurbase > 0)) {
            $operation = $baseCalcul->eurbase . " € + ";
        }
        $operation .= "($km km * " . $baseCalcul->eurkm . " €/km )";
        return $operation;
    }

    //Appel au webservice interne pour calculer la distance par la route entre deux villes
    public function distance($depart, $arrivee)
    {
        $client = new \GuzzleHttp\Client();

        try {
            $res = $client->request('GET', "https://geo.doliscan.fr/api/geo/$depart/$arrivee");

            //{"depart":"arcachon","departCP":"33120","arrivee":"bordeaux","arriveeCP":"33000-33100-33200-33300-33800","distanceM":65786.3,"distanceKM":65.79}
            Log::debug(" Code : " . $res->getStatusCode());
            Log::debug(" Contenu :" . $res->getBody());
            $name = "";
            if ($res->getStatusCode() == 200 && strlen($res->getBody())>10) {
                $j = json_decode($res->getBody());
                return $j->distanceKM;
            } else {
                return 0;
            }
        } catch (Exception $exception) {
            Log::debug("Erreur de calcul de distance");
        }
    }
}
