<?php
/*
 * TokenActif.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Middleware;

use App;
use Closure;
use App\Passport\PassToken;
use Lcobucci\JWT\Parser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\User;

class ActiveToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Log::debug(' ======== Middleware ActiveToken : handle ======== ');
        try {
            $tokenId = (new Parser())->parse($request->bearerToken())->getClaim('jti');
            $token = PassToken::find($tokenId);
            $token->update([
                'last_connexion_at' => now()
            ]);

            Log::debug(" Connexion via OAuth2, client : " . Auth::user()->email);

            // Log::debug('   token: ' . \json_encode($token));
            // if ($token) {
            //     $user = User::findOrFail($token->user_id);
            //     Log::debug('   user: ' . \json_encode($user));
            //     Auth::login($user);
            //     $token->update([
            //         'last_connexion_at' => now()
            //     ]);
            //     Log::debug(" Connexion via OAuth2, client : " . Auth::user()->email);
            // }
        } catch (\Throwable $th) {
            Log::debug(' Connexion par api_token (table users), client : ' . Auth::user()->email);
        }

        return $next($request);
    }
}
