<?php
/*
 * CleanNumbers.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\TransformsRequest as Middleware;
use Illuminate\Support\Facades\Log;


class CleanNumbers extends Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $used = 0;
        $cleanKeys = array('ht', 'ttc', 'tvaTx1', 'tvaTx2', 'tvaTx3', 'tvaTx4', 'tvaVal1', 'tvaVal2', 'tvaVal3', 'tvaVal4', 'distance');
        foreach ($request->all() as $key => $value) {
            if (in_array($key, $cleanKeys)) {
                if ($used == 0) {
                    //Pour eviter trop de logs qui ne servent à rien
                    Log::debug('========CleanNumbers handle===========');
                }
                $used++;
                $value = strval($this->floatvalue($value));
            }
        }
        return $next($request);
    }

    protected function floatvalue($val)
    {
        if ($val != "") {
            $v = $val;
            $val = str_replace(",", ".", $val);
            $val = preg_replace('/\.(?=.*\.)/', '', $val);
            Log::debug("  CleanNumbers floatvalue from $v converted to $val");
            return floatval($val);
        }
        return "";
    }
}
