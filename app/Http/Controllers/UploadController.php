<?php
/*
 * UploadController.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use Storage;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Handler\AbstractHandler;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use App\LdeFrais;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;

class UploadController extends Controller
{
    private $_ldf;

    /**
     * Handles the file upload
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws UploadMissingFileException
     * @throws \Pion\Laravel\ChunkUpload\Exceptions\UploadFailedException
     */
    public function upload(Request $request)
    {
        //Un chunck arrive
        Log::debug("UploadController: upload");
        // create the file receiver
        $receiver = new FileReceiver("file", $request, HandlerFactory::classFromRequest($request));

        // check if the upload is success, throw exception or return response you need
        if ($receiver->isUploaded() === false) {
            throw new UploadMissingFileException();
        }

        // receive the file
        $save = $receiver->receive();

        // check if the upload has finished (in chunk mode it will send smaller files)
        if ($save->isFinished()) {
            Log::debug(" upload finished");
            // save the file and return any response you need, current example uses `move` function. If you are
            // not using move, you need to manually delete the file by unlink($save->getFile()->getPathname())
            return $this->saveFile($save->getFile());
        }

        // we are in chunk mode, lets send the current progress
        /** @var AbstractHandler $handler */
        $handler = $save->handler();

        Log::debug(" upload en cours : " . $handler->getPercentageDone());
        return response()->json([
            "done" => $handler->getPercentageDone(),
            'status' => true
        ]);
    }

    /**
     * Saves the file
     *
     * @param UploadedFile $file
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function saveFile(UploadedFile $file)
    {
        Log::debug("UploadController: saveFile");

        $fileName = $this->createFilename($file);

        if ($this->_ldf) {
            Log::debug("  UploadController: ldf existe on raccroche les wagons...");
            Log::debug($this->_ldf);
            $finalPath = $this->_ldf->getFullDirName();
            if ($finalPath != "") {
                Log::debug("  UploadController: saveFile vers " . $finalPath . " pour " . $fileName);
                $file->move($finalPath, $fileName);
                $fullFilename = $finalPath . '/' . $fileName;
                $this->optimizeFile($finalPath, $fileName);
                $filecheck = sha1_file($fullFilename);
                $this->_ldf->fileCheck = $filecheck;
                $this->_ldf->save();

                //On génère un pdf ? -> si ce serveur n'a pas l'option signature & archive probante
                if (env('PDF_EXTERNAL_SIGN', '') == '') {
                    $pdf = str_replace(["jpeg","jpg"], "pdf", $fullFilename);
                    $this->_ldf->makePDF($fullFilename, $pdf);
                }
            } else {
                Log::debug("  UploadController: erreur, getFullDirName null !!!!");
            }
        } else {
            Log::debug("  UploadController: pas de ldf ...");
            //code usine
            // Group files by mime type
            $mime = str_replace('/', '-', $file->getMimeType());
            // Group files by the date (week
            $dateFolder = date("YmW");

            // Build the file path
            $filePath = "upload/{$mime}/{$dateFolder}/";
            $finalPath = storage_path("app/" . $filePath);

            // move the file name
            Log::debug("  UploadController: move $finalPath, $fileName ...");
            $file->move($finalPath, $fileName);

            return response()->json([
                'path' => $filePath,
                'name' => $fileName,
                'mime_type' => $mime
            ]);
        }
    }

    /**
     * Create unique filename for uploaded file
     * @param UploadedFile $file
     * @return string
     */
    protected function createFilename(UploadedFile $file)
    {
        //On cherche quelle LDF est liée à ce nom de fichier
        $ficUploadName = $file->getClientOriginalName();
        Log::debug("UploadController: createFilename pour $ficUploadName");
        if (Str::startsWith($ficUploadName, "waitForUpload-")) {
            //On a affaire à un fichier à relier à une LDF
            $this->_ldf = LdeFrais::where('fileName', '=', $ficUploadName)->first();

            if ($this->_ldf) {
                //On génère un bon nom de fichier
                $fic = $this->_ldf->generateFileName(true);
                $this->_ldf->fileName = $fic;
                $this->_ldf->save();
            } else {
                Log::debug("Erreur createFilename: la recherche de la ldf en cours n'a rien donné !!!");
            }
            Log::debug("UploadController: on attache à une LDF existante et on sauvegarde le fichier en " . $fic);
            Log::debug($this->_ldf);
            return $fic;
        } else {
            Log::debug("UploadController: code magique LDF absent on passe en mode normal");
            $extension = $file->getClientOriginalExtension();
            $filename = str_replace("." . $extension, "", $file->getClientOriginalName()); // Filename without extension

            // Add timestamp hash to name of the file
            $filename .= "_" . md5(time()) . "." . $extension;

            Log::debug("UploadController: createFilename : " . $filename);
            return $filename;
        }
    }

    public function checkChunk(Request $request)
    {
        Log::debug("UploadController: checkChunk for " . $request->resumableIdentifier);
        // The frontend library resumable.js calls this function before uploading every chunk to know if
        // it already exists. This is useful in case the upload was interrupted for some reason.

        // Get the path where the chunks are stored
        $path = storage_path('chunks/');

        // The last part of the chunks are formed by an identifier and the chunk number
        $fileName = '*' . $request->resumableIdentifier . '.' . $request->resumableChunkNumber . '.part';
        $fullName = $path . $fileName;
        Log::debug("  checkChunk for " . $fullName);

        // Search for a file that ends with the file name we defined
        $chunk = glob($fullName);

        if (count($chunk)) {
            // Let resumable.js know that the chunk exists
            Log::debug("  checkChunk ok");
            return response('ok', 200); // The chunk will not be re-uploaded
        } else {
            // Chunk not found
            Log::debug("  checkChunk err");
            return response('ko', 204); // The chunk will be uploaded
        }
    }


    //Optimise l'image car si on fait un appel direct ça ecrase le fichier pas encore sauvegardé
    private function optimizeFile($dir, $fic)
    {
        Log::debug("optimizeFile: $dir : $fic");
        $fullFicName = "$dir/$fic";
        if ($tfic = tempnam($dir, "optimize")) {
            Log::debug("  optimizeFile: $tfic");
            ImageOptimizer::optimize($fullFicName, $tfic);
            //On vérifie si la compression est ok
            if (filesize($tfic) < filesize($fullFicName)) {
                Log::debug("  optimizeFile: compression OK: " . filesize($tfic) . " avant " . filesize($fullFicName));
                if (\filetype($tfic) == \filetype($fullFicName)) {
                    Log::debug("  optimizeFile: rename $tfic en $fullFicName");
                    rename($tfic, $fullFicName);
                }
            }
        }
    }
}
