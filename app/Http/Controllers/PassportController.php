<?php
/*
 * PassportController.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App\Passport\PassToken;
use App\Passport\PassClient;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PassportController extends Controller
{
    /**
     * Accès à la page du choix du client
     *
     * @return \Illuminate\View\View
     */
    public function tokenForm()
    {
        Log::debug(" ============= TokenController : tokenForm ============== ");
        $clients = PassClient::whereUserId(Auth::id())->where('revoked', '!=', 1)->orderBy('name')->get(); // Liste des clients non révoqués de l'utilisateur
        session()->put('state', $state = Str::random(40));
        Log::debug(" Identification de la session : " . $state);

        $scopes = [];
        /* // Récupération de la liste des scopes disponibles
        $http = new \GuzzleHttp\Client;
        $response = $http->get(route('passport.scopes.index'), ['timeout' => 2]);
        $scopes = (array) json_decode($response->getBody());*/

        $nbTokens = count(PassToken::whereUserId(Auth::id())->get());
        $nbMax = config('passport.max_number_tokens');
        $authorized = $nbTokens < $nbMax;

        return view('passport.token.form', [
            'clients' => $clients,
            'state' => $state,
            'scopes' => $scopes,
            'authorized' => $authorized
        ]);
    }

    /**
     * Génération d'un code d'authentification temporaire obligatoire pour ensuite pouvoir créer un token
     *
     * @param Request $request
     * @return \Illuminate\Routing\Redirector
     */
    public function redirect(Request $request)
    {
        Log::debug(" ============= PassportController : redirect ============== ");

        $state = $request->session()->get('state');
        Log::debug(" Identification de la session : " . $state);

        $rules = [
            'client_id' => [
                'required', 'string', 'exists:oauth_clients,id',
                Rule::unique('oauth_access_tokens')->where(function ($query) { // On vérifie qu'il y a pas d'autres jetons actifs associés à cette application
                    return $query->where('revoked', '!=', 1);
                })
            ]
            // 'scopes' => 'nullable|array'
        ];

        $request = Validator::make($request->all(), $rules, [
            'client_id.unique' => "Cette appli possède déjà un jeton actif."
        ])->validate();

        $scopes = '';
        /* // Transformation de la liste (array) des scopes en chaine de caractères comme ceci => 'scope1 scope2 scope3 ...'
        $portees = $request['scopes'];
        foreach ($portees as $id) {
            $scopes .= $id . ' ';
        }*/

        $clientId = $request['client_id'];
        session()->put('client_id', $clientId); // On ajoute l'id de l'appli dans la session pour sauvegarder ce choix
        $query = http_build_query([
            'client_id' => $clientId,
            'redirect_uri' => route('passport.callback'),
            'response_type' => 'code',
            'scope' => $scopes,
            'state' => $state,
        ]);

        Log::debug(" Génération d'un code d'authentification par OAuth2. Id de l'appli : " . $clientId);
        return redirect('/oauth/authorize?' . $query);
    }

    /**
     * Validation de la demande de token avec saisie du code secret de l'aapli
     *
     * @param Request $request
     * @return \Illuminate\View\View|\Illuminate\Http\RedirectResponse|void
     */
    public function callbackForm(Request $request)
    {
        Log::debug(" ============= TokenController : callbackForm ============== ");

        // Si l'utilisateur annule la demande de token, on le renvoie à l'accueil
        if (isset($request['error']))
            return redirect()->route('webHome');

        $rules = [
            'state' => 'required|alpha_num',
            'code' => 'required|alpha_num'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
            abort(401);

        $request = $validator->validate();

        // On vérifie si c'est la bonne session.
        $state = session()->get('state');
        if ($state != $request['state']) {
            Log::debug(" Erreur de session - Code attendu : $state - Code reçu : " . $request['state']);
            abort(401);
        }

        Log::debug(" Identification de la session : " . $state);
        $code = $request['code'];
        $clientId = session()->get('client_id');
        $clientName = PassClient::findOrFail($clientId)->name;
        return view('passport.token.confirm', [
            'state' => $state,
            'code' => $code,
            'clientId' => $clientId,
            'clientName' => $clientName
        ]);
    }

    /**
     * Envoie de la requête de demande de token à OAuth2
     *
     * @param Request $request
     * @return \Illuminate\View\View|void
     */
    public function callbackStore(Request $request)
    {
        Log::debug(" ============= TokenController : callbackStore ============== ");
        $rules = [
            'client_id' => 'required|string|exists:oauth_clients,id',
            'code' => 'required|alpha_num',
            'client_secret' => 'required|alpha_num'
        ];

        $request = Validator::make($request->all(), $rules)->validate();
        $code = $request['code'];
        $clientId = $request['client_id'];
        $clientSecret = $request['client_secret'];
        $client = PassClient::findOrFail($clientId);

        if ($clientSecret != $client->secret) {
            Log::debug(" Erreur d'authentification du client ($clientId) : code secret invalide ");
            abort(401);
        }

        $http = new \GuzzleHttp\Client;
        $response = $http->post(route('passport.token'), [
            'form_params' => [
                'grant_type' => 'authorization_code',
                'client_id' => $clientId,
                'client_secret' => $clientSecret,
                'redirect_uri' => route('passport.callback'),
                'code' => $code
            ],
            'timeout' => 2,
            'http_errors' => false
        ]);

        // S'il y a une erreur, on enregistre l'erreur dans les logs
        if ($response->getStatusCode() < 200 || $response->getStatusCode() >= 300) {
            $errors = (array) json_decode($response->getBody());
            Log::debug($errors['error_description']);
            abort($response->getStatusCode());
        }

        Log::debug(" Client ($clientId) : Create token SUCCESS ");
        $tokens = (array) json_decode($response->getBody());
        $access = $tokens['access_token'];
        $refresh = $tokens['refresh_token'];
        session()->forget('state'); // On supprime le state (identifiant de la session) après la génération du token

        return view('passport.token.show', [
            'accessToken' => $access,
            'refreshToken' => $refresh,
            'expiresAccessToken' => config('passport.token_access_lifetime'),
            'expiresRefreshToken' => config('passport.token_refresh_lifetime'),
        ]);
    }

    /**
     * Rafraichissement de token : accès au formulaire
     *
     * @return \Illuminate\View\View
     */
    public function refreshForm()
    {
        Log::debug(" ============= TokenController : refreshForm ============== ");
        $clients = PassClient::whereUserId(Auth::id())->where('revoked', '!=', 1)->orderBy('name')->get(); // Liste des applis non révoqués de l'utilisateur
        return view('passport.token.refresh', [
            'clients' => $clients
        ]);
    }

    /**
     * Envoie de la requête de demande de rafraichissement d'un token à OAuth2 et renvoie des nouveaux tokens
     *
     * @param Request $request
     * @return \Illuminate\View\View|void
     */
    public function refreshStore(Request $request)
    {
        Log::debug(" ============= TokenController : refreshStore ============== ");
        $rules = [
            'client_id' => 'required|string|exists:oauth_clients,id',
            'refresh_token' => 'required|alpha_num',
            'client_secret' => 'required|alpha_num'
        ];

        $request = Validator::make($request->all(), $rules)->validate();
        $clientId = $request['client_id'];
        $clientSecret = $request['client_secret'];
        $client = PassClient::findOrFail($clientId);

        if ($clientSecret != $client->secret) {
            Log::debug(" Erreur d'authentification de l'appli ($clientId) : code secret invalide ");
            abort(401);
        }

        $refreshToken = $request['refresh_token'];
        $http = new \GuzzleHttp\Client;
        $response = $http->post(route('passport.token'), [
            'form_params' => [
                'grant_type' => 'refresh_token',
                'refresh_token' => $refreshToken,
                'client_id' => $clientId,
                'client_secret' => $clientSecret,
                'scope' => '',
            ],
            'timeout' => 2,
            'http_errors' => false
        ]);

        // S'il y a une erreur, on enregistre l'erreur dans les logs
        if ($response->getStatusCode() < 200 || $response->getStatusCode() >= 300) {
            $errors = (array) json_decode($response->getBody());
            Log::debug($errors['error_description']);
            abort($response->getStatusCode());
        }

        Log::debug(" Appli ($clientId) : Refresh token SUCCESS ");
        $tokens = (array) json_decode($response->getBody());
        $accessToken = $tokens['access_token'];
        $refresh = $tokens['refresh_token'];

        return view('passport.token.show', [
            'accessToken' => $accessToken,
            'refreshToken' => $refresh,
            'expiresAccessToken' => config('passport.token_access_lifetime'),
            'expiresRefreshToken' => config('passport.token_refresh_lifetime'),
        ]);
    }

    /**
     * Accès à la page de création de token d'accès personnel
     *
     * @return \Illuminate\View\View
     */
    public function personalAccessForm()
    {
        /*Log::debug(" ============= TokenController : personalAccessForm ============== ");
        $scopes = ['place-orders check-status']; // Todo : récupérer la liste des champs d'actions (scopes)
        return view('passport.token.personal-access', [
            'scopes' => $scopes
        ]);*/
    }

    /**
     *
     *
     * @return \Illuminate\View\View
     */
    public function personalAccessStore(Request $request)
    {
        /*Log::debug(" ============= TokenController : personalAccessStore ============== ");
        $rules = [
            'name' => 'required',
            // 'scopes' => 'nullable|array'
        ];

        $request = Validator::make($request->all(), $rules)->validate();
        $tokenName = $request['name'];
        $user = User::find(Auth::id());

        $token = $user->createToken($tokenName)->accessToken;
        return view('passport.token.show', [
            'accessToken' => $token,
            'expiresIn' => '',
            'refreshToken' => ''
        ]);*/
    }
}
