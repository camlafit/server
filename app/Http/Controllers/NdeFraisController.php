<?php
/*
 * NdeFraisController.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App;
use App\BaseCalculIks;
use App\Entreprise;
use App\LdeFrais;
use App\MoyenPaiement;
use App\NdeFrais;
use App\TypeFrais;
use App\User;
use Auth;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Intervention\Image\Gd\Shapes\EllipseShape;
use Illuminate\Support\Arr;
//Pour la transformation en PDF
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use App\Exports\ExportQuadratus;
use App\Exports\ExportFEC;
use Illuminate\Support\Facades\Crypt;
use App\Events\ExportNDF;

class NdeFraisController extends Controller
{
    private $_endOfMonth = 0;
    private $_month = 0;
    private $_year = 0;
    private $_ndf = null;
    private $_userID = 0;
    private $_user = null;
    private $_userEmail = "";
    private $_userName = "";

    private $_ecritureNum;
    private $_QuadraObject;
    private $_FECObject;
    private $_lignesExportCompta;

    public function __construct()
    {
        Log::debug('=================== NdeFraisController __construct');
    }

    public function __invoke($hash)
    {
        //TODO verifier si ce user a le droit
        $fullFilename = realpath(Crypt::decryptString($hash));
        Log::debug("NdeFraisController::__invoque on demande $fullFilename");
        if (file_exists($fullFilename)) {
        } else {
            $filename = "";
        }
        $filename = \basename($fullFilename);
        //On recupere le path
        $directory = storage_path() . "/NdeFrais/";
        $reste = \str_replace($directory, "", $fullFilename);
        Log::debug("NdeFraisController::il reste $reste");
        //On recupere la 1ere partie
        $tab = explode("/", $reste);
        $lenom  = $tab[0];
        $ladate = substr($tab[1], 0, strpos($tab[1], "-"));
        Log::debug("NdeFraisController::le login est $lenom et $ladate");
        //On remplace le "@" de l'adresse mail par un "-"
        $nom = \str_replace("@", "-", $ladate . "-" . $lenom . "-" . $filename);

        return response()->download($fullFilename, $nom);
    }

    /**
     * index: la liste des 12 dernières notes de frais de l'utilisateur
     *
     * @return void
     */
    public function index()
    {
        Log::debug("======NdeFraisController : index =============");
        //Les 12 dernières, à voir si on fait de la pagination ?
        Log::debug("  retourne : " . json_encode(Auth::user()->NdeFrais->sortByDesc('debut')->splice(0, 12)));
        return Auth::user()->NdeFrais->sortByDesc('debut')->splice(0, 12);
    }

    /**
     * show: recupère la note de frais $id
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        Log::debug("======NdeFraisController : show =============");
        $this->_ndf = NdeFrais::findOrFail($id);

        //vérification des droits d'accès
        if (php_sapi_name() != 'cli') {
            $this->authorize('view', $this->_ndf);
        }
        // Log::debug($ndf);
        // Log::debug('===================');
        return $this->_ndf;
    }

    /**
     * details: les détails de la note de frais $id
     *
     * @param  mixed $id
     * @return void
     */
    public function details($id)
    {
        Log::debug("==========NdeFraisController : details $id =========");

        //vérification des droits d'accès
        if (!isset($this->_ndf->id) || $this->_ndf->id != $id) {
            $this->_ndf = NdeFrais::findOrFail($id);
            if (php_sapi_name() != 'cli') {
                $this->authorize('view', $this->_ndf);
            }
        }

        $lignes = array();
        // sur l'api on mélange pro et perso, il faut donc pouvoir avoir l'info ... ainsi que le moyen de paiement
        $lignes = NdeFrais::findOrFail($id)->ldeFrais()->with('moyenPaiement')->with('typeFrais')->get()->sortBy('ladate');
        // Log::debug($lignes);
        return $lignes;
    }

    /**
     * webBuildPDF : genère le fichier PDF de la note de frais
     *
     * @param  mixed $id
     * @param  mixed $pdfFileOutput
     * @param  mixed $forceUpdate
     * @param  mixed $contexte : ExportToDolibarr par exemple pour que l'event soit capté par le plugin
     * @return void
     */
    public function webBuildPDF(int $id, string $pdfFileOutput = "", bool $forceUpdate = false, string $contexte = "")
    {
        Log::debug("NdeFraisController :: webBuildPDF pour $id");
        //TODO repasser en commentaire ... dev time
        $forceUpdate = true;
        $this->_ndf = NdeFrais::findOrFail($id);

        //vérification des droits d'accès
        if (php_sapi_name() != 'cli') {
            $this->authorize('view', $this->_ndf);
        }
        //autre methode
        // if (! Auth::user()->can('view', $this->_ndf)) {
        //     Log::debug("[policy error] webBuildPDF Erreur de vérification des droits d'acces pour userid " . Auth::user()->id . " sur la note de frais refid " . $id . " -> return home");
        //     return abort(403, 'Accès refusé');
        // }

        $this->_userID = $this->_ndf->user_id;
        $this->_user = User::find($this->_userID);
        $finDuMois   = Carbon::parse($this->_ndf->fin);
        $debutDuMois = Carbon::parse($this->_ndf->fin)->startOfMonth();
        $this->_endOfMonth = $finDuMois;
        $this->_month = $finDuMois->month;
        $this->_year = $finDuMois->year;
        $this->_ecritureNum = 1;

        $u = $this->_user;

        $this->_userName = $u->firstname . " " . $u->name;
        $this->_userEmail = $u->email;

        Log::debug("  NdeFraisController :: webBuildPDF :: utilisateur " . $this->_userName);

        $baseCalculIK = new BaseCalculIks($finDuMois);
        $ldeFrais = new LdeFrais();
        $lignesperso = array();
        $lignesIK = array();
        $lignespro = array();

        $idTypeFraisIK = TypeFrais::where('slug', 'ik')->pluck('id');
        $idTypeFraisIKRegule = TypeFrais::where('slug', 'ik-regule')->pluck('id');


        // Log::debug("NdeFraisController :: webBuildPDF 2");

        //On archive le PDF généré et s'il existe déjà on le passe directement sans le recalculer
        $fullFilename = $this->getfilePDFFullPath();
        $nomPJ = basename($fullFilename, ".pdf");
        $nomPDF = basename($fullFilename);
        $directory = dirname($fullFilename);

        //Attention, le PDF de la note de frais n'existe pas encore ou n'est pas à jour ...
        $this->_QuadraObject = new ExportQuadratus($this->_user, $nomPJ . "_quadratus.TXT", $directory, $finDuMois, $nomPDF);
        $this->_FECObject    = new ExportFEC($this->_user, $nomPJ . ".TXT", $directory, $finDuMois, $nomPDF);

        if (!is_dir($directory)) {
            mkdir($directory, 0770, true);
        }

        //Durant le dev on force la re-génération du PDF mais en prod on pourra utiliser le cache direct
        if (file_exists($fullFilename) && !$forceUpdate) {
            Log::debug("  NdeFraisController :: webBuildPDF :: Le fichier PDF existe déjà on le passe tel-quel");
            return response()->file($fullFilename);
        }
        Log::debug("  NdeFraisController :: webBuildPDF :: Le fichier PDF n'existe pas ou on a forcé sa génération");

        //depuis quand cet utilisateur utilise doliscan ? dès le début de l'année ou pas ?

        //attention si ça se trouve c'est un utilisateur sans notes de frais
        $utilisationDoliScanAnneeComplete = false;
        $debutUtilisationR = $this->_ndf->where('user_id', $this->_userID)->orderby('debut', 'asc')->first();
        if ($debutUtilisationR) {
            $debutUtilisation = $debutUtilisationR->ladate;
            if (Carbon::parse($this->_ndf->fin)->startOfYear() > $debutUtilisation) {
                $utilisationDoliScanAnneeComplete = true;
            }
        } else {
            //Jamais eu de note de frais, un admin ou autre, pas la peine d'aller plus loin
            Log::debug("  NdeFraisController :: webBuildPDF :: Cet utilisateur n'a pas de note de frais, return");
            return redirect('/');
        }

        // ==============================================================================  les frais payés perso  ==========================================================================================================================
        //On recupere les types de frais utilisés sur cette note de frais
        $paiementPersoId = MoyenPaiement::where('is_pro', '0')->pluck('id');
        $tabID = LdeFrais::where('nde_frais_id', $id)->whereIn('moyen_paiement_id', $paiementPersoId)->whereNotIn('type_frais_id', array($idTypeFraisIK, $idTypeFraisIKRegule))
            ->distinct()->pluck('type_frais_id');
        $typeFrais = TypeFrais::whereIn('id', $tabID)->get();
        Log::debug('  NdeFraisController :: webBuildPDF ======================================================== Frais payés perso =======================================================');
        // Log::debug($paiementPersoId);
        // Log::debug('===================');

        //Et ensuite on recupere tous les frais de cette note de frais, categorie par categorie
        // 6 avril on a un paiement total des frais perso pour le salarié donc une seule ligne de contrepartie
        $local_totalTTC = 0;
        foreach ($tabID as $tid) {
            $tf = TypeFrais::where('id', $tid)->first();
            $local_typeFrais = $tf->slug;
            $local_typeFraisTXT = strtoupper(Str::ascii($tf->label));
            // Log::debug("On tourne au $tid qui donne " . $local_typeFrais . " ou TXT = $local_typeFraisTXT ... et le code comptable " . $u->getComptaCode($local_typeFrais));

            $lignesperso[$tid] = $this->_ndf->ldeFrais->where('type_frais_id', $tid)->whereIn('moyen_paiement_id', $paiementPersoId);
            // $local_totalHT = 0;
            // $local_totalTVA = 0;
            // $local_totalTTC = 0;

            //A voir s'il faut une ligne en compta pour chaque facturette -> réponse oui donc export = 1
            foreach ($lignesperso[$tid] as $lignePerso) {
                $lignesPerso[$tid] = $this->computeCompta($lignePerso, 0, 1, $local_typeFrais, $local_typeFraisTXT);
                // $local_totalHT += $lignePerso->ht;
                $local_totalTTC += $lignePerso->ttc;
                // $local_totalTVA += $lignePerso->totalTVA;
            }

            //Ramasse miette ... si pas de tva et que le ht est à zéro alors HT = TTC
            // if ($local_totalTVA == 0 && $local_totalHT == 0) {
            //     $local_totalHT = $local_totalTTC;
            // }

            //Dans les cas des frais perso le document justificatif c'est le fichier PDF synthétique
            // $local_fichierJoint = $this->getfilePDFFullPathJustificatifs();

            // ====================== 6 avril on ne fait plus la globalisation des frais mais on fait le détail
            // //Les écritures comptables pour cette catégorie de frais
            // $this->addExportCompta($this->_ecritureNum, $u->getComptaCode($local_typeFrais), $local_typeFraisTXT, "", "", $local_typeFraisTXT, $local_totalHT, "0", $local_fichierJoint);
            // //La TVA déductible
            // if ($local_totalTVA > 0) {
            //     $this->addExportCompta($this->_ecritureNum, $u->getComptaCode('tvadeductible'), "TVA DEDUC.", "", "", "TVA DEDUC.", $local_totalTVA, "0", $local_fichierJoint);
            // }
            // //La contrepartie
            // $this->addExportCompta($this->_ecritureNum, $u->getComptaCode('compteperso'), $this->_userName, "", "", $this->_userName, "0", $local_totalTTC, $local_fichierJoint);
            // $this->_ecritureNum++;
        }
        // ====================== 6 avril on ne fait plus la globalisation des frais mais on fait le détail
        // //La contrepartie est par contre globale : le total est payé en une fois
        // Dans les cas des frais perso le document justificatif c'est le fichier PDF synthétique : nom special car
        // le hic c'est que le fichier PDF "local" n'existe pas encore a l'heure actuelle :-/
        $local_fichierJoint = "D0000NDF.PDF";
        $this->addExportCompta($this->_ecritureNum, $u->getComptaCode('compteperso'), $this->_userName, "", "", $this->_userName, "0", $local_totalTTC, $local_fichierJoint, "", false);
        $this->_ecritureNum++;

        // ==============================================================================  les IK (forcément perso)  ==========================================================================================================================
        //Et les IK a gérer en cas particulier : pour chaque véhicule on applique les calculs, cf
        //https://projets.cap-rel.fr/projects/open-notes-de-frais/wiki/Calcul_des_IK
        //La liste des véhicules utilisés cette année
        $vehicules = $ldeFrais->getVehiculesForUser($this->_ndf->user_id, $this->_year);
        $keepVehicules = collect([]);
        // Log::debug($vehicules);

        //pour chaque véhicule on cherche le km effectué ce mois-ci et le total annuel pour appliquer le bon barème
        foreach ($vehicules as $tvehicule) {
            // Log::debug("On cherche le kilométrage total pour $tvehicule ...");
            $totalKMthisYear = $ldeFrais->getKMforVehiculeThisYear($this->_ndf->user_id, $tvehicule, $this->_year, $this->_ndf->fin, $id);
            $kmThisMonth = $ldeFrais->getKMforVehiculeThisMonth($this->_ndf->user_id, $tvehicule, $id);
            $vehiculeCV = $ldeFrais->extractVehiculeCV($tvehicule);
            $vehiculeNom = $ldeFrais->extractVehiculeNom($tvehicule);
            $vehiculeKMavant = $ldeFrais->extractVehiculeKM($tvehicule);
            $vehiculeType = $ldeFrais->extractVehiculeType($tvehicule);
            $vehiculeAMC  = $ldeFrais->extractVehiculeAMC($tvehicule);


            Log::debug('======= IK ============');
            // Log::debug('> vehicule ' . $tvehicule);
            // Log::debug('> Nom du vehicule ' . $vehiculeNom);
            // Log::debug('> Km Avant utilisation de doliscan ' . $vehiculeKMavant);
            //Puis chaque ligne d'IK de la NDF en cours
            $listeIK = LdeFrais::where('nde_frais_id', $id)->where('type_frais_id', $idTypeFraisIK)->where('vehicule', $tvehicule)->orderby('ladate')->get();
            //On verifie le calcul des IK ... vieux bug qui traine
            foreach ($listeIK as $ligne) {
                $km = $ligne->distance;
                $montantVerif = $baseCalculIK->calcul($vehiculeCV, $km, $totalKMthisYear, false, null, $vehiculeAMC);
                if ($montantVerif != $ligne->ttc) {
                    // Log::debug("Erreur, montant a corriger, on avait " . $ligne->ttc . " au lieu de " . $montantVerif);
                    $ligne->ttc = $montantVerif;
                    //Sauvegarde du montant corrigé
                    $ligne->save();
                }
            }
            // Log::debug($listeIK);
            // Log::debug('===================');

            // Log::debug("on tourne au $tid");
            //On n'ajoute que les vehicules avec lesquels on a roulé
            if ($kmThisMonth > 0) {
                $lignesIK[$tvehicule]["nom"] = $vehiculeNom;
                $lignesIK[$tvehicule]["cv"] = $vehiculeCV;
                $lignesIK[$tvehicule]["totalKM"] = $totalKMthisYear;
                $lignesIK[$tvehicule]["KMavantDoli"] = $vehiculeKMavant;
                $lignesIK[$tvehicule]["kmPeriode"] = $kmThisMonth;
                $lignesIK[$tvehicule]["detailCalcul"] = $baseCalculIK->printCalcul($vehiculeCV, $kmThisMonth, $totalKMthisYear, false, null, $vehiculeAMC);
                $lignesIK[$tvehicule]["bareme"] = $baseCalculIK->printBareme();

                $lignesIK[$tvehicule]["changeTranche"] = false; //Passe a true lorsqu'on change de tranche de calcul d'ik
                $lignesIK[$tvehicule]["changeBareme"] = false; //Passe a true lorsqu'on change de bareme
                $lignesIK[$tvehicule]["changeBaremeImpact"] = 0; //Si = 0 le changement de bareme n'a pas change les valeurs (exemple 2018 -> 2019 pour 7cv)
                $lignesIK[$tvehicule]["MontantAnnuelCorrige"] = 0;
                $lignesIK[$tvehicule]["MontantAnnuelHorsRegules"] = 0;
                $lignesIK[$tvehicule]["detailCalculComplet"] = 0;

                $totalRegules = $this->TotalRegules($finDuMois, $tvehicule, $this->_year, $this->_userID);
                $totalRegulesAvantCeMois = $this->TotalRegules($debutDuMois, $tvehicule, $this->_year, $this->_userID);
                $lignesIK[$tvehicule]["MontantAnnuelHorsRegules"] = LdeFrais::where('user_id', $this->_ndf->user_id)->where('ladate', '<=', $finDuMois)->where('ladate', '>=', $this->_year . '-01-01')
                    ->where('type_frais_id', $idTypeFraisIK)->where('vehicule', $tvehicule)->sum('ttc');
                $lignesIK[$tvehicule]["MontantAnnuel"] = $lignesIK[$tvehicule]["MontantAnnuelHorsRegules"] + $totalRegulesAvantCeMois;

                //Detection du changement de bareme : le ministere a publie ses nouveaux baremes entre la date de la derniere note de frais et aujourd'hui
                //if($baseCalculIK->anneeBareme())
                $dateBaremeEnCours = $baseCalculIK->dateAnneeBaremeUpdated();
                if ($dateBaremeEnCours > $debutDuMois && $dateBaremeEnCours < $finDuMois) {
                    //Detection du changement de tranche : on re-déroule le calcul pour vérifier si ça matche
                    $montantA = $baseCalculIK->calcul($vehiculeCV, $kmThisMonth, $totalKMthisYear, false, $this->_year - 1) + $totalRegules;
                    $montantB = $baseCalculIK->calcul($vehiculeCV, $kmThisMonth, $totalKMthisYear) + $totalRegules;
                    // Log::debug("On change de bareme ... calcul rétroactif à faire pour une régule éventuelle :" . $dateBaremeEnCours);
                    // Log::debug("Montant avec l'ancien bareme" . $montantA);
                    // Log::debug("Montant avec le bareme de cette annee" . $montantB);

                    $lignesIK[$tvehicule]["changeBareme"] = true;
                    if ($montantA != $montantB) {
                        $lignesIK[$tvehicule]["changeBaremeImpact"] = $montantA - $montantB;
                        //ding dong on change de bareme
                        $lignesIK[$tvehicule]["MontantAnnuelBaremeA"] = $baseCalculIK->calcul($vehiculeCV, $totalKMthisYear, $totalKMthisYear, true, $this->_year - 1, $vehiculeAMC);
                        $lignesIK[$tvehicule]["MontantAnnuelBaremeB"] = $baseCalculIK->calcul($vehiculeCV, $totalKMthisYear, $totalKMthisYear, true, null, $vehiculeAMC);
                        $lignesIK[$tvehicule]["MontantAnnuelBaremeCorrige"] = $lignesIK[$tvehicule]["MontantAnnuelBaremeB"] - $lignesIK[$tvehicule]["MontantAnnuel"];

                        $lignesIK[$tvehicule]["DetailAnnuelBaremeA"] = $baseCalculIK->printCalcul($vehiculeCV, $totalKMthisYear, $totalKMthisYear, true, $this->_year - 1, $vehiculeAMC);
                        $lignesIK[$tvehicule]["DetailAnnuelBaremeB"] = $baseCalculIK->printCalcul($vehiculeCV, $totalKMthisYear, $totalKMthisYear, true, $this->_year, $vehiculeAMC);

                        //On ajoute une ligne de frais de type régule si pas déjà présente
                        $ldeFraisC = new LdeFraisController();
                        $ldeFraisC->newReguleIK($finDuMois, "Regule pour changement de barème", $lignesIK[$tvehicule]["MontantAnnuelBaremeCorrige"], $tvehicule, $vehiculeCV, $this->_ndf->id, $this->_userID);
                    }
                }

                //Detection du changement de tranche : on re-déroule le calcul pour vérifier si ça matche
                $montantA = $baseCalculIK->calcul($vehiculeCV, $kmThisMonth, $totalKMthisYear, false, null, $vehiculeAMC);
                $montantB = $baseCalculIK->calcul($vehiculeCV, $kmThisMonth, $totalKMthisYear - $kmThisMonth, false, null, $vehiculeAMC);

                if ($montantA != $montantB) {
                    //ding dong on change de tranche
                    $lignesIK[$tvehicule]["changeTranche"] = true;
                    $lignesIK[$tvehicule]["MontantAnnuelCorrige"] = $baseCalculIK->calcul($vehiculeCV, $totalKMthisYear, $totalKMthisYear, true, null, $vehiculeAMC);
                    $lignesIK[$tvehicule]["detailCalculComplet"] = $baseCalculIK->printCalcul($vehiculeCV, $totalKMthisYear, $totalKMthisYear, true, null, $vehiculeAMC);

                    $montantRegule = $lignesIK[$tvehicule]["MontantAnnuelCorrige"] - ($lignesIK[$tvehicule]["MontantAnnuel"]);
                    $lignesIK[$tvehicule]["MontantTrancheRegule"] = $montantRegule;
                    //On ajoute une ligne de frais de type régule si pas déjà présente
                    $ldeFraisC = new LdeFraisController();
                    $ldeFraisC->newReguleIK($finDuMois, "Regule pour changement de tranche", $montantRegule, $tvehicule, $vehiculeCV, $this->_ndf->id, $this->_userID);
                }
                $lignesIK[$tvehicule]["Montant"] = $montantA;

                $lignesIK[$tvehicule]["listeIKMonth"] = $listeIK;
                $keepVehicules->push($tvehicule);

                $this->addExportCompta($this->_ecritureNum, $u->getComptaCode('ik'), "INDEM.KM. $vehiculeNom", "", "", "INDEM.KM. $vehiculeNom", $montantA, "0", "", "", false);
                //La contrepartie
                $this->addExportCompta($this->_ecritureNum, $u->getComptaCode('compteperso'), $this->_userName, "", "", $this->_userName, "0", $montantA, "", "", false);
            }
        }

        // DB::enableQueryLog();
        // Log::debug("Resultat de la requete SQL:");
        Log::debug("Nouvelle liste de véhicules utilisés ce mois-ci : " . $keepVehicules);
        // Log::debug(DB::getQueryLog());

        // ==============================================================================  les frais payés pro  ==========================================================================================================================
        Log::debug('  NdeFraisController :: webBuildPDF ======================================================== Frais payés pro =======================================================');

        //On recupere les types de frais utilisés sur cette note de frais
        $paiementProId = MoyenPaiement::where('is_pro', '1')->pluck('id');
        $tabIDpro = LdeFrais::where('nde_frais_id', $id)->whereIn('moyen_paiement_id', $paiementProId)->distinct()->pluck('type_frais_id');
        $typeFraispro = TypeFrais::whereIn('id', $tabIDpro)->get();
        // Log::debug('======= pro ============');
        // Log::debug($paiementProId);
        // Log::debug('===================');

        //Et ensuite on recupere tous les frais de cette note de frais, categorie par categorie
        foreach ($tabIDpro as $tid) {
            $tf = TypeFrais::where('id', $tid)->first();
            $local_typeFrais = $tf->slug;
            $local_typeFraisTXT = strtoupper(Str::ascii($tf->label));
            // Log::debug("On tourne au $tid qui donne " . $local_typeFrais . " et le code comptable " . $u->getComptaCode($local_typeFrais));

            $lignespro[$tid] = $this->_ndf->ldeFrais->sortBy('ladate')->where('type_frais_id', $tid)->whereIn('moyen_paiement_id', $paiementProId);
            // Log::debug("*******************************************************************************************************************************************************************************************************************");
            // Log::debug($lignespro[$tid]);
            // Log::debug("*******************************************************************************************************************************************************************************************************************");

            //Dans le cas de ce qui est payé avec les moyens pros c'est sur qu'il faut une ligne par facturette
            foreach ($lignespro[$tid] as $ligne) {
                $ligne = $this->computeCompta($ligne, 1, 1, $local_typeFrais, $local_typeFraisTXT);
            }
        }

        $this->_ndf->update([
            'montant' => $this->_ndf->ldeFrais()->sum('ttc')
        ]);

        // $this->exportQUADRA($QUADRAfileName, $directory . $finDuMois->format("Ymd") . "-quadratus");

        // *************************** TODO ****************************
        // Référence de la note de frais

        //Société pour la partie gauche du cartouche
        // DB::enableQueryLog();
        $societe = $u->getEntreprise();

        // Log::debug(DB::getQueryLog());
        // Log::debug(" societe : " . $societe);

        if (is_null($societe) || $societe->name == "") {
            $societe = new Entreprise();
            $societe->name = "SPECIMEN";
            $societe->adresse = "Ce compte utilisateur n'est pas encore";
            $societe->cp = "rattaché à une entreprise ...";
            $societe->ville = "";
            $societe->tel = "";
            $societe->email = "";
            $societe->web = "";
        }
        // *************************** Debug HTML possible ****************************
        // if ($pdfFileOutput != "") {
        //     $fp = fopen($pdfFileOutput . ".html", "w");
        // } else {
        //     $fp = fopen($fullFilename . ".html", "w");
        // }

        // if ($fp) {
        //     $html = view('webprint', [
        //         'id' => $id,
        //         'note' => $this->_ndf,
        //         'typeFrais' => $typeFrais,
        //         'lignes' => $lignesperso,
        //         'vehicules' => $keepVehicules,
        //         'lignesIK' => $lignesIK,
        //         'typeFraispro' => $typeFraispro,
        //         'lignespro' => $lignespro,
        //         'societe'  => $societe,
        //         'userName' => $this->_userName,
        //         'userEmail' => $this->_userEmail,
        //         'utilisationDoliScanAnneeComplete' => $utilisationDoliScanAnneeComplete,
        //     ]);
        //     Log::debug("  Debug HTML vers $pdfFileOutput.html ou $fullFilename.html ...");
        //     fwrite($fp, $html);
        //     fclose($fp);
        // }

        // *************************** TODO ****************************
        $pdf = App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $pdf->loadView('webprint', [
            'id' => $id,
            'note' => $this->_ndf,
            'reference' => $this->_ndf->getReference(),
            'typeFrais' => $typeFrais,
            'lignes' => $lignesperso,
            'vehicules' => $keepVehicules,
            'lignesIK' => $lignesIK,
            'typeFraispro' => $typeFraispro,
            'lignespro' => $lignespro,
            'societe'  => $societe,
            'userName' => $this->_userName,
            'userEmail' => $this->_userEmail,
            'utilisationDoliScanAnneeComplete' => $utilisationDoliScanAnneeComplete,
        ]);

        $sujet = "Note de frais de " . $this->_ndf->label . " - " . $this->_userName;
        $this->addMetaToPDF($pdf, $sujet);

        //Si on a spécifié un nom de fichier lors de l'appel de la fonction on l'utilise
        $r = "";
        if ($pdfFileOutput != "") {
            $r = $pdf->save($pdfFileOutput);
        } else {
            $r = $pdf->save($fullFilename);
        }

        //On force la re-création des fichiers d'exports mais pour ca il faut que le PDF de la note de frais soit fait
        $this->_FECObject->export("", true);
        $this->_QuadraObject->export("", true);

        //On lance un event qui devrait être capté par les plugins ...
        // Log::debug("NdeFraisController :: webBuildPDF 3 "); // detail debug \serialize($this->_ndf)
        event(new ExportNDF($this->_ndf, "ndf", $this->_lignesExportCompta, $lignesperso, $lignespro, $lignesIK, $utilisationDoliScanAnneeComplete, $contexte));
        Log::debug("NdeFraisController :: webBuildPDF 4");

        return $r->stream($nomPDF);
    }

    /**
     * webBuildPDFJustificatifs: génère un PDF des justificatifs de la note de frais
     *
     * @param  mixed $id
     * @param  mixed $pdfFileOutput
     * @param  mixed $forceUpdate
     * @param  mixed $contexte
     * @return void
     */
    public function webBuildPDFJustificatifs(int $id, string $pdfFileOutput = "", bool $forceUpdate = false, string $contexte = "")
    {
        //TODO repasser en commentaire ... dev time
        $forceUpdate = true;

        $this->_ndf = NdeFrais::findOrFail($id);
        //vérification des droits d'accès
        if (php_sapi_name() != 'cli') {
            $this->authorize('view', $this->_ndf);
        }

        $this->_userID = $this->_ndf->user_id;
        $this->_user = User::find($this->_userID);
        $finDuMois = Carbon::parse($this->_ndf->fin);
        $debutDuMois = Carbon::parse($this->_ndf->fin)->startOfMonth();
        $this->_month = $finDuMois->month;
        $this->_year = $finDuMois->year;
        $this->_endOfMonth = $finDuMois;

        $lignesperso = array();
        $lignespro = array();

        $u = $this->_user;

        $this->_userName = $u->firstname . " " . $u->name;
        $this->_userEmail = $u->email;

        //On archive le PDF généré et s'il existe déjà on le passe directement sans le recalculer
        $fullFilename = $this->getfilePDFFullPathJustificatifs();
        $nomPJ = basename($fullFilename, ".pdf");
        $nomPDF = basename($fullFilename);
        $directory = dirname($fullFilename);

        $directoryImg = storage_path() . "/LdeFrais/" . $this->_userEmail . "/" . $finDuMois->format("Ym") . "/";

        Log::debug("webBuildPDFJustificatifs::Stockage du fichier dans $directory et nom de fichier $nomPJ ($fullFilename) pour " . $this->_userName);
        if (!is_dir($directory)) {
            mkdir($directory, 0770, true);
        }

        if (file_exists($fullFilename) && !$forceUpdate) {
            Log::debug("webBuildPDFJustificatifs::Le fichier PDF existe déjà on le passe tel-quel");
            return response()->file($fullFilename);
        }

        Log::debug("webBuildPDFJustificatifs::Le fichier PDF n'existe pas, on le génère a partir des éléments disponibles");
        // ==================== les frais payés perso
        //On recupere les types de frais utilisés sur cette note de frais
        $paiementPersoId = MoyenPaiement::where('is_pro', '0')->pluck('id');
        $tabID = LdeFrais::where('nde_frais_id', $id)->whereIn('moyen_paiement_id', $paiementPersoId)->distinct()->pluck('type_frais_id');
        $typeFrais = TypeFrais::whereIn('id', $tabID)->get();

        //Et ensuite on recupere tous les frais de cette note de frais, categorie par categorie
        foreach ($tabID as $tid) {
            // Log::debug("on tourne au $tid");
            $lignesperso[$tid] = $this->_ndf->ldeFrais->where('type_frais_id', $tid)->whereIn('moyen_paiement_id', $paiementPersoId);
        }

        // ==================== les frais payés pro
        //On recupere les types de frais utilisés sur cette note de frais
        $paiementProId = MoyenPaiement::where('is_pro', '1')->pluck('id');
        $tabIDpro = LdeFrais::where('nde_frais_id', $id)->whereIn('moyen_paiement_id', $paiementProId)->distinct()->pluck('type_frais_id');
        $typeFraispro = TypeFrais::whereIn('id', $tabIDpro)->get();
        // Log::debug('======= pro ============');
        // Log::debug($paiementProId);
        // Log::debug('===================');

        //Et ensuite on recupere tous les frais de cette note de frais, categorie par categorie
        foreach ($tabIDpro as $tid) {
            // Log::debug("on tourne au $tid");
            $lignespro[$tid] = $this->_ndf->ldeFrais->where('type_frais_id', $tid)->whereIn('moyen_paiement_id', $paiementProId);
        }

        //pour debug html
        // return view('webBuildPDFjustificatifs', [
        //     'id'         => $id,
        //     'note'       => $this->_ndf,
        //     'typeFrais'  => $typeFrais,
        //     'lignes'     => $lignes,
        //     'typeFraispro' => $typeFraispro,
        //     'lignespro'    => $lignespro,
        // ]);

        Log::debug("webBuildPDFJustificatifs::Direction blade dompdf.wrapper...");
        $pdf = App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $pdf->loadView('webprintjustificatifs', [
            'id' => $id,
            'note' => $this->_ndf,
            'reference' => $this->_ndf->getReference(),
            'typeFrais' => $typeFrais,
            'lignes' => $lignesperso,
            'typeFraispro' => $typeFraispro,
            'lignespro' => $lignespro,
            'userName' => $this->_userName,
            'userEmail' => $this->_userEmail,
            'imgPath' => $directoryImg,
        ]);

        Log::debug("webBuildPDFJustificatifs::Retour de blade ... on ajoute les meta données");
        $sujet = "Jutifificatifs de la note de frais de " . $this->_ndf->label . " - " . $this->_userName;
        $this->addMetaToPDF($pdf, $sujet);

        //Si on a spécifié un nom de fichier lors de l'appel de la fonction on l'utilise
        if ($pdfFileOutput != "") {
            Log::debug("webBuildPDFJustificatifs::Génération du PDF vers $pdfFileOutput");
            $r = $pdf->save($pdfFileOutput);
        } else {
            Log::debug("webBuildPDFJustificatifs::Génération du PDF vers $fullFilename");
            $r = $pdf->save($fullFilename);
        }
        Log::debug("webBuildPDFJustificatifs::Génération du PDF terminée");


        //On lance un event qui devrait être capté par les plugins ...
        Log::debug("NdeFraisController :: webBuildPDFJustificatifs un PDF est pret");
        event(new ExportNDF($this->_ndf, "justificatifs", $this->_lignesExportCompta, $lignesperso, $lignespro, "", -1, $contexte));

        return $r->stream($nomPJ);
    }

    /**
     * webDetails: deprecated
     *
     * @param  mixed $id
     * @return void
     */
    public function webDetails(int $id)
    {
        Log::debug("================== webDetails $id===============");
        $this->_ndf = NdeFrais::findOrFail($id);
        //vérification des droits d'accès
        if (php_sapi_name() != 'cli') {
            $this->authorize('view', $this->_ndf);
        }

        $lignesperso = array();
        $lignespro = array();

        // ==================== les frais payés perso
        //On recupere les types de frais utilisés sur cette note de frais
        //DB::enableQueryLog(); // Enable query log
        $paiementPersoId = MoyenPaiement::where('is_pro', '0')->pluck('id');
        $tabID = LdeFrais::where('nde_frais_id', $id)->whereIn('moyen_paiement_id', $paiementPersoId)->distinct()->pluck('type_frais_id');
        $typeFrais = TypeFrais::whereIn('id', $tabID)->get();
        Log::debug("===== paiementPersoId = $paiementPersoId");
        Log::debug("===== typeFrais = $typeFrais");
        //Log::debug($tabID);
        //Log::debug(DB::getQueryLog());

        //Et ensuite on recupere tous les frais de cette note de frais, categorie par categorie
        foreach ($tabID as $tid) {
            // Log::debug("on tourne au $tid");
            $lignesperso[$tid] = $this->_ndf->ldeFrais->where('type_frais_id', $tid);
        }

        // ==================== les frais payés pro
        //On recupere les types de frais utilisés sur cette note de frais
        $paiementProId = MoyenPaiement::where('is_pro', '1')->pluck('id');
        $tabIDpro = LdeFrais::where('nde_frais_id', $id)->whereIn('moyen_paiement_id', $paiementProId)->distinct()->pluck('type_frais_id');
        $typeFraispro = TypeFrais::whereIn('id', $tabIDpro)->get();

        //Et ensuite on recupere tous les frais de cette note de frais, categorie par categorie
        foreach ($tabIDpro as $tid) {
            // Log::debug("on tourne au $tid");
            $lignespro[$tid] = $this->_ndf->ldeFrais->where('type_frais_id', $tid);
            //On pourrait appliquer un filtre au résumé de la note de frais
            //Log::debug("===================webDetails Résumé: " . $lignespro[$tid]);
        }

        // Log::debug('=======lignes============');
        // Log::debug($lignes);
        // Log::debug('=======lignespro============');
        // Log::debug($lignespro);
        // Log::debug('===================');

        return view('webdetails', [
            'id' => $id,
            'typeFrais' => $typeFrais,
            'lignes' => $lignesperso,
            'typeFraispro' => $typeFraispro,
            'lignespro' => $lignespro,
            'status' => $this->_ndf->status,
        ]);
    }

    /**
     * TotalRegules : Calcul de la somme des regules pour cette annee pour ce vehicule pour cet utilisateur
     *
     * @param  mixed $finDuMois
     * @param  mixed $vehicule
     * @param  mixed $year
     * @param  mixed $userID
     * @return void
     */
    public function TotalRegules(Carbon $finDuMois, string $vehicule, string $year, int $userID)
    {
        //Log::debug('=======TotalRegules============');
        //L'ID des types de frais speciaux "regule"
        $idTypeFraisIKRegule = TypeFrais::where('slug', 'ik-regule')->pluck('id')->first();

        $total = LdeFrais::where('user_id', $userID)->where('ladate', '<=', $finDuMois)->where('ladate', '>=', $year . '-01-01')->where('type_frais_id', $idTypeFraisIKRegule)
            ->where('vehicule', $vehicule)->sum('ttc');

        // Log::debug('Total regules = ' . $total);
        return ($total);
    }

    /**
     * addMetaToPDF ajoute des méta données au PDF produit
     *
     * @param  mixed $pdf
     * @param  mixed $sujet
     * @return void
     */
    public function addMetaToPDF($pdf, string $sujet)
    {
        $pdf->getDomPdf()->add_info('Subject', $sujet);
        $pdf->getDomPdf()->add_info('Producer', config('app.name') . " " . config('app.version'));
        $pdf->getDomPdf()->add_info('Creator', config('app.name') . " " . config('app.version'));
    }

    /**
     * addExportCompta : L'appel commun qui ensuite génère le FEC ou le QUADRA (et d'autres dans le futur ?)
     *
     * @param  mixed $ecritureNum
     * @param  mixed $compteNum
     * @param  mixed $compteLib
     * @param  mixed $compAuxNum
     * @param  mixed $compAuxLib
     * @param  mixed $ecritureLib
     * @param  mixed $debit
     * @param  mixed $credit
     * @param  mixed $documentJustif
     * @param  mixed $ladate
     * @param  mixed $isPro
     * @return void
     */
    public function addExportCompta($ecritureNum, $compteNum, $compteLib, $compAuxNum, $compAuxLib, $ecritureLib, $debit, $credit, $documentJustif = "", $ladate = "", $isPro = true)
    {
        Log::debug("addExportCompta pour $compteLib $compAuxLib ... $compteNum : $debit | $credit");
        if ($compteNum == "") {
            Log::debug("addExportCompta ERREUR numéro de compte vide pour $compteLib ... $compteNum / ecriture $ecritureNum / $compAuxNum, $compAuxLib, $ecritureLib, $debit, $credit");
            return;
        }
        // Log::debug("====== addExportCompta fichier justif $documentJustif pour $compteLib ecriture $ecritureNum montant $debit, $credit");

        //Quadra
        $this->_QuadraObject->addLine($ecritureNum, $compteNum, $compteLib, $compAuxNum, $compAuxLib, $ecritureLib, $debit, $credit, $documentJustif, $ladate, $isPro);

        //FEC
        $this->_FECObject->addMiniLine($ecritureNum, $compteNum, $compteLib, $compAuxNum, $compAuxLib, $ecritureLib, $debit, $credit, $ladate, $isPro);

        //Plugins
        $this->_lignesExportCompta[] = json_encode([
            'ecritureNum'       => $ecritureNum,
            'compteNum'         => $compteNum,
            'compteLib'         => $compteLib,
            'compteAux'         => $compAuxNum,
            'compteAuxLib'      => $compAuxLib,
            'ecritureLib'       => $ecritureLib,
            'debit'             => $debit,
            'credit'            => $credit,
            'documentJustif'    => $documentJustif,
            'ladate'            => $ladate,
            'isPro'             => $isPro
        ]);
    }

    /**
     * getAllTVAfor : Regarde si pour cette note de frais, ce type de frais et ce moyen de paiement on a des Multi TVA ...
     *                Si oui, retourne un tableau avec les taux utilisés
     *
     * @param  mixed $id
     * @param  mixed $tid
     * @param  mixed $paiementID
     * @return void
     */
    public function getAllTVAfor($id, $tid, $paiementID)
    {
        $tab = array();
        $pid = implode(',', $paiementID);

        // Log::debug('====== getAllTVAfor : ' . $id . " / " . $tid . " / " . implode(',', $tab));
        //ATTENTION SQL EN DUR
        $SQL = "SELECT tvaTx1 as tx FROM lde_frais WHERE nde_frais_id='$id' AND type_frais_id='$tid' AND moyen_paiement_id IN ($pid)";
        $SQL .= " UNION SELECT tvaTx2 as tx FROM lde_frais WHERE nde_frais_id='$id' AND type_frais_id='$tid' AND moyen_paiement_id IN($pid)";
        $SQL .= " UNION SELECT tvaTx3 as tx FROM lde_frais WHERE nde_frais_id='$id' AND type_frais_id='$tid' AND moyen_paiement_id IN($pid)";
        $SQL .= " UNION SELECT tvaTx4 as tx FROM lde_frais WHERE nde_frais_id='$id' AND type_frais_id='$tid' AND moyen_paiement_id IN($pid)";
        $ta = DB::select($SQL);
        // Log::debug($SQL);

        foreach ($ta as $t) {
            if ($t->tx != null) {
                $tab[] = $t->tx;
            }
        }
        //Log::debug('====== Multi TVA : ' . implode(',', $tab));
        return $tab;
    }

    /**
     * extractTVA: Compose un tableau des differentes taxes utilisées
     *
     * @param  mixed $t1
     * @param  mixed $v1
     * @param  mixed $t2
     * @param  mixed $v2
     * @param  mixed $t3
     * @param  mixed $v3
     * @param  mixed $t4
     * @param  mixed $v4
     * @return void
     */
    private function extractTVA($t1, $v1, $t2, $v2, $t3, $v3, $t4, $v4)
    {
        $ret = array();
        if (isset($t1)) {
            $ret[$t1] = $v1;
        }
        if (isset($t2)) {
            $ret[$t2] = $v2;
        }
        if (isset($t3)) {
            $ret[$t3] = $v3;
        }
        if (isset($t4)) {
            $ret[$t4] = $v4;
        }
        return $ret;
    }

    /**
     * computeCompta :
     *                  Calcul comptable de la ligne, export_compta=1 si on veut créer la ligne dans le fichier d'export comptable
     *                  $is_pro = 1 alors on fait la contrepartie en 401 sinon pas de contrepartie c'est un perso avec la globalisation totale
     *                  (export_compta=0 par exemple pour les frais perso: l'écriture comptable est globalisée "fin de mois" ../ ou pas)
     *                  si is_pro=1 c'est que ça a ete paye avec un moyen de paiement de l'entreprise donc normalement c'est une facture fournisseur "classique"
     *
     * @param  mixed $ligne
     * @param  mixed $is_pro
     * @param  mixed $export_compta
     * @param  mixed $typeFraisSlug
     * @param  mixed $typeFraisTXT
     * @return void
     */
    private function computeCompta($ligne, $is_pro, $export_compta, $typeFraisSlug, $typeFraisTXT)
    {
        $u = $this->_user;
        $ladate = Carbon::parse($ligne->ladate);

        // Log::debug("=================================================================================================================================================================");
        // Log::debug("====== computeCompta : pro=$is_pro et export=$export_compta | $typeFraisSlug | $typeFraisTXT");
        $local_compAuxNum = "";
        $local_compAuxLib = "";
        $local_fichierJoint = $ligne->fileName;
        $local_totalTTC = $ligne->ttc;
        $local_totalHT = $ligne->ht;
        $local_totalTVA = 0;

        $local_typeFraisTXT = $typeFraisTXT;

        //Pas de récupération de TVA sur Hotel, Train, Taxi...
        if ($typeFraisSlug == "taxi" || $typeFraisSlug == "hotel" || $typeFraisSlug == "train") {
            $ligne->ht = 0;
            $local_totalHT = 0;
            //Si on a des montants de TVA enregistrés on les vire
            $ligne->tvaVal1 = 0;
            $ligne->tvaVal2 = 0;
            $ligne->tvaVal3 = 0;
            $ligne->tvaVal4 = 0;
            $ligne->$local_totalTVA = 0;
        }

        //On peut aussi avoir des multi taux de tva ... exemple resto et divers
        if ($typeFraisSlug == "restauration" || $typeFraisSlug == "divers") {
            //dans ces deux cas on ne permet pas d'entrer le montant ht, si il est présent c'est une erreur
            // $ligne->ht = 0;
            // if ($local_totalHT == 0) {
            $local_totalTVA = $ligne->tvaVal1 + $ligne->tvaVal2 + $ligne->tvaVal3 + $ligne->tvaVal4;
            $local_totalHT = $local_totalTTC - $local_totalTVA;
            $ligne->ht = $local_totalHT;
            $ligne->totalTVA = $local_totalTVA;
            // }
        }

        //peage et parking ...
        if ($typeFraisSlug == "peage") {
            $local_totalTVA = $ligne->tvaVal1 + $ligne->tvaVal2 + $ligne->tvaVal3 + $ligne->tvaVal4;
            if ($local_totalTVA == 0) {
                if ($ligne->ht > 0) {
                    $ligne->totalTVA = $ligne->ttc - $ligne->ht;
                    $local_totalTVA = $ligne->totalTVA;
                } else {
                    $local_totalTVA = 0;
                    $ligne->totalTVA = 0;
                    $ligne->ht = $ligne->ttc;
                }
            } else {
                $ligne->totalTVA = $local_totalTVA;
                $ligne->ht = $ligne->ttc - $local_totalTVA;
            }
        }

        //Et pour finir le super cas particulier du carburant
        $fraisCarburant = false;
        if ($typeFraisSlug == "carburant") {
            $fraisCarburant = true;
            $carbu = $ligne->extractVehiculeCarburant();
            $is_util = $ligne->extractVehiculeIsUtilitaire();
            //Sur les tickets de carburant la TVA est à 20% le montant HT est donc facile a calculer
            //TODO prévoir un changement de taux de TVA un jour ...
            $ht = round($ligne->ttc / 1.2, 2);

            $local_totalTVA = $this->carburantCalculTVARecuperable($ligne->ttc, $ht, $carbu, $is_util);
            $ligne->totalTVA = $local_totalTVA;

            $carbuTauxTVArecup = $this->carburantTauxTVArecuperableTourisme($carbu);
            $l = substr($ligne->label,0,5) . " ";
            if ($is_util) {
                $l .= "VU,";
                $carbuTauxTVArecup = $this->carburantTauxTVArecuperableUtilitaire($carbu);
            } else {
                $l .= "VP,";
            }
            $l .= substr($ligne->extractVehiculeNom(),0,6) . "," . $carbu;
            $ligne->label = $l;

            //Mais au niveau de l'export comptable ce qu'on "considère" comme le HT est en fait le %x du TTC correspondant au carburant
            //donc attention le nom de la variable "ht" ici est détournée ... DANGER !!! c'est uniquement pour beneficier de la factorisation du code generique
            //note sept 2020 : inversion avec $tvaNONrecup
            $local_totalHT = round($ht * $carbuTauxTVArecup, 2);

            //Sur le PDF qu'on exporte il faut bien indiquer le HT normal
            $ligne->ht = $ht;

            //la bonne valeur c'est x% du montant HT le x étant lié au type de carburant
            $tvaNONrecup = round($ligne->ttc * (1 - $carbuTauxTVArecup), 2);
            // Log::debug("TVA pour carburant : 0CBISA=$ligne->ttc 606140=$tvaNONrecup, 606141=$ligne->ht, HT(pour le PDF) = $local_totalHT et 445660=$local_totalTVA");

            //On change le type de frais en fonction du taux de récupération
            $typeFraisSlug .= $this->carburantGetTauxRecup($carbu, $is_util);

            Log::debug("On est sur le carburant et on modifie le typeFraisSlug pour avoir $typeFraisSlug");


            if ($export_compta && $tvaNONrecup > 0) {
                $this->addExportCompta($this->_ecritureNum, $u->getComptaCode('carburant0recup'), "CARBU TTC TVA NON RECUP", $local_compAuxNum, $local_compAuxLib, "CARBU TTC TVA NON RECUP", $tvaNONrecup, "0", $local_fichierJoint, $ladate, $is_pro);
            }
        }

        $local_labelFrais = strtoupper(Str::ascii(Str::limit($ligne->label), 20, '.'));

        // Log::debug("computeCompta : On tourne pour" . $typeFraisSlug . " et " . $local_fichierJoint);

        $local_totalTVAverif = 0;
        // Cas particulier d'une facture avec multi taux de tva ...
        //Les tva éventuelles
        $tabTVA = $this->extractTVA($ligne->tvaTx1, $ligne->tvaVal1, $ligne->tvaTx2, $ligne->tvaVal2, $ligne->tvaTx3, $ligne->tvaVal3, $ligne->tvaTx4, $ligne->tvaVal4);

        if ((count($tabTVA) > 0) && (!$fraisCarburant)) {
            foreach ($tabTVA as $tTaux => $local_tTVAmontant) {
                if ($local_tTVAmontant > 0) {
                    if ($export_compta) {
                        $this->addExportCompta($this->_ecritureNum, $u->getComptaCode('tvadeductible'), "TVA $tTaux%", $local_compAuxNum, $local_compAuxLib, $local_labelFrais, $local_tTVAmontant, "0", $local_fichierJoint, $ladate, $is_pro);
                    }
                    $local_totalTVAverif += $local_tTVAmontant;
                }
            }
        }
        if ($local_totalTVA > 0 && $local_totalTVAverif == 0) {
            if ($export_compta) {
                $this->addExportCompta($this->_ecritureNum, $u->getComptaCode('tvadeductible'), "TVA", $local_compAuxNum, $local_compAuxLib, $local_labelFrais, $local_totalTVA, "0", $local_fichierJoint, $ladate, $is_pro);
            }
        }

        //Ramasse miette ... si pas de tva et que le ht est à zéro alors HT = TTC
        if ($local_totalTVAverif == 0 && $local_totalHT == 0 && $local_totalTVA == 0) {
            $local_totalHT = $local_totalTTC;
        }

        if ($export_compta) {
            $this->addExportCompta($this->_ecritureNum, $u->getComptaCode($typeFraisSlug), $local_typeFraisTXT, $local_compAuxNum, $local_compAuxLib, $local_labelFrais, $local_totalHT, "0", $local_fichierJoint, $ladate, $is_pro);
        }

        //TODO La contrepartie ... code comptable ...
        if ($is_pro == 1) {
            $local_compAuxNum = "401" . $typeFraisSlug;
            $local_compAuxLib = $typeFraisSlug;
            if ($export_compta) {
                $this->addExportCompta($this->_ecritureNum, $u->getComptaCode('compteprocb'), "FOURNISSEURS", $local_compAuxNum, $local_compAuxLib, "$local_labelFrais", "0", $local_totalTTC, $local_fichierJoint, $ladate, $is_pro);
                $this->_ecritureNum++;
            }
        } else {
            $this->_ecritureNum++;
        }
        return $ligne;
    }


    /**
     * carburantCalculTVARecuperable
     *
     * @param  mixed $ttc montant ttc de la note de carburant
     * @param  mixed $carburant type de carburant (diesel / essence ...)
     * @param  mixed $is_vehicule_utilitaire = 1 si c'est un utilitaire
     * @return mixed : le montant de la tva récupérable
     */
    private function carburantCalculTVARecuperable($ttc, $ht, $carburant, $is_vehicule_utilitaire = false)
    {
        if ($is_vehicule_utilitaire) {
            Log::debug("carburantCalculTVARecuperable pour TTC $ttc (HT $ht) et type carburant $carburant et véhicule utilitaire");
            return round(($ttc - $ht) * $this->carburantTauxTVArecuperableUtilitaire($carburant), 2);
        }
        Log::debug("carburantCalculTVARecuperable pour TTC $ttc (HT $ht) et type carburant $carburant et véhicule particulier");
        return round(($ttc - $ht) * $this->carburantTauxTVArecuperableTourisme($carburant), 2);
    }

    /**
     * carburantTauxTVArecuperableUtilitaire
     *
     * @param  mixed $carburant
     * @return mixed : le taux de la TVA recuperable pour un Véhicule utilitaire
     */
    private function carburantTauxTVArecuperableUtilitaire($carburant)
    {
        switch ($carburant) {
            case "diesel":
                //100 % de recup
                return 1;
            case "essence":
                //progressif, cf https://www.legifiscal.fr/actualites-fiscales/2315-tva-essence-deductible-60-2020.html
                if ($this->getYear() <= "2017") {
                    return 0;
                } else if ($this->getYear() == "2018") {
                    return 0.2;
                } else if ($this->getYear() == "2019") {
                    return 0.4;
                } else if ($this->getYear() == "2020") {
                    return 0.6;
                } else if ($this->getYear() == "2021") {
                    return 0.8;
                } else if ($this->getYear() > "2021") {
                    return 1;
                }
            case "gpl":
                //100 % de recup
                return 1;
            case "gplgaz":
                //100 % de recup
                return 1;
        }
    }

    /**
     * carburantTauxTVArecuperableTourisme
     *
     * @param  mixed $carburant
     * @return mixed : le taux de la TVA recuperable pour un Véhicule de tourisme
     */
    private function carburantTauxTVArecuperableTourisme($carburant)
    {
        // Log::debug("carburantTauxTVArecuperableTourisme $carburant");
        switch ($carburant) {
            case "diesel":
                //80 % de recup
                return 0.8;
            case "essence":
                //progressif, cf https://www.legifiscal.fr/actualites-fiscales/2315-tva-essence-deductible-60-2020.html
                if ($this->getYear() <= "2016") {
                    return 0;
                } else if ($this->getYear() == "2017") {
                    return 0.1;
                } else if ($this->getYear() == "2018") {
                    return 0.2;
                } else if ($this->getYear() == "2019") {
                    return 0.4;
                } else if ($this->getYear() == "2020") {
                    return 0.6;
                } else if ($this->getYear() > "2020") {
                    return 0.8;
                }
            case "gpl":
                //100 % de recup
                return 1;
            case "gplgaz":
                //50 % de recup
                return 0.5;
        }
    }

    /**
     * getExportedFilesNames
     *
     * @return array : Listes des noms complets des fichiers exportés
     */
    public function getExportedFilesNames()
    {
        $tab[] = $this->getfilePDFFullPath();
        $tab[] = $this->getfilePDFFullPathJustificatifs();
        $tab[] = $this->_QuadraObject->getFullFileName();
        //TODO ?: activer l'export FEC en fonction des paramètres du destinataire
        // $tab[] = $this->_FECObject->getFullFileName();
        return $tab;
    }

    /**
     * getUser fonction publique pour récupérer l'utilisateur associé à cette note de frais
     *
     * @return void
     */
    public function getUser()
    {
        return $this->_user;
    }

    /**
     * getMailUser L'adresse mail de l'utilisateur
     *
     * @return void
     */
    public function getMailUser()
    {
        return $this->_user->getMail();
    }

    /**
     * getMailCompta : L'adresse mail du comptable à qui il faut envoyer les NDF
     *
     * @return void
     */
    public function getMailCompta()
    {
        return $this->_user->getMailCompta();
    }

    /**
     * getMailCopyTo L'adresse mail de la personne à mettre en copie
     *
     * @return void
     */
    public function getMailCopyTo()
    {
        return $this->_user->getMailCopyTo();
    }

    /**
     * getMonth
     *
     * @return void
     */
    public function getMonth()
    {
        return $this->_month;
    }

    /**
     * getYear
     *
     * @return void
     */
    public function getYear()
    {
        return $this->_year;
    }

    /**
     * carburantGetTauxRecup : gestion de la fiscalité du carburant
     *
     * @param  mixed $carburant
     * @param  mixed $is_vehicule_utilitaire
     * @return void
     */
    public function carburantGetTauxRecup($carburant, $is_vehicule_utilitaire = false)
    {
        // Log::debug("=========== carburantGetTauxRecup pour carburant=$carburant et is_utilitaire=$is_vehicule_utilitaire, année :" . $this->getYear());
        if ($is_vehicule_utilitaire == true) {
            switch ($carburant) {
                case "diesel":
                    //100 % de recup
                    return "100recup";
                    break;
                case "essence":
                    $c = "60recup";
                    if ($this->getYear() == "2020") {
                        $c = "60recup";
                    } else if ($this->getYear() >= "2021") {
                        $c = "80recup";
                    }
                    return $c;
                    break;
                case "gpl":
                    return "100recup";
                    break;
                case "gplgaz":
                    return "100recup";
                    break;
            }
        } else {
            switch ($carburant) {
                case "diesel":
                    return "80recup";
                    break;
                case "essence":
                    $c = "60recup";
                    if ($this->getYear() == "2020") {
                        $c = "60recup";
                    } else if ($this->getYear() >= "2021") {
                        $c = "80recup";
                    }
                    return $c;
                    break;
                case "gpl":
                    return "100recup";
                    break;
                case "gplgaz":
                    return "50recup";
                    break;
            }
        }
    }

    /**
     * getDownloadURI : Retourne un lien profond qui permet de venir télécharge le document
     *
     * @param  mixed $fullPath
     * @return void
     */
    public function getDownloadURI($fullPath)
    {
        return (config('app.url') . "/ndf/" . Crypt::encryptString($fullPath));
    }

    /**
     * getfilePDFFullPath
     *
     * @return void
     */
    public function getfilePDFFullPath()
    {
        if ($this->_ndf->_filePDFFullPath == "") {
            $finDuMois = Carbon::parse($this->_ndf->fin);
            $directory = storage_path() . "/NdeFrais/" . $this->_userEmail . "/";
            $nomPJ = $finDuMois->format("Ymd") . "-doliscan-note_de_frais-export";
            $nomPDF = $nomPJ . ".pdf";
            $this->_ndf->_filePDFFullPath = $directory . $nomPDF;
            $this->_ndf->_filePDFName = $nomPDF;
        }
        return $this->_ndf->_filePDFFullPath;
    }

    /**
     * getfilePDFName
     *
     * @return void
     */
    public function getfilePDFName()
    {
        if ($this->_ndf->_filePDFName == "") {
            $this->getfilePDFFullPath();
        }
        return $this->_ndf->_filePDFName;
    }

    /**
     * getfilePDFFullPathJustificatifs
     *
     * @return void
     */
    public function getfilePDFFullPathJustificatifs()
    {
        Log::debug("getfilePDFFullPathJustificatifs");
        if ($this->_ndf->_filePDFFullPathJustificatifs == "") {
            Log::debug("getfilePDFFullPathJustificatifs::on calcule");
            $finDuMois = Carbon::parse($this->_ndf->fin);
            $nomPJ = $finDuMois->format("Ymd") . "-doliscan-note_de_frais-export_justificatifs.pdf";
            //TODO verifier le storage_path avec Storagestorage_path('file') ...
            $directory = storage_path() . "/NdeFrais/" . $this->_userEmail . "/";
            $this->_ndf->_filePDFFullPathJustificatifs = $directory . $nomPJ;
            $this->_ndf->_filePDFNameJustificatifs = $nomPJ;
        }
        Log::debug("getfilePDFFullPathJustificatifs::on retourne " . $this->_ndf->_filePDFFullPathJustificatifs);
        return $this->_ndf->_filePDFFullPathJustificatifs;
    }

    /**
     * getfilePDFNameJustificatifs
     *
     * @return void
     */
    public function getfilePDFNameJustificatifs()
    {
        if ($this->_ndf->_filePDFNameJustificatifs == "") {
            $this->getfilePDFFullPathJustificatifs();
        }
        return $this->_ndf->_filePDFNameJustificatifs;
    }
}
