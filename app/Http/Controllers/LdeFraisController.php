<?php
/*
 * LdeFraisController.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

//Pour les tests de rotation de l'image
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;

use App\LdeFrais;
use App\TypeFrais;
use App\BaseCalculIks;
use App\MoyenPaiement;
use App\NdeFrais;
use Auth;
use Mail;
use App\Jobs\ProcessSendEmail;

class LdeFraisController extends Controller
{
    //
    public function index()
    {
        Log::debug("LdeFraisController index");
        Auth::user()->LdeFrais;
    }

    public function show($id)
    {
        Log::debug("==========LdeFraisController : show $id =========");
        //TODO dispositif pour eviter d'accéder à une ldf dont on a pas le droit ...
        $ldf = LdeFrais::findOrFail($id);

        //On ajoute deux champs composés: montant et texte pour simplifier l'affichage sur smartphone
        // if ($ldf->ht > 0 && ($ldf->ht != $ldf->ttc))
        //     $ldf->montant = "HT: " . $ldf->ht . "€";
        if ($ldf->ttc > 0) {
            if ($ldf->montant != "")
                $ldf->montant .= " / ";
            $ldf->montant .= "TTC: " . nbFR($ldf->ttc) . "€";
        }
        if ($ldf->montant == "")
            $ldf->montant = "Ce document n'a pas encore été analysé";


        if ($ldf->label != "") {
            $tf = new TypeFrais();
            $ldf->texte = $tf->getLabelFromID($ldf->type_frais_id) . " : " . $ldf->label;
        }
        if ($ldf->depart != "" && $ldf->arrivee != "" && $ldf->distance != "")
            $ldf->texte .= " " . $ldf->depart . " -> " . $ldf->arrivee . "(" . $ldf->distance . " km)";
        if ($ldf->invites != "")
            $ldf->texte .= " " . $ldf->invites;
        if ($ldf->texte == "")
            $ldf->texte = "Ce document n'a pas encore été analysé";

        //Le type de frais en toutes lettres
        $ldf->frais_slug = $ldf->typeFrais->slug;
        //Et le moyen de paiement
        $ldf->paiement_slug = $ldf->moyenPaiement->slug;

        //Est-ce qu'on a le droit de modifier cette facturette ? -> si la note n'est pas cloturée
        Log::debug("========== status : " . $ldf->ndeFrais->status . " a comparer avec " . NdeFrais::STATUS_CLOSED);
        if ($ldf->ndeFrais->status != NdeFrais::STATUS_CLOSED) {
            $ldf->isUpdatable = true;
        } else {
            $ldf->isUpdatable = false;
        }

        return $ldf;
    }

    public function store(Request $request)
    {
        Log::debug("========== LdeFraisController : store =========");

        //debug complet de la requete entrante ...
        $tmpfname = storage_path() . "/logs/" . time() . "-upload.debug";
        Log::debug('Pour le contenu complet de la requete, voir le fichier ' . $tmpfname);
        if ($fp = fopen($tmpfname, 'w')) {
            Log::debug("  ouveture ok fichier $tmpfname");
            fwrite($fp, $request);
            fwrite($fp, "\n\n");
            fwrite($fp, \json_encode($request->all()));
            fwrite($fp, "\n\n");
            fclose($fp);
        } else {
            Log::debug("  impossible d'ouvrir le fichier $tmpfname");
        }

        //Il faudrait esayer d'éviter les doublons ... donc même date, même montant, même auteur, même label ... beep
        //-> on update la ligne ? (ça arrive sur les resync)
        //Si on a affaire à des IK il faut faire une recherche un peu differente ...
        $tf = new TypeFrais();
        $LdeFrais = null;
        if ($request->input('type_frais_id') == $tf->getIDfromSlug("ik")) {
            $LdeFrais = LdeFrais::where('user_id', Auth::id())->where('ladate', $request->input('ladate'))
                ->where('depart', $request->input('depart'))
                ->where('arrivee', $request->input('arrivee'))
                ->where('distance', $request->input('distance'))
                ->where('vehicule', $request->input('vehicule'))
                ->where('type_frais_id', $request->input('type_frais_id'))
                ->where('label', $request->input('label'))->first();
        } else {
            $LdeFrais = LdeFrais::where('user_id', Auth::id())->where('ladate', $request->input('ladate'))
                ->where('ttc', $request->input('ttc'))
                ->where('label', $request->input('label'))->first();
        }
        if (null !== $LdeFrais) {
            Log::debug("==========LdeFraisController : doublon =========");
            // Log::debug(json_encode($LdeFrais));

            //On fait quoi des doublons ? si on veut autoriser l'écrasage de la photo ...
            // $LdeFrais->fileName  = "waitForUpload-" . Str::random(28) . ".jpeg";

            Log::debug("Retour code 201 avec la ligne de frais OK pour éviter le doublon...");
            return response()->json($LdeFrais, 201);
        }

        $LdeFrais = new LdeFrais();
        //Ajout d'une nouvelle note de frais
        $LdeFrais->label       = $request->input('label');
        $LdeFrais->ladate      = $request->input('ladate');
        $LdeFrais->ht          = $request->input('ht');
        $LdeFrais->ttc         = $request->input('ttc');
        $LdeFrais->tvaTx1      = $request->input('tvaTx1');
        $LdeFrais->tvaTx2      = $request->input('tvaTx2');
        $LdeFrais->tvaTx3      = $request->input('tvaTx3');
        $LdeFrais->tvaTx4      = $request->input('tvaTx4');
        $LdeFrais->tvaVal1     = $request->input('tvaVal1');
        $LdeFrais->tvaVal2     = $request->input('tvaVal2');
        $LdeFrais->tvaVal3     = $request->input('tvaVal3');
        $LdeFrais->tvaVal4     = $request->input('tvaVal4');
        $LdeFrais->depart      = $request->input('depart');
        $LdeFrais->arrivee     = $request->input('arrivee');
        $LdeFrais->distance    = $request->input('distance');
        $LdeFrais->invites     = $request->input('invites');
        $LdeFrais->vehicule    = $request->input('vehicule');
        $LdeFrais->vehiculecv  = $request->input('vehiculecv');
        $LdeFrais->user_id     = Auth::id();

        //On stoppe si on a pas le type de Frais
        Log::debug('On cherche TypeDeFrais : ' . $request->input('typeFrais'));
        if ($request->input('typeFrais') == "") {
            Log::debug('TypeDeFrais vide, early break');
            return;
        }

        //Calcul du HT ou du TTC en fonction des données fournies
        if ($LdeFrais->ht == 0 || is_null($LdeFrais->ht)) {
            $LdeFrais->ht = $LdeFrais->ttc - ($LdeFrais->tvaVal1 + $LdeFrais->tvaVal2 + $LdeFrais->tvaVal3 + $LdeFrais->tvaVal4);
            if ($LdeFrais->ht > $LdeFrais->ttc) {
                //TODO problème à gérer
            }
        }


        //Gestion de l'upload d'un fichier
        $filename  = "";
        $filecheck = "";


        Log::debug('======= STORE (request) ============');
        Log::debug($request);
        Log::debug('======= STORE (LdeFrais) ============');
        Log::debug(json_encode($LdeFrais));
        Log::debug('===================');

        //Note:
        //A une époque on expédiait le fichier dans le même paquet, voir plus bas pour la nouvelle gestion a
        //base de resumable.js ...
        $ladate          = Carbon::createFromFormat('Y-m-d', $LdeFrais->ladate);
        $ladateSub       = $ladate->format('Ym');
        $directory       = storage_path() . "/LdeFrais/" . Auth::user()->email . "/" . $ladateSub . "/";
        if (!is_dir($directory)) {
            mkdir($directory, 0770, true);
        }

        if (!is_null($request->input('afile'))) {
            Log::debug('======= STORE - UPLOAD FICHIER ============');
            $image = $request->input('afile');  // your base64 encoded
            //list($type, $image) = explode(';', $image);
            //list(, $image)      = explode(',', $image);
            //$imageType          = explode('/', $type)[1];
            //20190614 on a un seul fichier jpeg qui arrive

            $imageType = "jpeg";

            $filename        = $ladate->format('Ymd') . '-' . Str::limit(Str::slug($request->input('typeFrais'), 10)) . "-" . Str::limit(Str::slug($LdeFrais->label), 20) . '-' . Str::random(10) . '.' . $imageType;
            $filenameTmp     = $ladate->format('Ymd') . '-' . Str::limit(Str::slug($request->input('typeFrais'), 10)) . "-" . Str::limit(Str::slug($LdeFrais->label), 20) . '-' . Str::random(10) . '-TEMPORARY.' . $imageType;
            $fullFilename    = $directory . $filename;
            $fullFilenameTmp = $directory . $filenameTmp;
            Log::debug("Stockage du fichier dans $directory et nom de fichier aleatoire $filename donc nom complet $fullFilename");

            //\File::put( $fullFilename, base64_decode($image));
            //Il faut decoder l'image (bytearray from javascript Uint8Array)
            $imgOut = "";
            $tab = explode(",", $image);
            for ($n = 0; $n < count($tab); $n++) {
                $b = pack("C*", $tab[$n]);
                $imgOut .= $b;
            }
            \File::put($fullFilenameTmp, $imgOut);

            //On optimize et le résultat est envoyé dans le nom final
            ImageOptimizer::optimize($fullFilenameTmp, $fullFilename);

            //On verifie s'il faut faire une rotation de l'image à l'aide de tesseract
            //fausse bonne idée, on espère que l'utilisateur a fait le job !
            // $cmd = ["/usr/bin/tesseract", "$fullFilenameTmp", "stdout", "--psm", "0", "-l", "osd", "--oem", "0"];
            // Log::debug("======= Onlance : $cmd");

            // $process = new Process($cmd);
            // $process->run();

            // executes after the command finishes
            // if (!$process->isSuccessful()) {
            //erreur, commande n'existe pas ou autre, on sauvegarde ce fichier
            // rename($fullFilenameTmp, $fullFilename);
            // Log::debug("======= Erreur de lancement de tesseract");
            // } else {
            //tesseract fonctionne on essaye de prendre la valeur de rotation de l'image pour la tourner comme il faut
            /*
          Warning. Invalid resolution 0 dpi. Using 70 instead.
          Estimating resolution as 422
          Page number: 0
          Orientation in degrees: 270
          Rotate: 90
          Orientation confidence: 5.00
          Script: Latin
          Script confidence: 5.64
        */
            //     $retour = $process->getOutput();
            //     Log::debug("======= Retour de tesseract : $retour");
            //     preg_match_all("/Orientation in degrees:(.*)/", $retour, $tabOut);
            //     $rotation = trim($tabOut[1][0]);
            //     Log::debug("======= Rotation : $rotation");
            //     if ($rotation > 0) {
            //         Log::debug("======= Lancement de la rotation de : $rotation");
            //         // Load
            //         $source = imagecreatefromjpeg($fullFilenameTmp);
            //         // Rotate
            //         $rotate = imagerotate($source, $rotation, 0);
            //         // Output
            //         imagejpeg($rotate, $fullFilename);
            //         // Free the memory
            //         imagedestroy($source);
            //         imagedestroy($rotate);
            //         // Remove temp file
            //         unlink($fullFilenameTmp);
            //         Log::debug("======= rotation terminée");
            //     } else {
            //         Log::debug("======= rotation 0");
            //         rename($fullFilenameTmp, $fullFilename);
            //     }
            // }

            $filecheck = sha1_file($fullFilename);
            $LdeFrais->fileName  = $filename;
            $LdeFrais->fileCheck = $filecheck;

            Log::debug("Nom du fichier : $filename");
        } else {
            //On est peut-être en mode "resume.js" ... alors on créé un nom de fichier aléatoire imprévisible et on le retourne
            //pour que le resume.js nous l'envoie par la suite ...
            $LdeFrais->fileName  = "waitForUpload-" . Str::random(28) . ".jpeg";
        }

        //TODO entre le 1 et le 5 on accepte encore des frais du mois précédent
        //On essaye de faire que si on est entre le 1 et le 5 du mois et qu'on a un frais qui concerne la note de frais du mois précédent
        //on l'y rajoute
        $now = Carbon::now();
        $jour = $now->day;
        $mois = $now->month;
        $ndf = null;
        if (($jour <= 5) && ($ladate->addMonth()->month <= $mois)) {
            $ndf = Auth::user()->NdeFrais()->orderBy('id', 'DESC')->where('status', NdeFrais::STATUS_FREEZED)->first();
        }
        if (is_null($ndf)) {
            //La note de frais en cours: la dernière "ouverte" et si pas de ndf ouverte on en créé une pour le mois en cours ...
            $ndf = Auth::user()->NdeFrais()->orderBy('id', 'DESC')->where('status', NdeFrais::STATUS_OPEN)->first();
            if (is_null($ndf)) {
                $ndf = new NdeFrais();
                $ndf->makeNew(Auth::user()->id);
                Log::debug("Nouvelle note de frais disponible : id " . $ndf->id);
            }
            // $ndf = Auth::user()->NdeFrais()->orderBy('id', 'DESC')->where('status', NdeFrais::STATUS_OPEN)->first();
        }
        $LdeFrais->nde_frais_id = $ndf->id;

        //Si on a pas la cylindrée mais qu'on a la fiche d'identité du véhicule on en extrait la cylindrée
        if (is_null($LdeFrais->vehiculecv) && !is_null($LdeFrais->vehicule)) {
            preg_match("/.*;(\d)cv;.*/", $LdeFrais->vehicule, $res);
            $LdeFrais->vehiculecv = $res[1];
        }

        //Si la cylindrée n'est pas vide et qu'on a du km on essaye de calculer les IK
        if (!is_null($LdeFrais->vehiculecv) && !is_null($LdeFrais->distance)) {
            $b = new BaseCalculIks();
            $totalKMthisYear = $LdeFrais->getKMforVehiculeThisYear(Auth::user()->id,  $LdeFrais->vehicule, date('Y'), date('Y-m-d'), $LdeFrais->nde_frais_id);
            $LdeFrais->ttc   = $b->calcul($LdeFrais->vehiculecv, $LdeFrais->distance, $totalKMthisYear, false, null, $LdeFrais->extractVehiculeAMC($LdeFrais->vehicule));
        }

        $tf = new TypeFrais();
        $LdeFrais->type_frais_id = $tf->getIDfromSlug($request->input('typeFrais'));

        $mp = new MoyenPaiement();
        if ($request->input('typeFrais') == "ik") {
            $LdeFrais->moyen_paiement_id = $mp->getIDfromSlug("pseudo-perso-ik");;
        } else {
            //Rechercher dans la base ce qui correspond a cette etiquette
            $LdeFrais->moyen_paiement_id = $mp->getIDfromSlug($request->input('moyenPaiement'));;
        }
        if (is_null($LdeFrais->moyen_paiement_id)) {
            $LdeFrais->moyen_paiement_id = 1;
        }

        Log::debug("  ================ LdeFrais save ================ ");
        Log::debug($LdeFrais);
        $LdeFrais->save();
        // Log::debug($LdeFrais);
        //debug sql ?

        // $LdeFrais = LdeFrais::create($request->all());

        //Petite notification pour aller vérifier que tout se passe bien (phase de prelancement)
        $details = array(
            'to' => 'ping@doliscan.fr',
            'subject' => "doliscan: Nouvelle entrée",
            'message' => "A verifier, une nouvelle facturette viens d'arriver: \n\n" . $LdeFrais->toText() . "\n\n--\n" . env('APP_URL')
        );
        ProcessSendEmail::dispatch($details);

        Log::debug("Retour code 201 avec la ligne de frais OK...");
        return response()->json($LdeFrais, 201);
    }

    public function update(Request $request, int $id)
    {
        //TODO dispositif pour eviter de modifier une Ldf différente de celle qui est demandée
        //par exemple corruption de données anciennes, écrasement de ldf d'un autre utilisateur
        Log::debug(" ============= update LdeFrais $id ============== ");

        $l = LdeFrais::findOrFail($id);
        $l->update($request->all());

        return response()->json($l, 200);
    }

    public function delete(LdeFrais $LdeFrais, int $id)
    {
        Log::debug(" ============= delete LdeFrais ============== " . $LdeFrais->id);
        //TODO dispositif pour eviter de modifier une Ldf différente de celle qui est demandée
        //par exemple corruption de données anciennes, écrasement de ldf d'un autre utilisateur
        if (is_null($LdeFrais->id)) {
            Log::debug("  LdeFrais null ");
            $l = LdeFrais::findOrFail($id);
            $l->delete();
        } else {
            Log::debug("  LdeFrais not null ");
            $LdeFrais->delete();
        }
        // $l = LdeFrais::find($id);
        // $l->delete();
        Log::debug(" ============= delete LdeFrais end ============== ");
        return response()->json(array("message" => "DELETE OK"), 200);
    }

    public function webLdFDeleteGet(int $id)
    {
        //TODO dispositif pour eviter de modifier une Ldf différente de celle qui est demandée
        //par exemple corruption de données anciennes, écrasement de ldf d'un autre utilisateur
        Log::debug("===================webLdFDeleteGet $id");
        $l = LdeFrais::findOrFail($id);
        // Log::debug($l);
        $l->delete();

        return back();
    }

    public function webLdFUpdateGet(int $id)
    {
        Log::debug("===================webLdFUpdateGet $id");
        // Log::debug($tabID);
        // Log::debug('===================');
        $l = LdeFrais::findOrFail($id);
        //        Log::debug($l);

        $previous = LdeFrais::where('nde_frais_id', '=', $l->nde_frais_id)->where('id', '<', $id)->orderBy('id', 'desc')->first();
        $next     = LdeFrais::where('nde_frais_id', '=', $l->nde_frais_id)->where('id', '>', $id)->orderBy('id', 'asc')->first();
        $previousID = 0;
        $nextID     = 0;
        if ($previous) {
            $previousID = $previous->sid;
        }
        if ($next) {
            $nextID = $next->sid;
        }
        return view('webldfupdate', [
            'ldf'       => $l,
            'previous'  => $previousID,
            'next'      => $nextID,
            'userEmail' => $l->user->email,
        ]);
    }
    public function webLdFUpdatePost(Request $request, int $id)
    {
        //TODO dispositif pour eviter de modifier une Ldf différente de celle qui est demandée
        //par exemple corruption de données anciennes, écrasement de ldf d'un autre utilisateur
        Log::debug("===================webLdFUpdatePost $id");
        // Log::debug($tabID);
        // Log::debug('===================');
        $l = LdeFrais::findOrFail($id);
        if ($l) {
            $l->label       = $request->input('label');
            $l->ladate      = $request->input('ladate');
            $l->ht          = $request->input('ht');
            $l->ttc         = $request->input('ttc');
            // $l->tvaTx1      = $request->input('tauxtva1');
            // $l->tvaTx2      = $request->input('tauxtva2');
            // $l->tvaTx3      = $request->input('tauxtva3');
            // $l->tvaTx4      = $request->input('tauxtva4');
            $l->tvaVal1     = $request->input('tva1');
            $l->tvaVal2     = $request->input('tva2');
            $l->tvaVal3     = $request->input('tva3');
            $l->tvaVal4     = $request->input('tva4');
            $l->depart      = $request->input('depart');
            $l->arrivee     = $request->input('arrivee');
            $l->distance    = $request->input('distance');
            $l->invites     = $request->input('invites');
            $l->vehicule    = $request->input('vehicule');
            $l->vehiculecv  = $request->input('vehiculecv');

            $l->tvaTx1      = 0;
            $l->tvaTx2      = 5.5;
            $l->tvaTx3      = 10;
            $l->tvaTx4      = 20;


            $l->save();
            Log::debug($l);
        }
        return $this->webLdFUpdateGet($id);
    }

    //Ajoute une nouvelle regule d'ik... sauf si déjà présente
    //date, raison (bareme ou tranche), montant ttc, vehicule
    public function newReguleIK($ladate, $label, $ttc, $vehicule, $vehiculecv, $ndfid, $userid)
    {
        Log::debug("===================newReguleIK");
        $l = new LdeFrais();
        $mp = new MoyenPaiement();

        //L'ID des types de frais speciaux "regule"
        $idTypeFraisIKRegule = TypeFrais::where('slug', 'ik-regule')->pluck('id')->first();
        $idMoyenPaiement     = $mp->getIDfromSlug("pseudo-perso-ik");

        $testDoublon = LdeFrais::where('nde_frais_id', $ndfid)->where('user_id', $userid)->where('type_frais_id', $idTypeFraisIKRegule)->where('moyen_paiement_id', $idMoyenPaiement)
            ->where('label', $label)->count();

        if ($testDoublon == 0) {
            //Dans l'ideal il faudrait eviter de faire un doublon si on a déjà inséré cette regule
            $l->label               = $label;
            $l->ladate              = $ladate;
            $l->ttc                 = $ttc;
            $l->vehicule            = $vehicule;
            $l->vehiculecv          = $vehiculecv;
            $l->nde_frais_id        = $ndfid;
            $l->user_id             = $userid;
            $l->moyen_paiement_id   = $idMoyenPaiement;
            $l->type_frais_id       = $idTypeFraisIKRegule;
            $l->save();
        }
    }

    //Ajoute une nouvelle regule d'ik... sauf si déjà présente
    //date, raison (bareme ou tranche), montant ttc, vehicule
    public function geoDist(Request $request)
    {
        Log::debug("===================geoDist");
        $depart  = $request->depart;
        $arrivee = $request->arrivee;
        $l = LdeFrais::where('user_id', Auth::id())
            ->where('depart', '=', $depart)
            ->where('arrivee', '=', $arrivee)
            ->orderBy('ladate', 'desc')
            ->first();
        if ($l) {
            $r = array();
            $r["depart"] = $depart;
            $r["arrivee"] = $arrivee;
            $r["distance"] = $l->distance;
            $r["ladate"] = $l->ladate;
            return response()->json($r, 201);
        }
    }
}
