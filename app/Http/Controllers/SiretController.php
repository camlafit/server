<?php
/*
 * SiretController.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Log;

class SiretController extends Controller
{
    //
    public function search(Request $request)
    {
        Log::debug('========SiretController search===========');
        // Log::debug($request);

        $siren = substr($request['siret'],0,9);

        Log::debug('======== on cherche ' . $siren);

        //On propage la question au webservice ad hoc
        //http://192.168.16.126/api/search/520339938
        $client = new \GuzzleHttp\Client();

        $res = $client->request('GET', 'https://siret.cap-rel.fr/api/search/' . $siren);

        //{"verb":"search","query":"450656731","dateCreationEtablissement":"2008-05-15","adresse":"21 AV EUGENE ET MARC DULOUT","cp":"33600","ville":"PESSAC","pays":"FRANCE","name":"RYXEO"}
        Log::debug($res->getStatusCode());
        Log::debug($res->getBody());
        $name = "";
        if ($res->getStatusCode() == 200) {
            return $res->getBody();
        } else {
            return;
        }
        Log::debug('===================');
    }
}
