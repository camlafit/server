<?php
/*
 * LoginController.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers\Auth;

use App;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use App\Entreprise;
use Illuminate\Support\Facades\DB;
use App\Passport\PassToken;
use App\Passport\PassClient;
use App\Passport\AuthCode;
use Laravel\Passport\Passport;
use Lcobucci\JWT\Parser;


class LoginController extends Controller
{
    /*
  |--------------------------------------------------------------------------
  | Login Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles authenticating users for the application and
  | redirecting them to your home screen. The controller uses a trait
  | to conveniently provide its functionality to your applications.
  |
  */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout')->except('webLeaveImpersonate');
    }

    public function index()
    {
        return response()->json(\Auth::user());
    }

    public function login(Request $request)
    {
        activity('User')->log("New user login request for " . $request->email);

        Log::debug('========LoginController::login=========== ' . $request->email);
        // Log::debug($request);
        // Log::debug('===================');
        $this->validateLogin($request);

        if ($this->attemptLogin($request)) {
            Log::debug("  login success :-)");
            $user = $this->guard()->user();
            $user->generateToken();

            $user->nomComplet = $user->firstname . " " . $user->name;

            //Je ne sais pas si c'est une bonne idée ... a creuser
            $user->kmLastYear = $user->CalculDistanceAnnuelle(date('Y') - 1);
            $user->kmThisYear = $user->CalculDistanceAnnuelle(date('Y'));

            $link = $this->getUALink($request);

            //Si on fait une grosse modif du serveur qui necessite un upgrade du client
            $user->api_version = Config::get('constants.api.version');
            $user->api_version_message = "Votre application n'est pas compatible avec la version installée sur le serveur. Veuillez mettre à jour votre application depuis $link.";

            $user->official_app_message = "Une nouvelle version de l'application est disponible, installez-la depuis $link";
            $user->official_app_version = Config::get('constants.app.version');

            //   Log::debug('======== login ===========');
            //   Log::debug($user);

            return response()->json([
                'data' => $user->toArray(),
            ]);
        }
        Log::debug("  login error :-)");
        return $this->sendFailedLoginResponse($request);
    }

    public function showIndex()
    {
        if ($this->guard()->user()) {
            return view('home');
        }
        return view('welcome');
    }

    public function webLogin(Request $request)
    {
        activity('User')->log("New user login request for " . $request->email);

        //TODO
        /*
        activity('User')
            ->performedOn($user)
            ->causedBy($user)
            ->withProperties(['location' => 'backend login form'])
            ->log("New user login request for " . $user->email);
        */

        // Log::debug('========webLogin===========');
        // Log::debug($request);
        // Log::debug('===================');

        $this->validateLogin($request);

        if ($this->attemptLogin($request)) {
            $user = $this->guard()->user();
            //Pas de token web sinon on provoque le logout du client mobile/api
            //$user->generateToken();
            $user->nomComplet = $user->firstname . " " . $user->name;
        }

        return redirect('/home');
    }

    public function webLeaveImpersonate(Request $request)
    {
        Log::debug("=================== LoginController::webLeaveImpersonate =================");
        \Auth::user()->leaveImpersonation();
        return redirect(Config::get('sharp.custom_url_segment'));
    }


    public function logout(Request $request)
    {
        activity('User')->log("User logout for " . $request->email);
        $user = $this->guard()->user();

        if ($user) {
            $user->api_token = null;
            $user->save();
        }

        return response()->json(['data' => 'User logged out.'], 200);
    }

    /* check if this api Token is always usable */
    public function ping(Request $request)
    {
        activity('User')->log("User ping for " . $request->email);

        $token = $request->bearerToken();
        //Durant la transition passport
        if ($token == "") {
            if (isset($_SERVER['HTTP_AUTHORIZATION'])) {
                $headerRAW = $_SERVER['HTTP_AUTHORIZATION'];
                if (Str::startsWith($headerRAW, 'Bearer ')) {
                    $token = Str::substr($headerRAW, 7);
                }
            }
        }

        $email = $request->email;
        Log::debug("===================LoginController::ping token $token pour $email");
        /* via la clé d'api historique */
        $user = User::where([
            ['api_token', '=', $token],
            ['email',     '=', $email],
        ])->first();

        /* on essaye sur passport */
        if (!isset($user)) {
            $tokenID = (new Parser())->parse($token)->getClaim('jti');
            Log::debug('   tokenID: ' . $tokenID);
            //On essaye de voir dans passport
            $token = PassToken::findOrFail($tokenID);
            Log::debug('   token: ' . \json_encode($token));
            $user = User::findOrFail($token->user_id)->first();
            Log::debug('   user: ' . \json_encode($user));
            // return $u;
        }

        //Log::debug($user);
        if (isset($user)) {
            Log::debug("  ping success :-)");
            $link = $this->getUALink($request);

            $user->api_version = Config::get('constants.api.version');
            $user->api_version_message = "Votre application n'est pas compatible avec la version installée sur le serveur. Veuillez mettre à jour votre application depuis $link";

            $user->official_app_message = "Une nouvelle version de l'application est disponible, installez-la depuis $link";
            $user->official_app_version = Config::get('constants.app.version');

            return response()->json([
                'data' => $user->toArray(),
            ]);
        } else {
            Log::debug("  ping error :-(");
        }
        return response()->json(['error' => 'Unauthenticated'], 401);
        // return $this->sendFailedLoginResponse($request);
    }

    /* just say 'hello' */
    public function hello(Request $request)
    {
        activity('User')->log("Anonymous hello for " . $request->email);
        $email = $request->email;
        Log::debug("===================LoginController::hello pour $email");
        //Access-Control-Allow-Origin: *
        return response()->json(['message' => 'Hello world !'], 200);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return response()->json(['error' => 'Unauthenticated'], 401);
    }

    /* mise à jour des informations */
    public function update(Request $request)
    {
        Log::debug('======== LoginController::update ===========');
        Log::debug($request);
        Log::debug('===================');
        $user = $this->guard()->user();
        if ($user) {
            Log::debug('======== update in progress ... ===========');
            //$user->update($request->all());
            $user->fill($request->all());
            $user->save();
            Log::debug("======== update km before DS: " . $request->kmBeforeDS);
        }
        return response()->json($user, 200);
    }

    private function getUALink($request)
    {
        //La dernière version disponible de l'application ...
        //Note: on peut recupérer l'os utilisé dans la signature de l'application ...
        $ua = $request->header('User-Agent');
        $link = "";
        if (Str::contains($ua, 'Android')) {
            $link = "<a href=\"market://details?id=fr.caprel.doliscan\">Google Play</a> ou votre magasin d'applications.";
        } else {
            $link = "<a href='itms-apps://itunes.apple.com/fr/app/id1455241946' target='_system'>Apple iTunes</a>.";
        }
        $link .= " <i>(cliquez sur le lien pour accéder directement à la mise à jour)</i>";
        return $link;
    }


    /**
     * store: creation d'un nouveau compte
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        Log::debug("==========LoginController::store (create new account) =========");
        Log::debug($request);
        $code = 401;
        $u = null;
        //Avons-nous le droit de créer un compte ?
        if (sharp_user()->hasPermissionTo('create User')) {

            //Et si on a déjà un compte actif ? on le retourne
            //pas ->where('creator_id', \sharp_user()->id)
            //car on peut avoir un compte qui a ete créé par un autre "auteur"
            $u = User::where('email', $request->email)->first();
            if ($u) {
                Log::debug(" Le compte existe déjà " . json_encode($u));
                $code = 200;
            } else {
                //On ajoute le lien vers le createur du compte
                $request->merge(['creator_id' => \sharp_user()->id]);
                $request->merge(sharp_user()->getComptaConfig());

                $validatedData = $request->validate([
                    'firstname' => 'required|max:255',
                    'name' => 'required',
                    'email' => 'required|email|unique:users',
                ]);
                Log::debug(" Creation du compte " . json_encode($request->all()));
                $u = new User($request->all());
                $u->save();
                //Si on est sur un serveur de tests / demo il faudrait injecter des données de tests / démo au passage
                if (env('APP_ENV') != 'prod') {
                    $u->ajouteDataDemo();
                }
                $u->envoyerMailInvitation();
                Log::debug("Retour code 201 avec le compte créé : " . json_encode($u));
                $code = 201;
            }

            //$u existe, on essaye de l'associer a une entreprise
            //Amelioreation, si le createur du compte n'a qu'une entreprise on ajoute ce nouveau compte dans l'entreprise en question
            $roleid = $u->setMainRole('utilisateur');
            $entreprises_possibles = Entreprise::getMyEntreprises()->pluck('id')->toArray();
            if (count($entreprises_possibles) == 1) {
                Log::debug("Association de l'utilisateur dans l'entreprise : " . $entreprises_possibles[0]);
                $u->addRoleOnEntreprise($roleid, $entreprises_possibles[0]);
            }

            //Autre idée si on a passé le code SIRET dans la requete
            if (isset($request->siret)) {
                //Log::debug("SIRET communiqué, association de l'utilisateur dans l'entreprise ..." . preg_replace('/\D/', '', $request->siret));
                $cleanSIRET = preg_replace('/\D/', '', $request->siret);
                $e = Entreprise::firstWhere('siret', $cleanSIRET);
                if ($e) {
                    Log::debug("SIRET communiqué, association de l'utilisateur dans l'entreprise ($cleanSIRET) : " . $e->id);
                    $u->addRoleOnEntreprise($roleid, $e);
                }
            }
        } else {
            $code = 401;
        }

        //Au passage on retourne la clé API de l'utilisateur et pas la clé API via le nouveau module passport ...
        //On ameliore donc le post possible si on demande au passage la création d'une clé passport
        if (isset($request->askForAPI) && isset($request->askForAPIAppName)) {
            Log::debug("On demande une clé d'accès à l'API pour " . $request->askForAPIAppName);
            $appName = $request->askForAPIAppName;
            //L'app cliente existe peut-être déjà
            $client = PassClient::where('user_id', $u->id)->where('name', $appName)->where('revoked', 0)->first();

            if ($client && isset($client->id)) {
                Log::debug("PassClient existant !");
            } else {
                Log::debug("PassClient à créer...");
                $client = new PassClient([
                    'id'         => Str::uuid(),
                    'secret'     => Str::random(40),
                    'created_at' => now(),
                    'updated_at' => now(),
                    'user_id'    => $u->id,
                    'name'       => $appName,
                    'redirect'   => route('passport.callback'),
                ]);
                $client->save();
                //re read
                $client = PassClient::where('user_id', $u->id)->where('name', $appName)->where('revoked', 0)->first();
            }
            Log::debug("PassClient : " . \json_encode($client));
            Log::debug("PassClientSecret : " . $client->secret);
            Log::debug("Debug Guzzle :
                        'client_id'     = $client->id
                        'user_id'       = $u->id
                        'client_secret' = $client->secret
                        'name'          = $appName
                        'scope'         = ");

            $guzzle = new \GuzzleHttp\Client;
            $response = $guzzle->post(App::make('url')->to('/oauth/token'), [
                'form_params' => [
                    'grant_type'    => 'client_credentials',
                    'client_id'     => $client->id,
                    'user_id'       => $u->id,
                    'client_secret' => $client->secret,
                    'name'          => $appName . "Token",
                    'scope'         => '',
                ],
                'http_errors' => true
            ]);

            // S'il y a une erreur, on enregistre l'erreur dans les logs
            if ($response->getStatusCode() < 200 || $response->getStatusCode() >= 300) {
                $errors = (array) json_decode($response->getBody());
                Log::debug($response->getBody());
                abort($response->getStatusCode());
            }

            $res = json_decode((string) $response->getBody(), true);
            $cle = $res['access_token'];
            Log::debug($response->getBody());

            $token = PassToken::where('client_id', $client->id)->first();
            $tab['name']      = $appName . "Token";
            $tab['user_id']   = $u->id;
            $tab['client_id'] = $client->id;
            $token->update($tab);
            //Et on "passe" cette clé d'API au client à la place de la clé API standard de l'utilisateur
            $u->api_token = $cle;
        }

        // $token = PassToken::where('client_id', $clientId)->where('user_id', $u->id)->where('name', $appName . "Token")->first();
        // if ($token && isset($token->id)) {
        //     Log::debug("Token existant !");
        // } else {
        //     $token = new PassToken();
        //     $token->save();
        // }
        // }
        Log::debug("==========LoginController::store (end) =========");
        return response()->json($u, $code);
    }

    private function functionName()
    {
        throw new Exception('Method not implemented');
    }
}
