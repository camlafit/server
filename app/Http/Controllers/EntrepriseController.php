<?php
/*
 * EntrepriseController.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Log;

use App\Entreprise;
use App\User;
use Illuminate\Support\Str;

class EntrepriseController extends Controller
{
    public function index()
    {
        Log::debug("======EntrepriseController : index =============");
        return Entreprise::getMyEntreprises();
    }

    public function update(Request $request)
    {
        Log::debug(" ============= EntrepriseController : update ============== ");
        $e = null;
        $code = 500;
        $entreprises_possibles = Entreprise::getMyEntreprises()->pluck('id')->toArray();
        Log::debug(" Liste des entreprises possibles " . implode(",", $entreprises_possibles));
        if (count($entreprises_possibles) == 1) {
            // Log::debug(" Une seule possible ... on modifie si on a le droit");
            if (sharp_user()->hasPermissionTo('edit Entreprise', 'web')) {

                //On vire les espaces éventuels du SIRET
                $request->merge(['siret' => preg_replace('/\s+/', '', $request->siret)]);

                $validatedData = $request->validate([
                    'name' => 'required|max:255',
                    'adresse' => 'required',
                    'cp' => 'required|numeric',
                    'ville' => 'required',
                    'pays' => 'required',
                    'web' => 'required',
                    'tel' => 'required',
                    'siret' => 'required|unique:entreprises',
                    'email' => 'required|email',
                ]);

                Log::debug(" modification ok pour : ");
                Log::debug($request);
                $e = Entreprise::findOrFail($entreprises_possibles[0]);
                $e->update($request->all());
                $code = 200;
            } else {
                Log::debug(" cet utilisateur n'a pas le droit de mettre à jour l'entreprise.");
                $code = 403;
            }
        } else {
            $code = 404;
        }
        return response()->json($e, $code);
    }
}
