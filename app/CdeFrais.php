<?php
/*
 * CdeFrais.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/*
 *
 * Classeur de Frais : regroupement de notes de frais d'une entreprise (plusieurs utilisateurs)
 * pour avoir un seul fichier à importer pour la compta
 *
*/

namespace App;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use App\Entreprise;
use DateTime;

class CdeFrais extends Model
{
    protected $_ladate;
    protected $_entreprise;
    protected $_entrepriseSlug;
    protected $_directory; //Répertoire de stockage
    protected $_filenameZIP; //Le fichier ZIP

    public function __construct($entrepriseID, $ladate)
    {
        setlocale(LC_ALL, "fr_FR.UTF-8");
        $ladateYMD = Str::replaceArray("-", ['', ''], $ladate);
        $e = Entreprise::findOrFail($entrepriseID);
        $this->_ladate = $ladate;
        $this->_entrepriseSlug = Str::slug($e->name) . "-" . Str::slug($e->siret);
        $this->_entreprise     = $e->name;
        $this->_directory = "/" . storage_path() . "/CdeFrais/" . $this->_entrepriseSlug . "/" . $ladateYMD . "/";
        $this->_filenameZIP = "quadratus.zip";
        Log::debug("Export des CDF dans " . $this->_directory);
    }

    public function getFullFileName()
    {
        if (!is_dir($this->_directory)) {
            mkdir($this->_directory, 0770, true);
        }
        return $this->_directory . "/" . $this->_filenameZIP;
    }

    public function getDirectory()
    {
        if (!is_dir($this->_directory)) {
            mkdir($this->_directory, 0770, true);
        }
        return $this->_directory;
    }

    //Retourne un lien profond qui permet de venir télécharge le fichier zip
    public function getDownloadURI()
    {
        return (config('app.url') . "/cdf/" . Crypt::encryptString($this->getFullFileName()));
    }
}
