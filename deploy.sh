#!/bin/sh
#script a lancer sur la version "en prod"
#source https://medium.com/@gmaumoh/laravel-how-to-automate-deployment-using-git-and-webhooks-9ae6cd8dffae
# activate maintenance mode

#on affiche la date de début / date de fin pour avoir une idée de la durée totale, surtout pour la version
#demo où on fait un gros jeu d'essai ...
echo -n "================================ "
date

php artisan down --message="Mise à jour en cours ..." --retry=20

#dev ou prod ?
TESTDEV=`git branch | grep -w prod`
isDev="0"
if [ -z "${TESTDEV}" ]; then
    isDev="1"
fi

# update source code
git reset --hard
git pull

# update PHP dependencies
#composer install --no-interaction --no-dev --prefer-dist
# --no-interaction Do not ask any interactive question
# --no-dev  Disables installation of require-dev packages.
# --prefer-dist  Forces installation from package dist even for dev versions.

# update database

# pour eviter le pb qui fait que la version en prod retourne "local" ...
#php artisan config:cache
php artisan config:clear
#php artisan vendor:publish --provider="Code16\Sharp\SharpServiceProvider" --tag=assets

if [ "${isDev}" = "1" ]; then
    composer install --no-interaction
    composer update
    #copie du vendor hack a la place du vendor deploye par composer (font awesome 5 dans sharp)
    cp -a public/vendor.hack/sharp public/vendor
    php artisan down --message="Installation du jeu d'essai pour la version de démonstration ..." --retry=10
    if [ -f /tmp/doliscan_force_new_data ]; then
      php artisan migrate:refresh --force
      #injection du jeu d'essai (ou pas ?)
      php artisan db:seed
    else
      php artisan migrate --force
    fi
    php artisan down --message="Compilation des sources javascript ..." --retry=10
    npm install
    #si on est sur le pc de dev ...
    npm run dev
    #mais sur un serveur de demo on passe en prod pour le javascript
    npm run prod
else
    composer install --no-interaction --no-dev --prefer-dist
    composer update
    #copie du vendor hack a la place du vendor deploye par composer (font awesome 5 dans sharp)
    cp -a public/vendor.hack/sharp public/vendor
    # --force  Required to run when in production.
    php artisan down --message="Compilation des sources javascript ..." --retry=30
    npm run prod
    php artisan migrate --force
fi

#on optimize le tout mais provoque le pb suivant
php artisan optimize

#20200521 serveur prod affiche des messages de dev (blade) correctif du optimize
php artisan config:clear

# stop maintenance mode
php artisan up

# et on relance le superviseur qui gere les queues
echo "please run 'sudo systemctl restart supervisor' command to restart supervisor ..."

echo -n "################################ "
date
