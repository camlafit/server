# INSTALLATION

Préparation à l'installation d'un serveur DoliSCAN

## Pré-requis

Un système type AMP est nécessaire, en quelques lignes depuis une distribution debian :

```
apt install apache2 apache2-bin apache2-data apache2-utils libapache2-mod-evasive libapache2-mod-geoip libapache2-mod-rpaf libapache2-mod-xsendfile
apt install mariadb-client mariadb-common mariadb-server
apt install libapache2-mod-php7.3 php-bz2 php-curl php-gd php-intl php-json php-mbstring php-mysql php-xml php-zip php7.3-bz2 php7.3-cli php7.3-common php7.3-curl php7.3-gd php7.3-gmp php7.3-intl php7.3-json php7.3-mbstring php7.3-mysql php7.3-opcache php7.3-readline php7.3-xml php7.3-zip php-imagick
apt install jpegoptim optipng pngquant gifsicle webp
```

## Apache 2

Je ne vais pas vous expliquer comment configurer un serveur apache pour servir une application php c'est du super classique, rien de particulier à noter.

## MariaDB / MySQL

Configurez votre serveur de base de données comme d'habitude, notez bien vos identifiants & mots de passes...

## Récupérez les sources

```
git clone https://framagit.org/doliscan/server.git
cd server
git checkout -b prod #pour basculer sur la branche "prod" (stable) du code
composer install --no-interaction --no-dev --prefer-dist
cp -a public/vendor.hack/sharp public/vendor
npm run prod
```

## Configurez le fichier .env


```
cp .env.example .env
```

Editez le fichier .env et renseignez les différents champs, en particulier ceux-ci:


```
APP_DEBUG="true"
APP_URL="http://localhost"
APP_DOMAIN="doliscan.alpha.devtemp.fr"

ACTIVITY_LOGGER_ENABLED="true"
LOG_CHANNEL="single"

NODE_ENV="development"

DB_CONNECTION="mysql"
DB_HOST="127.0.0.1"
DB_PORT="3306"
DB_DATABASE="doliscan"
DB_USERNAME="xxxxxxxxx"
DB_PASSWORD="xxxxxxxxx"

# pour capter tous les mails sortants et les envoyer au développeur (mettre la ligne en commentaire en production)
DEVMODE_MAIL_TO="moi@devtemp.fr"

# adresse mail qui reçoit les notifications de creation de compte etc.
MAIL_NOTIFICATIONS="suivi@devtemp.fr"
MAIL_DRIVER="smtp"
MAIL_HOST="xxxxxxxxxxxxx"
MAIL_PORT="587"
MAIL_USERNAME="xxxxxxxxxxxxxxxx"
MAIL_PASSWORD="xxxxxxxxxxxxxxxx"
MAIL_ENCRYPTION="tls"
MAIL_FROM_ADDRESS="xxxxxxxxxxx"
```


## Finalisation de l'installation

```
php artisan key:generate
php artisan migrate --force
php artisan optimize
php artisan config:clear
```

## The end ?

Indiquez nous ce qui manque et ce qui reste à écrire: sav _at_ doliscan.fr

__Note:__ voir la doc web https://doliscan.fr/docs/

