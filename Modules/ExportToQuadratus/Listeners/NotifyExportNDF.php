<?php
/*
 * NotifyExportNDF.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Modules\ExportToQuadratus\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use App\NdeFrais;

class NotifyExportNDF
{
    private $_ndf;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
        // Log::debug("ExportToQuadratus::NotifyExportNDF :: construct ");
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $this->_ndf    = $event->ndf;

        //
        // Log::debug("ExportToQuadratus::NotifyExportNDF :: handle pour NDF id " . serialize($this->_ndf) . ":" . serialize($event));
    }
}
