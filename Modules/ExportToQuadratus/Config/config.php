<?php
/*
 * config.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

return [
    'name' => 'ExportToQuadratus',
    'desc' => 'Exporte vos notes de frais vers le logiciel Quadratus en fin de période sous la forme d\'un fichier ZIP contenant le fichier des écritures comptables à importer et toutes les pièces justificatives qui vont se joindre automatiquement à chaque écriture comptable. Ce module est activé par défaut.'
];
