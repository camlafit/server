<?php
/*
 * EventServiceProvider.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Modules\ExportToDolibarr\Providers;

use App\Events\EventsPlugins;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Events\ExportNDF;
use App\Events\EventsLdeFrais;
use Modules\ExportToDolibarr\Listeners\NotifyExportNDF;
use Modules\ExportToDolibarr\Listeners\NotifyPluginsList;
use Modules\ExportToDolibarr\Listeners\NotifyEventsLdeFrais;

class EventServiceProvider extends ServiceProvider
{
    //disabled
    // protected $listen = [
    //     ExportNDF::class => [
    //         NotifyExportNDF::class,
    //     ],
    //     EventsLdeFrais::class => [
    //         NotifyEventsLdeFrais::class,
    //     ],
    //     EventsPlugins::class => [
    //         NotifyPluginsList::class,
    //     ]
    // ];
}
