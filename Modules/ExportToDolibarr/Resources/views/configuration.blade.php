@extends('exporttodolibarr::layouts.master')

@section('content')
<div class="row justify-content-center">
    <div class="card">
        <div class="card-header">Configuration du connecteur Dolibarr - Étape 1</div>

        <div class="card-body">
            @if ($message)
            <div class="alert alert-success" role="alert">
                {{ $message }}
            </div>
            @endif
            @if ($messageErr)
            <div class="alert alert-danger" role="alert">
                {{ $messageErr }}
            </div>
            @endif
            <div>
                <p>Veuillez entrer l'adresse de connexion à l'API de votre serveur dolibarr ainsi qu'une clé de connexion qui a au moins les droits suivants : lister les tiers, lister et créer des factures, lister et créer des notes de frais.</p>
            </div>

            <form method="POST" action="{{ route('exporttodolibarr-configurationStep0') }}">
                @csrf

                <div class="form-group row">
                    <label for="dolibarr_uri" class="col-md-4 col-form-label text-md-right">{{ __('Adresse du serveur dolibarr') }}</label>

                    <div class="col-md-6">
                        <input id="dolibarr_uri" type="text" class="form-control{{ $errors->has('dolibarr_uri') ? ' is-invalid' : '' }}" name="dolibarr_uri" value="{{old('dolibarr_uri', $dolibarr_uri)}}" required autofocus>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="dolibarr_apikey" class="col-md-4 col-form-label text-md-right">{{ __('Clé de connexion à l\'API') }}</label>

                    <div class="col-md-6">
                        <input id="dolibarr_apikey" type="text" class="form-control{{ $errors->has('dolibarr_apikey') ? ' is-invalid' : '' }}" name="dolibarr_apikey" value="{{old('dolibarr_apikey', $dolibarr_apikey)}}" required>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ $btnLabel }}
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection
