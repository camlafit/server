@extends('exporttodolibarr::layouts.master')

@section('content')
<div class="row justify-content-center">
    <div class="card">
        <div class="card-header">Configuration du connecteur Dolibarr - Étape 4</div>

        <div class="card-body">
            @if ($message)
            <div class="alert alert-success" role="alert">
                {{ $message }}
            </div>
            @endif
            @if ($messageErr)
            <div class="alert alert-danger" role="alert">
                {{ $messageErr }}
            </div>
            @endif

            <div>
                <p>Nous devons maintenant configurer la connexion automatique des moyens de paiements professionnels.</p>
            </div>

            <form method="POST" action="{{ route('exporttodolibarr-configurationStep3') }}">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="label">Moyen de paiement</label>
                    </div>
                    <div class="form-group col-md-8">
                        <label for="ladate">Moyen de paiement dolibarr et compte bancaire à associer</label>
                    </div>
                </div>

                @foreach($moyensPaiementsDScan as $mp)

                <div class="form-row">
                    <div class="form-group col-md-4">
                        {{ $mp->label }}
                    </div>
                    <div class="form-group col-md-8">
                        <select name="{{ $mp->slug }}">
                             @if(isset($oldConf[$mp->slug]['doli_payment_id']))
                            <option value="{{$oldConf[$mp->slug]['doli_payment_id']}}:{{$oldConf[$mp->slug]['doli_payment_code']}}">{{$oldConf[$mp->slug]['doli_payment_code']}}</option>
                            @endif

                            @foreach($moyensPaiementsDBarr as $mpdbarr)
                            <option value="{{ $mpdbarr['id'] }}:{{ $mpdbarr['code'] }}">{{ $mpdbarr['label'] }} ({{ $mpdbarr['code'] }})</option>
                            @endforeach
                        </select>

                        <select name="bank-{{ $mp->slug }}">
                            @if(isset($oldConf[$mp->slug]['doli_bank_id']))
                           <option value="{{$oldConf[$mp->slug]['doli_bank_id']}}:{{$oldConf[$mp->slug]['doli_bank_label']}}">{{$oldConf[$mp->slug]['doli_bank_label']}}</option>
                           @endif
                        @foreach($banquesDBarr as $bankbarr)
                       <option value="{{ $bankbarr['id'] }}:{{ $bankbarr['label'] }}">{{ $bankbarr['label'] }}</option>
                       @endforeach
                    </select>

                    </div>
                </div>

                @endforeach

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ $btnLabel }}
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection
