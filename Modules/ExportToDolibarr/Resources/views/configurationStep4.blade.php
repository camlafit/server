@extends('exporttodolibarr::layouts.master')

@section('content')
<div class="row justify-content-center">
    <div class="card">
        <div class="card-header">Configuration du connecteur Dolibarr - Dernière étape</div>

        <div class="card-body">
            @if ($message)
            <div class="alert alert-success" role="alert">
                {{ $message }}
            </div>
            @endif
            @if ($messageErr)
            <div class="alert alert-danger" role="alert">
                {{ $messageErr }}
            </div>
            @endif

            <div>
                <p>Merci, la configuration est maintenant terminée, vous pouvez fermer cette fenêtre pour revenir à <a href="#" onClick="window.close();">l'interface d'administration de DoliSCAN</a>.</p>
            </div>
        </div>

    </div>
</div>
@endsection
