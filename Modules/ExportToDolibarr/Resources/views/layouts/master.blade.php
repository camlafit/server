{{-- On utilise un layout très basique pour ne pas perdre de place
     avec les menus et autres pieds de page de l'appli --}}
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-100">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'DoliScan') }}</title>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body class="d-flex flex-column h-100">
  <div id="app">
    <main role="main" class="flex-shrink-0 py-3">
      <div class="container" style="padding: unset;">
        @yield('content')
      </div>
    </main>
  </div>
</body>

</html>
