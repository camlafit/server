@extends('exporttodolibarr::layouts.master')

@section('content')
<div class="row justify-content-center">
    <div class="card">
        <div class="card-header">Configuration du connecteur Dolibarr - Étape 3</div>

        <div class="card-body">
            @if ($message)
            <div class="alert alert-success" role="alert">
                {{ $message }}
            </div>
            @endif
            @if ($messageErr)
            <div class="alert alert-danger" role="alert">
                {{ $messageErr }}
            </div>
            @endif

            <div>
                <p>Nous devons maintenant configurer la connexion automatique des frais payés à l'aide d'un moyen de paiement personnel avec les codes spécifiques des frais de Dolibarr.</p>
            </div>

            <form method="POST" action="{{ route('exporttodolibarr-configurationStep2') }}">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="label">Frais payé "perso"</label>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="ladate">Type de frais dolibarr à associer</label>
                    </div>
                </div>

                @foreach($typeFraisPerso as $frais)

                <div class="form-row">
                    <div class="form-group col-md-6">
                        {{ $frais->label }}
                    </div>
                    <div class="form-group col-md-6">
                        <select name="{{ $frais->slug }}">
                            @if(isset($oldConf[$frais->slug]))
                            <option value="{{$oldConf[$frais->slug]['doli_id']}}:{{$oldConf[$frais->slug]['doli_code']}}">{{$oldConf[$frais->slug]['doli_code']}}</option>
                            @endif

                            @foreach($fraisDoli as $frais)
                            <option value="{{ $frais['id'] }}:{{ $frais['code'] }}">{{ $frais['label'] }} ({{ $frais['code'] }})</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                @endforeach



                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ $btnLabel }}
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection
