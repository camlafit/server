<?php
/*
 * config.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

return [
    'name' => 'ExportToDolibarr',
    // 'desc' => "Connecteur vers dolibarr: Ce connecteur permet de créer automatiquement votre note de frais dans Dolibarr. <ul><li>- Pour en savoir plus consultez <a href='https://www.doliscan.fr/plugins/dolibarr'>la documentation</a></li><li>- Puis cliquez <a href='#' onClick=\"ConfigWindow=window.open(' %1\$s ','ConfigWindow','width=800,height=500');\">ici pour configurer votre connecteur</a> (ouverture d'une fenetre spéciale)</li></ul>",
    'desc' => "Connecteur vers dolibarr: Ce connecteur permet de créer automatiquement votre note de frais dans Dolibarr. <ul><li>- Pour en savoir plus consultez <a href='https://www.doliscan.fr/plugins/dolibarr'>la documentation</a></li></ul>",
];
