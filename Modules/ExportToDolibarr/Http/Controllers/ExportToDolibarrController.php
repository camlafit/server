<?php
/*
 * ExportToDolibarrController.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Modules\ExportToDolibarr\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Modules\ExportToDolibarr\Dolibarr;
use App\TypeFrais;
use App\MoyenPaiement;
use stdClass;

class ExportToDolibarrController extends Controller
{
    private $_dolibarr;

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('exporttodolibarr::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        Log::debug("ExportToDolibarrController: create");
        return view('exporttodolibarr::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        Log::debug("ExportToDolibarrController: store");
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        Log::debug("ExportToDolibarrController: show");
        return view('exporttodolibarr::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        Log::debug("ExportToDolibarrController: edit");

        return view('exporttodolibarr::edit');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display configuration
     * @return Response
     */
    public function configurationStep0()
    {
        $this->_dolibarr = new Dolibarr();

        return view('exporttodolibarr::configuration', [
            'message'           => '',
            'messageErr'        => '',
            'dolibarr_uri'      => $this->_dolibarr->getApiUrl(),
            'dolibarr_apikey'   => $this->_dolibarr->getApiKey(),
            'btnLabel'          => 'Sauvegarder'
        ]);
    }

    /**
     * Update the specified resource in storage: configuration de base de l'url et api
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function configurationStep0Update(Request $request)
    {
        Log::debug("ExportToDolibarrController: update");
        // Log::debug($request);

        $dol_api_uri = $request->dolibarr_uri;
        $dol_api_key = $request->dolibarr_apikey;

        //On teste la connexion sur l'API
        $this->_dolibarr = new Dolibarr($dol_api_uri, $dol_api_key);

        if ($this->_dolibarr->CheckDolibarrVersion() > 0) {
            $this->_dolibarr->CheckUser();
            $this->_dolibarr->save();
            $typeFraisPro       = TypeFrais::where('pro', '1')->get();
            $fournisseursDoli   = $this->_dolibarr->getFournisseurs();
            if (is_array($fournisseursDoli)) {
                //Le double json n'est pas optimal mais permet de transformer en tableau, pratique pour le passage a blade
                $oldconf            = json_decode(json_encode($this->_dolibarr->getPro()), true);
                // Log::debug($oldconf);
                return view('exporttodolibarr::configurationStep1', [
                    'message'           => 'Connexion avec le serveur Dolibarr validée, on passe à l\'étape 2 :',
                    'messageErr'        => '',
                    'btnLabel'          => 'Suivant',
                    'typeFraisPro'      => $typeFraisPro,
                    'fournisseursDoli'  => $fournisseursDoli,
                    'oldConf'           => $oldconf,
                ]);
            } else {
                return view('exporttodolibarr::configuration', [
                    'message'           => '',
                    'messageErr'        => 'Erreur de communication avec votre serveur dolibarr: ' . $fournisseursDoli,
                    'dolibarr_uri'      => 'http://192.168.16.183/api/index.php/',
                    'dolibarr_apikey'   => 'AHd2Gnza18Ad1Y11DCiYwUy85mQwj96J',
                    'btnLabel'          => 'Suivant'
                ]);
            }
        } else {
            return view('exporttodolibarr::configuration', [
                'message'           => '',
                'messageErr'        => 'Erreur de communication avec votre serveur dolibarr, vérifiez bien les informations saisies et vérifiez que votre serveur Dolibarr est au moins en version ' . Dolibarr::DOLIBARR_MIN_VERSION,
                'dolibarr_uri'      => 'http://192.168.16.183/api/index.php/',
                'dolibarr_apikey'   => 'AHd2Gnza18Ad1Y11DCiYwUy85mQwj96J',
                'btnLabel'          => 'Suivant'
            ]);
        }
    }

    /**
     * Update the specified resource in storage: frais payés pro
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function configurationStep1Update(Request $request)
    {
        Log::debug("ExportToDolibarrController: update step1");
        $this->_dolibarr = new Dolibarr();

        $typeFraisPro       = TypeFrais::where('pro', '1')->get();

        //On cree une structure de type tableau [index = slug doliscan] avec doli_id et doli_name
        $pro = array();
        foreach ($typeFraisPro as $type) {
            $s = $type->slug;
            Log::debug("on cherche $s ...");
            Log::debug($request->$s);
            //Le retour du formulaire est du type 21:Divers Resto (id:name)
            $tab = explode(":", $request->$s);
            $v = new stdClass();
            $v->doli_id     = $tab[0];
            $v->doli_name   = $tab[1];
            $pro[$s]        = $v;
        }

        $this->_dolibarr->setPro($pro);
        $this->_dolibarr->save();

        // =======================================================================================================================
        $typeFraisPerso     = TypeFrais::where('perso', '1')->get();

        //Le double json n'est pas optimal mais permet de transformer en tableau, pratique pour le passage a blade
        $oldconf            = json_decode(json_encode($this->_dolibarr->getPerso()), true);

        $fraisDoli          = $this->_dolibarr->getFrais();
        if (is_array($fraisDoli)) {
            return view('exporttodolibarr::configurationStep2', [
                'message'           => '',
                'messageErr'        => '',
                'btnLabel'          => 'Suivant',
                'typeFraisPerso'    => $typeFraisPerso,
                'fraisDoli'         => $fraisDoli,
                'oldConf'           => $oldconf,
            ]);
        } else {
            $typeFraisPro       = TypeFrais::where('pro', '1')->get();
            $fournisseursDoli   = $this->_dolibarr->getFournisseurs();
            $oldconf            = json_decode(json_encode($this->_dolibarr->getPro()), true);
            return view('exporttodolibarr::configurationStep1', [
                'message'           => '',
                'messageErr'        => 'Erreur de communication avec votre serveur dolibarr: ' . $fraisDoli,
                'btnLabel'          => 'Suivant',
                'typeFraisPro'      => $typeFraisPro,
                'fournisseursDoli'  => $fournisseursDoli,
                'oldConf'           => $oldconf,
            ]);
        }
    }

    /**
     * Update the specified resource in storage: frais payés perso
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function configurationStep2Update(Request $request)
    {
        Log::debug("ExportToDolibarrController: update perso step2");
        $this->_dolibarr = new Dolibarr();

        $typeFraisPerso       = TypeFrais::where('perso', '1')->get();

        //On cree une structure de type tableau [index = slug doliscan] avec doli_id et doli_name
        $perso = array();
        foreach ($typeFraisPerso as $type) {
            $s = $type->slug;
            Log::debug("on cherche $s ...");
            Log::debug($request->$s);
            //Le retour du formulaire est du type 21:Divers Resto (id:name)
            $tab = explode(":", $request->$s);
            $v = new stdClass();
            $v->doli_id     = $tab[0];
            $v->doli_code   = $tab[1];
            $perso[$s]      = $v;
        }

        //On sauvegarde les données
        $this->_dolibarr->setPerso($perso);
        $this->_dolibarr->save();

        //Le double json n'est pas optimal mais permet de transformer en tableau, pratique pour le passage a blade
        $oldconf            = json_decode(json_encode($this->_dolibarr->getMoyensPaiementsEtBanque()), true);

        // =======================================================================================================================
        //Et maintenant on se concentre sur les moyens de paiements & comptes bancaires
        $moyensPaiementsDScan   = MoyenPaiement::where('is_pro', '1')->get();
        $moyensPaiementsDBarr   = $this->_dolibarr->getMoyensPaiements();
        $banquesDBarr           = $this->_dolibarr->getComptesBanquaires();

        //On affiche le formulaire suivant
        if (is_array($moyensPaiementsDBarr) && is_array($banquesDBarr)) {
            return view('exporttodolibarr::configurationStep3', [
                'message'           => '',
                'messageErr'        => '',
                'moyensPaiementsDScan' => $moyensPaiementsDScan,
                'moyensPaiementsDBarr' => $moyensPaiementsDBarr,
                'banquesDBarr'      => $banquesDBarr,
                'btnLabel'          => 'Terminer',
                'oldConf'           => $oldconf,
            ]);
        } else {
            $fraisDoli          = $this->_dolibarr->getFrais();
            $oldconf            = json_decode(json_encode($this->_dolibarr->getPerso()), true);

            $messageErreur = "";
            //L'erreur est peut-être que d'un des deux ...
            if (! is_array($moyensPaiementsDBarr)) {
                $messageErreur .= $moyensPaiementsDBarr;
            }
            if (! is_array($banquesDBarr)) {
                $messageErreur .= $banquesDBarr;
            }

            return view('exporttodolibarr::configurationStep2', [
                'message'           => '',
                'messageErr'        => 'Erreur de communication avec votre serveur dolibarr: ' . $messageErreur,
                'btnLabel'          => 'Suivant',
                'typeFraisPerso'    => $typeFraisPerso,
                'fraisDoli'         => $fraisDoli,
                'oldConf'           => $oldconf,
            ]);
        }
    }

    /**
     * Update the specified resource in storage: moyens de paiements pro
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function configurationStep3Update(Request $request)
    {
        Log::debug("ExportToDolibarrController: update perso step3");
        $this->_dolibarr = new Dolibarr();


        // //On cree une structure de type tableau [index = slug doliscan] avec doli_payement_id et doli_bank_id
        $moyensPaiementsDScan   = MoyenPaiement::where('is_pro', '1')->get();

        $paiements = array();
        foreach ($moyensPaiementsDScan as $mp) {
            $pay  = $mp->slug;
            $bank = "bank-" . $mp->slug;
            Log::debug("moyen paiement, etape 3 on cherche $pay et $bank ...");
            Log::debug($request->$pay);
            Log::debug($request->$bank);

            //Le retour du formulaire est du type 21:Divers Resto (id:name)
            $tabPay  = explode(":", $request->$pay);
            $tabBank = explode(":", $request->$bank);
            $v = new stdClass();
            $v->doli_payment_id   = $tabPay[0];
            $v->doli_payment_code = $tabPay[1];
            $v->doli_bank_id      = $tabBank[0];
            $v->doli_bank_label   = $tabBank[1];
            $paiements[$mp->slug] = $v;
        }

        $this->_dolibarr->setMoyensPaiementsEtBanque($paiements);
        $this->_dolibarr->save();

        //On affiche le formulaire suivant
        return view('exporttodolibarr::configurationStep4', [
            'message'           => '',
            'messageErr'        => '',
            'btnLabel'          => 'Terminer',
        ]);
    }
}
