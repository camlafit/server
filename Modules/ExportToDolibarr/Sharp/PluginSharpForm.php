<?php
/*
 * PluginSharpForm.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */



 // ========================================================================
 //
 //
 // tentative d'intégration du plugin dans Sharp ... mais laissé de côté pour
 // l'instant, voir https://github.com/code16/sharp/issues/251
 //
 //
 //
 //
 // ========================================================================



namespace Modules\ExportToDolibarr\Sharp;

use App\User;
use App\Plugin;
use App\TypeFrais;
use Spatie\Permission\Models\Role;
use Code16\Sharp\Form\SharpForm;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Layout\FormLayoutFieldset;
use Code16\Sharp\Form\Fields\SharpFormNumberField;
use Code16\Sharp\Form\Layout\FormLayoutTab;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Fields\SharpFormListField;
use Code16\Sharp\Form\Fields\SharpFormHtmlField;
use Code16\Sharp\Form\Fields\SharpFormAutocompleteListField;
use Code16\Sharp\Form\Fields\SharpFormAutocompleteField;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;
use Illuminate\Support\Facades\Log;
use Code16\Sharp\Form\Fields\SharpFormTagsField;
use Code16\Sharp\Form\Fields\SharpFormSelectField;
use Code16\Sharp\Show\Layout\ShowLayoutColumn;
use App\Sharp\CustomFormFields\SharpCustomFormFieldCustButton;

class PluginSharpForm extends SharpForm
{
    use WithSharpFormEloquentUpdater;
    private $obj;

    function __construct($o)
    {
        $this->obj = $o;
        Log::debug("Modules::ExportToDolibarr::Sharp::PluginSharpForm::__construct");
    }

    function find($id): array
    {
        Log::debug("Modules::ExportToDolibarr::Sharp::PluginSharpForm::find");

        //On recupere quoi ?
        //les infos dans la bdd
        $p = Plugin::find($id)->toArray();

        //on recupere tout le contenu des types_de_frais
        $tall = TypeFrais::all();
        // Log::debug($tall);
        foreach ($tall as $type) {
            $var = $type->slug;
            $p[$var . "Slug"] = $type->slug;
            $p[$var . "Texte"] = $type->label;
        }
        $p['restaurationSlug'] = "restauration";

        $p['dolibarr_api_key']   = "AHd2Gnza18Ad1Y11DCiYwUy85mQwj96J";
        $p['dolibarr_uri']       = "http://192.168.16.183/api/index.php/";
        // Log::debug("PluginSharpForm :: find " . $id);

        // //On devrait donc aller chercher le find de
        // $r = \Modules\$p->name\PluginSharpForm\find($id);

        // return $this->transform(
        //     $r
        // );

        // return $this->setCustomTransformer("pictureuri", function ($picture, LdeFrais $l) {
        //     $r = $l->getImageURI();
        //     Log::debug("Création de pictureuri : $r");
        //     return $r;
        // })->transform(
        //     $lde
        // );

        return $p;
    }

    function update($id, array $data)
    {
        // Log::debug("PluginSharpForm :: update " . $id);
        // $instance = $id ? LdeFrais::findOrFail($id) : new LdeFrais;

        // return tap($instance, function ($ldefrais) use ($data) {
        //     //On ignore les champs qui n'existent pas dans la table
        //     $this->ignore(["type_frais_slug", "pictureuri"])
        //         ->save($ldefrais, $data);
        // });
    }

    function delete($id)
    {
        // Log::debug("PluginSharpForm :: delete " . $id);
        // LdeFrais::findOrFail($id)->delete();
    }


    function buildFormFields()
    {
        Log::debug("Modules::ExportToDolibarr::Sharp::PluginSharpForm::buildFormFields");
        $this->obj->addField(
            SharpFormTextField::make("dolibarr_api_key")
                ->setLabel("Clé de l'API")
        )->addField(
            SharpFormTextField::make("dolibarr_uri")
                ->setLabel("Adresse de l'API de Dolibarr")
        )->addField(
            SharpFormHtmlField::make('aide_compta_plugin_dolibarr_config')
                ->setInlineTemplate(
                    "<a href='#' onClick=\"HelpWindow=window.open('/docs/1.0/g-exportGlobal','HelpWindow','width=800,height=400'); return false;\">Aide (configuration dolibarr)</a>"
                )
        )->addField(
            SharpCustomFormFieldCustButton::make('dolibarr_uri_test')
                ->setLabel("Tester la connexion")
                ->setBtnLabel("Tester")
                ->setOnClick("javascript:alert('tata ici');")
                ->setBtnVal1("dolibarr_api_key")
                ->setBtnVal2("dolibarr_uri")
        )->addField(
            SharpFormListField::make("pro")
                ->setLabel("Frais payés avec un moyen de paiement pro")
                ->setAddable()->setAddText("Ajouter un lien")
                // ->setFormatter($formatter)
                ->setRemovable()
                ->addItemField(
                    SharpFormAutocompleteField::make("doliscan_id", "local")
                        ->setLabel("DoliSCAN")
                        ->setLocalSearchKeys(["name", "firstname", "email"])
                        ->setLocalValues(TypeFrais::all('id', 'label'))
                        ->setListItemInlineTemplate("{{label}}")
                        ->setResultItemInlineTemplate("{{label}}")
                )->addItemField(
                    SharpFormAutocompleteField::make("dolibarr_id", "remote")
                        ->setLabel("Dolibarr")
                        ->setListItemInlineTemplate("{{label}}")
                        ->setResultItemInlineTemplate("{{label}}")
                        ->setDynamicRemoteEndpoint("/dolibarr/{{dolibarr_uri}}/{{doliscan_id}}")
                    // ->setLocalSearchKeys(["label"])
                    // ->setLocalValues(TypeFrais::all('id', 'label'))
                )
        )->addField(
            SharpFormListField::make("perso")
                ->setLabel("Frais payés par un moyen de paiement personnel")
                ->setAddable()->setAddText("Ajouter un lien")
                // ->setFormatter($formatter)
                ->setRemovable()
                ->addItemField(
                    SharpFormAutocompleteField::make("doliscan_id", "local")
                        ->setLabel("DoliSCAN")
                        ->setLocalSearchKeys(["name", "firstname", "email"])
                        ->setLocalValues(TypeFrais::all('id', 'label'))
                        ->setListItemInlineTemplate("{{label}}")
                        ->setResultItemInlineTemplate("{{label}}")
                )->addItemField(
                    SharpFormAutocompleteField::make("dolibarr_id", "remote")
                        ->setLabel("Dolibarr")
                        ->setListItemInlineTemplate("{{label}}")
                        ->setResultItemInlineTemplate("{{label}}")
                        ->setDynamicRemoteEndpoint("/dolibarr/{{dolibarr_uri}}/{{doliscan_id}}")
                    // ->setLocalSearchKeys(["label"])
                    // ->setLocalValues(TypeFrais::all('id', 'label'))
                )
        );
        /*->addField(
            SharpFormTextField::make("ht")
                ->setLabel("HT")
        )->addField(
            SharpFormTextField::make("ttc")
                ->setLabel("TTC")
        )->addField(
            SharpFormSelectField::make(
                "type_frais_slug",
                TypeFrais::orderBy("id")->get()->pluck('label', 'slug')->all()
            )
                ->setDisplayAsDropdown()
                ->setMultiple(false)
                ->setLabel('Type de frais')
                ->setReadOnly(true)
        );
        */
    }

    function buildFormLayout()
    {
        Log::debug("Modules::ExportToDolibarr::Sharp::PluginSharpForm::buildFormLayout");

        $this->obj->addTab("Dolibarr", function (FormLayoutTab $tab) {
            $tab->addColumn(12, function (FormLayoutColumn $column) {
                $column->withFields('dolibarr_uri|12');
                $column->withFields('dolibarr_api_key|12');
                $column->withFields('dolibarr_uri_test|2');
            });
        })->addTab("Frais payés pro", function (FormLayoutTab $tab) {
            $tab->addColumn(12, function (FormLayoutColumn $column) {
                $column->withSingleField('pro', function (FormLayoutColumn $fieldset) {
                    return $fieldset->withFields('doliscan_id|6', 'dolibarr_id|6');
                });
            });
        })->addTab("Frais payés perso", function (FormLayoutTab $tab) {
            $tab->addColumn(12, function (FormLayoutColumn $column) {
                $column->withSingleField('perso', function (FormLayoutColumn $fieldset) {
                    return $fieldset->withFields('doliscan_id|6', 'dolibarr_id|6');
                });
            });
        });


        // $this->obj->addColumn(12, function (FormLayoutColumn $column) {
        //     $column
        //         ->withFieldset("Configuration générale", function (FormLayoutFieldset $fieldset) {
        //             return $fieldset->withFields('uri|12')
        //                 ->withFields('api_key|12');
        //         })->withFieldset("Tableau de correspondance DoliSCAN - Dolibarr", function (FormLayoutFieldset $fieldset) {
        //             return $fieldset->withFields('restaurationTexte|3', 'restaurationKey|2', 'restaurationID|2', 'aide_compta_plugin_dolibarr_config|3')
        //                 ->withFields('peageTexte|3', 'peageKey|2', 'peageID|2')
        //                 ->withFields('hotelTexte|3', 'hotelKey|2', 'hotelID|2')
        //                 ->withFields('trainTexte|3', 'trainKey|2', 'trainID|2')
        //                 ->withFields('taxiTexte|3', 'taxiKey|2', 'taxiID|2')
        //                 ->withFields('carburantTexte|3', 'carburantKey|2', 'carburantID|2')
        //                 ->withFields('diversTexte|3', 'diversKey|2', 'diversID|2')
        //                 ->withFields('ikTexte|3', 'ikKey|2', 'ikID|2')
        //                 ->withFields('ik-reguleTexte|3', 'ik-reguleKey|2', 'ik-reguleID|2');
        //             // ->withFields('tvaTx3|4', 'tvaVal3|2', 'tvaTx4|4', 'tvaVal4|2');
        //         });
        //     // ->withFieldset("Indemnités kilométriques ou Frais de Carburant :", function (FormLayoutFieldset $fieldset) {
        //     //     return $fieldset->withFields('depart|3', 'arrivee|3', 'distance|3', 'vehicule|3');
        //     // });
        // });
    }
}
