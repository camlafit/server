<?php
/*
 * DolibarrNdeFraisExportCommand.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Modules\ExportToDolibarr\Sharp\Commands;

use App\NdeFrais;
use App\Http\Controllers\NdeFraisController;
use Code16\Sharp\EntityList\Commands\InstanceCommand;
use Code16\Sharp\Form\Fields\SharpFormTextareaField;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class DolibarrNdeFraisExportCommand extends InstanceCommand
{

    /**
     * @return string
     */
    public function label(): string
    {
        return "Export vers Dolibarr";
    }

    public function description(): string
    {
        return "Exporte la note de frais vers Dolibarr : frais payés pro transformés en factures fournisseurs et frais payés perso en note de frais";
    }


    /**
     * @param string $instanceId
     * @param array $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function execute($instanceId, array $data = []): array
    {
        Log::debug("=============== DolibarrNdeFraisExportCommand::execute");
        $ndfC = new NdeFraisController;
        $ndfC->webBuildPDF($instanceId,"",false,"ExportToDolibarr");
        $ndfC->webBuildPDFJustificatifs($instanceId,"",false,"ExportToDolibarr");
        return $this->info("Action lancée, merci de vérifier sur votre Dolibarr si les données ont été transférées...");
    }

    /**
     * @param $instanceId
     * @return bool
     */
    public function authorizeFor($instanceId): bool
    {
        return true;
    }

    public function buildFormFields()
    {
    }

    public function buildFormLayout(FormLayoutColumn &$column)
    {
    }
}
