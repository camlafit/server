<?php
/*
 * Dolibarr.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Modules\ExportToDolibarr;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Carbon;
use App\PluginUserConfiguration;
use App\TypeFrais;
use App\LdeFrais;
use App\Plugin;
use stdClass;

class Dolibarr
{
    const DOLIBARR_MIN_VERSION = "10.0.0";  //Version minimale de dolibarr requise
    const DOLIBARR_DEFAULT_VAT = "20.0";    //Taux de TVA par défaut

    private $_apiKey; //La clé permettant de se connecter à l'api de dolibarr
    private $_apiUrl; //L'adresse de l'api de dolibarr
    private $_dol_user_id; //Le user id dolibarr

    private $_pro;   //Tableau de correspondance pour les frais payés pro
    private $_perso; //Tableau de correspondance pour les frais payés perso
    private $_moyensPaiementsEtBanque; //Tableau de correspondance pour les moyens de paiements pro et compte bancaire à utiliser

    private $_plugin_id;
    private $_puc;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct($url = "", $key = "")
    {
        // Log::debug("ExportToDolibarr::Dolibarr :: construct");
        //Si on est cron
        if(!isset(sharp_user()->id)) {
            return;
        }

        //On recupere l'id du plugin en cours
        $p = Plugin::where('name', 'ExportToDolibarr')->first();
        $this->_plugin_id = $p->id;

        //Puis on regarde s'il existe une configuration de ce plugin pour l'utilisateur courant
        $puc = PluginUserConfiguration::where('user_id', \sharp_user()->id)->where('plugin_id', $p->id)->first();
        if (is_null($puc)) {
            $puc = new PluginUserConfiguration();
        } else {
            $c = \json_decode($puc->config_value);
            if (isset($c->apiKey))
                $this->_apiKey = $c->apiKey;
            if (isset($c->apiUrl))
                $this->_apiUrl = $c->apiUrl;
            if (isset($c->pro))
                $this->_pro    = $c->pro;
            if (isset($c->perso))
                $this->_perso  = $c->perso;
            if (isset($c->dol_user_id))
                $this->_dol_user_id  = $c->dol_user_id;
            if (isset($c->moyensPaiementsEtBanque))
                $this->_moyensPaiementsEtBanque  = $c->moyensPaiementsEtBanque;
        }
        if ($url != "") {
            $this->_apiUrl = $url;
        }
        if ($key != "") {
            $this->_apiKey = $key;
        }
        $this->_puc = $puc;
        // Log::debug($this->_puc);
        // Log::debug("ExportToDolibarr::Dolibarr :: construct 2");
    }

    /**
     * save : sauvegarde de la configuration du plugin dans un json
     *
     * @return void
     */
    function save()
    {
        // Log::debug("ExportToDolibarr::Dolibarr :: save");

        //Creation de la structure json a sauvegarder
        $conf = new stdClass();
        $conf->apiKey   = $this->_apiKey;
        $conf->apiUrl   = $this->_apiUrl;
        $conf->pro      = $this->_pro;
        $conf->perso    = $this->_perso;
        $conf->moyensPaiementsEtBanque = $this->_moyensPaiementsEtBanque;
        $conf->dol_user_id  = $this->_dol_user_id;
        $json = \json_encode($conf);

        // Log::debug("ExportToDolibarr::Dolibarr :: save 2");
        $this->_puc->config_value   = $json;

        $this->_puc->plugin_id      = $this->_plugin_id;
        $this->_puc->user_id        = \sharp_user()->id;
        $this->_puc->save();
        // Log::debug("ExportToDolibarr::Dolibarr :: save 3");
    }

    /**
     * CallAPI: l'appel vers l'API de Dolibarr, code issu du wiki de Dolibarr
     * https://wiki.dolibarr.org/index.php/Module_Web_Services_API_REST_(developer)
     *
     * @param  mixed $method
     * @param  mixed $parturl
     * @param  mixed $data
     * @return void
     */
    function CallAPI($method, $parturl, $data = false)
    {
        $url = $this->_apiUrl . $parturl;
        Log::debug("ExportToDolibarr::CallAPI : URL=$url");

        $curl = curl_init();
        $httpheader = ['DOLAPIKEY: ' . $this->_apiKey];

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                $httpheader[] = "Content-Type:application/json";

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

                break;
            case "PUT":

                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
                $httpheader[] = "Content-Type:application/json";

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // Optional Authentication:
        //    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        //    curl_setopt($curl, CURLOPT_USERPWD, "username:password");

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $httpheader);


        $result = curl_exec($curl);

        curl_close($curl);
        Log::debug("ExportToDolibarr::CallAPI resultat : " . $result);

        return $result;
    }

    /**
     * CheckDolibarrVersion : vérifie si la version de dolibarr est compatible avec doliscan
     * (il faut l'API de la v12 à minima)
     *
     * @return void
     */
    function CheckDolibarrVersion()
    {
        $listResult = [];
        $listResult = $this->CallAPI("GET", "status");
        Log::debug("Retour du check CheckDolibarrVersion: " . $listResult);
        $listResult = json_decode($listResult, true);

        if (\is_null($listResult)) {
            Log::info("Erreur de communication avec Dolibarr, vérifiez votre installation");
            return -1;
        }

        if (is_array($listResult) && array_key_exists("success", $listResult)) {
            $v = $listResult['success']['dolibarr_version'];

            if (version_compare($v, Dolibarr::DOLIBARR_MIN_VERSION) < 0) {
                Log::info("Erreur : la version minimale de dolibarr doit-être " . Dolibarr::DOLIBARR_MIN_VERSION . " pour que ce script fonctionne.\nVotre serveur est en version $v ... faites une mise à jour puis relancez ce script");
                return -5;
            }
        } else {
            Log::info("Erreur de communication avec Dolibarr, vérifiez votre installation");
            return -4;
        }
        Log::info("Vérification de la version de dolibarr: OK");
        return 1;
    }

    /**
     * CheckUser : vérifie si cette clé d'API permet de s'authentifier en tant qu'utilisateur
     *             sur dolibarr
     *
     * @return void
     */
    function CheckUser()
    {
        $listResult = [];
        $listResult = $this->CallAPI("GET", "users/info");
        $listResult = json_decode($listResult, true);

        Log::debug($listResult);
        if (\is_null($listResult)) {
            Log::info("Erreur de communication (CheckUser 1) avec Dolibarr, vérifiez votre installation");
            return -1;
        }

        if (is_array($listResult) && array_key_exists("id", $listResult)) {
            $this->_dol_user_id = $listResult['id'];
        } else {
            Log::info("Erreur de communication (CheckUser 2) avec Dolibarr, vérifiez votre installation");
            return -4;
        }
        return 1;
    }


    /**
     * getFournisseurs : récupère la liste des fournisseurs de dolibarr
     *                   retourne un tableau ou un message d'erreur
     * @return void
     */
    function getFournisseurs()
    {
        Log::debug("ExportToDolibarr::Dolibarr:getFournisseurs");
        $r = [];
        $listResult = [];

        $param = ["sqlfilters" => "status='1'", "mode" => "4", "sortfield" => "nom", "limit" => "2000"];
        $listResult = $this->CallAPI("GET", "thirdparties", $param);
        $listResult = json_decode($listResult, true);

        if ($listResult != "") {
            if (is_array($listResult) && array_key_exists("error", $listResult)) {
                Log::debug("ExportToDolibarr::Dolibarr:getFournisseurs [warn] : " . $listResult['error']['message']);
                return "API Request (thirdparties) error: «" . $listResult['error']['message'] . "». Vérifiez que cet utilisateur a les droits lui permettant de consuter les tiers et fournisseurs...";
            }
            $i = 0;
            foreach ($listResult as $res) {
                $r[$i] = [
                    'id'   => $res['id'],
                    'name' => $res['name'],
                ];
                $i++;
            }
            Log::debug("ExportToDolibarr::Dolibarr:getFournisseurs [ok]");
        }
        Log::debug($r);
        return $r;
    }

    /**
     * getFrais : récupère la liste des frais
     *            retourne un tableau ou un message d'erreur
     * @return void
     */
    function getFrais()
    {
        Log::debug("ExportToDolibarr::Dolibarr:getFrais");
        $r = [];
        $listResult = [];

        $param = ["sortfield" => "code", "limit" => "100", "active" => "1"];
        $listResult = $this->CallAPI("GET", "setup/dictionary/expensereport_types", $param);
        $listResult = json_decode($listResult, true);

        if ($listResult != "") {
            if (is_array($listResult) && array_key_exists("error", $listResult)) {
                Log::debug("ExportToDolibarr::Dolibarr:getFrais [warn] : " . $listResult['error']['code'] . "/" . $listResult['error']['message']);
                return "API Request (setup/dictionary/expensereport_types) error: «" . $listResult['error']['message'] . "». Vérifiez que votre dolibarr expose l'API de gestion des notes de frais complètement (disponible à partir de la version 12) et que cet utilisateur à les droits suffisants.";
            }
            $i = 0;
            foreach ($listResult as $res) {
                $r[$i] = [
                    'id'   => $res['id'],
                    'code' => $res['code'],
                    'label' => $res['label'],
                ];
                $i++;
            }
            Log::debug("ExportToDolibarr::Dolibarr:getFrais [ok]");
        }
        Log::debug($r);
        return $r;
    }

    /**
     * getMoyensPaiements : récupère la liste des moyens de paiements
     *                      retourne un tableau ou un message d'erreur
     * @return void
     */
    function getMoyensPaiements()
    {
        Log::debug("ExportToDolibarr::Dolibarr:getMoyensPaiements");
        $r = [];
        $listResult = [];

        $param = ["sortfield" => "code", "limit" => "100", "active" => "1"];
        $listResult = $this->CallAPI("GET", "setup/dictionary/payment_types", $param);
        $listResult = json_decode($listResult, true);

        if ($listResult != "") {
            if (is_array($listResult) && array_key_exists("error", $listResult)) {
                Log::debug("ExportToDolibarr::Dolibarr:getMoyensPaiements [warn] : «" . $listResult['error']['code'] . "/" . $listResult['error']['message'] . "». Vérifiez que cet utilisateur a les droits de consultation des moyens de paiements.");
                return "API Request (setup/dictionary/payment_types) error: " . $listResult['error']['message'];
            }
            $i = 0;
            foreach ($listResult as $res) {
                $r[$i] = [
                    'id'   => $res['id'],
                    'code' => $res['code'],
                    'label' => $res['label'],
                ];
                $i++;
            }
            Log::debug("ExportToDolibarr::Dolibarr:getMoyensPaiements [ok]");
        }
        Log::debug($r);
        return $r;
    }

    /**
     * getComptesBanquaires : récupère la liste des comptes bancaires
     *                        retourne un tableau ou un message d'erreur
     * @return void
     */
    function getComptesBanquaires()
    {
        Log::debug("ExportToDolibarr::Dolibarr:getComptesBanquaires");
        $r = [];
        $listResult = [];

        $param = ["sortfield" => "label", "limit" => "100", "active" => "1"];
        $listResult = $this->CallAPI("GET", "bankaccounts", $param);
        $listResult = json_decode($listResult, true);


        if ($listResult != "") {
            if (array_key_exists("error", $listResult)) {
                Log::debug("ExportToDolibarr::Dolibarr:getComptesBanquaires [warn] : «" . $listResult['error']['code'] . "/" . $listResult['error']['message'] . "». Vérifiez que cet utilisateur a les droits d'accès à la liste des comptes bancaires.");
                return "API Request (bankaccounts) error: " . $listResult['error']['message'];
            }
            $i = 0;
            foreach ($listResult as $res) {
                $r[$i] = [
                    'id'   => $res['id'],
                    'label' => $res['label'],
                    'bank' => $res['bank'],
                ];
                $i++;
            }
            Log::debug("ExportToDolibarr::Dolibarr:getComptesBanquaires [ok]");
        }
        Log::debug($r);
        return $r;
    }

    /**
     * getApiUrl retourne l'url de l'api de dolibarr sauvegardée pour ce compte
     *
     * @return void
     */
    function getApiUrl()
    {
        return $this->_apiUrl;
    }

    /**
     * setApiUrl : stocke l'url de l'api
     *
     * @param  mixed $v
     * @return void
     */
    function setApiUrl($v)
    {
        $this->_apiUrl = $v;
    }

    /**
     * getApiKey : récupère la clé de l'api
     *
     * @return void
     */
    function getApiKey()
    {
        return $this->_apiKey;
    }

    /**
     * setApiKey : stocke la clé de l'api
     *
     * @param  mixed $v
     * @return void
     */
    function setApiKey($v)
    {
        $this->_apiKey = $v;
    }

    /**
     * getPro
     *
     * @param  mixed $doliscanSlug
     * @return void
     */
    function getPro($doliscanSlug = '')
    {
        if ($doliscanSlug) {
            return $this->_pro->$doliscanSlug;
        }
        return $this->_pro;
    }

    /**
     * setPro
     *
     * @param  mixed $t
     * @return void
     */
    function setPro($t)
    {
        $this->_pro = $t;
    }

    /**
     * getPerso
     *
     * @param  mixed $doliscanSlug
     * @return void
     */
    function getPerso($doliscanSlug = '')
    {
        if ($doliscanSlug) {
            return $this->_perso->$doliscanSlug;
        }
        return $this->_perso;
    }

    /**
     * setPerso
     *
     * @param  mixed $t
     * @return void
     */
    function setPerso($t)
    {
        $this->_perso = $t;
    }

    /**
     * getMoyensPaiementsEtBanque
     *
     * @param  mixed $doliscanSlug
     * @return void
     */
    function getMoyensPaiementsEtBanque($doliscanSlug = '')
    {
        if ($doliscanSlug) {
            return $this->_moyensPaiementsEtBanque->$doliscanSlug;
        }
        return $this->_moyensPaiementsEtBanque;
    }


    /**
     * setMoyensPaiementsEtBanque
     *     On sauvegarde les associations moyens de paiement et compte bancaire
     *     exemple $t['cb-pro'] = { doli_payement_id => 5, doli_bank_id => 2}
     *
     * @param  mixed $t
     * @return void
     */
    function setMoyensPaiementsEtBanque($t)
    {
        $this->_moyensPaiementsEtBanque = $t;
    }

    /**
     * getUserID
     *
     * @return void
     */
    function getUserID()
    {
        Log::debug("ExportToDolibarr::Dolibarr:getUserID -> " . $this->_dol_user_id);

        if ($this->_dol_user_id == "") {
            $this->CheckUser();
            $this->save();
        }
        return $this->_dol_user_id;
    }

    /**
     * setUserID
     *
     * @param  mixed $t
     * @return void
     */
    function setUserID($t)
    {
        $this->_dol_user_id = $t;
    }


    /**
     * On uploade un fichiers sur la GED de dolibarr et on le rattache à l'objet dolibarr $dol_id
     *
     * @param  mixed $dol_id : l'identifiant de l'objet dolibarr
     * @param  mixed $filename : le nom du fichier
     * @param  mixed $apimodule : sur quel point d'entrée de l'api on se connecte
     * @param  mixed $module : sur quel module dolibarr on attache ce document
     * @return void
     */
    function UploadDocument($dol_id, $filename, $apimodule, $module)
    {
        Log::debug("ExportToDolibarr::Dolibarr::UploadDocument On uploade $filename pour $module ref $dol_id ...");
        if (!file_exists($filename)) {
            return -1;
        }

        $listResult = [];
        $param = ["sqlfilters" => "rowid='" . $dol_id . "'", "sortfield" => "rowid", "limit" => "1"];
        $listResult = $this->CallAPI("GET", $apimodule, $param);
        $listResult = json_decode($listResult, true);

        if ($listResult != "") {
            if (is_array($listResult) && array_key_exists("error", $listResult)) {
                Log::debug("ExportToDolibarr::Dolibarr::UploadDocument [warn] Le $module ref \"$dol_id\" est introuvable ... traitement manuel requis...");
                return;
            }

            $fic = base64_encode(file_get_contents($filename));
            $newDocument = [
                "filename"          => str_replace("...", "-", basename($filename)),
                "modulepart"        => $module,
                "ref"               => trim($listResult[0]['ref']),
                "subdir"            => "",
                "filecontent"       => $fic,
                "fileencoding"      => "base64",
                "overwriteifexists" => 1,
            ];

            $newDocumentResult = $this->CallAPI("POST", "documents/upload", json_encode($newDocument));
            $newDocumentResult = json_decode($newDocumentResult, true);
            Log::debug("ExportToDolibarr::Dolibarr::UploadDocument  [ok] document $dol_id sauvegardé sur le serveur, retour : " . serialize($newDocumentResult));
        }
    }


    /**
     * CreateInvoice : On créé une facture fournisseur
     *
     * @return void
     */
    //
    function CreateInvoice($label, $frais, $dol_fournisseur_id, $total_ht, $total_ttc, $total_taxes, $dol_payment_id, $dol_payment_code, $dol_bank_id)
    {
        Log::debug("ExportToDolibarr::Dolibarr::CreateInvoice 1");
        $timeStampInvoice = Carbon::parse($frais['ladate'])->timestamp;

        $newInvoice = [
            "label"             => $label,
            "libelle"           => $label,
            "ref_supplier"      => Carbon::parse($frais['ladate'])->format("d/m/Y") . " : " . Dolibarr::nbf($total_ttc),
            "socid"             => $dol_fournisseur_id,
            "statut"            => '0', //Note: on la laisse ouverte : lors de la fermeture dolibarr génère un numéro de facture fournisseur qu'il faut écrire au stylo sur le document avant de mettre dans le classeur ...
            "paye"              => '1',
            "datec"             => $timeStampInvoice,
            "tms"               => $timeStampInvoice,
            "date"              => $timeStampInvoice,
            "total_ht"          => Dolibarr::nbf($total_ht),
            "total_tva"         => Dolibarr::nbf($total_taxes),
            "total_localtax1"   => "0.00",
            "total_localtax2"   => "0.00",
            "total_ttc"         => Dolibarr::nbf($total_ttc),
            "cond_reglement_id"     => "1",
            "fk_account"            => "1",
            "mode_reglement_id"     => $dol_payment_id,
            "mode_reglement_code"   => $dol_payment_code,
            "multicurrency_code"        => "EUR",
            "multicurrency_tx"          => "1.00",
            "multicurrency_total_ht"    => Dolibarr::nbf($total_ht),
            "multicurrency_total_tva"   => Dolibarr::nbf($total_taxes),
            "multicurrency_total_ttc"   => Dolibarr::nbf($total_ttc),
            "datep"                     => $timeStampInvoice,
            "paid"                      => "1",
            "cond_reglement_code"       => 'RECEP',
            "mode_reglement_code"       => $dol_payment_code,
            "fk_multicurrency"          => '0',
            "fk_user_author"            => $this->_dol_user_id,
            "fk_user_valid"             => $this->_dol_user_id,
            // "lines"                     => $lines,
            "brouillon"                 => '1', //Note: voir statut=0
        ];

        Log::debug("ExportToDolibarr::Dolibarr::CreateInvoice :: json: " . json_encode($newInvoice));

        $newInvoiceResult = $this->CallAPI("POST", "supplierinvoices", json_encode($newInvoice));
        $newInvoiceResult = json_decode($newInvoiceResult, true);

        //Log::debug("ExportToDolibarr::NotifyExportNDF :: retour de l'appel supplierinvoices: " . json_encode($newInvoiceResult));
        if (is_array($newInvoiceResult) && array_key_exists("error", $newInvoiceResult)) {
            Log::debug("ExportToDolibarr::Dolibarr::CreateInvoice [error] supplierinvoices " . json_encode($newInvoiceResult));
        } else {
            //On peut maintenant "modifier" la facture (si code retour OK) pour ajouter les lignes et autres infos
            if ($newInvoiceResult > 0) {
                $newInvoice["id"] = $newInvoiceResult;
                $newInvoiceResult2 = $this->CallAPI("PUT", "supplierinvoices/$newInvoiceResult", json_encode($newInvoice));
                Log::debug("ExportToDolibarr::Dolibarr::CreateInvoice :: retour du 2° appel: " . json_encode($newInvoiceResult2));

                //On ajoute les lignes sur la facture
                $this->lignesFacture($frais, $label, $total_ht, $total_ttc, $total_taxes, $newInvoiceResult);
                //Puis le règlement
                $payment = new stdClass;
                $payment->amount        = $total_ttc;
                $payment->type          = $dol_payment_code;
                $payment->date          = $frais['ladate'];
                $payment->datepaye      = $timeStampInvoice;
                $payment->paiementid    = $dol_payment_id;
                $payment->closepaidinvoices = "no";
                $payment->accountid     = $dol_bank_id;

                Log::debug("ExportToDolibarr::Dolibarr::CreateInvoice :: Etape 3: " . json_encode(array($payment)));
                $newInvoiceResult3 = $this->CallAPI("POST", "supplierinvoices/$newInvoiceResult/payments", json_encode($payment));
                Log::debug("ExportToDolibarr::Dolibarr::CreateInvoice :: retour du 3° appel: " . json_encode($newInvoiceResult3));


                // =====================================================================================================
                // Ensuite il faut joindre les fichiers provenant de doliscan ...
                //On ajoute la pièce jointe ...
                $lde = LdeFrais::where('id', $frais['id'])->first();

                $imgFic = $lde->getFullFileName();
                $pdfFic = str_replace(["jpeg","jpg"], "pdf", $imgFic);
                if (\file_exists($pdfFic)) {
                    $imgFic = $pdfFic;
                }
                Log::debug("ExportToDolibarr::Dolibarr::CreateInvoice :: Upload pièce jointe: " . $imgFic);
                $this->UploadDocument($newInvoiceResult, $imgFic, "supplierinvoices", "supplier_invoice");
            }
        }
    }


    /**
     * lignesFacture : Génère les lignes de facture et retourne un tableau
     *
     * @param  mixed $frais : l'objet frais complet
     * @param  mixed $label : l'intitulé de la facture (permet d'éviter les doublons)
     * @param  mixed $total_ht : le total ht
     * @param  mixed $total_ttc : le total ttc
     * @param  mixed $totalTaxes : le total des taxes
     * @param  mixed $invoiceID : l'identifiant dolibarr de la facture
     * @return array : tableau des lignes de la facture
     */
    function lignesFacture($frais, $label, $total_ht, $total_ttc, $totalTaxes, $invoiceID)
    {
        $commentaires = $frais['label'];
        if ($frais['invites'] != "") {
            $commentaires .= "<br />Invités: " . $frais['invites'];
        }

        //Dans le cas d'une facture fournisseur si on a un multi-taux de tva on fait autant de lignes dans la facture
        //qu'il y a de taux différents ...voir plus loin
        if ($this->nbTauxTVA($frais) > 1) {
            // $commentaires .= "<p>Multi taux de TVA:<ul>";
            // $commentaires .= $this->multiTauxTVACommentaires($frais);
            // $commentaires .= "</ul></p>";
            //$commentaires .= ". Plusieurs taux de TVA sont associés à cette facture.";
            $tvaTaux       = "0.00";
        } else {
            //On repasse sur le taux par defaut si jamais rien n'est correct
            $tvaTaux = Dolibarr::DOLIBARR_DEFAULT_VAT;

            if ($frais['tvaVal1'] > 0) {
                $tvaTaux = $frais['tvaTx1'];
            } else {
                if ($frais['tvaVal2'] > 0) {
                    $tvaTaux = $frais['tvaTx2'];
                } else {
                    if ($frais['tvaVal3'] > 0) {
                        $tvaTaux = $frais['tvaTx3'];
                    } else {
                        if ($frais['tvaVal4'] > 0) {
                            $tvaTaux = $frais['tvaTx4'];
                        }
                    }
                }
            }
        }
        $label .= addslashes($frais['label']);

        $tab = array();
        //Si multi taux de tva : plusieurs lignes sinon une seule
        if ($this->nbTauxTVA($frais) > 1) {

            $tauxActifs = $this->filtreTabTVA($frais);
            //La plaie du calcul inverse est de retomber sur un total "juste" (pourcentages et arrondis font bon ménages)
            //On est donc contraint de "dérouler" l'algo et de rectifier la dernière ligne ... donc on fait en sens inverse
            $calculTotalTTC = 0;
            foreach ($tauxActifs as $ta) {
                $tvaTaux    = $frais['tvaTx' . $ta];
                $localTaxes = $frais['tvaVal' . $ta];
                $local_ht   = $localTaxes * 100 / $tvaTaux;
                $local_ttc  = $local_ht + $localTaxes;
                $calculTotalTTC += $local_ttc;
            }
            //On verifie le total de TVA
            $localVerif  = $calculTotalTTC - $total_ttc;
            //On re-déroule en "corrigeant" la 1ere entrée ...
            foreach ($tauxActifs as $ta) {
                $tvaTaux    = $frais['tvaTx' . $ta];
                $localTaxes = $frais['tvaVal' . $ta];
                $local_ht   = $localTaxes * 100 / $tvaTaux;
                $local_ttc  = $local_ht + $localTaxes;
                if ($localVerif != 0) {
                    //Le montant TTC qu'on DOIT avoir pour que le recallage soit OK
                    $recale_ttc = $local_ttc - ($calculTotalTTC - $total_ttc);
                    $local_ttc = $recale_ttc;
                    $local_ht = $this->recaleTVA($local_ht, $local_ttc, $tvaTaux, $localTaxes, $localVerif, $total_ttc);
                    Log::debug("ExportToDolibarr::NotifyExportNDF :: verification des Taxes: $total_ttc / $calculTotalTTC : $localVerif");
                    // $local_ht -= $localVerif;
                    // $local_ttc = $local_ht + $localTaxes;
                    //Pour le prochain tour de boucle on ne repassera pas dans le correcteur :)
                    $localVerif = 0;
                }
                $tab[] = $this->ligneFacture($invoiceID, $label, $commentaires, $local_ht, $local_ttc, $localTaxes, $tvaTaux);
            }
        } else {
            $tab[] = $this->ligneFacture($invoiceID, $label, $commentaires, $total_ht, $total_ttc, $totalTaxes, $tvaTaux);
        }
        return $tab;
    }


    /**
     * ligneFacture Génère une ligne de détail de la facture et l'expédie sur dolibarr
     *
     * @param  mixed $id : identifiant dolibarr de la facture à laquelle on ajoute la ligne
     * @param  mixed $label : l'intitulé
     * @param  mixed $commentaires : les commentaires
     * @param  mixed $ht : le montant ht
     * @param  mixed $ttc : le montant ttc
     * @param  mixed $tva : le montant de la tva
     * @param  mixed $tvaTx : le taux de tva
     * @return int : code retour de dolibarr
     */
    function ligneFacture($id, $label, $commentaires, $ht, $ttc, $tva, $tvaTx)
    {
        $ligne = new stdClass();

        $ligne->fk_facture_fourn        = $id;
        $ligne->pu_ht                   = Dolibarr::nbf($ht);
        $ligne->subprice                = Dolibarr::nbf($ht);
        $ligne->pu_ttc                  = Dolibarr::nbf($ttc);
        $ligne->label                   = $label;
        $ligne->description             = $commentaires;
        $ligne->qty                     = "1";
        $ligne->total_ht                = Dolibarr::nbf($ht);
        $ligne->total_ttc               = Dolibarr::nbf($ttc);
        $ligne->total_tva               = Dolibarr::nbf($tva);
        $ligne->tva_tx                  = Dolibarr::nbf($tvaTx);
        $ligne->product_type            = "0";
        $ligne->product_label           = $label;
        $ligne->rang                    = "0";
        $ligne->multicurrency_code      = "EUR";
        $ligne->multicurrency_subprice  = Dolibarr::nbf($ht);
        $ligne->multicurrency_total_ht  = Dolibarr::nbf($ht);
        $ligne->multicurrency_total_tva = Dolibarr::nbf($tva);
        $ligne->multicurrency_total_ttc = Dolibarr::nbf($ttc);
        $ligne->libelle                 = $label;
        $ligne->tva                     = Dolibarr::nbf($tva);


        Log::debug("ExportToDolibarr::NotifyExportNDF::ligneFacture $id : " . json_encode($ligne));
        $lfResult = $this->CallAPI("POST", "supplierinvoices/$id/lines", json_encode($ligne));
        Log::debug("ExportToDolibarr::NotifyExportNDF::ligneFacture retour " . json_encode($lfResult));

        return $lfResult;
    }


    /**
     * nbTauxTVA Retourne le nombre de taux de tva actifs pour ce frais
     *
     * @param  mixed $frais
     * @return int : le nombre de taux de tva utilisés pour ce frais
     */
    public function nbTauxTVA($frais)
    {
        $nb = 0;
        for ($i = 1; $i < 5; $i++) {
            if ($frais["tvaVal$i"] > 0) $nb++;
        }
        return $nb;
    }

    /**
     * multiTauxTVACommentaires Retourne un petit texte html des taux actifs et des valeures correspondantes
     *
     * @param  mixed $frais
     * @return string : code html
     */
    public function multiTauxTVACommentaires($frais)
    {
        $r = "";
        for ($i = 1; $i < 5; $i++) {
            if ($frais["tvaVal$i"]) {
                $r .= "<li>Taux: " . $frais["tvaTx$i"] . "%, Montant de la TVA: " . $frais["tvaVal$i"] . "€ </li>";
            }
        }
        return $r;
    }

    /**
     * filtreTabTVA:  DoliSCAN stocke 4 valeurs de TVA ... et on ne sait pas quelles sont les
     *                valeures utilisées, cette fonction retourne un tableau limité aux taux actifs
     *
     * @param  mixed $frais
     * @return array : liste des index de tva utilisés
     */
    function filtreTabTVA($frais)
    {
        $tab = array();
        if ($frais['tvaVal1'] > 0) {
            $tab[] = '1';
        }
        if ($frais['tvaVal2'] > 0) {
            $tab[] = '2';
        }
        if ($frais['tvaVal3'] > 0) {
            $tab[] = '3';
        }
        if ($frais['tvaVal4'] > 0) {
            $tab[] = '4';
        }
        return $tab;
    }


    /**
     * recaleTVA : A cause des arrondis les calculs inverses de TVA ne sont pas bon
     *             il faut donc un outil qui fasse le boulot et retourne le HT qui
     *             fait que HT * Taux = montant et on se donne 10 tours pour y arriver
     *
     * @param  mixed $ht : le montant ho
     * @param  mixed $ttc
     * @param  mixed $taux : le taux de tva
     * @param  mixed $montantTVA : le montant de la tva
     * @param  mixed $difference
     * @param  mixed $total_ttc : le total ttc auquel on doit arriver
     * @param  mixed $tour : le numéro du tour en cours pour stopper au bout de 10
     * @return int : le montant HT à utiliser pour avoir le bon résultat
     */
    function recaleTVA($ht, $ttc, $taux, $montantTVA, $difference, $total_ttc, $tour = 0)
    {
        Log::debug("recaleTVA : tour $tour | $ht | $ttc | $taux | $montantTVA | $difference | $total_ttc |");
        $r = 0;
        if ($tour < 10) {
            $difference = $ttc - ($ht + $montantTVA);
            //Il faut ajouter ou supprimer ?
            if (($ht + $montantTVA) < $total_ttc) {
                $ht += round($difference / 2, 4);
            } else {
                $ht -= round($difference / 2, 4);
            }
            $montantTVA = $ht * $taux / 100;
            $nouveauTTC = round($montantTVA + $ht, 2);
            Log::debug("recaleTVA nouveau ht = $ht et nouveau montant $montantTVA ... on compare $nouveauTTC et $ttc ...");
            if (round($nouveauTTC, 2) == round($ttc, 2)) {
                $r = $ht;
                Log::debug("recaleTVA c'est bon, on stoppe au $tour tour pour un resultat $r");
            } else {
                $tour++;
                $r = $this->recaleTVA($ht, $ttc, $taux, $montantTVA, $difference, $total_ttc, $tour);
            }
        }
        Log::debug("recaleTVA end of tour $tour : return $r");
        return $r;
    }

    /**
     * ConvertTypeFrais: "Transforme" un numéro de type de frais doliscan en code dolibarr ... voir la config
     *
     * @param  mixed $doliSCANid : identifiant du frais doliscan
     * @param  mixed $typeRetour = num -> il faut retourner un numéro ou = code -> il faut retourner le code en texte
     * @param  mixed $proOuPerso : si c'est un frais pro ou perso
     * @return mixed : code dolibarr ou numéro de code
     */
    function ConvertTypeFrais(int $doliSCANid, string $typeRetour, string $proOuPerso)
    {
        Log::debug("ConvertTypeFrais : $doliSCANid : $typeRetour : $proOuPerso");
        //Il faut le slug a partir de l'id
        $tf = new TypeFrais();
        //On recupere le label de ce type de frais
        $slug = $tf->getSlugFromID($doliSCANid);
        $frais = "";
        $r = "";
        if ($slug) {
            if ($proOuPerso == "pro") {
                $frais = (object) $this->getPro($slug);
            } else {
                $frais = (object) $this->getPerso($slug);
            }
        }
        if ($frais) {
            Log::debug(\json_encode($frais));
            $r = $frais->$typeRetour;
            Log::debug("et ça donne $r");
        }
        return $r;
    }

    /**
     * nbf: formate le nombre et le retourne comme une chaine
     *
     * @param  mixed $nb
     * @return string
     */
    public static function nbf($nb)
    {
        return (string) \number_format($nb, 2, ".", "");
    }
}
