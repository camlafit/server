<?php
/*
 * NotifyPluginsList.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Modules\ExportToDolibarr\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Plugin;
use Illuminate\Support\Facades\Config;

class NotifyPluginsList
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
        //disabled
        return;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //disabled
        return;

        //
        $p = Plugin::where('name', 'ExportToDolibarr')->first();
        if (!$p) {
            $p = new Plugin();
            $p->name = Config::get('exporttodolibarr.name');
        }
        $p->description = sprintf(Config::get('exporttodolibarr.desc'), route('exporttodolibarr-configurationStep0'));

        //Le tableau de configuration des commandes que ce plugin expose
        $t = array();
        $t['command'] = 'dolibarrPluginNDF';
        $t['class'] = "Modules\\ExportToDolibarr\\Sharp\\Commands\\DolibarrNdeFraisExportCommand";
        $p->commands = json_encode($t);

        $p->save();
    }
}
