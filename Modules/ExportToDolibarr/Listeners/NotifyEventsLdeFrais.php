<?php
/*
 * NotifyEventsLdeFrais.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Modules\ExportToDolibarr\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use App\NdeFrais;
use stdClass;
use Illuminate\Support\Carbon;
use App\BaseCalculIks;
use App\LdeFrais;
use App\MoyenPaiement;
use App\TypeFrais;
use Illuminate\Support\Str;
use Modules\ExportToDolibarr\Dolibarr;

use function Psy\debug;

class NotifyEventsLdeFrais
{
    private $_ldf;
    private $_contexte;
    private $_dolibarr;    //accès à l'api dolibarr
    private $_typefrais;   //l'objet Type de Frais pour convertir les id en slug
    private $_dol_ret_id;  //id de la note de frais faite dans dolibarr
    private $_dol_ret_ref; //reference de la note de frais faite dans dolibarr
    private $_dol_fact_id; //id de la dernière facture faite dans dolibarr

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
        Log::debug("ExportToDolibarr::NotifyEventsLdeFrais :: construct 1");
        //disabled
        return;

        $this->_dolibarr    = new Dolibarr();
        $this->_typefrais   = new TypeFrais();
        $this->_dol_ret_id  = 0;
        $this->_dol_ret_ref = "";
        $this->_dol_fact_id = 0;
        Log::debug("ExportToDolibarr::NotifyEventsLdeFrais :: construct 2");
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //disabled
        return;

        Log::debug("ExportToDolibarr::NotifyEventsLdeFrais :: handle pour LdeFrais ");

        //Signal: SignedPDFAvailable ou PDFAvailable : selon si l'utilisateur a coché l'option archive probante
        //Pour l'instant on veut gérer uniquement les documents signés ...
        if ($event->contexte != "PDFAvailable") {
            Log::debug("ExportToDolibarr::NotifyEventsLdeFrais :: pas pour nous, on stoppe : " . $event->contexte);
            return;
        }
        $this->_ldf                = $event->ldf;
        $this->_contexte           = $event->contexte;

        Log::debug(json_encode($this->_ldf));

        //Si c'est payé pro alors c'est une facture fournisseur ...
        //On pousse la facture fournisseur sur dolibarr donc :)
        $paiementProId = MoyenPaiement::where('is_pro', '1')->pluck('id')->toArray();;
        if (in_array($this->_ldf->moyen_paiement_id, $paiementProId)) {

            $tf = new TypeFrais();
            //On recupere le label de ce type de frais
            $label = $tf->getLabelFromID($this->_ldf->type_frais_id);

            //le slug du moyen de paiement
            $mpSlug = MoyenPaiement::where('id', $this->_ldf->moyen_paiement_id)->pluck('slug')->first();
            //et le moyen de payement dolibarr qui correspond
            $doliMP = $this->_dolibarr->getMoyensPaiementsEtBanque($mpSlug);

            //calcul du total des taxes
            $totalTaxes = $this->_ldf->tvaVal1 + $this->_ldf->tvaVal2 + $this->_ldf->tvaVal3 + $this->_ldf->tvaVal4;
            Log::debug("Total Taxes 1 : $totalTaxes pour " . $this->_ldf->ttc . " et " . $this->_ldf->ht);

            $local_ht  = $this->_ldf->ht;
            $local_ttc = $this->_ldf->ttc;
            //Pas de TVA et HT à zéro alors on est sur des frais avec zéro récup -> modif pour dolibarr
            if ($totalTaxes == 0 && $local_ht == 0) {
                Log::debug("On a affaire à un frais dont la TVA ne doit pas être récupérée ou sans TVA ...");
                $local_ht = $local_ttc;
            } else {
                //Mais si par contre le HT existe alors on calcule le total de TVA
                if ($local_ht > 0) {
                    $totalTaxes = $this->_ldf->ttc - $this->_ldf->ht;
                }
            }

            Log::debug("Total Taxes : $totalTaxes");

            $this->_dolibarr->CreateInvoice(
                $label,
                $this->_ldf,
                $this->_dolibarr->ConvertTypeFrais($this->_ldf->type_frais_id, "doli_id", "pro"),
                $local_ht,
                $local_ttc,
                $totalTaxes,
                $doliMP->doli_payment_id,
                $doliMP->doli_payment_code,
                $doliMP->doli_bank_id
            );
        }
    }
}
