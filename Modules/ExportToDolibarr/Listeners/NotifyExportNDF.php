<?php
/*
 * NotifyExportNDF.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Modules\ExportToDolibarr\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use App\NdeFrais;
use stdClass;
use Illuminate\Support\Carbon;
use App\BaseCalculIks;
use App\LdeFrais;
use App\MoyenPaiement;
use App\TypeFrais;
use Illuminate\Support\Str;
use Modules\ExportToDolibarr\Dolibarr;

use function Psy\debug;

class NotifyExportNDF
{
    private $_ndf;
    private $_lignesFraisPerso;
    private $_lignesFraisPro;
    private $_lignesIK;
    private $_utilisationDoliScanAnneeComplete;
    private $_contexte;
    private $_dolibarr;    //accès à l'api dolibarr
    private $_typefrais;   //l'objet Type de Frais pour convertir les id en slug
    private $_dol_ret_id;  //id de la note de frais faite dans dolibarr
    private $_dol_ret_ref; //reference de la note de frais faite dans dolibarr
    private $_dol_fact_id; //id de la dernière facture faite dans dolibarr

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
        Log::debug("ExportToDolibarr::NotifyExportNDF :: construct 1");
        //disabled
        return;

        $this->_dolibarr = new Dolibarr();
        $this->_typefrais = new TypeFrais();
        $this->_dol_ret_id = 0;
        $this->_dol_fact_id = 0;
        Log::debug("ExportToDolibarr::NotifyExportNDF :: construct 2");
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //disabled
        return;

        Log::debug("ExportToDolibarr::NotifyExportNDF :: handle pour NDF ");
        //Pas pour nous
        if ($event->contexte != "ExportToDolibarr") {
            Log::debug("ExportToDolibarr::NotifyExportNDF :: pas pour nous, on stoppe");
            return;
        }
        $this->_ndf                = $event->ndf;
        $this->_lignesFraisPerso   = $event->lignesFraisPerso;
        $this->_lignesFraisPro     = $event->lignesFraisPro;
        $this->_lignesIK           = $event->lignesIK;
        $this->_utilisationDoliScanAnneeComplete = $event->utilisationDoliScanAnneeComplete;
        $this->_contexte           = $event->contexte;

        // Log::debug(json_encode($this->_lignesIK));
        Log::debug($this->_ndf);

        //Si le signal est pour une note de frais
        if ($event->ndfOrJustificatifs == "ndf") {
            //On la créé si elle n'est pas déjà présente
            if (!$this->SearchNDF()) {
                $this->CreateNDF();
            }
            //On attache la pièce jointe
            $this->_dolibarr->UploadDocument($this->_dol_ret_id, $this->_ndf->_filePDFFullPath, "expensereports", "expensereport");

            //et les frais pros -> on créé des factures
            $this->CreateInvoices();
        } else {
            //C'est le fichier des justificatifs
            $this->_dolibarr->UploadDocument($this->_dol_ret_id, $this->_ndf->_filePDFFullPathJustificatifs, "expensereports", "expensereport");
        }
    }

    /**
     * SearchNDF: Recherche si cette note de frais n'est pas déjà présente sur dolibarr
     *            et stocke la référence de cette note de frais dans _dol_ret_id
     *            et _dol_ret_ref si on en trouve une
     *
     * @return -1 si erreur, 0 si cette note n'existe pas, et 1 si elle existe
     */
    function SearchNDF()
    {
        $retour      = -1;
        $finDuMois   = Carbon::parse($this->_ndf->fin)->format("Y-m-d");
        $debutDuMois = Carbon::parse($this->_ndf->fin)->startOfMonth()->format("Y-m-d");

        $listResult = [];
        $param = ["sqlfilters" => "date_debut='$debutDuMois' AND date_fin='$finDuMois'", "sortfield" => "rowid", "limit" => "1", "user_ids" => $this->_dolibarr->getUserID()];
        $listResult = $this->_dolibarr->CallAPI("GET", "expensereports", $param);
        Log::debug("ExportToDolibarr::NotifyExportNDF retour: " . $listResult);
        $listResult = json_decode($listResult, true);

        if ($listResult != "") {
            if (is_array($listResult) && array_key_exists("error", $listResult)) {
                $retour = 0;
                Log::debug("ExportToDolibarr::NotifyExportNDF [warn] La note de frais est introuvable dans les notes de frais ...");
            } else {
                Log::debug("ExportToDolibarr::NotifyExportNDF [ok] une note de frais existe déjà pour cette période : " . $listResult[0]['id']);
                $retour = 1;
                $this->_dol_ret_id  = $listResult[0]['id'];
                $this->_dol_ret_ref = $listResult[0]['ref'];
            }
        }
        return $retour;
    }


    /**
     * CreateNDF: Création d'une nouvelle note de frais et Upload des justificatifs
     *
     * @return void
     */
    function CreateNDF()
    {
        Log::debug("ExportToDolibarr::NotifyExportNDF :: CreateNDF 1");
        Log::debug(json_encode($this->_lignesFraisPerso));

        $lines   = array_merge($this->lignesNDF(), $this->lignesIK());

        $finDuMois   = Carbon::parse($this->_ndf->fin)->format("Y-m-d");
        $dernierjour = Carbon::parse($this->_ndf->fin)->format("d");
        $mois        = Carbon::parse($this->_ndf->fin)->format("m");
        $annee       = Carbon::parse($this->_ndf->fin)->format("Y");
        $debutDuMois = Carbon::parse($this->_ndf->fin)->startOfMonth()->format("Y-m-d");
        $ref = $debutDuMois;

        $newExpenseReport = [
            "lines"             => $lines,
            "ref"               => $ref,
            "label"             => $ref,
            // "socid"             => '1',
            "fk_user_author"    => "'" . $this->_dolibarr->getUserID() . "'",
            "date_debut"        => "$debutDuMois",
            "date_fin"          => "$finDuMois",
            "date_debutday"     => '1',
            "date_debutmonth"   => "$mois",
            "date_debutyear"    => "$annee",
            "date_finday"       => "$dernierjour",
            "date_finmonth"     => "$mois",
            "date_finyear"      => "$annee",
        ];

        Log::debug("ExportToDolibarr::NotifyExportNDF :: json: " . json_encode($newExpenseReport));

        $newExpenseReportResult = $this->_dolibarr->CallAPI("POST", "expensereports", json_encode($newExpenseReport));
        $newExpenseReportResult = json_decode($newExpenseReportResult, true);

        $this->_dol_ret_id = $newExpenseReportResult;

        Log::debug("ExportToDolibarr::NotifyExportNDF :: retour de l'appel CreateNDF: " . json_encode($newExpenseReportResult));
        // =====================================================================================================
        // Ensuite il faut joindre les fichiers provenant de doliscan ...
        //Maintenant qu'on a l'ID de la NDF de dolibarr il est possible d'uploader tous les jutificatifs
        $this->UploadNDFJustifs();
    }

    /**
     * UploadNDFJustifs Associe chaque depense de la note de frais à son justificatif
     * //TODO: quand l'API de dolibarr le permettra ...
     *
     * @return void
     */
    function UploadNDFJustifs()
    {
    }

    /**
     * lignesIK : génère toutes les lignes d'indemnités kilométriques
     *
     * @return void
     */
    function lignesIK()
    {
        Log::debug("Lignes IK");
        Log::debug(json_encode($this->_lignesIK));
        $lines = array();
        //Les IK ... c'est un peu spécial

        //Premier tour : le vehicule
        foreach ($this->_lignesIK as $v) {
            $ikID = $this->_typefrais->getIDfromSlug('ik');
            foreach ($v['listeIKMonth'] as $l) {
                $label = $l['label'] . " " . $l['depart'] . " -> " . $l['arrivee'] . "(" . $l['distance'] . " km) " . $v['nom'] . " - " . $v['cv'] . "CV";
                $lines[] = $this->ligneNDF("Indemnités kilométriques", $label, $l['ladate'], $ikID, $l['ttc'], 0, $l['ttc'], 0);
            }

            $local_ht = 0;
            $local_ttc = 0;

            //Le "bilan" et détail du calcul
            $texte = "<p>Détail du calcul des indemnités kilométriques pour " . $v['nom'] . " - " . $v['cv'] . "CV:<ul>
            <li>Total de la distance parcourue cette année avec ce véhicule: " . $v['totalKM'] . " Km</li>
            <li>Dont distance parcourue ce mois-ci: " . $v['kmPeriode'] . " Km</li>
            <li>Puissance du véhicule: " . $v['cv'] . " CV</li>
            <li>Opération: " . $v['detailCalcul'] . " = " . $v['Montant'] . "€ " . $v['bareme'] . "</li>
            </ul></p>";

            //Et les cas particuliers :-)
            //1. Le changement de barème quand le ministère publie l'information
            if ($v['changeBareme']) {
                $texte .= "<p>Attention, changement de barème: le nouveau barème de l'année est maintenant connu, configuré et paramétré dans DoliScan !<ul>";
                if ($v['changeBaremeImpact'] != 0) {
                    $texte .= "<li>Le total des IK depuis le début de l'année selon barème A: " . $v['MontantAnnuelBaremeA'] . " € (" . $v['DetailAnnuelBaremeA'] . ")</li>";
                    $texte .= "<li>Le total des IK depuis le début de l'année selon barème B: " . $v['MontantAnnuelBaremeB'] . " € (" . $v['DetailAnnuelBaremeB'] . ")</li>";
                    $texte .= "<li>Total des IK versées cette année pour ce véhicule (mois en cours inclus): " . $v['MontantAnnuel'] . " €</li>";
                    $texte .= "<li>Donc régule de " . $v['MontantAnnuelBaremeCorrige'] . "€ portée sur cette note de frais.</li>";
                    $local_ttc = $local_ht = $v['MontantAnnuelBaremeCorrige'];
                } else {
                    $texte .= "<li>Le changement de barème n'a aucun impact dans votre cas.</li>";
                }
                $texte .= "</ul></p>";
            }

            //2. Le changement de tranche
            if ($v['changeTranche']) {
                $texte .= "<p>Attention, changement de tranche !<ul>";
                if ($this->_utilisationDoliScanAnneeComplete || $v['KMavantDoli'] == 0) {
                    $texte .= "<li>Total des IK versées cette année pour ce véhicule (mois en cours inclus): " . $v['MontantAnnuel'] . " €</li>";
                    $texte .= "<li>Le total des IK depuis le début de l'année devrait être de " . $v['MontantAnnuelCorrige'] . " € (" . $v['detailCalculComplet'] . ")</li>";
                    $texte .= "<li>Donc régule de " . $v["MontantTrancheRegule"] . " € portée sur cette note de frais.</li>";
                    $local_ttc = $local_ht = $v['MontantTrancheRegule'];
                }
                if ($this->_utilisationDoliScanAnneeComplete != true) {
                    $texte .= "<li>Note: vous n'utilisez pas doliscan depuis le début de l'année mais vous n'aviez pas non plus utilisé ce véhicule avant donc ce calcul est juste.</li>";
                } else {
                    $texte .= "<li>Le total des IK depuis le début de l'année devrait être de " . $v["MontantAnnuelCorrige"] . " € ( " . $v['detailCalculComplet'] . ") mais comme vous n'utilisez pas DoliScan depuis le début de l'année vous devrez faire un point avec votre service RH (car DoliScan ne connait pas le montant total des IK versées depuis le début de l'année pour ce véhicule). <br />";
                    $texte .= "Référence du texte de loi sur le calcul des IK et les changements de tranches: <a href=\"https://www.impots.gouv.fr/portail/particulier/questions/jutilise-plusieurs-vehicules-comment-dois-je-appliquer-le-bareme-kilometrique\">https://www.impots.gouv.fr</a>";
                    $texte .= "</li>";
                }
                $texte .= "</ul></p>";
            }

            $lines[] = $this->ligneNDF("Indemnités kilométriques", $texte, Carbon::parse($this->_ndf->fin)->format("Y-m-d"), $ikID, $local_ht, 0, $local_ttc, 0);
        }
        return $lines;
    }


    /**
     * lignesNDF : génère toutes les lignes de la note de frais
     *
     * @return void
     */
    function lignesNDF()
    {
        $lines = array();
        //Les frais sont regroupés par type
        foreach ($this->_lignesFraisPerso as $groupeFrais) {
            foreach ($groupeFrais as $frais) {
                Log::debug("LignesNDF : " . \json_encode($frais));
                $calculTotalTTC = 0;
                $tauxActifs = $this->_dolibarr->filtreTabTVA($frais);
                //Si multi taux de tva : plusieurs lignes sinon une seule
                if ($this->_dolibarr->nbTauxTVA($frais) > 1) {
                    //La plaie du calcul inverse est de retomber sur un total "juste" (pourcentages et arrondis font bon ménages)
                    //On est donc contraint de "dérouler" l'algo et de rectifier la dernière ligne ... donc on fait en sens inverse
                    $calculTotalTTC = 0;
                    foreach ($tauxActifs as $ta) {
                        $tvaTaux    = $frais['tvaTx' . $ta];
                        $localTaxes = $frais['tvaVal' . $ta];
                        $local_ht   = $localTaxes * 100 / $tvaTaux;
                        $local_ttc  = $local_ht + $localTaxes;
                        $calculTotalTTC += $local_ttc;
                    }
                    $localVerif  = $calculTotalTTC - $frais['ttc'];
                    //On re-déroule en "corrigeant" la 1ere entrée ...
                    foreach ($tauxActifs as $ta) {
                        $tvaTaux    = $frais['tvaTx' . $ta];
                        $localTaxes = $frais['tvaVal' . $ta];
                        $local_ht   = $localTaxes * 100 / $tvaTaux;
                        $local_ttc  = $local_ht + $localTaxes;
                        if ($localVerif != 0) {
                            //Le montant TTC qu'on DOIT avoir pour que le recallage soit OK
                            $recale_ttc = $local_ttc - ($calculTotalTTC - $frais['ttc']);
                            $local_ttc = $recale_ttc;
                            $local_ht = $this->_dolibarr->recaleTVA($local_ht, $local_ttc, $tvaTaux, $localTaxes, $localVerif, $frais['ttc']);
                            Log::debug("ExportToDolibarr::NotifyExportNDF :: verification des Taxes: " . $frais['ttc'] . " / $calculTotalTTC : $localVerif");
                            //Pour le prochain tour de boucle on ne repassera pas dans le correcteur :)
                            $localVerif = 0;
                        }
                        Log::debug("Ajout d'une ligne multiple taux de tva : $ta | $tvaTaux | $localTaxes | $local_ht | $local_ttc");
                        $lines[] = $this->ligneNDF($frais['label'], $frais['invites'], $frais['ladate'], $frais['type_frais_id'], $local_ht, $localTaxes, $local_ttc, $tvaTaux);
                    }
                } else {
                    Log::debug("Ajout d'une ligne simple");
                    $ta = reset($tauxActifs);
                    if ($ta) {
                        $tvaTaux = $frais['tvaTx' . $ta];
                        $localTaxes = $frais['tvaVal' . $ta];
                    } else {
                        //On a affaire à des frais sans tva
                        $tvaTaux = "0.00";
                        $localTaxes = "0.00";
                    }
                    $lines[] = $this->ligneNDF($frais['label'], $frais['invites'], $frais['ladate'], $frais['type_frais_id'], $frais['ht'], $localTaxes, $frais['ttc'], $tvaTaux);
                }
            }
        }
        return $lines;
    }

    /**
     * ligneNDF: génère une ligne de note de frais
     *
     * @param  mixed $label : l'intitulé
     * @param  mixed $invitesOrDetails : la liste des invités ou le détail à utiliser
     * @param  mixed $ladate : la date du frais
     * @param  mixed $type_frais_id : le code dolibarr correspondant à ce frais
     * @param  mixed $ht : le montant ht
     * @param  mixed $tva : le montant de la tva
     * @param  mixed $ttc : le ttc
     * @param  mixed $tvaTX : le taux de tva appliqué
     * @return object : une ligne
     */
    function ligneNDF($label, $invitesOrDetails, $ladate, $type_frais_id, $ht, $tva, $ttc, $tvaTX)
    {
        Log::debug("ExportToDolibarr::NotifyExportNDF :: ligneNDF");
        $ligne = new stdClass();

        $commentaires = addslashes($label);
        if ($invitesOrDetails != "" && $type_frais_id != $this->_typefrais->getIDfromSlug('ik')) {
            $commentaires .= " invités: " . addslashes($invitesOrDetails);
        }
        if ($type_frais_id == $this->_typefrais->getIDfromSlug('ik')) {
            $commentaires = $invitesOrDetails;
        }

        $ligne->qty                 = "1";
        $ligne->date                = $ladate;
        $ligne->type_fees_libelle   = addslashes($label);
        $ligne->type_fees_code      = $this->_dolibarr->ConvertTypeFrais($type_frais_id, "doli_code", "perso");
        $ligne->vatrate             = Dolibarr::nbf($tvaTX);
        $ligne->tva_tx              = Dolibarr::nbf($tvaTX);

        $ligne->total_ht            = Dolibarr::nbf($ht);
        $ligne->total_tva           = Dolibarr::nbf($tva);
        $ligne->total_ttc           = Dolibarr::nbf($ttc);
        $ligne->value_unit          = Dolibarr::nbf($ttc);
        $ligne->id                  = null;
        $ligne->rang                = "0";
        $ligne->fk_expensereport    = null;
        $ligne->fk_c_type_fees      = $this->_dolibarr->ConvertTypeFrais($type_frais_id, "doli_id", "perso");
        $ligne->comments            = $commentaires;

        Log::debug("ExportToDolibarr::NotifyExportNDF :: ligneNDF return " . json_encode($ligne));
        return $ligne;
    }

    /**
     * CreateInvoices : On créé les factures fournisseurs à partir des frais payés par un moyen de paiement pro
     *
     * @return void
     */
    function CreateInvoices()
    {
        Log::debug("ExportToDolibarr::NotifyExportNDF :: CreateInvoices 1");
        Log::debug(json_encode($this->_lignesFraisPro));

        //Les frais sont regroupés par type
        foreach ($this->_lignesFraisPro as $typeFraisID => $groupeFrais) {
            foreach ($groupeFrais as $frais) {
                //On recupere le label de ce type de frais
                $label = $this->_typefrais->getLabelFromID($typeFraisID);

                //le slug du moyen de paiement
                $mpSlug = MoyenPaiement::where('id', $frais['moyen_paiement_id'])->pluck('slug')->first();
                //et le moyen de payement dolibarr qui correspond
                $doliMP = $this->_dolibarr->getMoyensPaiementsEtBanque($mpSlug);

                //calcul du total des taxes
                $totalTaxes = $frais['tvaVal1'] + $frais['tvaVal2'] + $frais['tvaVal3'] + $frais['tvaVal4'];
                Log::debug("Total Taxes 1 : $totalTaxes pour " . $frais['ttc'] . " et " . $frais['ht']);

                $local_ht  = $frais['ht'];
                $local_ttc = $frais['ttc'];
                //Pas de TVA et HT à zéro alors on est sur des frais avec zéro récup -> modif pour dolibarr
                if ($totalTaxes == 0 && $local_ht == 0) {
                    Log::debug("On a affaire à un frais dont la TVA ne doit pas être récupérée ou sans TVA ...");
                    $local_ht = $local_ttc;
                } else {
                    //Mais si par contre le HT existe alors on calcule le total de TVA
                    if ($local_ht > 0) {
                        $totalTaxes = $frais['ttc'] - $frais['ht'];
                    }
                }

                Log::debug("Total Taxes : $totalTaxes");

                $this->_dolibarr->CreateInvoice(
                    $label,
                    $frais,
                    $this->_dolibarr->ConvertTypeFrais($typeFraisID, "doli_id", "pro"),
                    $local_ht,
                    $local_ttc,
                    $totalTaxes,
                    $doliMP->doli_payment_id,
                    $doliMP->doli_payment_code,
                    $doliMP->doli_bank_id
                );
            }
        }
    }
}
