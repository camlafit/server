<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('exporttodolibarr')->group(function() {
    Route::get('/', 'ExportToDolibarrController@index')->name("exporttodolibarr-index");

    //Configuration on donne l'uri du serveur et la clé de l'api
    Route::get('/configurationStep0', 'ExportToDolibarrController@configurationStep0')->name("exporttodolibarr-configurationStep0");
    Route::post('/configurationStep0', 'ExportToDolibarrController@configurationStep0Update');

    //Ensuite on connecte les frais pro avec les fournisseurs pour lesquels il faut créer des factures
    Route::get('/configurationStep1', 'ExportToDolibarrController@configurationStep1')->name("exporttodolibarr-configurationStep1");
    Route::post('/configurationStep1', 'ExportToDolibarrController@configurationStep1Update');

    //Et enfin on connecte les frais perso avec les "frais" dolibarr
    Route::get('/configurationStep2', 'ExportToDolibarrController@configurationStep2')->name("exporttodolibarr-configurationStep2");
    Route::post('/configurationStep2', 'ExportToDolibarrController@configurationStep2Update');

    //Fin de l'assistant configuration
    Route::get('/configurationStep3', 'ExportToDolibarrController@configurationStep3')->name("exporttodolibarr-configurationStep3");
    Route::post('/configurationStep3', 'ExportToDolibarrController@configurationStep3Update');
});
