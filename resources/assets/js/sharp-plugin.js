import Sharp from 'sharp-plugin';
// import VueZoomer from "vue-zoomer";
import 'viewerjs/dist/viewer.css'
import Viewer from 'v-viewer'
// import TextIcon from './components/TextIcon';
// Vue.use(Sharp, {
//     customFields: {
//         'textIcon': TextIcon
//     }
// });
import ImageZoomable from './components/ImageZoomable.vue';
import CustButton from './components/CustButton.vue';

Vue.use(Viewer, {
    defaultOptions: {
        zIndex: 9999,
        button: false,
        navbar: false,
        title: false,
        toolbar: {
            zoomIn: 1,
            zoomOut: 1,
            flipHorizontal: 1,
            flipVertical: 1,
            reset:1,
            prev: {
                show: false,
            },
            play: {
                show: false,
            },
            next: {
                show: false,
            },
        },
        tooltip: false,
        movable: true,
        zoomable: true,
        rotatable: false,
        scalable: true,
        transition: true,
        fullscreen: false,
        keyboard: false,
        inline: true,
        minHeight: 400,
        zoomOnWheel: true,
        slideOnTouch: false,
    }
})
Vue.use(Sharp, {
    customFields: {
        'imageZoomable': ImageZoomable,
        'custButton': CustButton,
    }
});
