      <h3>Frais payés par un moyen de paiement {{ $persopro }}</h3>

      @foreach($typeFrais as $type)
      @php($totalHT = 0)
      @php($totalTTC = 0)
      @php($totalKM = 0)
      @foreach($lignes[$type->id] as $ligne)
      @php($totalHT += $ligne->ht)
      @php($totalTTC += $ligne->ttc)
      @if($ligne->distance > 0)
      @php($totalKM += $ligne->distance)
      @endif
      @endforeach

      <table style="width: 100%;">
        <caption style="caption-side: top; border-bottom: solid black 1px; border-top: solid black 2px; text-align: left;"><h4>{{ $type->label }}</h4></caption>
        <tbody style="border-top: solid black 1px;">
        <tr>
          <th class="td80">Date</th>
          <th>Résumé</th>

          @if($persopro == "professionnel")
          <th class="td80">Moyen</th>
          @else
          <th class="tdvide"> </th>
          @endif

          @if($totalHT > 0)
          <th class="td10nb">HT</th>
          <th class="td10nb">TTC</th>
          @else
          <th class="tdvide"> </th>
          <th class="td80nb">Montant</th>
          @endif
          <th class="td80nb">Actions</th>
        </tr>
        @foreach($lignes[$type->id] as $ligne)
        <tr>
          <td class="td80nb">{{ $ligne->ladate }}</td>
          <td>{!! Illuminate\Support\Str::limit($ligne->getResume(),110) !!}</td>

          @if($persopro == "professionnel")
          <td class="td80"> {{ Illuminate\Support\Str::limit($ligne->moyenPaiement()->first()->label,110) }} </td>
          @else
          <td class="tdvide"> </td>
          @endif

          @if($ligne->ht > 0)
          <td class="td10nb">{{ nbFR($ligne->ht) }}€</td>
          @else
          <td class="tdvide"> </td>
          @endif
          <td class="td10nb">{{ nbFR($ligne->ttc) }}€</td>
          <td class="td80nb">
            @if($status == 1)
            <a href="{{ route('webLdFUpdate', ['id' => $ligne->sid]) }}">Éditer</a> | <a href="{{ route('webLdFDelete', ['id' => $ligne->sid]) }}">Supprimer</a>
            @else
            -
            @endif
          </td>
        </tr>
        @endforeach

        @if($totalKM > 0)
        @php($totalKMs = "(" . $totalKM . " km)")
        @else
        @php($totalKMs = "")
        @endif

        <tr>
          <td colspan="3" class="tdtotalleft">Total {{ $totalKMs }}</td>
          @if($totalHT > 0)
          <td class="tdtotalnb">{{ nbFR($totalHT) }}€</td>
          @else
          <td class="tdtotalvide"> </td>
          @endif
          <td class="tdtotalnb">{{ nbFR($totalTTC) }}€</td>
          <td class="td80nb"> </td>
        </tr>
      </tbody>
      </table>
      <br />
      @endforeach


      <table style="width: 100%;">
        <tr>
          <th colspan="2" style="border-bottom: solid black 1px; border-top: solid black 2px;">
            <h4>Récapitulatif {{ $recap }}</h4>
          </th>
        </tr>
        @php($grandTotal = 0)
        @foreach($typeFrais as $type)

        @php($total = 0)
        @foreach($lignes[$type->id] as $ligne)
        @php($total += $ligne->ttc)
        @endforeach

        <tr>
          <td>Total <i>{{ $type->label }}</i> </td>
          <td class="td10nb">{{ nbFR($total) }}€</td>
        </tr>

        @php($grandTotal += $total)
        @endforeach
        <tr>
          <td style="border-top: solid black 1px; border-bottom: solid black 1px; background: #eee;">Total global ({{ $persopro }})</td>
          <td style="border-top: solid black 1px; border-bottom: solid black 1px; background: #eee; text-align: right; padding-right: 10px;">{{ nbFR($grandTotal) }}€</td>
        </tr>

      </table>

