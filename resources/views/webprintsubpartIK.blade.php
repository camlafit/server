@if($vehicules->count() > 0)

<tr>
    <td colspan="3" class="tdh2">
        <h2>Indemnités kilométriques</h2>
    </td>
    <td class="tdh2">
        &nbsp;
    </td>
    <td class="tdh2">
        &nbsp;
    </td>
    <td class="tdh2">
        &nbsp;
    </td>
</tr>

@foreach($vehicules as $vehicule)
@php($totalHT = 0)
@php($totalTTC = 0)
@php($totalKM = 0)
@foreach($lignesIK[$vehicule]["listeIKMonth"] as $ligne)
@php($totalHT += $ligne->ht)
@php($totalTTC += $ligne->ttc)
@if($ligne->distance > 0)
@php($totalKM += $ligne->distance)
@endif
@endforeach

<tr>
    <td colspan="3" class="tdh3">
        <h3>Véhicule : <i>{{ $lignesIK[$vehicule]["nom"] }} - {{ $lignesIK[$vehicule]["cv"] }} CV</i></h3>
    </td>
    <td class="tdh3">
        &nbsp;
    </td>
    <td class="tdh3">
        &nbsp;
    </td>
    <td class="tdh3">
        &nbsp;
    </td>
</tr>

@foreach($lignesIK[$vehicule]["listeIKMonth"] as $ligne)
<tr>
    <td class="td10nb date">{{ $ligne->ladate }}</td>
    @if($persopro == "professionnel")
    <td class="td80">{!! Illuminate\Support\Str::limit($ligne->getResume(),110) !!}</td>
    <td class="td10nb"> {{ $ligne->moyenPaiement()->first()->label }} </td>
    @else
    <td class="td80" colspan="2">{!! Illuminate\Support\Str::limit($ligne->getResume(),110) !!}</td>
    @endif

    @if($ligne->ht > 0)
    <td class="td10nb">{{ nbFR($ligne->ht) }}&nbsp;€</td>
    @else
    <td class="tdvide"> </td>
    @endif
    <td class="tdvide"> </td>
    <td class="td10nb">{{ nbFR($ligne->ttc) }}&nbsp;€</td>
</tr>
@endforeach

{{-- si on a une regule pour changement de bareme --}}
@if($lignesIK[$vehicule]["changeBaremeImpact"])
<tr>
    <td class="td10nb"></td>
    <td class="td80" colspan="2">Régule pour changement de barème</td>
    <td class="tdvide"> </td>
    <td class="tdvide"> </td>
    <td class="td10nb">
        {{ nbFR($lignesIK[$vehicule]["MontantAnnuelBaremeCorrige"]) }}&nbsp;€
    </td>
</tr>
@php($totalTTC += $lignesIK[$vehicule]["MontantAnnuelBaremeCorrige"])
@endif


{{-- si on a une regule pour changement de tranche --}}
@if($lignesIK[$vehicule]["changeTranche"] && (($utilisationDoliScanAnneeComplete == true) || ($lignesIK[$vehicule]["KMavantDoli"] == 0)))
<tr>
    <td class="td10nb"></td>
    <td class="td80" colspan="2">Régule pour changement de tranche</td>
    <td class="tdvide"> </td>
    <td class="tdvide"> </td>
    <td class="td10nb">
        {{ nbFR($lignesIK[$vehicule]["MontantTrancheRegule"]) }}&nbsp;€
    </td>
</tr>
@php($totalTTC += ($lignesIK[$vehicule]["MontantTrancheRegule"]))
@endif

@if($totalKM > 0)
@php($totalKMs = $lignesIK[$vehicule]["nom"] . " - " . $lignesIK[$vehicule]["cv"] . " CV (" . $totalKM . "
km)")
@else
@php($totalKMs = "")
@endif

<tr>
    <td colspan="3" class="tdtotalleft">Total {{ $totalKMs }}</td>
    @if($totalHT > 0)
    <td class="tdtotalnb">{{ nbFR($totalHT) }}&nbsp;€</td>
    @else
    <td class="tdtotalvide"> </td>
    @endif
    <td class="tdtotalvide"> </td>
    <td class="tdtotalnb">{{ nbFR($totalTTC) }}&nbsp;€</td>
</tr>

<tr>
    <td colspan="6" class="tdDetailCalcul">
        <div class="detailCalculIK">
            <ul title="Détail du calcul des indemnités kilométriques pour {{ $lignesIK[$vehicule]["nom"] }} - {{ $lignesIK[$vehicule]["cv"] }} CV:">
                @if(($utilisationDoliScanAnneeComplete == true) || ($lignesIK[$vehicule]["KMavantDoli"] == 0))
                <li>Total de la distance parcourue cette année avec ce véhicule : {{ $lignesIK[$vehicule]["totalKM"] }} km
                </li>
                @else
                <li>Total de la distance parcourue cette année avec ce véhicule (y compris la distance initiale parcourue avant
                    l'utilisation de DoliScan) : {{ $lignesIK[$vehicule]["totalKM"] }} km</li>
                @endif
                <li>Dont distance parcourue ce mois-ci : {{ $lignesIK[$vehicule]["kmPeriode"] }} km</li>
                <li>Puissance du véhicule : {{ $lignesIK[$vehicule]["cv"] }} CV</li>
                <li>Opération : {{ $lignesIK[$vehicule]["detailCalcul"] }} = {{ $lignesIK[$vehicule]["Montant"] }}&nbsp;€
                    {{ $lignesIK[$vehicule]["bareme"] }}</li>
                @if($lignesIK[$vehicule]["changeBareme"])
                <li>Attention, changement de barème: le nouveau barème de l'année est maintenant connu, configuré et
                    paramétré dans DoliScan !
                    <ul>
                        @if($lignesIK[$vehicule]["changeBaremeImpact"] != 0)
                        <li>Le total des IK depuis le début de l'année selon barème A
                            {{ nbFR($lignesIK[$vehicule]["MontantAnnuelBaremeA"]) }}&nbsp;€
                            ({{ $lignesIK[$vehicule]["DetailAnnuelBaremeA"] }})</li>
                        <li>Le total des IK depuis le début de l'année selon barème B
                            {{ nbFR($lignesIK[$vehicule]["MontantAnnuelBaremeB"]) }}&nbsp;€
                            ({{ $lignesIK[$vehicule]["DetailAnnuelBaremeB"] }})</li>
                        <li>Total des IK versées cette année pour ce véhicule (mois en cours inclus):
                            {{ $lignesIK[$vehicule]["MontantAnnuel"] }}&nbsp;€</li>
                        <li>Donc régule de {{ nbFR($lignesIK[$vehicule]["MontantAnnuelBaremeCorrige"]) }}&nbsp;€ portée sur
                            cette note de frais.</li>
                        @else
                        <li>Le changement de barème n'a aucun impact dans votre cas.</li>
                        @endif
                    </ul>
                </li>
                @endif
                @if($lignesIK[$vehicule]["changeTranche"])
                <li>Attention, changement de tranche !
                    <ul>
                        @if(($utilisationDoliScanAnneeComplete == true) || ($lignesIK[$vehicule]["KMavantDoli"] == 0))
                        <li>Total des IK versées cette année pour ce véhicule (mois en cours inclus):
                            {{ $lignesIK[$vehicule]["MontantAnnuel"] }}&nbsp;€</li>
                        <li>Le total des IK depuis le début de l'année devrait être de
                            {{ nbFR($lignesIK[$vehicule]["MontantAnnuelCorrige"]) }}&nbsp;€
                            ({{ $lignesIK[$vehicule]["detailCalculComplet"] }} )</li>
                        <li>Donc régule de {{ nbFR($lignesIK[$vehicule]["MontantTrancheRegule"]) }}&nbsp;€ portée sur cette
                            note de frais.</li>
                        @if($utilisationDoliScanAnneeComplete != true)
                        <li>Note: vous n'utilisez pas doliscan depuis le début de l'année mais vous n'aviez pas non plus utilisé
                            ce véhicule avant donc ce calcul est juste.</li>
                        @endif
                        @else
                        <li>Le total des IK depuis le début de l'année devrait être de
                            {{ nbFR($lignesIK[$vehicule]["MontantAnnuelCorrige"]) }}&nbsp;€
                            ({{ $lignesIK[$vehicule]["detailCalculComplet"] }} ) mais comme vous n'utilisez pas DoliScan
                            depuis le début de l'année vous devrez faire un point avec votre service RH (car DoliScan ne
                            connait pas le montant total des IK versées depuis le début de l'année pour ce véhicule). <br />
                            Référence du texte de loi sur le calcul des IK et les changements de tranches: <a href="https://www.impots.gouv.fr/portail/particulier/questions/jutilise-plusieurs-vehicules-comment-dois-je-appliquer-le-bareme-kilometrique">https://www.impots.gouv.fr</a>
                        </li>
                        @endif
                    </ul>
                </li>
                @else
                @endif
            </ul>
        </div>
    </td>
</tr>

@endforeach
@endif
