@extends('emails.template_text')

@section('content')

Suivi automatique du {{  $datedujour }}


Vous avez quelques factures incomplètes...
------------------------------------------

Il est temps de vérifier votre note de frais du mois car quelques tickets font encore apparaître un montant nul. Vous pouvez corriger ces informations depuis l'application où en cliquant sur les liens ci-dessous.

@foreach($lignes as $ligne)
    * {{ $ligne['label'] }} du {{ dateFR($ligne['ladate']) }} : {{ $currentURI }}/webLdfUpdate/{{ $ligne['sid'] }}
@endforeach

@endsection('content')
