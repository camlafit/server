@extends('emails.template')

@section('content')
<h2>Note de frais...</h2>

<p>Vous recevez cette note de frais car l'utilisateur {{ $userName }} ({{ $userEmail }}) vous a désigné(e) comme destinataire de ses documents comptables.</p>

{{-- <h2>Fichier au format FEC</h2>

<p>Un fichier au format FEC est proposé ci-joint</p> --}}

<h2>Fichier à importer dans Quadratus</h2>

<p>Le fichier quadratus.zip vous permet d'importer les pièces justificatives automatiquement.</p>

<p>Pour ce faire vous devrez extraire le contenu du fichier ZIP et importer ensuite le fichier .TXT, tous les documents PDF présents dans le même répertoire que le fichier TXT seront importés dans quadratus.</p>

<p><i>Idée : faire quelques captures d'écran de la procédure et expliquer qu'il faut créer un journal "NDF".</i></p>

<h2>Documents à télécharger</h2>

<p>Veuillez cliquer sur les liens ci-dessous pour télécharger vos documents :</p>

<ul>
    @foreach($attachFiles as $ficURI)
    <li><a href="{{ $ficURI['uri'] }}">{{ $ficURI['label'] }}</a></li>
    @endforeach
</ul>

@endsection('content')
