<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ config('app.name', 'DoliScan') }}</title>
    <style type="text/css">
        @viewport {
            width: device-width;
            zoom: 1.0;
        }
        @media screen and (max-width: 600px) {
            body {
            }
        }

        @media screen and (min-width: 610px) {
            h2,p,ul,li {
                width: 800px;
            }
        }

        body {
            font-family: sans-serif;
            font-size: 11pt;
        }

        h1 {
            font-family: 'Nunito', sans-serif;
            font-size: +300%;
            margin-bottom: 0px;
            margin-top: 0px;
        }

        h2 {
            padding-left: 10px;
            background-color: gray;
            color: #fff;
        }

        p {
            margin: 0;
            padding: 0;
        }
        ul, li {
            font-size: 11pt;
        }

        .soustitre {
            margin-left: 40px;
            margin-top: -10px;
            font-family: Georgia, serif;
            font-size: 13pt;
            line-height: 13pt;
            color: #000000;
            font-style: italic
        }

        .ladate {
            margin-left: 200px;
            margin-top: 10px;
            font-family: Georgia, serif;
            font-size: 13pt;
            line-height: 13pt;
            color: #000000;
        }
    </style>
</head>

<body>
    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAAA9CAYAAADoByY0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAADXwAAA18BGTBJ2QAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAA4lSURB
        VHic7Z17lFVVHcc/lxlghhlAcGQUdBB5JCAIGJCCD5QyMCPSfCSpYSFq5iNFc6kt1DJFMiWURCsS
        TVNXSwPfYIDvBCHLByoQiEiYjIKID7A/vvt4L3fuOfc89jn3Dp3PWsOa4Z6797737N/ev/37ffc+
        FezcPAS8DawqcTtSmiktSt2AlJRyJjWQlBQPUgNJSfEgNZCUFA9SA0lJ8SA1kJQUD1IDSUnxIDWQ
        lBQPUgOxRw2QKXUjgGqgdakbsbNQCfw2wPUbgS3AWmA5sAT4MIZ2NQf2AU4Dvg50Qd9DDfAJ8BJw
        P3AnsDmm+iuBQ4HDgSFAL6AWaAQqgFZoAHwNeBF4FngQ+CCm9uSzJ3A2cJHlcuuBK4BZwNOWy85n
        egbd0JN8vqEK3YSuwL7AYODfwD3An4B3YmhkFB4CrgWesFhmG2AqMAKYDswBVua8XgUcAJwAjAF+
        BvzeYv17AucAJwLLgEeARcgQtuRdW4nuU39kSEcCL5j2P2mxTYW4AjjX1PmMxXJ7Af9Eg3R/YJPF
        svPZ+sU/IckAA4FrgDXA74Du0dtljYdQR7ZFF2Ap6vSVPq7fC1gI3ER0d7Yd8GvUMS4FOoUoowIY
        i2aTeUDviG1yoyXyME4Ebrdcdi80Q98N3Gq57HwiG0gu1cBZyFCuQlN8qbFpIO2Qq3J8wPe1Ah4D
        Jkeo+wg0U/8KaBuhnFy+A6wAJmF/7XQccAsyyFeB3SyW7RjIrmiwONpi2flYNRCHDsAfgMWUfjax
        aSA3oJkjDB3RzRwS4r2TkBp5WMi6vegAPIzc4yqL5T4BDDK/X4LddYhjIACjkFq7zmL5ucRiIA6n
        Am8RrlPYwpaBdAPeJFon+gEwO+B7pqBZqz5CvcWoAGagBbyN6Fdv4Lmcv3dHs4itiGmugQDcjNbA
        cRCrgQCMBNaRHU2SxpaBXARcHbGMlgRzOy9FUcL2Eev1QwaYifz6qO7WjSi6l8vdwFERy3XIN5Aa
        4HX8B5qCELuBgL6Yt9ACN2lsGcg8FAVKijHIrdo9wTpbAn9Dg1pYatBaqSbv/0cAcyOUm0u+gQAc
        BGxAET6bJGIgoBF4EcknJm0ZyHJ0Y5JgD2A9uulJE3UdMgGYVuD/M8C/kKsalUIGAvALFPK2GXBI
        zEAywALgRwnUlYstA/kv9kcnN+5EuZvmyBJgP5fXzkXpgKi4GUgrtF4700IdDokZCGjxtgHYJaH6
        wJ6BrMT9xttkEFqz1SZQl20OxDv52AG5X1FnKTcDAeiHBrOeEetw2Jqky/MK6rA/TrBOW6wmvqRa
        LhejZGBc8pQ4OQNv2dJGYD7Kv8TFSyiYMgtF56yQ1AwCyrqvIbm1iK0Z5OdIVhInuyGdVIeY64mD
        OhSIKTY7DCW67MRrBgH1rQXATyPWAwm7WA7LsCv/8MKWgfRBI6CtLHYhJgL3xlh+nFyI9F1+WIIG
        yrAUMxBQMOBdYP8I9UDCLpbDg0jA1px4GXiUeBfPI1FWu7nRAkWvbvF5/QzsLqQLsRJFTv+IheRn
        0jPIKOJXkjrYlJrUobXIOZbKy2ct8KWYyo6TUWht4ZdaJA8JG6zxM4M4PAD8MmQ9UCIXqwFNf0lg
        W83bA0VipiHZuy3ao/vgRyFcbjyAxIlBmE74gSaIgdSjqGBYHVtJDCQDfEbTbGsc+DGQgwm2tqgD
        7kNK2PEoAx2V/ZDWq7nRFc18QZXb/VBUM0xSL4iBgOT9rxOuv5VkDfI52uTSrgR159MRRTzGB3jP
        u8AxSG/0PeR2TUEL+bDUEu/Gn7iYgPz8TwK+7yWUr0hCvvMXtPPwurAFJD2DgL6cJLRZfmaQg4iW
        mOsFXIkWhs+jfEBQ92sY2h7QnGiF1hJh5SPj0EwclKAzCMiFXYW2RwehJC4WaMRJYgaxvQbxIgMc
        gqTXa1FC1O+aYn+k92pOnIC+37BUofVB0IEyjIGAZqs1yGvwS0kMpB65E0mcAJKkgeTSD20aehZ/
        sod6lD0vh1NR/LIA+FbEMqYQfKdlWAMBuB5p3fxSEgM5lOTciVIZCCg/cBHwH/wlxt5DEb7mwH5o
        NI4adeuBMvBBAh1RDKQa5bT8Rt22liKsOJwdd5wVogI4Fm3674qiXqAR9ml0CMKrltpzDfAbdMNt
        st2U/S6SYQ9APrsbLyDB32rL7YiDM4A3CBbccGM7moni2hWYy0cosDIHbb9YV+wNpYhiHYkOMXCj
        GzKCw5F4byA6XmgwWszOQ3uor8KOIG0k8a6HbkMSkhlFrpsPfDXGdtiiLRq8HkG6sag/c5HBJcVi
        tE13Jj5d2iRdrL2Rpqna5fU9UD5gbJFyqlAE5MYi1/lxsRYDfYtcE5X2wPt4S+Z7IDfL5uEJcTAR
        qWVt0RLNrH7D5FFcLIdK5MX8sMh1ia9BrkXukRv3oFMw/NAGuVlesfRyMRDQqHVVkWsWAqck0JYo
        LMP+bscrKbwTsRA2DAQk63kHnZDpRqIGUo9GyL1dXu+Jtpq6zS6FGIe3u1ZOBnIccg+9+AbKMFvb
        y2CZYejgPNs0oNyYn3yULQMB7XBdiPtSI9FM+lR0XtYql9dHI6XvRwHKvB+NZnHK0G2xnOLbduci
        FzRJnzwIxTZFhWU18BTxnEzixXQ0QZzvdVESM8gxKNPsNULcTDgB28soQlSIcppBuqOQZjEGoMjX
        3rG2Jjid0JbpuAaj0fibnWzOIKBBax2F+0AiM0hf1PmPw3sraUc0egblPeI7Wc8mdSgnUoylKIF2
        F8kv2CfgPoiNR9G4uDRjD6NgRhwnSHrxFnABOkO4ST4mbgPpTnYf+t+LXLuJcJqotihCVO70w3/u
        Zgq6cbNJbj0yEa3pCgkPWwCnUzxUHYXtaNNVKdzLO1Be5/JCL8blYg1FN/lUn9dfhg4sCEJLtI/b
        7aRzGy7Wxdg5E+tRgh1Y0BoFIO4m/oPAz0JbYd1m4qOI/1kcoECO1/0E+y6Ww65oLTQ05/9iiWK1
        AM5Dse0gW2uHomdcBNEjjURnIblhw0Ac3zhKMnEkWqQH3f7ZGvgzirR0jlC/G1VooboIbxHfHODk
        GOovxF14H7gQl4GA7vUrZNXY1g1kOJJ8P0Lwg9YyKHnjd5TNoOyzl9zB1iL9ciTOC3NObjcUuQu7
        9yGDDkVYi2QStgSNh6CcxvV4a6G6obVTkPB7FA5D35ebaxmngYDcSCcBbcVAapBmagH6wsdEKGsw
        0kT52Zt9KVLMevnoNqNYk9HxmW4Rs0IcinYe2ghf9kcj/TNopAtrKMPRJqJl+FsQX43/E0tskEGR
        SbfnfsRtILVotj8CYxsfo2yin5++yBU6Hj0r4zGU3JuN3Ckbo9sotGA6mcJBhD3QI83mUVzbbzvM
        6xwqfRt6zFohWpk670WPChvqcl1YRqGB4Q2UmR+O9wxQZdowGfgHWkuMxV+ApjW6v0mdS+xwDsqJ
        FSJuAwENHCuATzNIReqXRhSqdR7i+bx5/6eWG7gnkoqPQKP2WnSjeyP/cAba6rmtSDnTkG7I6zPO
        Ri7UCp9tqwa+D3wXuR/LUX6gGh3+1gUZ3SyUyNzus9ygdEOiwRFI0NmIgiIbkdZoF3Q6fHs0WyxE
        6xm/nxPk7pyCPm+S7IIGt6NpesBHA4ryBX3SV1AuAb5d7ht0MihUXI+mu5Uo71EuVCGJTAc0cGxA
        xhyXUXhRh4yz1tS/Eck3NpSgLSkpKSkpKSkpKSkpKSkpKSkpOzNt2DHn8GWanhxi+9nzfdB23zB1
        tEF5qVwuND/F6EjwZ6G0QbqxQmRQaPZM3LVr1TTd93++eU9BKVEpDm1Icedzso9Lbo1O+/gcdWLn
        pk8gezxQD7InGzbQ9HnqNUhFXINC0oPM7zXm9yqUCHwT2AvY17zvdPM3qFP1Q6rpSpRUdeo5HBlx
        bud6zfxUmnbnGlDnnL9PRMnnNqgfOjmuDNq7n/9ZOpn3OKrwnux4QvyBKA90uylnEFJZVJn2tzP1
        PIkMc4Bp4xsoNP8BBSjXrZ3/r3yG5ClPoQMF7kOj8fsoWbcUdYS30U3vg/ZVj0Od4jSyj1s+DGXd
        G1AnPxmpVT8EJpnfG5HBfYQSjptR0vcAU0cDMtI6lHQcDWwxbXnOlL8a5VycvT6OgZxtPs9JKIM/
        Bs1MY5HsfzjK0TQi2dB75nP0Ne3ZRjaHczYyriFoV+rl5prxSPcH0vC9adpyGer4nVAytS0aTA42
        bbvAfL63USL6tcK3I51BypHtKOGXQR3iNiS7aEQ3eBY6sbE/Gi03oxF3d3SzHYYj0V0l0m99imaj
        dei+b0Kd0jlMvAF15EOQlOd5pEe63pSxCM1ijyHFa0tTzuPISPOpQZugliID7W7aOx+N4B+b9x5h
        2tEbGd375vO/bMrpbdp8B5ohhuSUl6vkdtrW2pSzCMmfppJ9LFsNmilqkGEX1SGmBlKenA7cijrt
        OiS92YxmDGe0q0CjbBfUEWeiA/AcHJVERzRrTCa7/WASGsn7kh1Br0Mjck+yj2JwyuiCOpYjKdoV
        ZendlBgtyKoJGtCo7nTGryBjX2/+7ow6/w3AX5GEqILsaSNdUWeuNp93LzSz3oQMDmTAzuGCByPj
        yGUfZMTbkGGeh5TRhagg61lVpC5W+dEeuVjrUceYiDroTNTxjkUj6TbkRryD1i0DUad1pDhd0Rph
        E+pAY5EhdEanpyxFnfhF5DIdgASWH5tr1yDZykjkzqwyda1HLsvrpvx6dpy5QEbZiFyYWvP6QNR5
        70M+/zdRp38cbaobiGT1p5l2zUOz21qke+tr2vAo0mENQrqyLUiMuc58X44WDuRifQ0Zz3Lz3QxF
        s9ZCmp4R0AfNzAPQgFDuRzClpPjiJ8TkDf0Pt7QC6R66Rp4AAAAASUVORK5CYII=" alt="Logo DoliSCAN" />

    {{-- <h1>DoliScan</h1>
    <span class="soustitre">Votre assistant "notes de frais"</span> --}}

    @yield('content')

    <h2>Besoin d'un petit coup de pouce ?</h2>

    <p>Consultez la documentation en ligne disponible sur le site : <a href="https://doliscan.fr/docs/">doliscan.fr/docs/</a>.</p>

    <p>N'hésitez pas à nous demander de l'aide par courriel: <a href="mailto:aide@doliscan.fr">aide@doliscan.fr</a> précisez votre demande le plus possible pour qu'on puisse vous apporter une aide rapide et efficace.</p>

    {{-- <h2>Résumé du mois</h2>
    <p>
        <ul>
            <li>Ce mois-ci vous avez parcouru ... km</li>
            <li>Resto</li>
            <li>Hotel</li>
            <li>.../...</li>
        </ul>
    </p>

    <h2>Votre compte</h2>
    <p>
        <ul>
            <li>Vous utilisez DoliScan depuis le dateouverturecompte</li>
            <li>Votre profil utilisateur est complet</li>
            <li>Vous pouvez le mettre à jour depuis l'application</li>
        </ul>
    </p>


    <h2>Mais aussi ... quelques informations</h2>
    <p>
        Saviez-vous qu'en cas d'utilisation de plusieurs véhicules les indemnités kilométriques devaient être
        individualisées ?
        Pour plus de détails consultez <a href="https://www.impots.gouv.fr/portail/particulier/questions/jutilise-plusieurs-vehicules-comment-dois-je-appliquer-le-bareme-kilometrique">le
            site du ministère</a>.
    </p> --}}


    <pre>
--
Mail envoyé depuis la plate-forme doliscan.fr
{{ config('app.name') }}
{{ \Illuminate\Support\Str::limit($currentURI, 40, $end='...') }}
</pre>
</body>

</html>
