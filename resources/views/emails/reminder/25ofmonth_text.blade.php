@extends('emails.template_text')

@section('content')

C'est bientôt la fin du mois
----------------------------

On approche de la fin du mois :-)

Il vous reste encore quelques jours pour flasher les frais du mois et vérifier que tout est en ordre.

N'oubliez pas que vous pouvez vous connecter depuis un ordinateur sur le site https://doliscan.fr, peut-être plus pratique que votre smartphone pour consulter l'historique complet des frais du mois et éventuellement les corriger.

Actualité ...
-------------



@endsection('content')
