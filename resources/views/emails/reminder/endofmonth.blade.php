@extends('emails.template')

@section('content')

<h2>C'est la fin du mois :-)</h2>

<p>Et voilà le mois se termine.</p>

<p>&nbsp;</p>

<p><u>Note :</u> <i>vous pourrez encore flasher vos frais du mois en cours jusqu'au 5 du mois prochain :-)</i></p>

<p>Une nouvelle note de frais s'ouvre automatiquement et selon la date que vous entrez vos frais seront associés à la note en cours où à la nouvelle.</p>

<p>Ainsi, imaginez par exemple que nous sommes le 3 juin, vous flashez un frais du 28 mai, il ira sur la note du mois de mai (vous avez jusqu'au 5 pour ça), par contre si vous flashez un frais du 2 juin il sera inséré sur la note de frais de juin.</p>

@endsection('content')
