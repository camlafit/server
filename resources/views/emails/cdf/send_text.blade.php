@extends('emails.template_text')

@section('content')
Classeur de Notes de frais...
-----------------------------

Le classeur des notes de frais de la société regroupe l'ensemble des notes de frais des salariés de la société.

Ce classeur vous permet d'importer en une seule passe la totalité des notes de frais et des justificatifs associés.


Fichier à importer dans Quadratus
---------------------------------

Le fichier quadratus.zip proposé en téléchargement ci-dessous vous permet d'importer les pièces justificatives automatiquement.

Pour ce faire vous devrez extraire le contenu du fichier ZIP et importer ensuite le fichier .TXT, tous les documents PDF présents dans le même répertoire que le fichier TXT seront importés dans quadratus.

Documents à télécharger
-----------------------

Veuillez cliquer sur les liens ci-dessous pour télécharger vos documents :

@foreach($attachFiles as $ficURI)
    * {{ $ficURI['label'] }}: {{ $ficURI['uri'] }}
@endforeach

@endsection('content')
