@yield('content')

Besoin d'un petit coup de pouce ?
---------------------------------

Consultez la documentation en ligne disponible sur le site : https://doliscan.fr/docs/

N'hésitez pas à nous demander de l'aide par courriel: aide@doliscan.fr, précisez votre demande le plus possible pour qu'on puisse vous apporter une aide rapide et efficace.

--
Mail envoyé depuis la plate-forme doliscan.fr
{{ config('app.name') }}
{{ \Illuminate\Support\Str::limit($currentURI, 40, $end='...') }}
