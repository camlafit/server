@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
  <div class="col-md-8">
    <div class="card">
      <div class="card-header">Dashboard</div>

      <div class="card-body">
        @if (session('status'))
        <div class="alert alert-success" role="alert">
          {{ session('status') }}
        </div>
        @endif

        <p>Bienvenue !</p>

        <p>Pour bien démarrer vous pouvez suivre les quelques étapes ci-dessous:
          <ul>
            @hasrole('utilisateur')
            <li class="nav-item">
              <a href="https://www.doliscan.fr/lapplication/">Installer l'application sur votre smartphone</a>
            </li>
            @endhasrole
            {{-- <li class="nav-item">
              <a href="{{ route('webConfig') }}">Compléter votre profil</a>
            </li> --}}
            <li class="nav-item">
              <a href="/docs">Lire la documentation</a>
            </li>
            @can('show NdeFrais')
            <li class="nav-item"><a href="{{ Config::get('sharp.custom_url_segment') }}/list/ndeFrais">Voir vos notes de frais</a></li>
            @endcan
            @hasrole('responsableEntreprise')
            <li class="nav-item"><a href="{{ Config::get('sharp.custom_url_segment') }}">Accéder aux outils des Responsables d'Entreprise</a></li>
            @endhasrole
            @hasrole('adminEntreprise')
            <li class="nav-item"><a href="{{ Config::get('sharp.custom_url_segment') }}">Accéder aux outils des DSI</a></li>
            @endhasrole
            @hasrole('adminRevendeur')
            <li class="nav-item"><a href="{{ Config::get('sharp.custom_url_segment') }}">Accéder aux outils des Revendeurs</a></li>
            @endhasrole
            @hasrole('superAdmin')
            <li class="nav-item"><a href="{{ Config::get('sharp.custom_url_segment') }}">Accéder aux outils des Administrateurs</a></li>
            @endhasrole
          </ul>
        </p>
      </div>
    </div>
  </div>
</div>
@endsection
