<footer class="footer mt-auto">
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <h5 class="font-weight-bold text-uppercase mt-3 mb-4">L'application</h5>
          <ul>
            <li><a href="https://apps.apple.com/fr/app/id1455241946">Installer sur iPhone/iPad</a></li>
            <li><a href="https://play.google.com/store/apps/details?id=fr.caprel.doliscan">Installer sur Android</a></li>
            <li><a href="https://www.doliscan.fr/documentation">Documentation utilisateur</a></li>
            <li><a href="https://www.doliscan.fr/serveur">Le serveur</a></li>
            <li><a href="https://www.doliscan.fr/webservices">Les webservices</a></li>
          </ul>
        </div>
        <div class="col-md-3">
          <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Partenaires</h5>
          <ul>
            <li><a href="https://www.doliscan.fr/revendeurs">Revendeurs</a></li>
            <li><a href="https://www.doliscan.fr/experts-comptables">Experts-Comptables</a></li>
            <li><a href="https://www.doliscan.fr/entreprises">Entreprises</a></li>
            <li><a href="https://www.doliscan.fr/independants">Indépendants</a></li>
            <li><a href="https://doliscan.org/">Développeurs / Communauté</a></li>
          </ul>
        </div>
        <div class="col-md-3">
          <h5 class="font-weight-bold text-uppercase mt-3 mb-4">L'entreprise</h5>
          <ul>
            <li><a href="https://www.doliscan.fr/a-propos">À propos</a></li>
            <li><a href="https://www.doliscan.fr/mentions-legales">Mentions légales</a></li>
            <li><a href="https://www.doliscan.fr/rgpd">RGPD</a></li>
            <li><a href="https://www.doliscan.fr/cgu">CGU</a></li>
            <li><a href="https://www.doliscan.fr/contact">Contact</a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
