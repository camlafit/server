@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Création d\'un compte') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('webRegisterPost') }}">
                        @csrf

                        @if (env('APP_ENV')!='prod')
                        <div class="form-group row mb-0">
                            <p>Vous êtes sur un serveur de dev ou de démonstration. Vous pouvez créer un compte personnel si vous le souhaitez mais sachez que <a href="https://doliscan.fr/docs/1.0/g-roles#demo-link">les comptes de démonstration</a> devraient fonctionner pour vous permettre de <a href="{{ route('webLogin') }} ">tester rapidement</a> cette application ...</p>
                        </div>
                        @endif

                        <div class="form-group row">
                            <label for="firstname" class="col-md-4 col-form-label text-md-right">{{ __('Prénom') }}</label>
                            <div class="col-md-6">
                                <input id="firstname" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname') }}" required autofocus>
                                @if ($errors->has('firstname'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('firstname') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nom') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                                @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Adresse email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        {{-- <div class="form-group row">
                            <label for="code" class="col-md-4 col-form-label text-md-right">{{ __('Code coupon (offre spéciale)') }}</label>

                            <div class="col-md-6">
                                <input id="code" type="code" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" name="code" value="{{ old('code') }}" required>

                                @if ($errors->has('code'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('code') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div> --}}

                        <div class="form-group row">
                            <label for="cgu" class="col-form-label container-checkbox">
                                <input id="cgu" class="col-md-1 checkbox-cgu" type="checkbox" name="cgu" required>
                                <span class="checkmark"></span>
                                @if ($errors->has('cgu'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('cgu') }}</strong>
                                </span>
                                @endif
                                @if (env('APP_ENV')=='prod')
                                Je confirme avoir plus de 18 ans et accepte les <a href={{URL::to('/cgu')}} target="_blank">Conditions générales de service</a> DoliSCAN.<br />
                                J'ai noté que les 15 premiers jours sont gratuits et qu'à l'issu de cette période la société CAP-REL clôturera mon compte si je ne donne pas suite à la proposition d'utilisation payante de la plate-forme.<br />
                                Je n'ai aucun engagement et peux mettre fin à tout moment à la période d'essai. Une fois l'activation de l'offre commerciale tout période (d'un mois) commencée est due.
                                @else
                                J'ai bien compris que ce serveur est une installation de dév/beta/tests et m'engage à ne pas l'utiliser pour autre chose que des tests.<br />
                                @endif
                            </label>
                        </div>

                        {{-- <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Mot de passe souhaité') }}</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                            @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                </div>

                <div class="form-group row">
                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmation du mot de passe') }}</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div> --}}

                @if (env('APP_ENV')=='prod')
                <div class="form-group row mb-0">
                    <p>Si vous voulez tester l'application peut-être pouvez-vous utiliser le <a href="https://doliscan.devtemp.fr/">serveur de tests & démonstration</a> ?</p>
                </div>
                @endif

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Envoyer') }}
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
