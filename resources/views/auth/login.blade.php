@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Identification') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('webLogin') }}">
                        @csrf

                        @if (env('APP_ENV')!='prod')
                        <div class="form-group row mb-0">
                            <p>Vous êtes sur un serveur de dev ou de démo, <a href="https://doliscan.fr/docs/1.0/g-roles#demo-link">les comptes de démonstration</a> devraient fonctionner pour vous permettre de tester rapidement cette application... sinon cliquez sur le lien <a href="{{ route('webRegister') }}">{{ __('créer votre accès') }}</a>.</p>
                        </div>
                        @endif


                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Adresse mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Mot de passe') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Mémoriser mon identifiant') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Connexion') }}
                                </button>

                                @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Mot de passe oublié ?') }}
                                </a>
                                @endif


                            </div>
                        </div>



                        @if (Route::has('register'))
                        <div class="form-group row" style="margin-top: 2rem;">
                            <p><i><u>Note:</u> Si vous n'avez pas encore de compte vous pouvez <a href="{{ route('webRegister') }}">{{ __('créer votre accès') }}</a></i></p>
                        </div>
                        @endif
                        @if (env('APP_ENV')=='prod')
                        <div class="form-group row mb-0">
                            <p>Si vous voulez tester l'application peut-être pouvez-vous utiliser le <a href="https://doliscan.devtemp.fr/">serveur de tests & démonstration</a> ?</p>
                        </div>
                        @endif

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
