@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Vérification de votre adresse mail') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Un lien de vérification vous a été envoyé à votre adresse mail.') }}
                        </div>
                    @endif

                    {{ __('Avant de continuer veuillez cliquer sur le lien présent dans le mail d\'activation de votre compte'.') }}
                    {{ __('Si vous n\'avez pas reçu le mail en question, vérifiez dans vos indésirables ... ou cliquez ') }}, <a href="{{ route('verification.resend') }}">{{ __('ici pour en recevoir un nouveau') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
