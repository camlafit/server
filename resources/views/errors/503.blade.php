@extends('errors::illustrated-layout')

@section('code', '503')
@section('title', __('Service indisponible'))

@section('image')
    <div style="background-image: url({{ asset('/svg/503.svg') }});" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
    </div>
@endsection

@section('message', __($exception->getMessage() ?: 'Bonjour, une maintenance est en cours. Merci de revenir dans quelques minutes. Notre équipe technique fait de son mieux...'))
