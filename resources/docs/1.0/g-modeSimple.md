# DoliSCAN : Les différents modes d'utilisation


## Mode normal

Le mode normal est celui qui oblige l'utilisateur à saisir les informations minimales nécessaires pour la constitution de sa note de frais (montant TTC, objet, etc.).

Cette saisie est sous sa responsabilité, si une erreur de saisie est faite c'est à lui de la corriger.

## Mode simple

Le mode simple est là pour répondre uniquement à un cas très particulier d'utilisation : la personne qui s'appuie sur un secrétariat pour faire la saisie des informations. L'utilisateur choisit alors uniquement la rubrique du frais, fait une photo et c'est tout. Pour chaque frais son secrétariat recevra un courriel pour saisir les informations complémentaires (montant TTC, objet, etc.)

En fin de mois, tout frais incomplet ne pourra pas être comptabilisé…

> {info} Le mode simple est donc à utiliser de manière très exceptionnelle
