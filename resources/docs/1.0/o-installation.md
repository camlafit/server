# DoliSCAN : le backend utilisable sur un ordinateur

---

- [DoliSCAN sur ordinateur](#section-1)

<a name="section-1"></a>
## Connexion


<a name="section-2"></a>
## L'interface


<a name="section-3"></a>
## Utilisation quotidienne


<a name="section-4"></a>
## Export du tableau récap en fin de mois


<a name="section-5"></a>
## Export du fichier PDF avec tous les justificatifs


<a name="section-6"></a>
## Envoi automatique au comptable !=)


<a name="section-7"></a>
## Opérations spéciales
