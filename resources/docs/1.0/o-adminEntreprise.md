# DoliSCAN : le rôle de l'administrateur de l'entreprise (DSI)

---

- [Profil utilisateur adminEntreprise](#section-1)

<a name="section-1"></a>
## Connexion

L'administrateur de l'entreprise a à sa disposition des outils de gestion qui lui permettent de créer des collaborateurs et de configurer les comptes utilisateurs.

C'est un rôle classiquement affecté à la direction des systèmes d'information (DSI) de l'entreprise.

L'adresse de connexion est <a href="{{ Config::get('app.url')}}">{{ Config::get('app.url')}}</a>


<a name="section-2"></a>
## L'interface

Une fois connecté vous pourrez aller sur le "backend", l'arrière-plan réservé aux DSI. Via cette interface vous pourrez gérer vos collaborateurs et leur créer des comptes pour qu'ils puissent utiliser l'application sur leur smartphone.

