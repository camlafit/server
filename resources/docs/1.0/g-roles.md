# DoliSCAN : Les utilisateurs et rôles

- [Généralités](#generalites-link)
- [Utilisateurs de démonstration](#demo-link)


<a name="generalites-link">
## [Généralités](#generalites-link)

DoliSCAN propose par défaut un certain nombre de rôles concernant les profils utilisateurs :

> {info} En fonction du rôle affecté à un utilisateur, celui-ci aura ou non accès à certaines actions…

Choix du rôle principal de l'utilisateur :
- **inactif** : lorsqu'un utilisateur n'est plus actif, par exemple lorsqu'il a quitté la société, les archives sont toujours disponibles mais son compte est désactivé
- **utilisateur** : le type de compte normal, l'utilisateur peut créer des notes de frais, et les gérer depuis son smartphone ou un ordinateur
- **correcteur** : lorsqu'on a coché la case "Mode Simple" de la fiche d'un utilisateur, celui-ci peut envoyer des photos de justificatifs sans aucune information complémentaire, le correcteur aura donc la charge de compléter chaque fiche…
- **serviceComptabilite** : ce type de compte est à réserver à votre service comptable, les comptes utilisateurs de type "serviceComptabilite" reçoivent les notes de frais automatiquement au format du logiciel correspondant à leurs préférences
- **responsableEntreprise** : ce type de compte est à utiliser pour le chef d'entreprise
- **adminEntreprise** : le compte type du DSI qui pourra créer / modifier des utilisateurs pour son entreprise
- **adminRevendeur** : un compte du type Administrateur / Revendeur qui permet de créer des entreprises et clients

<a name="demo-link">
## [Utilisateurs de démonstration](#demo-link)

Si vous souhaitez tester DoliSCAN vous pouvez vous connecter à une instance de démonstration (par exemple https://doliscan.devtemp.fr/webLogin) avec l'un des comptes suivants (mot de passe **azaz** pour chaque compte):

<!-- - **admin@devtemp.fr** : le compte super administrateur qui a tous les droits -->
- **revendeur@devtemp.fr** : profil de revendeur informatique qui a les droits pour créer des entreprises
- **dsi@devtemp.fr** : profil du responsable informatique de l'entreprise qui a en charge la gestion "technique" (création des comptes etc.)
- **patron@devtemp.fr** : profil utilisateur de type chef d'entreprise (rôle "responsableEntreprise")
- **compta@devtemp.fr** : le service comptable de l'entreprise qui recevra toutes les notes de frais de tous les collaborateurs
- **secretaire@devtemp.fr** : le secretariat qui peut compléter la saisie des notes de frais du patron
- **com1@devtemp.fr** : un commercial qui gère ses notes de frais
- **com2@devtemp.fr** : un second commercial qui gère ses notes de frais

__Note:__ Le contenu de la base de tests est remis à zéro chaque nuit
