# DoliSCAN : le rôle du responsable d'entreprise

---

- [Profil utilisateur responsableEntreprise](#section-1)

<a name="section-1"></a>
## Connexion

Connectez-vous sur votre serveur DoliSCAN, pour connaitre son adresse "web" regardez dans l'application sur votre smartphone :

![Menu droite](images/app/menu-droite-01.jpg)


<a name="section-2"></a>
## L'interface

Une fois connecté, vous pourrez aller sur le "backend" réservé aux responsables d'entreprises. Via cette interface vous pourrez gérer vos collaborateurs et leur créer des comptes pour qu'ils puissent utiliser l'application sur leur smartphone.

