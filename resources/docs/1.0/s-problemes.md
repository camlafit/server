# DoliSCAN : l'application sur smartphone

---

## Problèmes possibles


<a name="section-1"></a>
## Erreur de transfert lors de l'envoi du document

Si votre connexion internet est lente, de mauvaise qualité ou carrément inexistante, l'envoi de la pièce justificative peut provoquer une erreur.

