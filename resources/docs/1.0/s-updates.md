# DoliSCAN : Les versions de l'application

---

- [1.0.20](#1.0.20)
- [1.0.19](#1.0.19)

<a name="1.0.20"></a>
## 1.0.20

Cette version apporte les modifications suivantes :


<a name="1.0.19"></a>
## 1.0.19

Cette version apporte les modifications suivantes :
- Un bouton "Valider" est maintenant présent sur chaque fiche de saisie, plus facile à "voir" que la coche bleue en haut à droite
- En cas de saisie incomplète, un petit cadre rouge entoure maintenant les champs obligatoires

