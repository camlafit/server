# DoliSCAN : l'application sur smartphone

---

<a name="section-1"></a>
## Installation

### Android

Pour installer l'application sur votre smartphone ou tablette Android vous pouvez le faire depuis [Google Play](https://play.google.com/store/apps/details?id=fr.caprel.doliscan) (installation la plus facile et normale), ou directement en [téléchargeant le fichier APK](https://download.doliscan.fr/) sur le site (réservé aux utilisateurs avancés).

### IOS

Pour installer l'application sur votre iPhone ou iPad vous devez passer par [iTunes ou App Store](https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=1455241946&mt=8)


<a name="section-2"></a>
## Activation de votre compte utilisateur

Avant de vous lancer véritablement dans l'application, pensez à valider la création de votre compte. Pour cela, suivez les indications que vous avez reçues par courrier électronique (un lien spécial permet d'activer votre compte et de choisir un mot de passe).

Si votre compte utilisateur n'a pas été créé ou que vous n'avez pas reçu le courriel d'activation contactez-nous par courrier électronique à [sav@doliscan.fr](mailto:sav@doliscan.fr).
