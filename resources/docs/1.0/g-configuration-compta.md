# DoliSCAN : Configuration de la comptabilité

DoliSCAN permet d'exporter votre note de frais sous la forme d'un beau fichier PDF à la fin du mois mais il permet **aussi et surtout** d'exporter un fichier qui sera importé automatiquement dans le logiciel comptable de la société ou de votre expert-comptable !

> {info} Les comptes par défaut peuvent être choisis au moment de la création de votre société pour qu'ils s'appliquent automatiquement à tous les utilisateurs. Pour savoir comment faire regardez à la fin de cette page

Voici la liste des comptes comptables utilisés que vous pouvez modifier pour correspondre à VOTRE plan comptable:

| Code Comptable | Type de frais | Description |
|  : | :-   |  :-  |
| 625100 | IK | ce code sera utilisé pour toutes les écritures d'indemnités kilométriques |
| 625100 | Péages & Parking | ce code sera utilisé pour toutes les écritures concernant les péages et parkings |
| 625100 | Train & Avion | ce code sera utilisé pour toutes les écritures concernant les transports en train et avion |
| 625100 | Taxi | ce code sera utilisé pour toutes les écritures concernant des frais de taxi |
| 625600 | Hôtel / Hébergement | ce code sera utilisé pour toutes les écritures concernant les frais d'hébergement (hôtel…) |
| 625700 | Restauration  | ce code sera utilisé pour toutes les écritures concernant les frais de restauration (restaurant, snack, sandwichs, café…) |
| 471 | Frais divers | ce compte d'attente sera affecté à toutes les dépenses que l'utilisateur aura classé en "Divers" et permettra donc au comptable de choisir le compte le plus adapté au cas par cas |
| 445660 | TVA déductible | ce code sera utilisé pour toutes les écritures concernant la TVA déductible |
| 606140 | Carburant zéro récup. tva | ce code sera utilisé pour toutes les écritures concernant les achats de carburant dont la TVA n'est pas récupérable |
| 606141 | Carburant 60% récup. tva | ce code sera utilisé pour toutes les écritures concernant les achats de carburant dont la TVA est récupérable à 60% (exemple en 2020 pour l'essence d'un véhicule utilitaire) |
| 606142 | Carburant 80% récup. tva | ce code sera utilisé pour toutes les écritures concernant les achats de carburant dont la TVA est récupérable à 80% (exemple en 2020 pour le diesel d'un véhicule de tourisme) |
| 606143 | Carburant 100% récup. tva | ce code sera utilisé pour toutes les écritures concernant les achats de carburant dont la TVA est récupérable à 100% (exemple en 2020 pour le diesel d'un véhicule utilitaire) |
| 421000 | Personnel | code utilisé pour tout paiement fait par des moyens personnels |
| 401CBPRO | CB de l'entreprise | ce code sera utilisé pour toutes les écritures de l'utilisateur pour les paiements réalisés à l'aide de la carte bancaire société qui lui est affecté (par exemple pour un chef d'entreprise) |
| 401ESPPRO | Espèces de l'entreprise | ce code sera utilisé pour toutes les écritures de l'utilisateur pour les paiements réalisés à l'aide d'espèces de la société |

## Exemple de fichier exporté

Une fois configuré correctement, DoliSCAN vous proposera alors par exemple un fichier respectant la structure FEC pour importer vos écritures comptables.

Exemple ci-dessous:

```
NDF	NOTE DE FRAIS	1	20181231	625100	NDF-ES PEAGE ET PARKINGS	ES1812	20181231	PEAGE ET PARKINGS	127,74	0
NDF	NOTE DE FRAIS	1	20181231	445660	NDF-ES TVA DEDUC.			ES1812	20181231	TVA DEDUC.	        25,56	0
NDF	NOTE DE FRAIS	1	20181231	421000	NDF-ES ERIC SEIGNE			ES1812	20181231	ERIC SEIGNE	        0	    153,3
```

## Choix des comptes à affecter par défaut à vos utilisateurs

Avec votre compte administrateur, qui vous permet de créer vos utilisateurs, vous pouvez choisir quels seront les comptes à affecter par défaut: il vous suffit de cliquer dans le menu "Préconfiguration" et de compléter le formulaire, vos préférences seront alors appliquées à tous les prochains comptes utilisateurs créés !

Vous n'aurez alors ensuite plus qu'à configurer les éventuels comptes spécifiques pour chaque utilisateur.

Il est donc particulièrement important de bien configurer vos comptes comptables AVANT de créer des utilisateurs.
