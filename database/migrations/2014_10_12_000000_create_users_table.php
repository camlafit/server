<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Hash;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Sur la 1ere table pour ne pas avoir de log de la phase initiale de creation
        activity()->disableLogging();

        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstname');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('adresse')->nullable();
            $table->string('cp')->nullable();
            $table->string('ville')->nullable();
            $table->string('pays')->nullable()->default('France');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('api_token', 60)->unique()->nullable();
            $table->string('send_copy_to')->nullable()->default(null); //A qui faut il envoyer les NDF en copie
            $table->string('compta_email')->nullable()->default(null); //Adresse mail du comptable
            //Les comptes comptables associés sous la forme d'un json
            $table->integer('compta_ik')->nullable()->default(625100);
            $table->integer('compta_peage')->nullable()->default(625100);
            $table->integer('compta_hotel')->nullable()->default(625600);
            $table->integer('compta_train')->nullable()->default(625100);
            $table->integer('compta_carburant0recup')->nullable()->default(606140);
            $table->integer('compta_carburant60recup')->nullable()->default(606141);
            $table->integer('compta_carburant80recup')->nullable()->default(606142);
            $table->integer('compta_carburant100recup')->nullable()->default(606143);
            $table->integer('compta_taxi')->nullable()->default(625100);
            $table->integer('compta_restauration')->nullable()->default(625700);
            $table->integer('compta_divers')->nullable()->default(471);
            $table->integer('compta_compteperso')->nullable()->default(421000);
            $table->string('compta_compteprocb', 20)->nullable()->default('401CBPRO');
            $table->string('compta_compteproesp', 20)->nullable()->default('401ESPPRO');
            $table->integer('compta_tvadeductible')->nullable()->default(44566000);
            //role principal de l'utilisateur
            $table->integer('main_role')->nullable()->default(1);
            //a true si cet utilisateur a le droit d'envoyer des photos sans aucune info sur ses LDF
            $table->boolean('mode_simple')->default(false);
            //le lien vers le correcteur
            $table->unsignedBigInteger('corrector_id')->nullable()->default(NULL);
            //qui est le "pere" de ce compte
            $table->unsignedBigInteger('creator_id')->default(1);
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('corrector_id')
                ->references('id')->on('users');
            // ->onDelete('cascade');

            $table->foreign('creator_id')
                ->references('id')->on('users');
            // ->onDelete('cascade');

            $table->softDeletes();
        });

        Schema::disableForeignKeyConstraints();
        DB::table('users')->insert(
            array(
                'firstname' => 'Super',
                'name' => 'Administrateur',
                'email' => 'admin@' . env('APP_DOMAIN'),
                'adresse' => 'Société CAP-REL',
                'password' => Hash::make('admin'),
                'creator_id' => '1',
            )
        );
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
