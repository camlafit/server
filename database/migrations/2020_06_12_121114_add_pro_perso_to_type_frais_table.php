<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddProPersoToTypeFraisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('type_frais', function (Blueprint $table) {
            //
            $table->boolean('pro')->after('account');
            $table->boolean('perso')->after('pro');
        });

        //On ajoute le pro / perso aux bonnes lignes
        DB::statement("UPDATE `type_frais` SET `pro` = '1' WHERE (`slug` != 'ik' AND `slug` != 'ik-regule')");
        DB::statement("UPDATE `type_frais` SET `perso` = '1' WHERE (`slug` != 'carburant')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('type_frais', function (Blueprint $table) {
            //
        });
    }
}
