<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntreprisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entreprises', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('adresse')->nullable();
            $table->string('cp')->nullable();
            $table->string('ville')->nullable();
            $table->string('pays')->nullable();
            $table->string('email')->nullable();
            $table->string('web')->nullable();
            $table->string('tel')->nullable();
            $table->string('siret')->nullable();
            $table->unsignedBigInteger('creator_id');
            $table->timestamps();

            $table->foreign('creator_id')
                ->references('id')->on('users');
            //Non on souhaite garder les entreprises si on supprime le createur
            // ->onDelete('cascade');

            //On essaye de ne pas avoir de doublons
            $table->unique(['email','siret']);

            // on archive les entreprises supprimees
            $table->softDeletes();
        });

        DB::table('entreprises')->insert(
            array(
                'name' => 'CAP-REL',
                'adresse' => '15 Lot. Les Jardins de Montèze',
                'cp' => '30380',
                'ville' => 'Saint Christol Les Alès',
                'pays' => 'France',
                'email' => 'contact@cap-rel.fr',
                'web' => 'cap-rel.fr',
                'tel' => '06 987 444 01',
                'siret' => '844 431 239 00012',
                'creator_id' => '1',
                'created_at' => '2018-11-01'
            )
        );

        DB::table('entreprises')->insert(
            array(
                'name' => 'Informatique-Libre',
                'adresse' => '28 Rue de Chavailles',
                'cp' => '33185',
                'ville' => 'Le Haillan',
                'pays' => 'France',
                'email' => 'contact@informatique-libre.com',
                'web' => 'informatique-libre.com',
                'tel' => '00',
                'creator_id' => '1',
                'created_at' => '2019-08-08'
            )
        );

        DB::table('entreprises')->insert(
            array(
                'name' => 'Client alpha',
                'adresse' => '12 rue du A',
                'cp' => '33000',
                'ville' => 'Bordeaux',
                'pays' => 'France',
                'email' => 'x',
                'web' => 'x',
                'tel' => 'x',
                'creator_id' => '1',
                'created_at' => now()
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entreprises');
    }
}
