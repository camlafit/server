<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class CreatePermissionTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableNames = config('permission.table_names');
        $columnNames = config('permission.column_names');

        Schema::create($tableNames['permissions'], function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('guard_name');
            $table->timestamps();
        });

        Schema::create($tableNames['roles'], function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('guard_name');
            $table->timestamps();
        });

        Schema::create($tableNames['model_has_permissions'], function (Blueprint $table) use ($tableNames, $columnNames) {
            $table->unsignedBigInteger('permission_id');

            $table->string('model_type');
            $table->unsignedBigInteger($columnNames['model_morph_key']);
            $table->index([$columnNames['model_morph_key'], 'model_type'], 'model_has_permissions_model_id_model_type_index');

            $table->foreign('permission_id')
                ->references('id')
                ->on($tableNames['permissions'])
                ->onDelete('cascade');

            $table->primary(
                ['permission_id', $columnNames['model_morph_key'], 'model_type'],
                'model_has_permissions_permission_model_type_primary'
            );
        });

        Schema::create($tableNames['model_has_roles'], function (Blueprint $table) use ($tableNames, $columnNames) {
            $table->unsignedBigInteger('role_id');

            $table->string('model_type');
            $table->unsignedBigInteger($columnNames['model_morph_key']);
            $table->index([$columnNames['model_morph_key'], 'model_type'], 'model_has_roles_model_id_model_type_index');

            $table->foreign('role_id')
                ->references('id')
                ->on($tableNames['roles'])
                ->onDelete('cascade');

            $table->primary(
                ['role_id', $columnNames['model_morph_key'], 'model_type'],
                'model_has_roles_role_model_type_primary'
            );
        });

        Schema::create($tableNames['role_has_permissions'], function (Blueprint $table) use ($tableNames) {
            $table->unsignedBigInteger('permission_id');
            $table->unsignedBigInteger('role_id');

            $table->foreign('permission_id')
                ->references('id')
                ->on($tableNames['permissions'])
                ->onDelete('cascade');

            $table->foreign('role_id')
                ->references('id')
                ->on($tableNames['roles'])
                ->onDelete('cascade');

            $table->primary(['permission_id', 'role_id'], 'role_has_permissions_permission_id_role_id_primary');
        });

        app('cache')
            ->store(config('permission.cache.store') != 'default' ? config('permission.cache.store') : null)
            ->forget(config('permission.cache.key'));

        //On ajoute les données minimales ...

        //Les lignes de frais
        Permission::create(['name' => 'show LdeFrais']);
        Permission::create(['name' => 'create LdeFrais']);
        Permission::create(['name' => 'edit LdeFrais']);
        Permission::create(['name' => 'delete LdeFrais']);

        //Les notes de frais
        Permission::create(['name' => 'show NdeFrais']);
        Permission::create(['name' => 'create NdeFrais']);
        Permission::create(['name' => 'edit NdeFrais']);
        Permission::create(['name' => 'delete NdeFrais']);

        //Les utilisateurs
        Permission::create(['name' => 'show User']);
        Permission::create(['name' => 'create User']);
        Permission::create(['name' => 'edit User']);
        Permission::create(['name' => 'delete User']);
        Permission::create(['name' => 'assign MainRole to User']);

        //Les entreprises
        Permission::create(['name' => 'show Entreprise']);
        Permission::create(['name' => 'create Entreprise']);
        Permission::create(['name' => 'edit Entreprise']);
        Permission::create(['name' => 'delete Entreprise']);

        //Les roles (globaux)
        Permission::create(['name' => 'show Role']);
        Permission::create(['name' => 'create Role']);
        Permission::create(['name' => 'edit Role']);
        Permission::create(['name' => 'delete Role']);

        //La base de calcul des ik
        Permission::create(['name' => 'show baseCalculIk']);
        Permission::create(['name' => 'create baseCalculIk']);
        Permission::create(['name' => 'edit baseCalculIk']);
        Permission::create(['name' => 'delete baseCalculIk']);

        //Le premier role - role par defaut : role minimal - aucun droit
        $role = Role::create(['name' => 'inactif']);

        // -----------------------------------------------------
        $role = Role::create(['name' => 'utilisateur']);
        $role->givePermissionTo('show LdeFrais');
        $role->givePermissionTo('create LdeFrais');
        $role->givePermissionTo('edit LdeFrais');
        $role->givePermissionTo('delete LdeFrais');

        $role->givePermissionTo('show NdeFrais');
        $role->givePermissionTo('create NdeFrais');
        $role->givePermissionTo('edit NdeFrais');
        $role->givePermissionTo('delete NdeFrais');

        // -----------------------------------------------------
        $role = Role::create(['name' => 'correcteur']);
        $role->givePermissionTo('show LdeFrais');
        $role->givePermissionTo('edit LdeFrais');
        $role->givePermissionTo('delete LdeFrais');

        $role->givePermissionTo('show NdeFrais');
        $role->givePermissionTo('edit NdeFrais');

        // -----------------------------------------------------
        $role = Role::create(['name' => 'serviceComptabilite']);

        // -----------------------------------------------------
        $role = Role::create(['name' => 'responsableEntreprise']);
        $role->givePermissionTo('show LdeFrais');
        $role->givePermissionTo('create LdeFrais');
        $role->givePermissionTo('edit LdeFrais');
        $role->givePermissionTo('delete LdeFrais');

        $role->givePermissionTo('show NdeFrais');
        $role->givePermissionTo('create NdeFrais');
        $role->givePermissionTo('edit NdeFrais');
        $role->givePermissionTo('delete NdeFrais');

        //Note: uniquement SES utilisateurs ...
        $role->givePermissionTo('show User');
        $role->givePermissionTo('create User');
        $role->givePermissionTo('edit User');
        $role->givePermissionTo('delete User');
        $role->givePermissionTo('assign MainRole to User');

        //Note: uniquement SON entreprise
        $role->givePermissionTo('show Entreprise');

        // -----------------------------------------------------
        // ... le DSI par exemple, il a le droit de créer ses utilisateurs mais pas de
        // créer d'autres entreprises
        $role = Role::create(['name' => 'adminEntreprise']);
        //Note: uniquement SES utilisateurs ...
        $role->givePermissionTo('show User');
        $role->givePermissionTo('create User');
        $role->givePermissionTo('edit User');
        $role->givePermissionTo('delete User');
        $role->givePermissionTo('assign MainRole to User');

        //Note: uniquement SON entreprise
        $role->givePermissionTo('show Entreprise');
        $role->givePermissionTo('edit Entreprise');

        // -----------------------------------------------------
        // L'administrateur revendeur peut créer des entreprises et des utilisateurs
        $role = Role::create(['name' => 'adminRevendeur']);
        // décidion mars 2020: un revendeur ne peut pas avoir de notes de frais
        // $role->givePermissionTo('show LdeFrais');
        // $role->givePermissionTo('create LdeFrais');
        // $role->givePermissionTo('edit LdeFrais');
        // $role->givePermissionTo('delete LdeFrais');
        // $role->givePermissionTo('show NdeFrais');
        // $role->givePermissionTo('create NdeFrais');
        // $role->givePermissionTo('edit NdeFrais');
        // $role->givePermissionTo('delete NdeFrais');

        $role->givePermissionTo('show User');
        $role->givePermissionTo('create User');
        $role->givePermissionTo('edit User');
        $role->givePermissionTo('delete User');
        $role->givePermissionTo('assign MainRole to User');

        $role->givePermissionTo('show Entreprise');
        $role->givePermissionTo('create Entreprise');
        $role->givePermissionTo('edit Entreprise');
        $role->givePermissionTo('delete Entreprise');

        // -----------------------------------------------------
        $role = Role::create(['name' => 'superAdmin']);
        $role->givePermissionTo('show LdeFrais');
        $role->givePermissionTo('create LdeFrais');
        $role->givePermissionTo('edit LdeFrais');
        $role->givePermissionTo('delete LdeFrais');

        $role->givePermissionTo('show NdeFrais');
        $role->givePermissionTo('create NdeFrais');
        $role->givePermissionTo('edit NdeFrais');
        $role->givePermissionTo('delete NdeFrais');

        $role->givePermissionTo('show User');
        $role->givePermissionTo('create User');
        $role->givePermissionTo('edit User');
        $role->givePermissionTo('delete User');
        $role->givePermissionTo('assign MainRole to User');

        $role->givePermissionTo('show Entreprise');
        $role->givePermissionTo('create Entreprise');
        $role->givePermissionTo('edit Entreprise');
        $role->givePermissionTo('delete Entreprise');

        $role->givePermissionTo('show Role');
        $role->givePermissionTo('create Role');
        $role->givePermissionTo('edit Role');
        $role->givePermissionTo('delete Role');

        $role->givePermissionTo('show baseCalculIk');
        $role->givePermissionTo('create baseCalculIk');
        $role->givePermissionTo('edit baseCalculIk');
        $role->givePermissionTo('delete baseCalculIk');

        //Super administrateur pour le compte admin
        $superadmin = User::where('email', 'admin@' . env('APP_DOMAIN'))->first();
        if ($superadmin) {
            $superadmin->setMainRole('superAdmin');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tableNames = config('permission.table_names');

        Schema::drop($tableNames['role_has_permissions']);
        Schema::drop($tableNames['model_has_roles']);
        Schema::drop($tableNames['model_has_permissions']);
        Schema::drop($tableNames['roles']);
        Schema::drop($tableNames['permissions']);
    }
}
