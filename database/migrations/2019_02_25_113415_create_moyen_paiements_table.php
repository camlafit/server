<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoyenPaiementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moyen_paiements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label',250);
            $table->string('slug',250);
            $table->boolean('is_pro');
            $table->timestamps();
        });

        //On a un fake pour les IK qui n'ont pas de moyen de paiement "en tant que tel"
        //mais pour garder un traitement générique j'utilise cette astuce
        DB::table('moyen_paiements')->insert(
          array(
            'label' => 'Pseudo perso IK',
            'slug' => 'pseudo-perso-ik',
            'is_pro' => false,
            'created_at' => now()
          )
        );
        DB::table('moyen_paiements')->insert(
          array(
            'label' => 'Perso',
            'slug' => 'perso',
            'is_pro' => false,
            'created_at' => now()
          )
        );
        DB::table('moyen_paiements')->insert(
          array(
            'label' => 'CB pro',
            'slug' => 'cb-pro',
            'is_pro' => true,
            'created_at' => now()
          )
        );
        DB::table('moyen_paiements')->insert(
          array(
            'label' => 'Chèque pro',
            'slug' => 'cheque-pro',
            'is_pro' => true,
            'created_at' => now()
          )
        );
        DB::table('moyen_paiements')->insert(
          array(
            'label' => 'Autre pro',
            'slug' => 'autre-pro',
            'is_pro' => true,
            'created_at' => now()
          )
        );
        DB::table('moyen_paiements')->insert(
          array(
            'label' => 'Espèces pro',
            'slug' => 'especes-pro',
            'is_pro' => true,
            'created_at' => now()
          )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moyen_paiements');
    }
}
