<?php

use Illuminate\Database\Seeder;
use App\Entreprise;

class EntreprisesTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    // Let's clear the Entreprise table first
    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    DB::table('entreprise_user')->truncate();
    DB::table('eprefs')->truncate();
    Entreprise::truncate();
    DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    $e = new Entreprise();
    $e->name = 'DevTEMP SAS';
    $e->adresse = '18 rue des Grandes Girafes';
    $e->cp = '33210';
    $e->ville = 'Pujols-sur-Ciron';
    $e->pays = 'FRANCE';
    $e->email = 'contact@devtemp.fr';
    $e->web = 'www.devtemp.fr';
    $e->creator_id = 1;
    $e->save();
  }
}
