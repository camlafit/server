<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;
use App\LdeFrais;
use App\User;
use App\BaseCalculIks;
use App\MoyenPaiement;
use App\TypeFrais;
use App\NdeFrais;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DatabaseSeeder extends Seeder
{
  /**
   * Seed the application's database.
   *
   * @return void
   */
  public function run()
  {
    //Comme un fait appel aussi a seed depuis l'objet user pour injecter des donnees il faut ruser un peu ...
    if (App::isDownForMaintenance()) {

      // $this->call(ArticlesTableSeeder::class);
      $this->call(EntreprisesTableSeeder::class);
      $this->call(UsersTableSeeder::class);
      $this->call(NdeFraisTableSeeder::class);
      $this->call(LdeFraisTableSeeder::class);

      /*
       * Note: on ne peut pas trop réfléchir à un seeder "table par table" pour les notes de frais et les lignes de frais
       *   il faut pouvoir créer une note pour un mois, ajouter des frais et ensuite clôturer la note et passer à la suivante
       *   -> donc je fais cet algo de "seed" ici et non dans chaque table
       */

      //On génère des notes de frais
      //Liste des utilisateurs qui utilisent l'application
      $dirigeants   = array('patron@' . env('APP_DOMAIN')); //A accès aux moyens de paiements pro
      $utilisateurs = array('com1@' . env('APP_DOMAIN'), 'com2@' . env('APP_DOMAIN')); //N'ont que des paiements perso
      $toutlemonde  = array_merge($dirigeants, $utilisateurs);

      $faker = \Faker\Factory::create('fr_FR');
      $faker->seed(142);
      $faker->addProvider(new \App\Faker\DoliSCAN($faker));
      $faker->addProvider(new \Faker\Provider\fr_FR\Person($faker));

      foreach ($toutlemonde as $u) {
        $this->seedDataForUser($u, $faker, $dirigeants, 12);
      }
    }
  }

  public function seedDataForUser($u, $faker, $dirigeants = array(), $nbmois)
  {
    Log::debug("DatabaseSeeder::seedDataForUser : $u");

    $user = User::where('email', $u)->first();
    if (!$user) {
      return -1;
    }
    $calculetteIK = new BaseCalculIks();
    $ldeFrais     = new LdeFrais();

    //Moyens de paiements pour dirigeants : perso et pro
    $paiementsDirigeantsIDs = MoyenPaiement::where('id', '>', 1)->pluck('id');
    //Pour les utilisateurs normaux : que perso
    $paiementsUtilisateursIDs = MoyenPaiement::where('id', '>', 1)->where('is_pro', '0')->pluck('id');

    //On garde le même couple vehicule pro/perso pour chaque utilisateur sur l'année ...
    $vehiculePro = $faker->vehiculePro;
    $vehiculePerso = $faker->vehiculePerso;

    //Moyens de paiements : selon dirigeant ou pas
    $paiementsIDs = $paiementsUtilisateursIDs;
    $is_patron = 0;
    if (in_array($u, $dirigeants)) {
      $paiementsIDs = $paiementsDirigeantsIDs;
      $is_patron = 1;
    }

    //On créé 1 an de notes de frais pour chaque utilisateur
    for ($m = $nbmois; $m > 0; $m--) {
      $debut  = Carbon::now()->subMonths($m)->startOfMonth();
      $fin  = Carbon::now()->subMonths($m)->endOfMonth();
      print "Début du mois :" . $debut . " et fin " . $fin . "\n";

      //On créé la note "ouverte"
      $ndf = NdeFrais::create([
        'label' => $debut->monthName . " " . $debut->year,
        'debut' => $debut,
        'fin' => $fin,
        'status' => NdeFrais::STATUS_OPEN,
        'montant' => 0,
        'user_id' => $user->id,
        'created_at' => $debut,
      ]);


      //Et on ajoute des frais ----------------------------------------------------------------------------------------------------------

      //Frais de restauration
      Log::debug('=================== restauration multi TVA start');
      $rID = TypeFrais::where('slug', 'restauration')->first()->id;
      for ($j = 0; $j < $faker->numberBetween(1, 18); $j++) {
        // $directory    = storage_path() . "/LdeFrais/$u/";
        // if (!is_dir($directory)) mkdir($directory);
        // $image = $faker->image($directory, 600, 400, 'technics', true, true, 'DoliScan');
        // $image = str_replace($directory . "/", "", $image);
        $ht = $faker->randomFloat(2, 50, 450);
        //Le HT ... on a de la TVA à 10 et à 20 ... on fait un bricolage pour les tests 4/5° de la note à 10% et 1/5° de la note à 20%
        $tva10 = round((float) ($ht / 5) * 4 * 0.10, 2);
        $tva20 = round((float) ($ht / 5) * 0.20, 2);
        $ttc = $ht + $tva10 + $tva20;
        $ladate = $faker->dateTimeBetween($debut, $fin)->format("Y-m-d");
        $invites = $faker->name;


        //On a un nombre variable d'invités au resto ... entre 1 et 6
        for ($inv = 1; $inv < $faker->numberBetween(0, 6); $inv++) {
          $invites .= ", " . $faker->name;
        }
        DB::enableQueryLog();
        $tab = [
          'label' => $faker->nameRestauration,
          // 'fileName' => $image,
          'ladate' => $ladate,
          'created_at' => $ladate,
          'ttc' => $ttc,
          'ht' => $ht,
          'tvaTx1' => "10%",
          'tvaVal1' => $tva10,
          'tvaTx2' => "20%",
          'tvaVal2' => $tva20,
          'invites' => $invites,
          'user_id' => $user->id,
          'type_frais_id' => $rID,
          'moyen_paiement_id' => $faker->randomElement($paiementsIDs),
          'nde_frais_id' => $ndf->id,
        ];
        LdeFrais::create($tab);
        Log::debug('Creation de frais de restauration avec multi TVA: ' . implode(',', $tab));
        Log::debug(DB::getQueryLog());
        DB::disableQueryLog();
      }
      Log::debug('=================== restauration multi TVA end');


      //Frais de carburant (Pour un véhicule utilitaire)
      $rID = TypeFrais::where('slug', 'carburant')->first()->id;

      $cv = $ldeFrais->extractVehiculeCV($vehiculePro);
      for ($i = 0; $i < $faker->numberBetween(1, 4); $i++) {
        // $directory    = storage_path() . "/LdeFrais/$u/";
        // if (!is_dir($directory)) mkdir($directory);
        // $image = $faker->image($directory, 600, 400, 'technics', true, true, 'DoliScan');
        // $image = str_replace($directory . "/", "", $image);
        $ladate = $faker->dateTimeBetween($debut, $fin)->format("Y-m-d");
        LdeFrais::create([
          'label' => $faker->nameCarburant,
          // 'fileName' => $image,
          'ladate' => $ladate,
          'created_at' => $ladate,
          'vehicule' => $vehiculePro,
          'vehiculecv' => $cv,
          'ttc' => $faker->randomFloat(2, 70, 120),
          'user_id' => $user->id,
          'type_frais_id' => $rID,
          'moyen_paiement_id' => $faker->randomElement($paiementsIDs),
          'nde_frais_id' => $ndf->id,
        ]);
      }
      //Si on est le patron on a peut-être un véhicule "particulier"
      if ($is_patron) {

        $cv = $ldeFrais->extractVehiculeCV($vehiculePerso);
        for ($i = 0; $i < $faker->numberBetween(1, 4); $i++) {
          $ladate = $faker->dateTimeBetween($debut, $fin)->format("Y-m-d");
          // $directory    = storage_path() . "/LdeFrais/$u/";
          // if (!is_dir($directory)) mkdir($directory);
          // $image = $faker->image($directory, 600, 400, 'technics', true, true, 'DoliScan');
          // $image = str_replace($directory . "/", "", $image);
          LdeFrais::create([
            'label' => $faker->nameCarburant,
            // 'fileName' => $image,
            'ladate' => $ladate,
            'created_at' => $ladate,
            'vehicule' => $vehiculePerso,
            'vehiculecv' => $cv,
            'ttc' => $faker->randomFloat(2, 70, 120),
            'user_id' => $user->id,
            'type_frais_id' => $rID,
            'moyen_paiement_id' => $faker->randomElement($paiementsIDs),
            'nde_frais_id' => $ndf->id,
          ]);
        }
      }

      //Des IK si on est pas le patron
      if (!$is_patron) {
        $rID = TypeFrais::where('slug', 'ik')->first()->id;
        for ($i = 0; $i < $faker->numberBetween(1, 40); $i++) {
          $depart = $faker->nameVille;
          $arrivee = $faker->nameVille;
          $distance = $calculetteIK->distance($depart, $arrivee);
          $cv = $ldeFrais->extractVehiculeCV($vehiculePerso);
          $ladate = $faker->dateTimeBetween($debut, $fin)->format("Y-m-d");

          echo "  IK pour $depart -> $arrivee : $distance \n";
          // $directory    = storage_path() . "/LdeFrais/$u/";
          // if (!is_dir($directory)) mkdir($directory);
          // $image = $faker->image($directory, 600, 400, 'technics', true, true, 'DoliScan');
          // $image = str_replace($directory . "/", "", $image);
          if ($distance > 0 && $distance < 1000) {
            LdeFrais::create([
              'label' => "Rdv client " . $faker->name(),
              // 'fileName' => $image,
              'depart' => $depart,
              'arrivee' => $arrivee,
              'distance' => $distance,
              'vehicule' => $vehiculePerso,
              'vehiculecv' => $cv,
              'ttc' => $calculetteIK->calcul($cv, $distance, 0),
              'ladate' => $ladate,
              'created_at' => $ladate,
              'user_id' => $user->id,
              'type_frais_id' => $rID,
              'moyen_paiement_id' => $faker->randomElement($paiementsIDs),
              'nde_frais_id' => $ndf->id,
            ]);
          } else {
            echo "    error: $distance nulle ou >= 1000km pour $depart -> $arrivee on ne fait pas l'insert !\n";
          }
        }
      }


      //Frais de péage et parking
      $rID = TypeFrais::where('slug', 'peage')->first()->id;
      for ($i = 0; $i < $faker->numberBetween(1, 20); $i++) {
        // $directory    = storage_path() . "/LdeFrais/$u/";
        // if (!is_dir($directory)) mkdir($directory);
        // $image = $faker->image($directory, 600, 400, 'technics', true, true, 'DoliScan');
        // $image = str_replace($directory . "/", "", $image);
        //Peage
        $ht = $faker->randomFloat(2, 4, 80);
        //Le HT ... on a de la TVA à 20%
        $tva20 = (float) $ht * 0.20;
        $ttc = $ht + $tva20;
        $ladate = $faker->dateTimeBetween($debut, $fin)->format("Y-m-d");

        LdeFrais::create([
          'label' => $faker->namePeage,
          // 'fileName' => $image,
          'ladate' => $ladate,
          'created_at' => $ladate,
          'ttc' => $ttc,
          'ht' => $ht,
          'tvaTx1' => "20%",
          'tvaVal1' => $tva20,
          'user_id' => $user->id,
          'type_frais_id' => $rID,
          'moyen_paiement_id' => $faker->randomElement($paiementsIDs),
          'nde_frais_id' => $ndf->id,
        ]);

        //Parking - il n'y a quasi jamais de tva indiquée sur les tickets
        $ladate = $faker->dateTimeBetween($debut, $fin)->format("Y-m-d");
        LdeFrais::create([
          'label' => $faker->nameParking,
          // 'fileName' => $image,
          'ladate' => $ladate,
          'created_at' => $ladate,
          'ttc' => $faker->randomFloat(2, 4, 80),
          'user_id' => $user->id,
          'type_frais_id' => $rID,
          'moyen_paiement_id' => $faker->randomElement($paiementsIDs),
          'nde_frais_id' => $ndf->id,
        ]);
      }

      //Frais d'hotel
      $rID = TypeFrais::where('slug', 'hotel')->first()->id;
      for ($i = 0; $i < $faker->numberBetween(1, 6); $i++) {
        $ladate = $faker->dateTimeBetween($debut, $fin)->format("Y-m-d");

        // $directory    = storage_path() . "/LdeFrais/$u/";
        // if (!is_dir($directory)) mkdir($directory);
        // $image = $faker->image($directory, 600, 400, 'technics', true, true, 'DoliScan');
        // $image = str_replace($directory . "/", "", $image);
        LdeFrais::create([
          'label' => $faker->nameHotel,
          // 'fileName' => $image,
          'ladate' => $ladate,
          'created_at' => $ladate,
          'ttc' => $faker->randomFloat(2, 50, 160),
          'user_id' => $user->id,
          'type_frais_id' => $rID,
          'moyen_paiement_id' => $faker->randomElement($paiementsIDs),
          'nde_frais_id' => $ndf->id,
        ]);
      }

      //Taxi
      $rID = TypeFrais::where('slug', 'taxi')->first()->id;
      for ($i = 0; $i < $faker->numberBetween(1, 10); $i++) {
        // $directory    = storage_path() . "/LdeFrais/$u/";
        // if (!is_dir($directory)) mkdir($directory);
        // $image = $faker->image($directory, 600, 400, 'technics', true, true, 'DoliScan');
        // $image = str_replace($directory . "/", "", $image);
        $ladate = $faker->dateTimeBetween($debut, $fin)->format("Y-m-d");

        LdeFrais::create([
          'label' => $faker->nameTaxi,
          // 'fileName' => $image,
          'ladate' => $ladate,
          'created_at' => $ladate,
          'ttc' => $faker->randomFloat(2, 10, 120),
          'user_id' => $user->id,
          'type_frais_id' => $rID,
          'moyen_paiement_id' => $faker->randomElement($paiementsIDs),
          'nde_frais_id' => $ndf->id,
        ]);
      }

      //Frais Divers
      $rID = TypeFrais::where('slug', 'divers')->first()->id;
      for ($i = 0; $i < $faker->numberBetween(1, 10); $i++) {
        $ht = $faker->randomFloat(2, 5, 100);
        //Le HT ... on a de la TVA à 20%
        $tva20 = (float) $ht * 0.20;
        $ttc = $ht + $tva20;
        // $directory    = storage_path() . "/LdeFrais/$u/";
        // if (!is_dir($directory)) mkdir($directory);
        // $image = $faker->image($directory, 600, 400, 'technics', true, true, 'DoliScan');
        // $image = str_replace($directory . "/", "", $image);
        $ladate = $faker->dateTimeBetween($debut, $fin)->format("Y-m-d");

        LdeFrais::create([
          'label' => $faker->nameDivers,
          // 'fileName' => $image,
          'ladate' => $ladate,
          'created_at' => $ladate,
          'ttc' => $ttc,
          'ht' => $ht,
          'tvaTx1' => "20%",
          'tvaVal1' => $tva20,
          'user_id' => $user->id,
          'type_frais_id' => $rID,
          'moyen_paiement_id' => $faker->randomElement($paiementsIDs),
          'nde_frais_id' => $ndf->id,
        ]);
      }

      //Frais de train ou d'avion
      $rID = TypeFrais::where('slug', 'train')->first()->id;
      for ($i = 0; $i < $faker->numberBetween(1, 8); $i++) {
        // $directory    = storage_path() . "/LdeFrais/$u/";
        // if (!is_dir($directory)) mkdir($directory);
        // $image = $faker->image($directory, 600, 400, 'technics', true, true, 'DoliScan');
        // $image = str_replace($directory . "/", "", $image);
        $ladate = $faker->dateTimeBetween($debut, $fin)->format("Y-m-d");

        LdeFrais::create([
          'label' => $faker->nameTrainOuAvion,
          // 'fileName' => $image,
          'ladate' => $ladate,
          'created_at' => $ladate,
          'ttc' => $faker->randomFloat(2, 35, 240),
          'user_id' => $user->id,
          'type_frais_id' => $rID,
          'moyen_paiement_id' => $faker->randomElement($paiementsIDs),
          'nde_frais_id' => $ndf->id,
        ]);
      }


      //Et on ferme la NDF
      $ndf->status = NdeFrais::STATUS_OPEN;
      $ndf->close();
    }
  }
}
